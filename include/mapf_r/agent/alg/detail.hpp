#pragma once

#include "mapf_r/agent/alg.hpp"

namespace mapf_r::agent::detail {
    template <size_t degV>
    pair<Interval, Interval>
    sim_unsafe_intervals_tp(const Agent&, const Agent&,
                            const Interval& t1, const Interval& t2, Interval confl_int);
    template <size_t degV>
    Interval sim_unsafe_interval_tp(const Agent&, const Agent&,
                                    const Interval& t1, const Interval& t2);

    extern template pair<Interval, Interval>
        sim_unsafe_intervals_tp<1>(const Agent&, const Agent&,
                                   const Interval& t1, const Interval& t2, Interval confl_int);
    extern template pair<Interval, Interval>
        sim_unsafe_intervals_tp<2>(const Agent&, const Agent&,
                                   const Interval& t1, const Interval& t2, Interval confl_int);
    extern template Interval sim_unsafe_interval_tp<1>(const Agent&, const Agent&,
                                                       const Interval& t1, const Interval& t2);
    extern template Interval sim_unsafe_interval_tp<2>(const Agent&, const Agent&,
                                                       const Interval& t1, const Interval& t2);
}
