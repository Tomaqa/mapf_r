#pragma once

namespace mapf_r::agent::parabolic {
    Polynom<4> dist_coefs(Vec a, Vec b, Vec c, Float d)
    {
        // F(t) = t^2*a + t*b + c
        // || F(t) || = d
        // (t^2*a + t*b + c)^2 - d^2 = 0
        // t^4*a^2 + t^3*2*a*b + t^2*(2*a*c + b^2) + t*2*b*c + c^2 - d^2 = 0

        const Float c4 = a*a;
        const Float c3 = 2*a*b;
        const Float c2 = 2*a*c + b*b;
        const Float c1 = 2*b*c;
        const Float c0 = c*c - d*d;

        assert(c4 >= 0);
        return {c4, c3, c2, c1, c0};
    }

    template <typename BT0, typename CT0>
    Polynom<4> collision_coefs(const Agent& a1, const Agent& a2, BT0 bt0, CT0 ct0)
    {
        auto& s1 = a1.cstate();
        auto& s2 = a2.cstate();

        // F(t) = pos + t*v + t*(T-t)*n
        // F(t) - G(t) = [fpos_x + t*fv_x + t*(fT-t)*fn_x - (gpos_x + t*gv_x + t*(gT-t)*gn_x), .. y]
        //             = [fpos_x-gpos_x + t*(fv_x-gv_x) + t*((fT-t)*fn_x - (gT-t)*gn_x), .. y]
        //             = [fpos_x-gpos_x + t*(fv_x-gv_x) + t*(fT*fn_x - t*fn_x - gT*gn_x + t*gn_x, .. y]
        //             = [fpos_x-gpos_x + t*(fv_x-gv_x) - t^2*(fn_x-gn_x) + t*(fT*fn_x - gT*gn_x), .. y]
        //             = [pos_x + t*v_x - t^2*n_x + t*(fT*fn_x - gT*gn_x), .. y]
        //             = pos + t*v - t^2*n + t*(fnT - gnT)
        //             = pos + t*v - t^2*n + t*nT
        //             = pos + t*(v + nT) - t^2*n
        //             = pos + t*vnT - t^2*n

        const Vec pos(s1.cpos() - s2.cpos());
        const Vec v(s1.clinear_v() - s2.clinear_v());
        const Vec n1(s1.cnormal_v());
        const Vec n2(s2.cnormal_v());
        const Float T1 = s1.cduration();
        const Float T2 = s2.cduration();
        const Vec n(n1 - n2);
        const Vec nT(n1*T1 - n2*T2);
        const Vec vnT(v + nT);

        const Float rad = a1.cradius() + a2.cradius();

        // F(t - t0) = pos + (t - t0)*v + (t - t0)*(T - t + t0)*n
        //.. = F(t) + t*2*{t0*n} - {t0*(v + n*(t0 + T))}

        if constexpr (is_dummy_v<CT0>)
            return dist_coefs(-n, vnT, pos, rad);
        else return dist_coefs(-n, vnT + 2*bt0, pos - ct0, rad);
    }

    Polynom<4> conflict_coefs(const Agent& a1, const Agent& a2,
                              const Interval& t1, const Interval& t2)
    {
        auto& s1 = a1.cstate();
        auto& s2 = a2.cstate();

        // F(t - tF) - G(t - tG)
        //.. = pos + t*v - t^2*n + t*(fnT - gnT)  +  t*2*(tF*nF - tG*nG) - tF*(vF + tF*nF + fnT) + tG*(vG + tG*nG + gnT)
        //.. = F(t) - G(t) + t*2*{tF*nF - tG*nG} - { tF*(vF + nF*(tF + fT)) - tG*(vG + nG*(tG + gT)) }

        const Float t1l = t1.clower();
        const Float t2l = t2.clower();
        const Vec n1(s1.cnormal_v());
        const Vec n2(s2.cnormal_v());

        const Vec v1(s1.clinear_v());
        const Vec v2(s2.clinear_v());
        const Float T1 = s1.cduration();
        const Float T2 = s2.cduration();

        const auto bt0 = t1l*n1 - t2l*n2;
        const auto ct0 = t1l*(v1 + n1*(t1l + T1)) - t2l*(v2 + n2*(t2l + T2));

        return collision_coefs(a1, a2, bt0, ct0);
    }

    Polynom<4> conflict_coefs(const Polynom<4>& /*coll_coefs*/, const Agent&, const Agent&,
                              const Interval& /*t1*/, const Interval& /*t2*/)
    {
        NOT_IMPLEMENTED_YET;
    }
}
