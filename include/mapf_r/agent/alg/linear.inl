#pragma once

namespace mapf_r::agent::linear {
    Polynom<2> dist_coefs(Vec k, Vec q, Float d)
    {
        // F(t) = t*k + q
        // || F(t) || = d
        // (t*k + q)^2 - d^2 = 0
        // t^2*k^2 + t*2*k*q + q^2 - d^2 = 0
        // ... quadratic inequation in t

        const Float a = k*k;
        const Float b = 2*k*q;
        const Float c = q*q - d*d;

        assert(a >= 0);
        return {a, b, c};
    }

    Polynom<2> dist_coefs(const Polynom<2>& coefs, Vec k, Vec q0, Vec q_plus)
    {
        // F(t) = t*k + (q0 + q_plus)
        // || F(t) || = d
        // (t*k + (q0 + q_plus))^2 - d^2 = 0
        // t^2*k^2 + t*2*k*(q0 + q_plus) + (q0 + q_plus)^2 - d^2
        //.. = t^2*k^2 + t*2*k*q0 + t*2*k*q_plus + q0^2 + 2*q0*q_plus + q_plus^2 - d^2 = 0
        //.. = (t^2*k^2 + t*2*k*q0 + q0^2 - d^2) + t*2*k*q_plus + 2*q0*q_plus + q_plus^2 = 0

        const auto [a_, b_, c_] = coefs;
        assert(a_ >= 0);

        const Float a = a_;
        const Float b = b_ + 2*k*q_plus;
        const Float c = c_ + 2*q0*q_plus + q_plus*q_plus;

        assert(a >= 0);
        return {a, b, c};
    }

    template <typename QT0>
    Polynom<2> collision_coefs(const Agent& a1, const Agent& a2, QT0 qt0)
    {
        auto& s1 = a1.cstate();
        auto& s2 = a2.cstate();

        // F(t) = pos + t*v
        // F(t) - G(t) = [fpos_x + t*fv_x - (gpos_x + t*gv_x), .. y]
        //             = [fpos_x-gpos_x + t*(fv_x-gv_x), .. y]
        //             = [pos_x + t*v_x, .. y] = pos + t*v

        // origin of coordinate system will move with agent A
        const Vec pos(s1.cpos() - s2.cpos());           // position of agent B in moving coordinate system
        const Vec v(s1.clinear_v() - s2.clinear_v());   // speed of agent B in moving coordinate system

        // collision if Euclidean distance to origin of agent B in moving coordinate system less than rad1+rad2

        const Float rad = a1.cradius() + a2.cradius();

        // F(t - t0) = pos + (t - t0)*v = pos + t*v - t0*v = F(t) - t0*v

        if constexpr (is_dummy_v<QT0>)
            return dist_coefs(v, pos, rad);
        else return dist_coefs(v, pos - qt0, rad);
    }

    Polynom<2> conflict_coefs(const Agent& a1, const Agent& a2, const Interval& t1, const Interval& t2)
    {
        auto& s1 = a1.cstate();
        auto& s2 = a2.cstate();

        // F(t - tF) - G(t - tG) = pos + t*v - tF*vF + tG*vG = pos + t*v - (vtF - vtG)

        const Vec vt1(t1.clower()*s1.clinear_v());
        const Vec vt2(t2.clower()*s2.clinear_v());

        return collision_coefs(a1, a2, vt1 - vt2);
    }

    Polynom<2> conflict_coefs(const Polynom<2>& coll_coefs, const Agent& a1, const Agent& a2,
                              const Interval& t1, const Interval& t2)
    {
        auto& s1 = a1.cstate();
        auto& s2 = a2.cstate();

        assert(s2.ct() == 0);

        // F(t - tF) - G(t - tG) = pos + t*v - tF*vF + tG*vG = pos + t*v - (vtF - vtG)

        const Vec pos(s1.cpos() - s2.cpos());
        const Vec v1(s1.clinear_v());
        const Vec v2(s2.clinear_v());
        const Vec v(v1 - v2);
        const Vec vt1(t1.clower()*v1);
        const Vec vt2(t2.clower()*v2);
        const Vec vt(vt1 - vt2);

        return dist_coefs(coll_coefs, v, pos, -vt);
    }

    bool aligned_in_conflict(const Agent& a1, const Agent& a2, const Interval& t1, const Interval& t2)
    {
        assert(a1.cstate().idle() || a1.cstate().cpos() != a1.cstate().pos_at(0) || t1.clower() == 0);
        assert(a2.cstate().idle() || a2.cstate().cpos() != a2.cstate().pos_at(0) || t2.clower() == 0);
        return aligned_in_conflict(a1, a2, t1&t2);
    }

    Interval aligned_conflict(const Agent& a1, const Agent& a2, const Interval& t1, const Interval& t2)
    {
        assert(a1.cstate().idle() || a1.cstate().cpos() != a1.cstate().pos_at(0) || t1.clower() == 0);
        assert(a2.cstate().idle() || a2.cstate().cpos() != a2.cstate().pos_at(0) || t2.clower() == 0);
        return aligned_conflict(a1, a2, t1&t2);
    }

    Optional<Float> aligned_max_conflict(const Agent& a1, const Agent& a2, const Interval& t1, const Interval& t2)
    {
        assert(a1.cstate().idle() || a1.cstate().cpos() != a1.cstate().pos_at(0) || t1.clower() == 0);
        assert(a2.cstate().idle() || a2.cstate().cpos() != a2.cstate().pos_at(0) || t2.clower() == 0);
        return aligned_max_conflict(a1, a2, t1&t2);
    }

    Optional<Float> aligned_max_conflict(const Agent& a1, const Agent& a2, const Interval& intersect_t)
    {
        return linear::max_collision(a1, a2, intersect_t);
    }
}
