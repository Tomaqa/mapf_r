#pragma once

#include "mapf_r/agent/alg.hpp"
#include "mapf_r/agent/alg/linear.hpp"

namespace mapf_r::agent::linear::detail {
    Interval unsafe_interval(const Agent& a1, const Agent& a2,
                             const Interval& t1, const Interval& t2);
}
