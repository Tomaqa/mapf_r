#pragma once

namespace mapf_r::agent::linear {
    inline Polynom<2> dist_coefs(Vec k, Vec q, Float d);
    inline Polynom<2> dist_coefs(const Polynom<2>& coefs, Vec k, Vec q0, Vec q_plus);

    template <typename QT0 = Dummy>
    Polynom<2> collision_coefs(const Agent&, const Agent&, QT0 qt0 = {});

    inline bool is_collision(const Agent& a1, const Agent& a2){ return is_collision_tp<1>(a1, a2); }
    inline bool is_collision(const Polynom<2>& coefs)          { return is_collision_tp<1>(coefs); }
    inline Interval collision(const Agent& a1, const Agent& a2)  { return collision_tp<1>(a1, a2); }
    Interval collision(const Polynom<2>& coefs);

    inline Optional<Float> max_collision(const Agent& a1, const Agent& a2, const Interval& t)
                                                          { return max_collision_tp<1>(a1, a2, t); }
    Optional<Float> max_collision(const Polynom<2>& coefs, const Interval&);

    inline Polynom<2> conflict_coefs(const Agent&, const Agent&,
                                     const Interval& t1, const Interval& t2);
    inline Polynom<2> conflict_coefs(const Polynom<2>& coll_coefs,
                                     const Agent&, const Agent&,
                                     const Interval& t1, const Interval& t2);

    inline bool in_conflict(const Agent& a1, const Agent& a2,
                            const Interval& t1, const Interval& t2)
                                                       { return in_conflict_tp<1>(a1, a2, t1, t2); }
    inline bool in_conflict(const Polynom<2>& coll_coefs, const Agent& a1, const Agent& a2,
                            const Interval& t1, const Interval& t2)
                                           { return in_conflict_tp<1>(coll_coefs, a1, a2, t1, t2); }
    inline bool in_conflict(const Polynom<2>& coefs, const Interval& intersect_t)
                                                   { return in_conflict_tp<1>(coefs, intersect_t); }
    inline Interval conflict(const Agent& a1, const Agent& a2,
                             const Interval& t1, const Interval& t2)
                                                          { return conflict_tp<1>(a1, a2, t1, t2); }
    inline Interval conflict(const Polynom<2>& coll_coefs, const Agent& a1, const Agent& a2,
                             const Interval& t1, const Interval& t2)
                                              { return conflict_tp<1>(coll_coefs, a1, a2, t1, t2); }
    inline Interval conflict(const Polynom<2>& coefs, const Interval& intersect_t)
                                                      { return conflict_tp<1>(coefs, intersect_t); }

    inline Optional<Float> max_conflict(const Agent& a1, const Agent& a2,
                                        const Interval& t1, const Interval& t2)
                                                      { return max_conflict_tp<1>(a1, a2, t1, t2); }
    inline Optional<Float> max_conflict(const Polynom<2>& coefs, const Interval& intersect_t)
                                                  { return max_conflict_tp<1>(coefs, intersect_t); }

    void align_agents(Agent&, Agent&, const Interval& t1, const Interval& t2);
    pair<Agent, Agent>
    aligned_agents(const Agent&, const Agent&, const Interval& t1, const Interval& t2);

    inline bool aligned_in_conflict(const Agent&, const Agent&, const Interval& t1, const Interval& t2);
    bool aligned_in_conflict(const Agent&, const Agent&, const Interval& intersect_t);
    inline Interval aligned_conflict(const Agent&, const Agent&, const Interval& t1, const Interval& t2);
    Interval aligned_conflict(const Agent&, const Agent&, const Interval& intersect_t);

    inline Optional<Float> aligned_max_conflict(const Agent&, const Agent&, const Interval& t1, const Interval& t2);
    inline Optional<Float> aligned_max_conflict(const Agent&, const Agent&, const Interval& intersect_t);

    pair<Interval, Interval>
    unsafe_intervals(const Agent&, const Agent&, const Interval& t1, const Interval& t2);
}
