#pragma once

namespace mapf_r::agent::parabolic {
    inline Polynom<4> dist_coefs(Vec a, Vec b, Vec c, Float d);

    template <typename BT0 = Dummy, typename CT0 = Dummy>
    inline Polynom<4> collision_coefs(const Agent&, const Agent&, BT0 bt0 = {}, CT0 ct0 = {});

    inline bool is_collision(const Agent& a1, const Agent& a2){ return is_collision_tp<2>(a1, a2); }
    inline bool is_collision(const Polynom<4>& coefs)          { return is_collision_tp<2>(coefs); }
    inline Interval collision(const Agent& a1, const Agent& a2)  { return collision_tp<2>(a1, a2); }
    Interval collision(const Polynom<4>& coefs);

    inline Optional<Float> max_collision(const Agent& a1, const Agent& a2, const Interval& t)
                                                          { return max_collision_tp<2>(a1, a2, t); }
    Optional<Float> max_collision(const Polynom<4>& coefs, const Interval&);

    inline Polynom<4> conflict_coefs(const Agent&, const Agent&,
                                     const Interval& t1, const Interval& t2);
    inline Polynom<4> conflict_coefs(const Polynom<4>& coll_coefs,
                                     const Agent&, const Agent&,
                                     const Interval& t1, const Interval& t2);

    inline bool in_conflict(const Agent& a1, const Agent& a2,
                            const Interval& t1, const Interval& t2)
                                                       { return in_conflict_tp<2>(a1, a2, t1, t2); }
    inline bool in_conflict(const Polynom<4>& coll_coefs, const Agent& a1, const Agent& a2,
                            const Interval& t1, const Interval& t2)
                                           { return in_conflict_tp<2>(coll_coefs, a1, a2, t1, t2); }
    inline bool in_conflict(const Polynom<4>& coefs, const Interval& intersect_t)
                                                   { return in_conflict_tp<2>(coefs, intersect_t); }
    inline Interval conflict(const Agent& a1, const Agent& a2,
                             const Interval& t1, const Interval& t2)
                                                          { return conflict_tp<2>(a1, a2, t1, t2); }
    inline Interval conflict(const Polynom<4>& coll_coefs, const Agent& a1, const Agent& a2,
                             const Interval& t1, const Interval& t2)
                                              { return conflict_tp<2>(coll_coefs, a1, a2, t1, t2); }
    inline Interval conflict(const Polynom<4>& coefs, const Interval& intersect_t)
                                                      { return conflict_tp<2>(coefs, intersect_t); }

    inline Optional<Float> max_conflict(const Agent& a1, const Agent& a2,
                                        const Interval& t1, const Interval& t2)
                                                      { return max_conflict_tp<2>(a1, a2, t1, t2); }
    inline Optional<Float> max_conflict(const Polynom<4>& coefs, const Interval& intersect_t)
                                                  { return max_conflict_tp<2>(coefs, intersect_t); }

    pair<Interval, Interval>
    unsafe_intervals(const Agent&, const Agent&, const Interval& t1, const Interval& t2);
}
