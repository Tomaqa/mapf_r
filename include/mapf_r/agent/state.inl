#pragma once

namespace mapf_r::agent::state {
    Float Base::scale(Float t_) const
    {
        assert(cduration() != 0);
        return t_/cduration();
    }

    Float Base::dt(Float t_) const noexcept
    {
        return t_ - ct();
    }

    Float Base::dt() const noexcept
    {
        return dt(cduration());
    }

    ////////////////////////////////////////////////////////////////

    void V_base::reset_pos() noexcept
    {
        reset_pos(cinit_pos());
    }

    void V_base::reset(Float t_) noexcept
    {
        reset(cinit_pos(), t_);
    }

    Pos V_base::compute_end_pos(Vec vec_) const
    {
        return compute_end_pos(cinit_pos(), vec_);
    }

    Pos V_base::compute_end_pos() const
    {
        return compute_end_pos(cvec());
    }

    Vec V_base::compute_vec(Pos_pair from_to)
    {
        return compute_vec(from_to.first, from_to.second);
    }

    Vec V_base::compute_vec(Pos from, Pos to)
    {
        return {from, to};
    }

    Vec V_base::compute_vec(Pos to) const
    {
        return compute_vec(cinit_pos(), to);
    }

    ////////////////////////////////////////////////////////////////

    template <Class B, Class S>
    Pos Crtp_mixin<B, S>::cinit_pos() const noexcept
    {
        return This::cthat().cinit_pos_impl();
    }

    template <Class B, Class S>
    Pos Crtp_mixin<B, S>::cend_pos() const noexcept
    {
        return This::cthat().cend_pos_impl();
    }

    template <Class B, Class S>
    Vec Crtp_mixin<B, S>::cvec() const noexcept
    {
        return This::cthat().cvec_impl();
    }

    template <Class B, Class S>
    Float Crtp_mixin<B, S>::cdist() const noexcept
    {
        return This::cthat().cdist_impl();
    }

    template <Class B, Class S>
    Float Crtp_mixin<B, S>::cdist_impl() const noexcept
    {
        return compute_dist();
    }

    template <Class B, Class S>
    Vec Crtp_mixin<B, S>::clinear_v() const noexcept
    {
        return This::cthat().clinear_v_impl();
    }

    template <Class B, Class S>
    Vec Crtp_mixin<B, S>::cnormal_v() const noexcept
    {
        return This::cthat().cnormal_v_impl();
    }

    template <Class B, Class S>
    Float Crtp_mixin<B, S>::compute_dist() const noexcept
    {
        return This::cthat().compute_dist_impl();
    }

    template <Class B, Class S>
    Float Crtp_mixin<B, S>::compute_dist_impl() const noexcept
    {
        return compute_dist(cvec());
    }

    template <Class B, Class S>
    Float Crtp_mixin<B, S>::compute_dist(Vec vec_) noexcept
    {
        return dist(vec_);
    }

    template <Class B, Class S>
    Float Crtp_mixin<B, S>::compute_duration(auto&&... args) const
    {
        return This::cthat().compute_duration_impl(FORWARD(args)...);
    }

    template <Class B, Class S>
    void Crtp_mixin<B, S>::set_duration(auto&&... args)
    {
        This::duration() = compute_duration(FORWARD(args)...);
    }

    template <Class B, Class S>
    void Crtp_mixin<B, S>::set(auto&&... args)
    {
        set_t(0, FORWARD(args)...);
    }

    template <Class B, Class S>
    void Crtp_mixin<B, S>::set_t(Float t_, auto&&... args)
    {
        This::reset_t(t_);
        set_only(FORWARD(args)...);
    }

    template <Class B, Class S>
    void Crtp_mixin<B, S>::set_at(Pos pos_, auto&&... args)
    {
        set_at_t(pos_, 0, FORWARD(args)...);
    }

    template <Class B, Class S>
    void Crtp_mixin<B, S>::set_at_t(Pos pos_, Float t_, auto&&... args)
    {
        This::reset(pos_, t_);
        set_only(FORWARD(args)...);
    }

    template <Class B, Class S>
    void Crtp_mixin<B, S>::set_only(auto&&... args)
    {
        set_duration(FORWARD(args)...);
        This::that().post_set(FORWARD(args)...);
    }

    template <Class B, Class S>
    Vec Crtp_mixin<B, S>::dvec(Float dt_) const
    {
        return This::cthat().dvec_impl(dt_);
    }

    template <Class B, Class S>
    Vec Crtp_mixin<B, S>::vec_at(Float t_) const
    {
        return This::cthat().vec_at_impl(t_);
    }

    template <Class B, Class S>
    Vec Crtp_mixin<B, S>::dvec_impl(Float dt_) const
    {
        return linear_dvec(cvec(), dt_);
    }

    template <Class B, Class S>
    Vec Crtp_mixin<B, S>::vec_at_impl(Float t_) const
    {
        return linear_vec_at(cvec(), t_);
    }

    template <Class B, Class S>
    Pos Crtp_mixin<B, S>::pos_at(Float t_) const
    {
        return This::cinit_pos() + vec_at(t_);
    }

    template <Class B, Class S>
    Pos Crtp_mixin<B, S>::advanced_pos(Float dt_) const
    {
        return This::cpos() + dvec(dt_);
    }

    template <Class B, Class S>
    void Crtp_mixin<B, S>::advance(Float dt_)
    {
        This::pos() += dvec(dt_);
        This::t() += dt_;
        This::that().post_advance(dt_);
    }

    template <Class B, Class S>
    Float Crtp_mixin<B, S>::scale_dist(Float d) const
    {
        return d/cdist();
    }

    template <Class B, Class S>
    Float Crtp_mixin<B, S>::to_dist(Float t_) const
    {
        return This::scale(t_)*cdist();
    }

    template <Class B, Class S>
    Float Crtp_mixin<B, S>::to_t(Float d) const
    {
        return scale_dist(d)*This::cduration();
    }

    template <Class B, Class S>
    String Crtp_mixin<B, S>::name() noexcept
    {
        return That::name_impl();
    }

    template <Class B, Class S>
    String Crtp_mixin<B, S>::to_string() const
    {
        return This::cthat().to_string_head()
             + This::cthat().to_string_body()
             + This::cthat().to_string_tail();
    }

    template <Class B, Class S>
    String Crtp_mixin<B, S>::to_string_body() const
    {
        return Inherit::to_string();
    }

    ////////////////////////////////////////////////////////////////

    template <Class B>
    Float V_mixin<B>::compute_duration_impl(auto&&... args) const
    {
        return scompute_duration(This::cvec(), FORWARD(args)...);
    }

    template <Class B>
    Float V_mixin<B>::scompute_duration(Vec vec_, auto&&... args)
    {
        return That::scompute_duration_impl(vec_, FORWARD(args)...);
    }

    template <Class B>
    Float V_mixin<B>::scompute_duration_impl(Vec vec_, Float abs_v)
    {
        return Base::compute_duration(vec_, abs_v);
    }

    template <Class B>
    void V_mixin<B>::set_to(Pos to, auto&&... args)
    {
        set_to_t(to, 0, FORWARD(args)...);
    }

    template <Class B>
    void V_mixin<B>::set_to_t(Pos to, Float t_, auto&&... args)
    {
        set_to_t(This::compute_vec(to), t_, FORWARD(args)...);
    }

    template <Class B>
    void V_mixin<B>::set_to(Vec vec_, auto&&... args)
    {
        set_to_t(vec_, 0, FORWARD(args)...);
    }

    template <Class B>
    void V_mixin<B>::set_to_t(Vec vec_, Float t_, auto&&... args)
    {
        This::set_vec(vec_);
        This::set_t(t_, FORWARD(args)...);
    }

    template <Class B>
    void V_mixin<B>::set_at(Pos_pair from_to, auto&&... args)
    {
        set_at_t(move(from_to), 0, FORWARD(args)...);
    }

    template <Class B>
    void V_mixin<B>::set_at(Pos from, Pos to, auto&&... args)
    {
        set_at_t(from, to, 0, FORWARD(args)...);
    }

    template <Class B>
    void V_mixin<B>::set_at(Pos from, Vec vec_, auto&&... args)
    {
        set_at_t(from, vec_, 0, FORWARD(args)...);
    }

    template <Class B>
    void V_mixin<B>::set_at_t(Pos_pair from_to, Float t_, auto&&... args)
    {
        set_at_t(from_to.first, This::compute_vec(move(from_to)), t_, FORWARD(args)...);
    }

    template <Class B>
    void V_mixin<B>::set_at_t(Pos from, Pos to, Float t_, auto&&... args)
    {
        set_at_t(from, This::compute_vec(from, to), t_, FORWARD(args)...);
    }

    template <Class B>
    void V_mixin<B>::set_at_t(Pos from, Vec vec_, Float t_, auto&&... args)
    {
        This::set_vec(vec_);
        Inherit::set_at_t(from, t_, FORWARD(args)...);
    }

    template <Class B>
    Vec V_mixin<B>::dvec_impl(Float dt_) const
    {
        // I assume that it is more efficient than scaling all the time
        return This::linear_dvec(This::clinear_v(), v_tag, dt_);
    }

    template <Class B>
    Vec V_mixin<B>::vec_at_impl(Float t_) const
    {
        return This::linear_vec_at(This::clinear_v(), v_tag, t_);
    }

    ////////////////////////////////////////////////////////////////

    Vec Parabolic::dnormal(Float dt_) const
    {
        return dnormal(cnormal_v(), v_tag, dt_);
    }

    Vec Parabolic::normal_at(Float t_) const
    {
        return normal_at(cnormal_v(), v_tag, t_);
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace mapf_r::agent {
    bool State::idle() const noexcept
    {
        return valid<Idle>();
    }

    bool State::linear() const noexcept
    {
        return valid<Linear>();
    }

    bool State::parabolic() const noexcept
    {
        return valid<Parabolic>();
    }

    const state::Idle& State::cget_idle() const
    {
        return cget<Idle>();
    }

    state::Idle& State::get_idle()
    {
        return get<Idle>();
    }

    const state::Linear& State::cget_linear() const
    {
        return cget<Linear>();
    }

    state::Linear& State::get_linear()
    {
        return get<Linear>();
    }

    const state::Parabolic& State::cget_parabolic() const
    {
        return cget<Parabolic>();
    }

    state::Parabolic& State::get_parabolic()
    {
        return get<Parabolic>();
    }

    template <State_c S>
    S& State::set(auto&&... args)
    {
        return set_tp<S>(FORWARD(args)...);
    }

    state::Idle& State::set_idle(auto&&... args)
    {
        return set<Idle>(FORWARD(args)...);
    }

    state::Linear& State::set_linear(auto&&... args)
    {
        return set<Linear>(FORWARD(args)...);
    }

    state::Parabolic& State::set_parabolic(auto&&... args)
    {
        return set<Parabolic>(FORWARD(args)...);
    }

    state::Idle& State::set_idle_at(Pos pos)
    {
        return set_tp<Idle>(pos);
    }

    state::Linear& State::set_linear_from(Pos pos, auto&&... args)
    {
        return set_tp<Linear>(pos, FORWARD(args)...);
    }

    state::Parabolic& State::set_parabolic_from(Pos pos, auto&&... args)
    {
        return set_tp<Parabolic>(pos, FORWARD(args)...);
    }

    template <State_c S>
    S& State::set_tp(auto&&... args)
    {
        return emplace<S>(FORWARD(args)...);
    }

    template <State_c S>
    S& State::set_tp()
    {
        return visit([this](auto& s) -> S& { return set_tp<S>(move(s)); });
    }

    state::Linear& State::set_linear_to(auto&&... args)
    {
        return set_linear(cpos(), FORWARD(args)...);
    }

    state::Parabolic& State::set_parabolic_to(auto&&... args)
    {
        return set_parabolic(cpos(), FORWARD(args)...);
    }

    auto State::cpos() const noexcept
    {
        return visit([](auto& s){ return s.cpos(); });
    }

    state::Pos State::cinit_pos() const noexcept
    {
        return visit([](auto& s){ return s.cinit_pos(); });
    }

    state::Pos State::cend_pos() const noexcept
    {
        return visit([](auto& s){ return s.cend_pos(); });
    }

    Vec State::cvec() const noexcept
    {
        return visit([](auto& s){ return s.cvec(); });
    }

    Float State::cdist() const noexcept
    {
        return visit([](auto& s){ return s.cdist(); });
    }

    Float State::cduration() const noexcept
    {
        return visit([](auto& s){ return s.cduration(); });
    }

    Float State::ct() const noexcept
    {
        return visit([](auto& s){ return s.ct(); });
    }

    Vec State::clinear_v() const noexcept
    {
        return visit([](auto& s){ return s.clinear_v(); });
    }

    Vec State::cnormal_v() const noexcept
    {
        return visit([](auto& s){ return s.cnormal_v(); });
    }

    void State::reset_pos(auto&&... args) noexcept
    {
        visit([CAPTURE_PACK(args)](auto& s){ s.reset_pos(FORWARD(args)...); });
    }

    void State::reset_t(auto&&... args) noexcept
    {
        visit([CAPTURE_PACK(args)](auto& s){ s.reset_t(FORWARD(args)...); });
    }

    void State::reset(auto&&... args) noexcept
    {
        visit([CAPTURE_PACK(args)](auto& s){ s.reset(FORWARD(args)...); });
    }

    Float State::dt(auto&&... args) const noexcept
    {
        return visit([CAPTURE_PACK(args)](auto& s){ return s.dt(FORWARD(args)...); });
    }

    Vec State::dvec(Float dt_) const
    {
        return visit([dt_](auto& s){ return s.dvec(dt_); });
    }

    Vec State::vec_at(Float t) const
    {
        return visit([t](auto& s){ return s.vec_at(t); });
    }

    Vec State::linear_dvec(auto&&... args) const
    {
        return visit([CAPTURE_PACK(args)](auto& s){ return s.linear_dvec(FORWARD(args)...); });
    }

    Vec State::linear_vec_at(auto&&... args) const
    {
        return visit([CAPTURE_PACK(args)](auto& s){ return s.linear_vec_at(FORWARD(args)...); });
    }

    state::Pos State::pos_at(Float t) const
    {
        return visit([t](auto& s){ return s.pos_at(t); });
    }

    state::Pos State::advanced_pos(Float dt_) const
    {
        return visit([dt_](auto& s){ return s.advanced_pos(dt_); });
    }

    void State::advance(Float dt_)
    {
        visit([dt_](auto& s){ s.advance(dt_); });
    }

    Float State::to_dist(Float t) const
    {
        return visit([t](auto& s){ return s.to_dist(t); });
    }

    Float State::to_t(Float d) const
    {
        return visit([d](auto& s){ return s.to_t(d); });
    }
}
