#pragma once

namespace mapf_r::agent::state {
    class Base;
    template <Class B, Class S> class Crtp_mixin;
    class Idle;
    template <Class> class V_mixin;
    class Linear;
    class Parabolic;

    struct V_tag : Tag<Vec> {};
    constexpr V_tag v_tag;

    using Pos = Coord;
    using Pos_pair = pair<Pos, Pos>;
}

namespace mapf_r::agent {
    template <typename T>
    concept State_c = requires (T t) { {t.cpos()} -> same_as<state::Pos>; };

    class State;
}

namespace mapf_r::agent::state {
    class Base : public Static<Base> {
    public:
        Base()                                                                            = default;
        ~Base()                                                                           = default;
        Base(const Base&)                                                                 = default;
        Base& operator =(const Base&)                                                     = default;
        Base(Base&&)                                                                      = default;
        Base& operator =(Base&&)                                                          = default;
        explicit Base(Pos);

        auto cpos() const noexcept                                                  { return _pos; }

        Float cduration() const noexcept                                       { return _duration; }
        Float ct() const noexcept                                                     { return _t; }

        void reset_pos(Pos) noexcept;
        void reset_t(Float t_ = 0) noexcept;
        void reset(Pos, Float t_ = 0) noexcept;

        inline Float dt(Float t_) const noexcept;
        inline Float dt() const noexcept;

        bool equals(const This&) const noexcept;

        String to_string() const;
    protected:
        Base(Pos, Float duration_);

        Float& duration() noexcept                                             { return _duration; }
        Float& t() noexcept                                                           { return _t; }

        auto& pos() noexcept                                                        { return _pos; }

        /// t \in [0, cduration()]
        inline Float scale(Float t_) const;
    private:
        Pos _pos{};

        Float _duration{};
        Float _t{};
    };

    class V_base : public Inherit<Base, V_base> {
    public:
        using Base = This;

        V_base()                                                                          = default;
        ~V_base()                                                                         = default;
        V_base(const V_base&)                                                             = default;
        V_base& operator =(const V_base&)                                                 = default;
        V_base(V_base&&)                                                                  = default;
        V_base& operator =(V_base&&)                                                      = default;
        V_base(const Parent&);
        V_base(Parent&&);
        explicit V_base(Pos);
        V_base(Pos_pair);
        V_base(Pos from, Pos to);
        V_base(Pos, Vec);

        Pos cinit_pos() const noexcept                                         { return _init_pos; }
        Vec cvec() const noexcept                                                   { return _vec; }
        Vec clinear_v() const noexcept                                         { return _linear_v; }

        void reset_pos(Pos) noexcept;
        inline void reset_pos() noexcept;
        void reset(Pos, Float t_ = 0) noexcept;
        inline void reset(Float t_ = 0) noexcept;

        Vec linear_dvec(Vec, Float dt_) const;
        Vec linear_vec_at(Vec, Float t_) const;
        static Vec linear_dvec(Vec, V_tag, Float dt_);
        Vec linear_vec_at(Vec, V_tag, Float t_) const;

        bool equals(const This&) const noexcept;
    protected:
        Pos& init_pos() noexcept                                               { return _init_pos; }
        Vec& vec() noexcept                                                         { return _vec; }

        Vec& linear_v() noexcept                                               { return _linear_v; }

        static bool valid_vec(Vec) noexcept;
        static void check_vec(Vec);
        bool valid_vec() const noexcept;
        void check_vec() const;

        void set_init_pos(Pos) noexcept;

        static Pos compute_end_pos(Pos from, Vec);
        inline Pos compute_end_pos(Vec) const;
        inline Pos compute_end_pos() const;
        inline static Vec compute_vec(Pos_pair);
        inline static Vec compute_vec(Pos from, Pos to);
        inline Vec compute_vec(Pos to) const;
        void set_vec(Vec);

        static Float compute_duration(Vec, Float abs_v);
        static Float compute_duration(Float len, Float abs_v);

        Vec compute_linear_v() const;
        void set_linear_v();
    private:
        Pos _init_pos{};
        Vec _vec{};
        Vec _linear_v{};
    };

    template <Class B, Class S>
    class Crtp_mixin : public Inherit<B, Crtp_mixin<B, S>>, public tomaqa::Crtp<S> {
    public:
        using Inherit = tomaqa::Inherit<B, Crtp_mixin<B, S>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using typename tomaqa::Crtp<S>::That;

        using Inherit::Inherit;

        Pos cinit_pos() const noexcept;
        Pos cend_pos() const noexcept;
        Vec cvec() const noexcept;
        Float cdist() const noexcept;

        Vec clinear_v() const noexcept;
        Vec cnormal_v() const noexcept;

        void set(auto&&...);
        void set_t(Float t_, auto&&...);
        void set_at(Pos, auto&&...);
        void set_at_t(Pos, Float t_, auto&&...);

        Vec dvec(Float dt_) const;
        Vec vec_at(Float t_) const;

        Pos pos_at(Float t_) const;
        Pos advanced_pos(Float dt_) const;
        void advance(Float dt_);

        Float to_dist(Float t_) const;
        Float to_t(Float d) const;

        static String name() noexcept;

        String to_string() const;
    protected:
        Float cdist_impl() const noexcept;

        static consteval Vec cnormal_v_impl() noexcept                          { return zero_vec; }

        Float compute_dist() const noexcept;
        Float compute_dist_impl() const noexcept;
        static Float compute_dist(Vec) noexcept;

        Float compute_duration(auto&&...) const;
        void set_duration(auto&&...);

        void set_only(auto&&...);
        void post_set(auto&&...)                                                                 { }

        Vec dvec_impl(Float dt_) const;
        Vec vec_at_impl(Float t_) const;

        void post_advance(Float /*dt_*/)                                                         { }

        /// d \in [0, cdist()]
        Float scale_dist(Float d) const;

        String to_string_head() const                                                 { return ""; }
        String to_string_body() const;
        String to_string_tail() const                                                 { return ""; }
    };

    class Idle : public Inherit<Crtp_mixin<Base, Idle>, Idle> {
    public:
        using Inherit::Inherit;
        Idle(Pos pos_, Float duration_);

        void set_duration(Float duration_);
    protected:
        friend Inherit::Crtp_mixin;

        Pos cinit_pos_impl() const noexcept                                       { return cpos(); }
        Pos cend_pos_impl() const noexcept                                        { return cpos(); }
        static consteval Vec cvec_impl() noexcept                               { return zero_vec; }

        static consteval Vec clinear_v_impl() noexcept                       { return cvec_impl(); }

        static consteval Float compute_dist_impl() noexcept                            { return 0; }

        static constexpr Float compute_duration_impl(Float duration_) noexcept { return duration_; }

        static bool valid_duration(Float duration_) noexcept;
        static void check_duration(Float duration_);
        bool valid_duration() const noexcept                 { return valid_duration(cduration()); }
        void check_duration() const                          { return check_duration(cduration()); }

        static constexpr Vec dvec_impl(Float /*dt_*/) noexcept               { return cvec_impl(); }
        static constexpr Vec vec_at_impl(Float /*t_*/) noexcept              { return cvec_impl(); }

        static String name_impl() noexcept                                        { return "idle"; }

        String to_string_head() const;
        String to_string_tail() const;
    };

    template <Class B>
    class V_mixin : public Inherit<B, V_mixin<B>> {
    public:
        using Inherit = tomaqa::Inherit<B, V_mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using typename Inherit::That;

        using Inherit::Inherit;

        void set_to(Pos, auto&&...);
        void set_to_t(Pos, Float t_, auto&&...);
        void set_to(Vec, auto&&...);
        void set_to_t(Vec, Float t_, auto&&...);

        // hiding the inherited
        void set_at(Pos_pair, auto&&...);
        void set_at(Pos from, Pos to, auto&&...);
        void set_at(Pos from, Vec, auto&&...);
        void set_at_t(Pos_pair, Float t_, auto&&...);
        void set_at_t(Pos from, Pos to, Float t_, auto&&...);
        void set_at_t(Pos from, Vec, Float t_, auto&&...);
    protected:
        friend Inherit::Crtp_mixin;

        Pos cinit_pos_impl() const noexcept                            { return Base::cinit_pos(); }
        Pos cend_pos_impl() const noexcept                       { return This::compute_end_pos(); }
        Vec cvec_impl() const noexcept                                      { return Base::cvec(); }

        Vec clinear_v_impl() const noexcept                            { return Base::clinear_v(); }

        Float compute_duration_impl(auto&&...) const;
        static Float scompute_duration(Vec, auto&&...);
        static Float scompute_duration_impl(Vec, Float abs_v);

        void post_set(Float abs_v);

        Vec dvec_impl(Float dt_) const;
        Vec vec_at_impl(Float dt_) const;

        String to_string_body() const;
    };

    class Linear : public Inherit<V_mixin<Crtp_mixin<V_base, Linear>>, Linear> {
    public:
        using Inherit::Inherit;
        Linear(Pos_pair, Float abs_v);
        Linear(Pos from, Pos to, Float abs_v);
        Linear(Pos, Vec, Float abs_v);
    protected:
        friend Inherit::Crtp_mixin;

        static String name_impl() noexcept                                      { return "linear"; }
    };

    extern template class V_mixin<Crtp_mixin<V_base, Linear>>;

    class Parabolic : public Inherit<V_mixin<Crtp_mixin<V_base, Parabolic>>, Parabolic> {
    public:
        using Inherit::Inherit;
        Parabolic()                                                                       = default;
        ~Parabolic()                                                                      = default;
        Parabolic(const Parabolic&)                                                       = default;
        Parabolic& operator =(const Parabolic&)                                           = default;
        Parabolic(Parabolic&&)                                                            = default;
        Parabolic& operator =(Parabolic&&)                                                = default;
        Parabolic(Pos_pair, Float abs_v, Float delta);
        Parabolic(Pos from, Pos to, Float abs_v, Float delta);
        Parabolic(Pos, Vec, Float abs_v, Float delta);

        Vec cnormal() const noexcept                                             { return _normal; }

        Float normal_coef(Float delta) const noexcept;
        Float normal_v_coef(Float delta) const noexcept;

        Vec dnormal(Vec, Float dt_) const;
        Vec normal_at(Vec, Float t_) const;
        Vec dnormal(Vec, V_tag, Float dt_) const;
        Vec normal_at(Vec, V_tag, Float t_) const;
        inline Vec dnormal(Float dt_) const;
        inline Vec normal_at(Float t_) const;

        bool equals(const This&) const noexcept;
    protected:
        friend Inherit::Crtp_mixin;
        friend Inherit::V_mixin;

        Vec& normal() noexcept                                                   { return _normal; }
        Vec cnormal_v_impl() const noexcept                                    { return _normal_v; }
        Vec& normal_v() noexcept                                               { return _normal_v; }

        static Float scompute_duration_impl(Vec, Float abs_v, Float delta);

        Vec compute_normal(Float delta) const;
        Vec compute_normal_v(Float delta) const;
        void set_normal(Float delta);

        void post_set(Float abs_v, Float delta);

        Vec dvec_impl(Float dt_) const;
        Vec vec_at_impl(Float t_) const;

        static String name_impl() noexcept                                   { return "parabolic"; }
    private:
        Vec _normal{};
        Vec _normal_v{};
    };

    extern template class V_mixin<Crtp_mixin<V_base, Parabolic>>;
}

namespace mapf_r::agent {
    class State : protected Inherit<Union<state::Idle, state::Linear, state::Parabolic>, State> {
    public:
        using Pos = state::Pos;

        using Idle = state::Idle;
        using Linear = state::Linear;
        using Parabolic = state::Parabolic;

        using Inherit::Inherit;
        explicit State(Pos);
        explicit State(Pos, Float duration);
        explicit State(Pos from, Pos to, Float abs_v);
        explicit State(Pos, Vec, Float abs_v);
        explicit State(Pos from, Pos to, Float abs_v, Float delta);
        explicit State(Pos, Vec, Float abs_v, Float delta);
        explicit State(String, Float radius, Float abs_v);
        //+ use directly a ref. to agent?
        State(istream&, Float radius, Float abs_v);
        State(istream&& is, Float radius, Float abs_v)                : State(is, radius, abs_v) { }
        State(istream&, Float radius, Float abs_v, Pos);
        State(istream&& is, Float radius, Float abs_v, Pos pos)  : State(is, radius, abs_v, pos) { }

        void parse(istream&, Float radius, Float abs_v);

        inline bool idle() const noexcept;
        inline bool linear() const noexcept;
        inline bool parabolic() const noexcept;

        using Inherit::cget;
        using Inherit::get;
        inline const Idle& cget_idle() const;
        inline Idle& get_idle();
        inline const Linear& cget_linear() const;
        inline Linear& get_linear();
        inline const Parabolic& cget_parabolic() const;
        inline Parabolic& get_parabolic();

        template <State_c S> S& set(auto&&...);
        inline Idle& set_idle(auto&&...);
        inline Linear& set_linear(auto&&...);
        inline Parabolic& set_parabolic(auto&&...);

        inline Idle& set_idle_at(Pos);
        inline Linear& set_linear_from(Pos, auto&&...);
        inline Parabolic& set_parabolic_from(Pos, auto&&...);

        inline Linear& set_linear_to(auto&&...);
        inline Parabolic& set_parabolic_to(auto&&...);

        inline auto cpos() const noexcept;
        inline Pos cinit_pos() const noexcept;
        inline Pos cend_pos() const noexcept;
        inline Vec cvec() const noexcept;
        inline Float cdist() const noexcept;
        inline Float cduration() const noexcept;
        inline Float ct() const noexcept;
        inline Vec clinear_v() const noexcept;
        inline Vec cnormal_v() const noexcept;

        inline void reset_pos(auto&&...) noexcept;
        inline void reset_t(auto&&...) noexcept;
        inline void reset(auto&&...) noexcept;

        inline Float dt(auto&&...) const noexcept;
        inline Vec dvec(Float dt_) const;
        inline Vec vec_at(Float t) const;
        inline Vec linear_dvec(auto&&...) const;
        inline Vec linear_vec_at(auto&&...) const;

        inline Pos pos_at(Float t) const;
        inline Pos advanced_pos(Float dt_) const;
        inline void advance(Float dt_);

        inline Float to_dist(Float t) const;
        inline Float to_t(Float d) const;

        bool equals(const This& rhs) const noexcept                 { return Inherit::equals(rhs); }

        using Inherit::to_string;
    protected:
        template <State_c S> static constexpr bool is_idle_v = is_same_v<Rm_cvref<S>, Idle>;
        template <State_c S> static constexpr bool is_linear_v = is_same_v<Rm_cvref<S>, Linear>;
        template <State_c S> static constexpr bool is_parabolic_v = is_same_v<Rm_cvref<S>, Parabolic>;

        void parse(istream&, Float radius, Float abs_v, Pos last_pos);

        template <State_c S> S& set_tp(auto&&...);
        template <State_c S> S& set_tp();
    };
}
