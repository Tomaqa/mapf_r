#pragma once

namespace mapf_r::agent {
    template <size_t degV>
    Polynom<degV*2> collision_coefs_tp(const Agent& a1, const Agent& a2, auto&&... args)
    {
        if constexpr (degV == 1)
            return linear::collision_coefs(a1, a2, FORWARD(args)...);
        else if constexpr (degV == 2)
            return parabolic::collision_coefs(a1, a2, FORWARD(args)...);
        else static_assert(false_tp<integral_constant<size_t, degV>>);
    }

    template <size_t degV>
    bool is_collision_tp(const Agent& a1, const Agent& a2, auto&&... args)
    {
        return is_collision_tp<degV>(collision_coefs_tp<degV>(a1, a2, FORWARD(args)...));
    }

    template <size_t degV>
    Interval collision_tp(const Agent& a1, const Agent& a2, auto&&... args)
    {
        return collision_tp<degV>(collision_coefs_tp<degV>(a1, a2, FORWARD(args)...));
    }

    template <size_t degV>
    Interval collision_tp(const Polynom<degV*2>& coefs)
    {
        if constexpr (degV == 1)
            return linear::collision(coefs);
        else if constexpr (degV == 2)
            return parabolic::collision(coefs);
        else static_assert(false_tp<integral_constant<size_t, degV>>);
    }

    template <size_t degV>
    Optional<Float> max_collision_tp(const Agent& a1, const Agent& a2, const Interval& t, auto&&... args)
    {
        return max_collision_tp<degV>(collision_coefs_tp<degV>(a1, a2, FORWARD(args)...), t);
    }

    template <size_t degV>
    Optional<Float> max_collision_tp(const Polynom<degV*2>& coefs, const Interval& t)
    {
        if constexpr (degV == 1)
            return linear::max_collision(coefs, t);
        else if constexpr (degV == 2)
            return parabolic::max_collision(coefs, t);
        else static_assert(false_tp<integral_constant<size_t, degV>>);
    }

    template <size_t degV>
    Polynom<degV*2> conflict_coefs_tp(const Agent& a1, const Agent& a2,
                                      const Interval& t1, const Interval& t2, auto&&... args)
    {
        if constexpr (degV == 1)
            return linear::conflict_coefs(a1, a2, t1, t2, FORWARD(args)...);
        else if constexpr (degV == 2)
            return parabolic::conflict_coefs(a1, a2, t1, t2, FORWARD(args)...);
        else static_assert(false_tp<integral_constant<size_t, degV>>);
    }

    template <size_t degV>
    Polynom<degV*2> conflict_coefs_tp(const Polynom<degV*2>& coll_coefs,
                                      const Agent& a1, const Agent& a2,
                                      const Interval& t1, const Interval& t2, auto&&... args)
    {
        if constexpr (degV == 1)
            return linear::conflict_coefs(coll_coefs, a1, a2, t1, t2, FORWARD(args)...);
        else if constexpr (degV == 2)
            return parabolic::conflict_coefs(coll_coefs, a1, a2, t1, t2, FORWARD(args)...);
        else static_assert(false_tp<integral_constant<size_t, degV>>);
    }

    template <size_t degV>
    bool in_conflict_tp(const Agent& a1, const Agent& a2,
                        const Interval& t1, const Interval& t2, auto&&... args)
    {
        return in_conflict_tp<degV>(conflict_coefs_tp<degV>(a1, a2, t1, t2, FORWARD(args)...), t1&t2);
    }

    template <size_t degV>
    bool in_conflict_tp(const Polynom<degV*2>& coll_coefs, const Agent& a1, const Agent& a2,
                        const Interval& t1, const Interval& t2, auto&&... args)
    {
        return in_conflict_tp<degV>(conflict_coefs_tp<degV>(coll_coefs, a1, a2, t1, t2, FORWARD(args)...), t1&t2);
    }

    bool in_conflict(const Interval& coll_int, const Interval& t1, const Interval& t2)
    {
        return in_conflict(coll_int, t1&t2);
    }

    template <size_t degV>
    Interval conflict_tp(const Agent& a1, const Agent& a2,
                         const Interval& t1, const Interval& t2, auto&&... args)
    {
        return conflict_tp<degV>(conflict_coefs_tp<degV>(a1, a2, t1, t2, FORWARD(args)...), t1&t2);
    }

    template <size_t degV>
    Interval conflict_tp(const Polynom<degV*2>& coll_coefs, const Agent& a1, const Agent& a2,
                         const Interval& t1, const Interval& t2, auto&&... args)
    {
        return conflict_tp<degV>(conflict_coefs_tp<degV>(coll_coefs, a1, a2, t1, t2, FORWARD(args)...), t1&t2);
    }

    Interval conflict(const Interval& coll_int, const Interval& t1, const Interval& t2)
    {
        return conflict(coll_int, t1&t2);
    }

    template <size_t degV>
    Optional<Float> max_conflict_tp(const Agent& a1, const Agent& a2,
                                    const Interval& t1, const Interval& t2, auto&&... args)
    {
        return max_conflict_tp<degV>(conflict_coefs_tp<degV>(a1, a2, t1, t2, FORWARD(args)...), t1&t2);
    }

    template <size_t degV>
    Optional<Float> max_conflict_tp(const Polynom<degV*2>& coefs, const Interval& intersect_t)
    {
        return max_collision_tp<degV>(coefs, intersect_t);
    }

    template <size_t degV>
    pair<Interval, Interval>
    unsafe_intervals_tp(const Agent& a1, const Agent& a2, const Interval& t1, const Interval& t2, auto&&... args)
    {
        if constexpr (degV == 1)
            return linear::unsafe_intervals(a1, a2, t1, t2, FORWARD(args)...);
        else if constexpr (degV == 2)
            return parabolic::unsafe_intervals(a1, a2, t1, t2, FORWARD(args)...);
        else static_assert(false_tp<integral_constant<size_t, degV>>);
    }
}

#include "mapf_r/agent/alg/linear.inl"
#include "mapf_r/agent/alg/parabolic.inl"
