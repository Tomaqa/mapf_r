#pragma once

#include "mapf_r/agent.hpp"

#include "mapf_r/graph.hpp"

namespace mapf_r::agent {
    class Layout;

    using Layout_link = Layout*;
}

namespace mapf_r::agent {
    class Layout : public Static<Layout> {
    public:
        using Start_id = graph::Vertex::Id;
        using Start_ids_map = Hash<Id, Start_id>;
        using Goal_id = Start_id;
        using Goal_ids_map = Hash<Id, Goal_id>;

        using Radii_map = Hash<Id, Float>;
        using Abs_vs_map = Hash<Id, Float>;

        static constexpr Float default_radius = 0.5;
        static constexpr Float default_abs_v = 1;

        Layout()                                                                          = default;
        ~Layout()                                                                         = default;
        Layout(const Layout&)                                                             = default;
        Layout& operator =(const Layout&)                                                 = default;
        Layout(Layout&&)                                                                  = default;
        Layout& operator =(Layout&&)                                                      = default;
        Layout(Start_ids_map, Goal_ids_map);
        Layout(istream&);
        Layout(istream&& is)                                                        : Layout(is) { }

        void parse(istream&);
        void parse_l(istream&);
        void parse_krur(istream&);

        const auto& cstart_ids_map() const noexcept                       { return _start_ids_map; }
        inline const auto& cstart_id_of(const Id&) const;
        const auto& cgoal_ids_map() const noexcept                         { return _goal_ids_map; }
        inline const auto& cgoal_id_of(const Id&) const;

        const auto& cradii_map() const noexcept                               { return _radii_map; }
        inline const auto& cradius_of(const Id&) const;
        const auto& cabs_vs_map() const noexcept                             { return _abs_vs_map; }
        inline const auto& cabs_v_of(const Id&) const;

        inline const auto& cagent_id_of_start(const Start_id&) const;
        inline const auto& cagent_id_of_goal(const Goal_id&) const;
        inline bool contains_start(const Start_id&) const noexcept;
        inline bool contains_goal(const Goal_id&) const noexcept;
        inline const Id* find_agent_id_of_start(const Start_id&) const noexcept;
        inline const Id* find_agent_id_of_goal(const Goal_id&) const noexcept;

        inline size_t size() const noexcept;
        inline bool empty() const noexcept;
    protected:
        using Start_to_agent_ids_map = Hash<Start_id, Id>;
        using Goal_to_agent_ids_map = Hash<Goal_id, Id>;

        void init();

        auto& start_ids_map() noexcept                                    { return _start_ids_map; }
        inline auto& start_id_of(const Id&);
        auto& goal_ids_map() noexcept                                      { return _goal_ids_map; }
        inline auto& goal_id_of(const Id&);

        auto& radii_map() noexcept                                            { return _radii_map; }
        inline auto& radius_of(const Id&);
        auto& abs_vs_map() noexcept                                          { return _abs_vs_map; }
        inline auto& abs_v_of(const Id&);

        const auto& cstart_to_agent_ids_map() const noexcept     { return _start_to_agent_ids_map; }
        auto& start_to_agent_ids_map() noexcept                  { return _start_to_agent_ids_map; }
        inline auto& agent_id_of_start(const Start_id&);
        const auto& cgoal_to_agent_ids_map() const noexcept       { return _goal_to_agent_ids_map; }
        auto& goal_to_agent_ids_map() noexcept                    { return _goal_to_agent_ids_map; }
        inline auto& agent_id_of_goal(const Goal_id&);
    private:
        Start_ids_map _start_ids_map{};
        Goal_ids_map _goal_ids_map{};

        Radii_map _radii_map{};
        Abs_vs_map _abs_vs_map{};

        Start_to_agent_ids_map _start_to_agent_ids_map{};
        Goal_to_agent_ids_map _goal_to_agent_ids_map{};
    };
}

#include "mapf_r/agent/layout.inl"
