#pragma once

#include "mapf_r/agent.hpp"

#include "mapf_r/graph.hpp"
#include "mapf_r/agent/layout.hpp"

namespace mapf_r::agent::plan {
    class Local;
    struct Global;

    struct Local_states;
    struct Global_states;
}

namespace mapf_r::agent::plan {
    class Local : public Static<Local> {
    public:
        struct Step;

        using Steps = Vector<Step>;

        Local()                                                                           = default;
        ~Local()                                                                          = default;
        Local(const Local&)                                                               = default;
        Local& operator =(const Local&)                                                   = default;
        Local(Local&&)                                                                    = default;
        Local& operator =(Local&&)                                                        = default;
        Local(Steps steps_)                                               : _steps(move(steps_)) { }
        Local(istream&);
        Local(istream&& is)                                                          : Local(is) { }
        explicit Local(graph::Vertex::Id start_id);
        Local(graph::Vertex::Id start_id, graph::Vertex::Id goal_id);

        void parse(istream&);
        ostream& compose(ostream&) const;
        inline auto compose() const;

        const auto& csteps() const noexcept                                       { return _steps; }
        const Step& cfirst_step() const;
        const Step& clast_step() const;

        inline size_t size() const noexcept;
        inline bool empty() const noexcept;

        void add_step(Step);

        Float final_time() const noexcept;

        String to_string() const;
    protected:
        auto& steps() noexcept                                                    { return _steps; }

        void parse_step(istream&);
        static Step parsed_step(istream&);
        static ostream& compose_step(ostream&, const Step&);
    private:
        Steps _steps{};
    };

    struct Global : Inherit<Map<Id, Local>, Global> {
        using Inherit::Inherit;
        Global(istream&);
        Global(istream&& is)                                                        : Global(is) { }
        explicit Global(const Layout&);

        void parse(istream&);
        ostream& compose(ostream&) const;
        inline auto compose() const;

        Float max_time() const noexcept;
        Float makespan() const noexcept                                       { return max_time(); }

        Float sum_times() const noexcept;

        const graph::Vertex::Id& cstart_id_of(const Id&) const;
        const graph::Vertex::Id& cgoal_id_of(const Id&) const;

        inline const auto& cagent_id_of_start(const graph::Vertex::Id&) const;
        inline const auto& cagent_id_of_goal(const graph::Vertex::Id&) const;
        inline bool contains_start(const graph::Vertex::Id&) const noexcept;
        inline bool contains_goal(const graph::Vertex::Id&) const noexcept;
        const Id* find_agent_id_of_start(const graph::Vertex::Id&) const noexcept;
        const Id* find_agent_id_of_goal(const graph::Vertex::Id&) const noexcept;

        String to_string() const;
    };

    struct Local_states : Inherit<Vector<State>, Local_states> {
        using Inherit::Inherit;
        Local_states(Float radius_, Float abs_v_);
        Local_states(const Local&, const Graph&, Float radius_, Float abs_v_);
        //+ directly use an `Agent` or some `Agent::Properties` ?
        Local_states(istream&, Float radius_, Float abs_v_);
        Local_states(istream&& is, Float radius_, Float abs_v_)
                                                             : Local_states(is, radius_, abs_v_) { }

        void parse(istream&);

        Float total_time() const noexcept;

        String to_string() const;

        Float radius{};
        Float abs_v{};
    };

    struct Global_states : Inherit<Map<Id, Local_states>, Global_states> {
        using Inherit::Inherit;
        Global_states(const Global&, const Graph&, const Layout&);
        Global_states(istream&);
        Global_states(istream&& is)                                          : Global_states(is) { }

        void parse(istream&);

        Float max_time() const noexcept;
        Float makespan() const noexcept                                       { return max_time(); }

        Float sum_times() const noexcept;

        String to_string() const;
    };
}

namespace mapf_r::agent::plan {
    struct Local::Step {
        graph::Vertex::Id from_id, to_id;
        Float abs_time;
        Float move_time;
        Float wait_time{0.};

        constexpr Float rel_time() const noexcept                  { return wait_time + move_time; }
        constexpr Float final_time() const noexcept                { return abs_time + rel_time(); }
        constexpr Float wait_abs_time() const noexcept              { return abs_time + wait_time; }

        String to_string() const;
    };
}

#include "mapf_r/agent/plan.inl"
