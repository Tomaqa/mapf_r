#pragma once

#include "mapf_r/agent.hpp"

namespace mapf_r::agent {
    template <size_t degV>
    Polynom<degV*2> collision_coefs_tp(const Agent&, const Agent&, auto&&...);

    template <size_t degV>
    bool is_collision_tp(const Agent&, const Agent&, auto&&...);
    template <size_t degV>
    bool is_collision_tp(const Polynom<degV*2>& coefs);
    template <size_t degV>
    Interval collision_tp(const Agent&, const Agent&, auto&&...);
    template <size_t degV>
    Interval collision_tp(const Polynom<degV*2>& coefs);

    extern template bool is_collision_tp<1>(const Polynom<2>&);
    extern template bool is_collision_tp<2>(const Polynom<4>&);

    bool is_collision(const Agent&, const Agent&);
    Interval collision(const Agent&, const Agent&);

    template <size_t degV>
    Optional<Float> max_collision_tp(const Agent&, const Agent&, const Interval&, auto&&...);
    template <size_t degV>
    Optional<Float> max_collision_tp(const Polynom<degV*2>& coefs, const Interval&);

    Optional<Float> max_collision(const Agent&, const Agent&, const Interval&);

    template <size_t degV>
    Polynom<degV*2> conflict_coefs_tp(const Agent&, const Agent&,
                                      const Interval& t1, const Interval& t2, auto&&...);
    template <size_t degV>
    Polynom<degV*2> conflict_coefs_tp(const Polynom<degV*2>& coll_coefs,
                                      const Agent&, const Agent&,
                                      const Interval& t1, const Interval& t2, auto&&...);

    template <size_t degV>
    bool in_conflict_tp(const Agent&, const Agent&, const Interval& t1, const Interval& t2, auto&&...);
    template <size_t degV>
    bool in_conflict_tp(const Polynom<degV*2>& coll_coefs, const Agent&, const Agent&,
                        const Interval& t1, const Interval& t2, auto&&...);
    template <size_t degV>
    bool in_conflict_tp(const Polynom<degV*2>& coefs, const Interval& intersect_t);
    inline bool in_conflict(const Interval& coll_int, const Interval& t1, const Interval& t2);
    bool in_conflict(const Interval& coll_int, const Interval& intersect_t);
    template <size_t degV>
    Interval conflict_tp(const Agent&, const Agent&, const Interval& t1, const Interval& t2, auto&&...);
    template <size_t degV>
    Interval conflict_tp(const Polynom<degV*2>& coll_coefs, const Agent&, const Agent&,
                         const Interval& t1, const Interval& t2, auto&&...);
    template <size_t degV>
    Interval conflict_tp(const Polynom<degV*2>& coefs, const Interval& intersect_t);
    inline Interval conflict(const Interval& coll_int, const Interval& t1, const Interval& t2);
    Interval conflict(const Interval& coll_int, const Interval& intersect_t);

    extern template bool in_conflict_tp<1>(const Polynom<2>&, const Interval& intersect_t);
    extern template bool in_conflict_tp<2>(const Polynom<4>&, const Interval& intersect_t);
    extern template Interval conflict_tp<1>(const Polynom<2>&, const Interval& intersect_t);
    extern template Interval conflict_tp<2>(const Polynom<4>&, const Interval& intersect_t);

    bool in_conflict(const Agent&, const Agent&, const Interval& t1, const Interval& t2);
    Interval conflict(const Agent&, const Agent&, const Interval& t1, const Interval& t2);

    template <size_t degV>
    Optional<Float>
    max_conflict_tp(const Agent&, const Agent&, const Interval& t1, const Interval& t2, auto&&...);
    template <size_t degV>
    Optional<Float> max_conflict_tp(const Polynom<degV*2>& coefs, const Interval& intersect_t);

    Optional<Float> max_conflict(const Agent&, const Agent&, const Interval& t1, const Interval& t2);

    template <size_t degV>
    pair<Interval, Interval>
    unsafe_intervals_tp(const Agent&, const Agent&, const Interval& t1, const Interval& t2, auto&&...);

    pair<Interval, Interval>
    unsafe_intervals(const Agent&, const Agent&, const Interval& t1, const Interval& t2);
}

#include "mapf_r/agent/alg/linear.hpp"
#include "mapf_r/agent/alg/parabolic.hpp"

#include "mapf_r/agent/alg.inl"
