#pragma once

#include "mapf_r/agent/plan.hpp"

#include "mapf_r/graph/alg.hpp"

namespace mapf_r::agent::plan {}

namespace mapf_r::graph {
    Properties make_properties(const agent::plan::Global_states&);
}

#include "mapf_r/agent/plan/alg.inl"
