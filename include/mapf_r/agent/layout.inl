#pragma once

namespace mapf_r::agent {
    const auto& Layout::cstart_id_of(const Id& id_) const
    {
        return cstart_ids_map()[id_];
    }

    auto& Layout::start_id_of(const Id& id_)
    {
        return start_ids_map()[id_];
    }

    const auto& Layout::cgoal_id_of(const Id& id_) const
    {
        return cgoal_ids_map()[id_];
    }

    auto& Layout::goal_id_of(const Id& id_)
    {
        return goal_ids_map()[id_];
    }

    const auto& Layout::cradius_of(const Id& id_) const
    {
        return cradii_map()[id_];
    }

    auto& Layout::radius_of(const Id& id_)
    {
        return radii_map()[id_];
    }

    const auto& Layout::cabs_v_of(const Id& id_) const
    {
        return cabs_vs_map()[id_];
    }

    auto& Layout::abs_v_of(const Id& id_)
    {
        return abs_vs_map()[id_];
    }

    const auto& Layout::cagent_id_of_start(const Start_id& sid) const
    {
        return cstart_to_agent_ids_map()[sid];
    }

    auto& Layout::agent_id_of_start(const Start_id& sid)
    {
        return start_to_agent_ids_map()[sid];
    }

    const auto& Layout::cagent_id_of_goal(const Goal_id& gid) const
    {
        return cgoal_to_agent_ids_map()[gid];
    }

    auto& Layout::agent_id_of_goal(const Goal_id& gid)
    {
        return goal_to_agent_ids_map()[gid];
    }

    bool Layout::contains_start(const Start_id& sid) const noexcept
    {
        return cstart_to_agent_ids_map().contains(sid);
    }

    bool Layout::contains_goal(const Goal_id& gid) const noexcept
    {
        return cgoal_to_agent_ids_map().contains(gid);
    }

    const Id* Layout::find_agent_id_of_start(const Start_id& sid) const noexcept
    {
        return cstart_to_agent_ids_map().cfind_l(sid);
    }

    const Id* Layout::find_agent_id_of_goal(const Goal_id& gid) const noexcept
    {
        return cgoal_to_agent_ids_map().cfind_l(gid);
    }

    size_t Layout::size() const noexcept
    {
        assert(cstart_ids_map().size() == cgoal_ids_map().size());
        return cstart_ids_map().size();
    }

    bool Layout::empty() const noexcept
    {
        return size() == 0;
    }
}
