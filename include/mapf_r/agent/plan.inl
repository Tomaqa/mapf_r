#pragma once

#include <tomaqa/util/compose.hpp>

namespace mapf_r::agent::plan {
    auto Local::compose() const
    {
        return Compose_proxy<This>{*this};
    }

    size_t Local::size() const noexcept
    {
        return csteps().size();
    }

    bool Local::empty() const noexcept
    {
        return csteps().empty();
    }

    ////////////////////////////////////////////////////////////////

    auto Global::compose() const
    {
        return Compose_proxy<This>{*this};
    }

    const auto& Global::cagent_id_of_start(const graph::Vertex::Id& sid) const
    {
        if (auto aid_l = find_agent_id_of_start(sid)) return *aid_l;
        THROW(error);
    }

    const auto& Global::cagent_id_of_goal(const graph::Vertex::Id& gid) const
    {
        if (auto aid_l = find_agent_id_of_goal(gid)) return *aid_l;
        THROW(error);
    }

    bool Global::contains_start(const graph::Vertex::Id& sid) const noexcept
    {
        return find_agent_id_of_start(sid);
    }

    bool Global::contains_goal(const graph::Vertex::Id& gid) const noexcept
    {
        return find_agent_id_of_goal(gid);
    }
}
