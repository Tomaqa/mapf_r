#pragma once

namespace mapf_r::smt {
    String make_var_name_at(const Agent& ag, String name, Idx k)
    {
        return make_var_name_at(make_var_name(ag, move(name)), k);
    }

    String vertex_expr_name()
    {
        return "V";
    }

    String make_vertex_expr_name(const Agent& ag)
    {
        return make_var_name(ag, vertex_expr_name());
    }

    String make_vertex_expr_name_at(const Agent& ag, Idx k)
    {
        return make_var_name_at(ag, vertex_expr_name(), k);
    }

    String make_vertex_bool_name(const Agent& ag, const Vertex& vertex)
    {
        return make_var_name(ag, vertex_bool_name(vertex));
    }

    String make_vertex_bool_name_at(const Agent& ag, const Vertex& vertex, Idx k)
    {
        return make_var_name_at(ag, vertex_bool_name(vertex), k);
    }

    String make_abs_time_var_name(const Agent& ag)
    {
        return make_var_name(ag, abs_time_var_name);
    }

    String make_rel_time_var_name(const Agent& ag)
    {
        return make_var_name(ag, rel_time_var_name);
    }

    String make_wait_time_var_name(const Agent& ag)
    {
        return make_var_name(ag, wait_time_var_name);
    }

    String make_move_time_var_name(const Agent& ag)
    {
        return make_var_name(ag, move_time_var_name);
    }

    String make_abs_time_var_name_at(const Agent& ag, Idx k)
    {
        return make_var_name_at(ag, abs_time_var_name, k);
    }

    String make_rel_time_var_name_at(const Agent& ag, Idx k)
    {
        return make_var_name_at(ag, rel_time_var_name, k);
    }

    String make_wait_time_var_name_at(const Agent& ag, Idx k)
    {
        return make_var_name_at(ag, wait_time_var_name, k);
    }

    String make_move_time_var_name_at(const Agent& ag, Idx k)
    {
        return make_var_name_at(ag, move_time_var_name, k);
    }

    template <Side sideV, Precision_c P>
    Rational to_rational(Float val)
    {
        return to_apx_rational<sideV, P, Float, Rational>(val);
    }
}
