#pragma once

#include "mapf_r/graph.hpp"

#include <queue>

namespace mapf_r::graph::conf {
    struct Min_dist_default {
        using Conv_dist = identity;
    };
}

namespace mapf_r::graph {
    struct Properties;

    template <Class ConfT> class Min_dist_from_tp;
    template <Class ConfT> class Min_dist_tp;
    class Reachable;

    using Min_dist_from = Min_dist_from_tp<conf::Min_dist_default>;
    using Min_dist = Min_dist_tp<conf::Min_dist_default>;

    using N_jumps = int;

    using Path = Vector<Vertex::Id>;

    size_t compute_vertices_bit_width(const Graph&);

    Properties make_properties(const Graph&);

    template <Class ConfT = conf::Min_dist_default>
    Path make_shortest_path_of(const Min_dist_tp<ConfT>&);

    extern template Path make_shortest_path_of(const Min_dist_tp<conf::Min_dist_default>&);

    template <Class ConfT = conf::Min_dist_default>
    Vector<Vertex::Id> sorted_successors_of(const Min_dist_tp<ConfT>&, const Vertex::Id&);

    extern template Vector<Vertex::Id>
    sorted_successors_of(const Min_dist_tp<conf::Min_dist_default>&, const Vertex::Id&);
}

namespace mapf_r::graph {
    struct Properties {
        Coord min;
        Coord max;

        size_t min_deg{};
        size_t max_deg{};

        size_t vertices_bit_width{};

        Float width() const noexcept;
        Float height() const noexcept;
    };

    template <Class ConfT>
    class Min_dist_from_tp {
    public:
        using Conf = ConfT;

        Min_dist_from_tp(const Graph&, const Vertex::Id& start_id_);

        const auto& graph() const noexcept                                        { return _graph; }
        const Vertex::Id& start_id() const noexcept                            { return _start_id; }

        inline Float to(const Vertex::Id&) const;
        inline N_jumps n_jumps_to(const Vertex::Id&) const;

        inline Vertex::Id predecessor_of(const Vertex::Id&) const;
        inline bool is_predecessor_of(const Vertex::Id&, const Vertex::Id&) const;
    protected:
        template <Class> friend class Min_dist_tp;

        using Dist_to_map = Vector<Float>;
        using N_jumps_to_map = Vector<N_jumps>;

        using Predecessor_map = Vector<Vertex::Id>;

        Dist_to_map dist_to_map{};
        N_jumps_to_map n_jumps_to_map{};

        Predecessor_map predecessor_map{};
    private:
        struct Queue_pair;

        using Queue = std::priority_queue<Queue_pair>;

        const Graph& _graph;
        Vertex::Id _start_id;
    };

    extern template class Min_dist_from_tp<conf::Min_dist_default>;

    template <Class ConfT>
    class Min_dist_tp : public Inherit<Min_dist_from_tp<ConfT>> {
    public:
        using Inherit = tomaqa::Inherit<Min_dist_from_tp<ConfT>>;

        using typename Inherit::Conf;

        Min_dist_tp(const Graph&, const Vertex::Id& start_id_, const Vertex::Id& goal_id_);

        using Inherit::graph;
        using Inherit::start_id;
        const Vertex::Id& goal_id() const noexcept                              { return _goal_id; }

        using Inherit::to;
        using Inherit::n_jumps_to;
        inline Float from(const Vertex::Id&) const;
        inline N_jumps n_jumps_from(const Vertex::Id&) const;

        Float operator ()() const                                          { return to(goal_id()); }
        N_jumps n_jumps() const                                    { return n_jumps_to(goal_id()); }

        using Inherit::predecessor_of;
        using Inherit::is_predecessor_of;
        inline Vertex::Id successor_of(const Vertex::Id&) const;
        inline bool is_successor_of(const Vertex::Id&, const Vertex::Id&) const;
    protected:
        using typename Inherit::Dist_to_map;
        using typename Inherit::N_jumps_to_map;
        using Dist_from_map = Dist_to_map;
        using N_jumps_from_map = N_jumps_to_map;

        using typename Inherit::Predecessor_map;
        using Successor_map = Predecessor_map;

        Dist_from_map dist_from_map{};
        N_jumps_from_map n_jumps_from_map{};

        Successor_map successor_map{};
    private:
        Vertex::Id _goal_id;
    };

    extern template class Min_dist_tp<conf::Min_dist_default>;

    class Reachable {
    public:
        using Vertex_ids = Hash<Vertex::Id>;

        Reachable(const Graph&, const Vertex::Id& start_id_);

        const auto& graph() const noexcept                                        { return _graph; }
        const Vertex::Id& start_id() const noexcept                            { return _start_id; }

        const auto& vertex_ids() const noexcept                              { return _vertex_ids; }
        inline N_jumps n_jumps() const noexcept;

        void compute_next_jump();
    protected:
        inline const auto& cvertex_ids_at(N_jumps) const;
        inline auto& vertex_ids_at(N_jumps);
    private:
        const Graph& _graph;
        Vertex::Id _start_id;

        Vertex_ids _vertex_ids{};
        Vector<Vertex_ids> _vertex_ids_vector{};
    };
}

namespace mapf_r::graph {
    template <Class ConfT>
    struct Min_dist_from_tp<ConfT>::Queue_pair {
        Vertex::Id vid;
        Float dist{};

        // instead of max-heap, we want min-heap
        friend bool operator <(const Queue_pair& lhs, const Queue_pair& rhs)
                                                                     { return lhs.dist > rhs.dist; }
    };
}

#include "mapf_r/graph/alg.inl"
