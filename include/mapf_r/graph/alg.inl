#pragma once

namespace mapf_r::graph {
    template <Class ConfT>
    Float Min_dist_from_tp<ConfT>::to(const Vertex::Id& vid) const
    {
        return dist_to_map[vid];
    }

    template <Class ConfT>
    N_jumps Min_dist_from_tp<ConfT>::n_jumps_to(const Vertex::Id& vid) const
    {
        return n_jumps_to_map[vid];
    }

    template <Class ConfT>
    Vertex::Id Min_dist_from_tp<ConfT>::predecessor_of(const Vertex::Id& vid) const
    {
        return predecessor_map[vid];
    }

    template <Class ConfT>
    bool Min_dist_from_tp<ConfT>::is_predecessor_of(const Vertex::Id& pred_vid, const Vertex::Id& of_vid) const
    {
        return predecessor_of(of_vid) == pred_vid;
    }

    ////////////////////////////////////////////////////////////////

    template <Class ConfT>
    Float Min_dist_tp<ConfT>::from(const Vertex::Id& vid) const
    {
        return dist_from_map[vid];
    }

    template <Class ConfT>
    N_jumps Min_dist_tp<ConfT>::n_jumps_from(const Vertex::Id& vid) const
    {
        return n_jumps_from_map[vid];
    }

    template <Class ConfT>
    Vertex::Id Min_dist_tp<ConfT>::successor_of(const Vertex::Id& vid) const
    {
        return successor_map[vid];
    }

    template <Class ConfT>
    bool Min_dist_tp<ConfT>::is_successor_of(const Vertex::Id& succ_vid, const Vertex::Id& of_vid) const
    {
        return successor_of(of_vid) == succ_vid;
    }

    ////////////////////////////////////////////////////////////////

    N_jumps Reachable::n_jumps() const noexcept
    {
        return _vertex_ids_vector.size()-1;
    }

    const auto& Reachable::cvertex_ids_at(N_jumps n) const
    {
        return _vertex_ids_vector[n];
    }

    auto& Reachable::vertex_ids_at(N_jumps n)
    {
        return const_cast<Vertex_ids&>(cvertex_ids_at(n));
    }
}
