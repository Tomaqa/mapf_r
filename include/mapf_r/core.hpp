#pragma once

#include <tomaqa.hpp>

namespace mapf_r {
    using namespace tomaqa;
    using namespace tomaqa::util;
    using namespace tomaqa::math;
    using math::nan;
}

#include "mapf_r/core.inl"
