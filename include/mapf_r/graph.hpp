#pragma once

#include "mapf_r/core.hpp"

namespace mapf_r::graph {
    class Vertex;
}

namespace mapf_r {
    class Graph;

    using Graph_link = Graph*;
}

namespace mapf_r::graph {
    class Vertex : public Static<Vertex> {
    public:
        using Id = Idx;

        using Neighbor_ids = Hash<Id>;

        static constexpr Id invalid_id = invalid_idx;

        Vertex()                                                                           = delete;
        ~Vertex()                                                                         = default;
        Vertex(const Vertex&)                                                             = default;
        Vertex& operator =(const Vertex&)                                                 = default;
        Vertex(Vertex&&)                                                                  = default;
        Vertex& operator =(Vertex&&)                                                      = default;
        Vertex(Id id_, Coord pos_)                            : _id(move(id_)), _pos(move(pos_)) { }

        const auto& cid() const noexcept                                             { return _id; }

        Coord cpos() const noexcept                                                 { return _pos; }

        const auto& cneighbor_ids() const noexcept                         { return _neighbor_ids; }

        inline size_t deg() const noexcept;

        bool is_neighbor(const Id&) const noexcept;

        void add_neighbor(const Id&);

        bool equals(const Vertex&) const noexcept;

        String to_string() const;
    protected:
        Coord& pos() noexcept                                                       { return _pos; }

        auto& neighbor_ids() noexcept                                      { return _neighbor_ids; }
    private:
        Id _id;

        Coord _pos;

        Neighbor_ids _neighbor_ids{};
    };
}

namespace mapf_r {
    class Graph : public Static<Graph> {
    public:
        using Vertex = graph::Vertex;

        using Vertices = Vector<Vertex>;

        Graph()                                                                           = default;
        ~Graph()                                                                          = default;
        Graph(const Graph&)                                                               = default;
        Graph& operator =(const Graph&)                                                   = default;
        Graph(Graph&&)                                                                    = default;
        Graph& operator =(Graph&&)                                                        = default;
        Graph(istream&);
        Graph(istream&& is)                                                          : Graph(is) { }

        void parse(istream&);
        void parse_g(istream&);
        void parse_mpf(istream&);
        void parse_mapr(istream&);

        const auto& cvertices() const noexcept                                 { return _vertices; }
        inline const auto& cvertex(const Vertex::Id&) const;

        size_t cn_edges() const noexcept                                        { return _n_edges; }

        void add_vertex(Vertex);

        void add_edge(const Vertex::Id&, const Vertex::Id&);

        String to_string() const;
    protected:
        auto& vertices() noexcept                                              { return _vertices; }
        inline auto& vertex(const Vertex::Id&);

        size_t& n_edges() noexcept                                              { return _n_edges; }
    private:
        template <char vertex_lbracket, char vertex_id_delim,
                  char edge_lbracket = '{', char edge_delim = ',',
                  predicate<istream&, char> OtherCharsPred = True_pred>
        void parse_basic_impl(istream&, OtherCharsPred = {});

        Vertices _vertices{};

        size_t _n_edges{};
    };
}

#include "mapf_r/graph.inl"
