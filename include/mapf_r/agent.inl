#pragma once

namespace mapf_r {
    Float Agent::default_delta() const noexcept
    {
        return cradius();
    }

    template <agent::State_c S>
    S& Agent::set_state(auto&&... args)
    {
        return state().template set<S>(FORWARD(args)...);
    }

    Agent::State::Idle& Agent::set_idle_state(auto&&... args)
    {
        return state().set_idle(FORWARD(args)...);
    }

    Agent::State::Linear& Agent::set_linear_state(auto&&... args)
    {
        return state().set_linear(FORWARD(args)..., cabs_v());
    }

    Agent::State::Parabolic& Agent::set_parabolic_state(auto&&... args)
    {
        return set_parabolic_state_with_delta(default_delta(), FORWARD(args)...);
    }

    Agent::State::Parabolic& Agent::set_parabolic_state_with_delta(Float delta_, auto&&... args)
    {
        return state().set_parabolic(FORWARD(args)..., cabs_v(), delta_);
    }

    Agent::State::Linear& Agent::set_linear_state_to(auto&&... args)
    {
        return state().set_linear_to(FORWARD(args)..., cabs_v());
    }

    Agent::State::Parabolic& Agent::set_parabolic_state_to(auto&&... args)
    {
        return set_parabolic_state_to_with_delta(default_delta(), FORWARD(args)...);
    }

    Agent::State::Parabolic& Agent::set_parabolic_state_to_with_delta(Float delta_, auto&&... args)
    {
        return state().set_parabolic_to(FORWARD(args)..., cabs_v(), delta_);
    }

    void Agent::linear_state_set_to(auto&&... args)
    {
        state().get_linear().set_to(FORWARD(args)..., cabs_v());
    }

    void Agent::parabolic_state_set_to(auto&&... args)
    {
        parabolic_state_set_to_with_delta(default_delta(), FORWARD(args)...);
    }

    void Agent::parabolic_state_set_to_with_delta(Float delta_, auto&&... args)
    {
        state().get_parabolic().set_to(FORWARD(args)..., cabs_v(), delta_);
    }
}

#include "mapf_r/agent/state.inl"
