#pragma once

#include "mapf_r/core.hpp"

namespace mapf_r::agent {
    class Base;

    using Id = Idx;
}

namespace mapf_r {
    // Agent with a state
    class Agent;

    using Agents = Vector<Agent>;
}

namespace mapf_r::agent {
    class Base : public Static<Base> {
    public:
        Base()                                                                             = delete;
        ~Base()                                                                           = default;
        Base(const Base&)                                                                 = default;
        Base& operator =(const Base&)                                                     = default;
        Base(Base&&)                                                                      = default;
        Base& operator =(Base&&)                                                          = default;
        Base(Id id_, Float rad, Float abs_v_);
        Base(Float rad, Float abs_v_);

        const auto& cid() const noexcept                                             { return _id; }

        Float cradius() const noexcept                                           { return _radius; }

        Float cabs_v() const noexcept                                             { return _abs_v; }

        bool equals(const This&) const noexcept;

        String to_string() const;
    protected:
        static Id make_id();

        bool valid() const noexcept;
        void check() const;
        bool valid_radius() const noexcept;
        void check_radius() const;
        bool valid_abs_v() const noexcept;
        void check_abs_v() const;

        Float& radius() noexcept                                                 { return _radius; }

        Float& abs_v() noexcept                                                   { return _abs_v; }
    private:
        Id _id;

        Float _radius;

        Float _abs_v;
    };
}

#include "mapf_r/agent/state.hpp"

namespace mapf_r {
    class Agent : public Inherit<agent::Base, Agent> {
    public:
        using Id = agent::Id;

        using State = agent::State;
        using Pos = agent::state::Pos;

        using Inherit::Inherit;
        Agent()                                                                            = delete;
        ~Agent()                                                                          = default;
        Agent(const Agent&)                                                               = default;
        Agent& operator =(const Agent&)                                                   = default;
        Agent(Agent&&)                                                                    = default;
        Agent& operator =(Agent&&)                                                        = default;
        Agent(Id id_, Float rad, Float abs_v_, State);
        Agent(Float rad, Float abs_v_, State);
        Agent(Id id_, Float rad, Float abs_v_, Pos pos)    : Agent(id_, rad, abs_v_, State(pos)) { }
        Agent(Float rad, Float abs_v_, Pos pos)                 : Agent(rad, abs_v_, State(pos)) { }
        Agent(Id id_, Float rad, Float abs_v_, State::Parabolic, Float delta_);
        Agent(Float rad, Float abs_v_, State::Parabolic, Float delta_);

        const auto& cstate() const noexcept                                       { return _state; }
        auto& state() noexcept                                                    { return _state; }

        inline Float default_delta() const noexcept;

        template <agent::State_c S> S& set_state(auto&&...);
        inline State::Idle& set_idle_state(auto&&...);
        inline State::Linear& set_linear_state(auto&&...);
        inline State::Parabolic& set_parabolic_state(auto&&...);

        inline State::Linear& set_linear_state_to(auto&&...);
        inline State::Parabolic& set_parabolic_state_to(auto&&...);

        inline void linear_state_set_to(auto&&...);
        inline void parabolic_state_set_to(auto&&...);

        void parabolic_state_set_delta(Float delta_)                { parabolic_state_set(delta_); }

        bool equals(const This&) const noexcept;

        String to_string() const;
    protected:
        void init();

        inline State::Parabolic& set_parabolic_state_with_delta(Float, auto&&...);

        inline State::Parabolic& set_parabolic_state_to_with_delta(Float, auto&&...);

        inline void parabolic_state_set_to_with_delta(Float, auto&&...);

        void idle_state_set();
        void linear_state_set();
        void parabolic_state_set(Float delta_);
        void parabolic_state_set()                         { parabolic_state_set(default_delta()); }
    private:
        State _state{};
    };
}

#include "mapf_r/agent.inl"
