#pragma once

namespace mapf_r::smt::solver {
    Mathsat::Expr Mathsat::make_bool_impl(const String& id)
    {
        return make_var(id, _bool_type);
    }

    Mathsat::Expr Mathsat::make_real_impl(const String& id)
    {
        return make_var(id, _real_type);
    }

    Mathsat::Expr Mathsat::make_int(const String& id)
    {
        return make_int_directly(id);
    }

    Mathsat::Expr Mathsat::make_int_impl(const String& id)
    {
        return make_var(id, _int_type);
    }

    Mathsat::Expr Mathsat::make_bv_impl(const String& id, size_t bits)
    {
        return make_var(id, ::msat_get_bv_type(csolver(), bits));
    }

    Mathsat::Expr Mathsat::make_bool_impl(bool b)
    {
        Expr e = b ? ::msat_make_true(solver()) : ::msat_make_false(solver());
        assert(!MSAT_ERROR_TERM(e));
        return e;
    }

    Mathsat::Expr Mathsat::make_int(unsigned val)
    {
        return make_int_directly(val);
    }

    Mathsat::Expr Mathsat::make_int_impl(unsigned val)
    {
        assert(val <= unsigned(limits<int>::max()));
        Expr e = ::msat_make_int_number(solver(), val);
        assert(!MSAT_ERROR_TERM(e));
        return e;
    }

    Mathsat::Expr Mathsat::make_bv_impl(unsigned val, size_t bits)
    {
        Expr e = ::msat_make_bv_int_number(solver(), val, bits);
        assert(!MSAT_ERROR_TERM(e));
        return e;
    }

    Mathsat::Expr Mathsat::neg(const Expr& e)
    {
        Expr t = ::msat_make_not(solver(), e);
        assert(!MSAT_ERROR_TERM(t));
        return t;
    }

    Mathsat::Expr Mathsat::conj(const Expr& lhs, const Expr& rhs)
    {
        Expr t = ::msat_make_and(solver(), lhs, rhs);
        assert(!MSAT_ERROR_TERM(t));
        return t;
    }

    Mathsat::Expr Mathsat::disj(const Expr& lhs, const Expr& rhs)
    {
        Expr t = ::msat_make_or(solver(), lhs, rhs);
        assert(!MSAT_ERROR_TERM(t));
        return t;
    }

    Mathsat::Expr Mathsat::iff(const Expr& lhs, const Expr& rhs)
    {
        Expr t = ::msat_make_iff(solver(), lhs, rhs);
        assert(!MSAT_ERROR_TERM(t));
        return t;
    }

    Mathsat::Expr Mathsat::plus(const Expr& lhs, const Expr& rhs)
    {
        Expr t = ::msat_make_plus(solver(), lhs, rhs);
        assert(!MSAT_ERROR_TERM(t));
        return t;
    }

    Mathsat::Expr Mathsat::minus(const Expr& lhs, const Expr& rhs)
    {
        // this is really suggested by the authors
        return plus(lhs, times(rhs, _minus_one));
    }

    Mathsat::Expr Mathsat::times(const Expr& lhs, const Expr& rhs)
    {
        Expr t = ::msat_make_times(solver(), lhs, rhs);
        assert(!MSAT_ERROR_TERM(t));
        return t;
    }

    Mathsat::Expr Mathsat::eq(const Expr& lhs, const Expr& rhs)
    {
        Expr t = ::msat_make_equal(solver(), lhs, rhs);
        assert(!MSAT_ERROR_TERM(t));
        return t;
    }

    Mathsat::Expr Mathsat::leq(const Expr& lhs, const Expr& rhs)
    {
        Expr t = ::msat_make_leq(solver(), lhs, rhs);
        assert(!MSAT_ERROR_TERM(t));
        return t;
    }

    Mathsat::Expr Mathsat::get_model_value_impl(const Expr& e) const
    {
        return ::msat_model_eval(model, e);
    }

    bool Mathsat::to_bool(const Expr& e) const
    {
        return ::msat_term_is_true(csolver(), e);
    }

    size_t Mathsat::vertex_expr_bit_width() const
    {
        return util::max(Inherit::vertex_expr_bit_width(), 16UL);
    }

    void Mathsat::assert_expr_impl(Expr e)
    {
        [[maybe_unused]] const int res = ::msat_assert_formula(solver(), e);
        assert(res == 0);
    }

    void Mathsat::push_impl()
    {
        [[maybe_unused]] const int res = ::msat_push_backtrack_point(solver());
        assert(res == 0);
    }

    void Mathsat::pop_impl()
    {
        [[maybe_unused]] const int res = ::msat_pop_backtrack_point(solver());
        assert(res == 0);
    }

    void Mathsat::add_solver_preferred_variable(Expr e)
    {
        ::msat_add_preferred_for_branching(solver(), e, MSAT_TRUE);
    }

    void Mathsat::clear_solver_preferred_variables()
    {
        ::msat_clear_preferred_for_branching(solver());
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace mapf_r::smt {
    String to_string(const solver::mathsat::Conf::Sat_result& res)
    {
        return solver::Mathsat::to_string(res);
    }

    ostream& operator <<(ostream& os, const solver::mathsat::Conf::Sat_result& res)
    {
        return solver::Mathsat::print(os, res);
    }
}
