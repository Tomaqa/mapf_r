#pragma once

#include "mapf_r/smt.hpp"

#include "mapf_r/graph/alg.hpp"
#include "mapf_r/agent/layout.hpp"
#include "mapf_r/agent/plan.hpp"

#include <tomaqa/util/profile.hpp>

#include <iostream>

namespace mapf_r::smt::solver {
    struct Profile;

    class Base;
}

namespace mapf_r::smt::solver::profile::conf {
    namespace aux {
        struct Base {
            using Parent = Profile;
        };
    }

    struct Declare_consts : aux::Base {
        static constexpr const char* name = "declare_consts";
    };
    struct Assert_positions : aux::Base {
        static constexpr const char* name = "assert_positions";
    };
    struct Assert_transitions : aux::Base {
        static constexpr const char* name = "assert_transitions";
    };
    struct Check_sat : aux::Base {
        static constexpr const char* name = "check_sat";
    };
    struct Get_model : aux::Base {
        static constexpr const char* name = "get_model";
    };
    struct Assert_any_conflict : aux::Base {
        static constexpr const char* name = "assert_any_conflict";
    };
    struct Minimize : aux::Base {
        static constexpr const char* name = "minimize";
    };
}

namespace mapf_r::smt::solver::profile {
    using Declare_consts = util::profile::Tp<conf::Declare_consts>;
    using Assert_positions = util::profile::Tp<conf::Assert_positions>;
    using Assert_transitions = util::profile::Tp<conf::Assert_transitions>;
    using Check_sat = util::profile::Tp<conf::Check_sat>;
    using Get_model = util::profile::Tp<conf::Get_model>;
    using Assert_any_conflict = util::profile::Tp<conf::Assert_any_conflict>;
    using Minimize = util::profile::Tp<conf::Minimize>;
}

namespace mapf_r::smt::solver::profile::conf {
    struct Get_agent_values : aux::Base {
        static constexpr const char* name = "get_agent_values";
        using Parent = profile::Assert_any_conflict;
    };
    struct Unsafe_intervals {
        static constexpr const char* name = "unsafe_intervals";
        using Parent = profile::Assert_any_conflict;
    };
    struct Conflict_clause {
        static constexpr const char* name = "conflict clause";
        using Parent = profile::Assert_any_conflict;
    };
}

namespace mapf_r::smt::solver::profile {
    using Get_agent_values = util::profile::Tp<conf::Get_agent_values>;
    using Unsafe_intervals = util::profile::Tp<conf::Unsafe_intervals>;
    using Conflict_clause = util::profile::Tp<conf::Conflict_clause>;
}

namespace mapf_r::smt::solver {
    struct Profile : Inherit<util::profile::Main, Profile> {
        using Inherit::Inherit;

        profile::Declare_consts declare_consts{*this};
        profile::Assert_positions assert_positions{*this};
        profile::Assert_transitions assert_transitions{*this};
        profile::Check_sat check_sat{*this};
        profile::Get_model get_model{*this};
        profile::Assert_any_conflict assert_any_conflict{*this};
        profile::Get_agent_values get_agent_values{assert_any_conflict};
        profile::Unsafe_intervals unsafe_intervals{assert_any_conflict};
        profile::Conflict_clause conflict_clause{assert_any_conflict};
        profile::Minimize minimize{*this};

        long n_conflicts{};

        String to_string() const;
    };

    class Base : public Dynamic<Base> {
    public:
        template <Class> class Run;

        struct Options;

        using Precision = precision::Low;

        static constexpr bool profiling = true;

        Base()                                                                            = default;
        virtual ~Base()                                                                   = default;
        Base(const Base&)                                                                 = default;
        Base& operator =(const Base&)                                                     = default;
        Base(Base&&)                                                                      = default;
        Base& operator =(Base&&)                                                          = default;

        const auto& coptions() const noexcept                              { return *_options_ptr; }
        auto& options() noexcept                                           { return *_options_ptr; }

        inline const auto& cgraph() const;
        inline const auto& clayout() const;

        const auto& cgraph_properties() const                          { return _graph_properties; }

        Flag is_sat() const noexcept                                             { return _is_sat; }

        Float cmin_objective_lower() const noexcept                 { return _min_objective_lower; }

        Idx cfinal_k() const noexcept                                           { return _final_k; }
        Float cfinal_objective_time() const noexcept               { return _final_objective_time; }
        Float cfinal_objective_lower() const noexcept             { return _final_objective_lower; }

        bool finished() const noexcept                                         { return _finished; }

        const Profile& cprofile() const noexcept                                { return _profile; }

        inline bool producing_smt2() const noexcept;

        void set_graph(Graph&);
        void set_layout(agent::Layout&);

        void produce_smt2_to(ostream&);

        Float guaranteed_suboptimal_coef() const noexcept;

        // Reimplement at least one of them
        virtual void parse(String);
        virtual void parse(istream&);

        virtual void parse_file(const Path&);

        void solve(ostream& = std::cout);

        virtual agent::plan::Global make_plan(ostream& = std::cout);
    protected:
        using Options_ptr = Unique_ptr<Options>;

        /// to make the computed distances consistent with apx. rational values used in SMT solver
        struct Graph_min_dist_conf;

        using Graph_min_dist = graph::Min_dist_tp<Graph_min_dist_conf>;

        using Graph_min_dists_map = Vector<Graph_min_dist>;
        using Graph_reachables_map = Vector<graph::Reachable>;
        using Graph_shortest_paths_map = Vector<graph::Path>;

        static constexpr Side default_side = Side::left;

        static inline Options_ptr new_options();

        inline auto& graph();
        inline auto& layout();

        inline ostream& smt2_os() const noexcept;

        const auto& cagents() const noexcept                                     { return _agents; }
        auto& agents() noexcept                                                  { return _agents; }

        auto& graph_properties()                                       { return _graph_properties; }
        const auto& cgraph_min_dists_map() const noexcept           { return _graph_min_dists_map; }
        auto& graph_min_dists_map() noexcept                        { return _graph_min_dists_map; }
        inline const auto& cgraph_min_dist_of(const agent::Id&) const;
        inline auto& graph_min_dist_of(const agent::Id&);
        const auto& cgraph_reachables_map() const noexcept         { return _graph_reachables_map; }
        auto& graph_reachables_map() noexcept                      { return _graph_reachables_map; }
        inline const auto& cgraph_reachable_of(const agent::Id&) const;
        inline auto& graph_reachable_of(const agent::Id&);
        const auto& cgraph_shortest_paths_map() const noexcept { return _graph_shortest_paths_map; }
        auto& graph_shortest_paths_map() noexcept              { return _graph_shortest_paths_map; }
        inline const auto& cgraph_shortest_path_of(const agent::Id&) const;
        inline auto& graph_shortest_path_of(const agent::Id&);

        void set_sat() noexcept                                                  { _is_sat = true; }
        void set_unsat() noexcept                                               { _is_sat = false; }

        Float& min_objective_lower() noexcept                       { return _min_objective_lower; }

        Idx& final_k() noexcept                                                 { return _final_k; }
        Float& final_objective_time() noexcept                     { return _final_objective_time; }
        Float& final_objective_lower() noexcept                   { return _final_objective_lower; }

        void set_finished() noexcept                                           { _finished = true; }

        Profile& profile() const noexcept                                       { return _profile; }

        template <Side = default_side, Precision_c = Precision>
        static inline Rational to_rational(Float);

        static Float move_time_float_of(const Agent&, const graph::Vertex& from, const graph::Vertex& to);
        static Float move_time_float_of_dist(const Agent&, Float);
        static inline Rational move_time_to_rational(Float);
        static inline Rational
            move_time_rational_of(const Agent&, const graph::Vertex& from, const graph::Vertex& to);
        static inline Rational move_time_rational_of_dist(const Agent&, Float);

        virtual void solve_init();
        virtual void solve_finish();

        virtual void get_model();

        virtual void push();
        virtual void pop();

        /// Should be called in `make_*` in the derived solver where `Expr` is already known
        virtual void make_bool_pre(const String&);
        virtual void make_real_pre(const String&);
        virtual void make_int_pre(const String&);
        virtual void make_bv_pre(const String&, size_t);

        /// Pre-hook of `Solver::check_sat`
        virtual void check_sat_pre();

        // No other hooks for `assert_expr` nor `check_sat` here because they use tp. argument types

        void print_smt2(const Printable auto&) const;
        void print_smt2_declare_const(const String&, const String& type_str) const;

        virtual void print_smt2_head() const;
        virtual void print_smt2_tail() const;

        void print_smt2_push() const;
        void print_smt2_pop() const;
        void print_smt2_make_bool(const String&) const;
        void print_smt2_make_real(const String&) const;
        void print_smt2_make_int(const String&) const;
        void print_smt2_make_bv(const String&, size_t) const;
        void print_smt2_check_sat() const;
    private:
        virtual void solve_main(ostream& = std::cout)                                           = 0;

        virtual void get_model_impl()                                                           = 0;

        virtual void push_impl()                                                                = 0;
        virtual void pop_impl()                                                                 = 0;

        virtual agent::plan::Global make_plan_impl(ostream&)                                    = 0;

        virtual void print_smt2_head_set_logic() const                                          = 0;

        Options_ptr _options_ptr{new_options()};

        Graph_link _graph_l{};
        agent::Layout_link _layout_l{};

        ostream* _smt2_os_l{};

        Agents _agents{};

        graph::Properties _graph_properties{};
        Graph_min_dists_map _graph_min_dists_map{};
        Graph_reachables_map _graph_reachables_map{};
        Graph_shortest_paths_map _graph_shortest_paths_map{};

        Flag _is_sat{};

        Float _min_objective_lower{};

        Idx _final_k{};
        Float _final_objective_time{};
        Float _final_objective_lower{};

        bool _finished{};

        mutable Profile _profile{profiling};
    };
}

namespace mapf_r::smt::solver {
    struct Base::Options {
        Flag optimize_makespan{};
        Flag optimize_soc{};

        float suboptimal_coef{};

        bool produce_smt2{};
    };

    struct Base::Graph_min_dist_conf {
        struct Conv_dist {
            inline Float operator ()(Float);
        };
    };
}

#include "mapf_r/smt/solver/base.inl"
