#pragma once

namespace mapf_r::smt::solver::z3 {
    Context& Model::ctx() const
    {
        return static_cast<Context&>(Inherit::ctx());
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace mapf_r::smt::solver {
    Z3::Expr Z3::make_bool_impl(const String& id)
    {
        return context().bool_const(id.c_str());
    }

    Z3::Expr Z3::make_real_impl(const String& id)
    {
        return context().real_const(id.c_str());
    }

    Z3::Expr Z3::make_bv_impl(const String& id, size_t bits)
    {
        return context().bv_const(id.c_str(), bits);
    }

    Z3::Expr Z3::make_bool_impl(bool b)
    {
        return context().bool_val(b);
    }

    Z3::Expr Z3::make_real_impl(Rational rat)
    {
        return context().real_val(rat.numerator(), rat.denominator());
    }

    Z3::Expr Z3::make_bv_impl(unsigned val, size_t bits)
    {
        return context().bv_val(val, bits);
    }

    Z3::Expr Z3::neg(const Expr& e)
    {
        return !e;
    }

    Z3::Expr Z3::conj(const Expr& lhs, const Expr& rhs)
    {
        return lhs && rhs;
    }

    Z3::Expr Z3::disj(const Expr& lhs, const Expr& rhs)
    {
        return lhs || rhs;
    }

    Z3::Expr Z3::implies(const Expr& lhs, const Expr& rhs)
    {
        return ::z3::implies(lhs, rhs);
    }

    Z3::Expr Z3::plus(const Expr& lhs, const Expr& rhs)
    {
        return lhs + rhs;
    }

    Z3::Expr Z3::minus(const Expr& lhs, const Expr& rhs)
    {
        return lhs - rhs;
    }

    Z3::Expr Z3::times(const Expr& lhs, const Expr& rhs)
    {
        return lhs*rhs;
    }

    Z3::Expr Z3::eq(const Expr& lhs, const Expr& rhs)
    {
        return lhs == rhs;
    }

    Z3::Expr Z3::leq(const Expr& lhs, const Expr& rhs)
    {
        return lhs <= rhs;
    }

    Z3::Expr Z3::min(const Expr& lhs, const Expr& rhs)
    {
        return ::z3::min(lhs, rhs);
    }

    Z3::Expr Z3::max(const Expr& lhs, const Expr& rhs)
    {
        return ::z3::max(lhs, rhs);
    }

    Z3::Expr Z3::make_disj(Expr_vector vec)
    {
        return ::z3::mk_or(vec);
    }

    void Z3::get_model_impl()
    {
        model = solver().get_model();
    }

    Z3::Expr Z3::get_model_value_impl(const Expr& e) const
    {
        return model.eval(e);
    }

    bool Z3::to_bool(const Expr& e) const
    {
        return ::z3::to_check_result(e.bool_value()) == ::z3::sat;
    }

    Float Z3::to_float(const Expr& e) const
    {
        return e.as_double();
    }

    Z3::Expr Z3::get_min_objective_time_impl()
    {
        return solver().lower(minimize_handle);
    }

    void Z3::assert_expr_impl(Expr e)
    {
        solver().add(e);
    }

    void Z3::assert_expr_vector(Expr_vector& vec)
    {
        solver().add(vec);
    }

    void Z3::clear_expr_vector(Expr_vector& vec)
    {
        vec.resize(0);
    }

    void Z3::push_impl()
    {
        solver().push();
    }

    void Z3::pop_impl()
    {
        solver().pop();
    }

    Z3::Sat_result Z3::check_sat_impl()
    {
        // It seems that assumptions in `check` do not work with minimize ...
        return solver().check();
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace mapf_r::smt {
    String to_string(const solver::z3::Conf::Sat_result& res)
    {
        return solver::Z3::to_string(res);
    }
}
