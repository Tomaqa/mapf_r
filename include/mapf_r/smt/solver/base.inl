#pragma once

namespace mapf_r::smt::solver {
    Base::Options_ptr Base::new_options()
    {
        return make_unique<Options>();
    }

    const auto& Base::cgraph() const
    {
        expect(_graph_l, "Graph has not been set yet!");
        return *_graph_l;
    }

    auto& Base::graph()
    {
        return const_cast<Graph&>(cgraph());
    }

    const auto& Base::clayout() const
    {
        expect(_layout_l, "Layout has not been set yet!");
        return *_layout_l;
    }

    auto& Base::layout()
    {
        return const_cast<agent::Layout&>(clayout());
    }

    ostream& Base::smt2_os() const noexcept
    {
        assert(_smt2_os_l);
        assert(producing_smt2());
        return *_smt2_os_l;
    }

    const auto& Base::cgraph_min_dist_of(const agent::Id& aid) const
    {
        return cgraph_min_dists_map()[aid];
    }

    auto& Base::graph_min_dist_of(const agent::Id& aid)
    {
        return graph_min_dists_map()[aid];
    }

    const auto& Base::cgraph_reachable_of(const agent::Id& aid) const
    {
        return cgraph_reachables_map()[aid];
    }

    auto& Base::graph_reachable_of(const agent::Id& aid)
    {
        return graph_reachables_map()[aid];
    }

    const auto& Base::cgraph_shortest_path_of(const agent::Id& aid) const
    {
        return cgraph_shortest_paths_map()[aid];
    }

    auto& Base::graph_shortest_path_of(const agent::Id& aid)
    {
        return graph_shortest_paths_map()[aid];
    }

    bool Base::producing_smt2() const noexcept
    {
        return coptions().produce_smt2;
    }

    template <Side sideV, Precision_c P>
    Rational Base::to_rational(Float val)
    {
        return smt::to_rational<sideV, P>(val);
    }

    Rational Base::move_time_to_rational(Float t)
    {
        return to_rational(t);
    }

    Rational
    Base::move_time_rational_of(const Agent& ag, const graph::Vertex& from, const graph::Vertex& to)
    {
        const Float t = move_time_float_of(ag, from, to);
        return move_time_to_rational(t);
    }

    void Base::print_smt2(const Printable auto& arg) const
    {
        smt2_os() << "(" << arg << ")" << std::endl;
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace mapf_r::smt::solver {
    Float Base::Graph_min_dist_conf::Conv_dist::operator ()(Float d)
    {
        //! const Rational rat = move_time_rational_of_dist(ag, d);
        const Rational rat = move_time_to_rational(d);
        return math::to_float(rat);
    }
}
