#pragma once

namespace mapf_r::smt::solver {
    const auto& Opensmt::csolver() const
    {
        return const_cast<Orig_solver&>(Inherit::csolver()).getMainSolver();
    }

    auto& Opensmt::solver()
    {
        return const_cast<::MainSolver&>(csolver());
    }

    const auto& Opensmt::clogic() const
    {
        return const_cast<Orig_solver&>(Inherit::csolver()).getLRALogic();
    }

    auto& Opensmt::logic()
    {
        return const_cast<::ArithLogic&>(clogic());
    }

    Opensmt::Expr Opensmt::make_bool_impl(const String& id)
    {
        return logic().mkBoolVar(id.c_str());
    }

    Opensmt::Expr Opensmt::make_real_impl(const String& id)
    {
        return logic().mkRealVar(id.c_str());
    }

    Opensmt::Expr Opensmt::make_bool_impl(bool b)
    {
        return b ? logic().getTerm_true() : logic().getTerm_false();
    }

    Opensmt::Expr Opensmt::neg(const Expr& e)
    {
        return logic().mkNot(e);
    }

    Opensmt::Expr Opensmt::conj(const Expr& lhs, const Expr& rhs)
    {
        return logic().mkAnd(lhs, rhs);
    }

    Opensmt::Expr Opensmt::disj(const Expr& lhs, const Expr& rhs)
    {
        return logic().mkOr(lhs, rhs);
    }

    Opensmt::Expr Opensmt::implies(const Expr& lhs, const Expr& rhs)
    {
        return logic().mkImpl(lhs, rhs);
    }

    Opensmt::Expr Opensmt::plus(const Expr& lhs, const Expr& rhs)
    {
        return logic().mkPlus(lhs, rhs);
    }

    Opensmt::Expr Opensmt::minus(const Expr& lhs, const Expr& rhs)
    {
        return logic().mkMinus(lhs, rhs);
    }

    Opensmt::Expr Opensmt::times(const Expr& lhs, const Expr& rhs)
    {
        return logic().mkTimes(lhs, rhs);
    }

    Opensmt::Expr Opensmt::eq(const Expr& lhs, const Expr& rhs)
    {
        return logic().mkEq(lhs, rhs);
    }

    Opensmt::Expr Opensmt::leq(const Expr& lhs, const Expr& rhs)
    {
        return logic().mkLeq(lhs, rhs);
    }

    Opensmt::Expr Opensmt::geq(const Expr& lhs, const Expr& rhs)
    {
        return logic().mkGeq(lhs, rhs);
    }

    Opensmt::Expr Opensmt::lt(const Expr& lhs, const Expr& rhs)
    {
        return logic().mkLt(lhs, rhs);
    }

    Opensmt::Expr Opensmt::gt(const Expr& lhs, const Expr& rhs)
    {
        return logic().mkGt(lhs, rhs);
    }

    Opensmt::Expr Opensmt::make_disj(Expr_vector vec)
    {
        return logic().mkOr(move(vec));
    }

    Opensmt::Expr Opensmt::get_model_value_impl(const Expr& e) const
    {
        return model_ptr->evaluate(e);
    }

    bool Opensmt::to_bool(const Expr& e) const
    {
        return clogic().isTrue(e);
    }

    auto& Opensmt::get_orig_number(const Expr& e) const
    {
        return clogic().getNumConst(e);
    }

    void Opensmt::assert_expr_impl(Expr e)
    {
        solver().insertFormula(move(e));
    }

    void Opensmt::push_to_expr_vector(Expr_vector& vec, Expr e)
    {
        vec.push_m(move(e));
    }

    void Opensmt::push_impl()
    {
        solver().push();
    }

    void Opensmt::pop_impl()
    {
        solver().pop();
    }

    void Opensmt::add_solver_preferred_variable(Expr e)
    {
        //++ requires conversion from Expr to Var which seems to not exist
        //++ solver().setDecisionVar(e, true);
    }

    void Opensmt::clear_solver_preferred_variables()
    {
        //++ order_heap?
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace mapf_r::smt {
    String to_string(const solver::opensmt::Conf::Sat_result& res)
    {
        return solver::Opensmt::to_string(res);
    }

    ostream& operator <<(ostream& os, const solver::opensmt::Conf::Sat_result& res)
    {
        return solver::Opensmt::print(os, res);
    }
}
