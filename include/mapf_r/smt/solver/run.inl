#pragma once

namespace mapf_r::smt::solver {
    template <Class S>
    String Base::Run<S>::usage() const
    {
        return Inherit::usage() + lusage();
    }

    template <Class S>
    String Base::Run<S>::lusage() const
    {
        return usage_row('g', "Path to graph definition")
             + usage_row('l', "Path to layout definition")
             + usage_row('p', "Path to resulting plan")
             + usage_row('t', "Produce the corresponding SMT-LIB2 trace file at given path")
             + usage_row('F', "Cost function to be optimized (makespan|M|soc|S)")
             + usage_row('B', "Bounded-suboptimal coefficient >1")
             ;
    }

    template <Class S>
    void Base::Run<S>::init()
    {
        Inherit::init();
        linit();
    }

    template <Class S>
    void Base::Run<S>::linit()
    {
        auto& s = solver();
        expect(_graph_ptr, "Graph is not set!");
        s.set_graph(graph());
        expect(_layout_ptr, "Layout of agents is not set!");
        s.set_layout(layout());
    }

    template <Class S>
    void Base::Run<S>::do_stuff()
    {
        auto& s = solver();

        //- if (cipath().empty()) s.parse(is());
        //- else s.parse_file(cipath());

        s.solve(os());
    }

    template <Class S>
    void Base::Run<S>::finish()
    {
        Inherit::finish();
        lfinish();
    }

    template <Class S>
    void Base::Run<S>::lfinish()
    {
        print_result();
        _printed_result = true;
        print_profiling();
        _printed_profiling = true;
    }

    template <Class S>
    String Base::Run<S>::getopt_str() const noexcept
    {
        return Inherit::getopt_str() + lgetopt_str();
    }

    template <Class S>
    String Base::Run<S>::lgetopt_str() const noexcept
    {
        return "g:l:p:t:F:B:";
    }

    template <Class S>
    bool Base::Run<S>::process_opt(char c)
    {
        if (Inherit::process_opt(c)) return true;
        return lprocess_opt(c);
    }

    template <Class S>
    bool Base::Run<S>::lprocess_opt(char c)
    {
        using tomaqa::to_string;

        String str;
        float coef;

        auto& s = solver();
        auto& opts = s.options();

        switch (c) {
        default: return false;
        case 'g':
            _graph_ptr = make_unique<Graph>(ifstream(optarg));
            return true;
        case 'l':
            _layout_ptr = make_unique<agent::Layout>(ifstream(optarg));
            return true;
        case 'p':
            set_plan(optarg);
            return true;
        case 't':
            set_smt2(optarg);
            s.produce_smt2_to(_smt2_ofs);
            return true;
        case 'F':
            str = optarg;
            expect(contains({"makespan", "soc", "M", "S"}, str),
                   "Expected a cost function, got: "s + str);
            if (tolower(str[0]) == 's') {
                assert(str == "soc" || str == "S");
                opts.optimize_soc = true;
                opts.optimize_makespan = false;
            }
            else {
                assert(str == "makespan" || str == "M");
                opts.optimize_makespan = true;
                opts.optimize_soc = false;
            }
            return true;
        case 'B':
            coef = to_value_check<float>(optarg);
            expect(coef > 1, "Expected bounded-suboptimal coefficient >1, got: "s + to_string(coef));
            opts.suboptimal_coef = coef;
            return true;
        }
    }

    template <Class S>
    void Base::Run<S>::set_plan(const String& path_str)
    {
        set_ofstream(_plan_path, _plan_ofs, path_str, "resulting plan");
    }

    template <Class S>
    void Base::Run<S>::set_smt2(const String& path_str)
    {
        set_ofstream(_smt2_path, _smt2_ofs, path_str, "corresponding SMT-LIB2 file");
    }

    template <Class S>
    void Base::Run<S>::set_ofstream(Path& path, ofstream& ofs,
                                    const String& path_str, const String& msg)
    {
        using tomaqa::to_string;

        path = path_str;
        ofs.open(path);
        expect(ofs, "The output file for the "s + msg + " is not writable: " + to_string(path));
    }

    template <Class S>
    void Base::Run<S>::process_additional_args()
    {
        constexpr int graph_case = 0;
        constexpr int layout_case = 1;
        constexpr int plan_case = 2;
        static const String prefixes[] = {"graph=", "layout=", "plan="};

        //+ can be generalized -> into `util::Run`
        constexpr int n = size(prefixes);
        for (int i = 0; i < n; ++i) {
            if (_argc - optind <= 0) break;
            String_view sw = _argv[optind++];
            int pos = i;
            for (int j = 0; j < n; ++j) {
                auto& prefix = prefixes[j];
                if (!sw.starts_with(prefix)) continue;
                pos = j;
                sw.remove_prefix(prefix.size());
                break;
            }
            switch (pos) {
            default:
            case graph_case:
                _graph_ptr = make_unique<Graph>(ifstream(sw));
                break;
            case layout_case:
                _layout_ptr = make_unique<agent::Layout>(ifstream(sw));
                break;
            case plan_case:
                set_plan(sw);
                break;
            }
        }

        Inherit::process_additional_args();
    }

    template <Class S>
    void Base::Run<S>::print_result()
    {
        if (_printed_result) return;

        auto& s = solver();
        if (!s.is_sat()) return;

        auto& os_ = os();
        //! it should handle even unfinished plans
        if (!s.finished()) return s.print_final_objective_time(os_);

        auto plan = s.make_plan(os_);
        _plan_ofs << plan.compose();
    }

    template <Class S>
    void Base::Run<S>::print_profiling() const
    {
        if (_printed_profiling) return;

        os() << endl << csolver().cprofile();
    }

    template <Class S>
    void Base::Run<S>::signal_handler(int signal)
    {
        if (auto& prof = solver().profile(); !prof.finished()) {
            while (prof.n_laps() < 2) prof.lap();
            prof.finish();
        }

        auto& os_ = os();
        os_ << endl;
        finish();
        os_.flush();

        Inherit::signal_handler(signal);
    }
}
