#pragma once

#include "mapf_r/smt/solver.hpp"

#include <optimathsat.h>

namespace mapf_r::smt::solver::mathsat {
    struct Conf : conf::Default {
        using Solver = ::msat_env;

        using Expr = ::msat_term;
        using Expr_vector = Vector<Expr>;
        using Sat_result = ::msat_result;

        static constexpr Sat_result sat = MSAT_SAT;
        static constexpr Sat_result unsat = MSAT_UNSAT;
        static constexpr Sat_result unknown = MSAT_UNKNOWN;

        static constexpr bool use_vertex_bvs = true;

        /// Assumptions with optimization is not supported
        static constexpr bool solve_with_assumptions = !optimize;
    };
}

namespace mapf_r::smt::solver {
    class Mathsat;
}

// make these as visible as possible
// .. but it seems that it does not work with 'mapf_r' only
namespace mapf_r::smt {
    inline String to_string(const solver::mathsat::Conf::Sat_result&);
    inline ostream& operator <<(ostream&, const solver::mathsat::Conf::Sat_result&);
}

namespace mapf_r::smt::solver {
    class Mathsat : public Inherit<Solver<mathsat::Conf>, Mathsat> {
    public:
        using Inherit::Inherit;
        virtual ~Mathsat() noexcept;
        Mathsat(Mathsat&&) noexcept;
        Mathsat& operator =(Mathsat&&) noexcept;

        using Inherit::parse;
        void parse(String) override;

        void parse_file(const Path&) override;
    protected:
        void move_to(Mathsat&)&&;
        void lmove_to(Mathsat&)&&;

        inline Expr make_bool_impl(const String&) override;
        inline Expr make_real_impl(const String&) override;
        // it seems that it is the same as if it was real, though
        inline Expr make_int(const String&) override;
        inline Expr make_int_impl(const String&) override;
        inline Expr make_bv_impl(const String&, size_t) override;
        inline Expr make_bool_impl(bool) override;
        Expr make_real_impl(Rational) override;
        inline Expr make_int(unsigned) override;
        inline Expr make_int_impl(unsigned) override;
        inline Expr make_bv_impl(unsigned, size_t) override;

        inline Expr neg(const Expr&) override;
        inline Expr conj(const Expr&, const Expr&) override;
        inline Expr disj(const Expr&, const Expr&) override;
        inline Expr iff(const Expr&, const Expr&) override;
        inline Expr plus(const Expr&, const Expr&) override;
        inline Expr minus(const Expr&, const Expr&) override;
        inline Expr times(const Expr&, const Expr&) override;
        inline Expr eq(const Expr&, const Expr&) override;
        inline Expr leq(const Expr&, const Expr&) override;
        Expr min(const Expr&, const Expr&) override;
        Expr max(const Expr&, const Expr&) override;

        void get_model_impl() override;
        inline Expr get_model_value_impl(const Expr&) const override;
        inline bool to_bool(const Expr&) const override;
        Float to_float(const Expr&) const override;
        Rational to_rational(const Expr&) const override;
        // not more efficient, it still has to use `set_mpq` and `msat_term_to_number`
        // unsigned to_int(const Expr&) const override;

        Expr get_min_objective_time_impl() override;

        inline size_t vertex_expr_bit_width() const override;

        inline void assert_expr_impl(Expr) override;

        inline void push_impl() override;
        inline void pop_impl() override;

        Sat_result check_sat_impl() override;

        inline void add_solver_preferred_variable(Expr) override;
        inline void clear_solver_preferred_variables() override;

        void assert_minimize_impl() override;

        void solve_init() override;

        String expr_to_string(const Expr&) const override;
        String expr_to_smt2(const Expr&) const override;

        ::msat_objective objective{};

        ::msat_model model{};
    private:
        Expr make_var(const String&, ::msat_type);
        inline Expr make_real_impl(::mpq_t);

        void set_mpq(::mpq_t, const Expr& e) const;

        bool _destroy{true};

        ::msat_type _bool_type{};
        ::msat_type _real_type{};
        ::msat_type _int_type{};

        Expr _inf{};
        Expr _eps{};

        Expr _minus_one{};
    };
}

#include "mapf_r/smt/solver/mathsat/tp.hpp"

#include "mapf_r/smt/solver/mathsat.inl"
