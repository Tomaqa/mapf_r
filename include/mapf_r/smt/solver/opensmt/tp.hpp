#pragma once

namespace mapf_r::smt {
    extern template class
        Solver<solver::opensmt::Conf, ::opensmt_logic, const char*, Unique_ptr<::SMTConfig>>;
}
