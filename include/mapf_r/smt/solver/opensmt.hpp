#pragma once

#include "mapf_r/smt/solver.hpp"

#include <opensmt/opensmt2.h>

namespace mapf_r::smt::solver::opensmt {
    struct Conf : conf::Default {
        using Solver = ::Opensmt;

        using Expr = ::PTRef;
        using Expr_vector = ::vec<Expr>;
        using Sat_result = ::sstat;

        static inline const Sat_result sat = ::s_True;
        static inline const Sat_result unsat = ::s_False;
        static inline const Sat_result unknown = ::s_Undef;

        //+ try with ints/reals
        static constexpr bool use_vertex_bools = true;

        static constexpr bool manual_optimize = true;

        //+ static constexpr bool solve_with_assumptions = true;
    };
}

namespace mapf_r::smt::solver {
    class Opensmt;
}

// make these as visible as possible
// .. but it seems that it does not work with 'mapf_r' only
namespace mapf_r::smt {
    inline String to_string(const solver::opensmt::Conf::Sat_result&);
    inline ostream& operator <<(ostream&, const solver::opensmt::Conf::Sat_result&);
}

namespace mapf_r::smt::solver {
    class Opensmt
        : public Inherit<Solver<opensmt::Conf, ::opensmt_logic, const char*, Unique_ptr<::SMTConfig>>,
                         Opensmt> {
    public:
        static_assert(Solver_conf::manual_optimize);

        using Inherit::Inherit;
        Opensmt();
        virtual ~Opensmt()                                                                = default;
        Opensmt(Opensmt&&)                                                                = default;
        Opensmt& operator =(Opensmt&&)                                                    = default;

        using Inherit::parse;
        void parse(String) override;
    protected:
        static Unique_ptr<::SMTConfig> new_config();

        inline const auto& csolver() const;
        inline auto& solver();

        inline const auto& clogic() const;
        inline auto& logic();

        inline Expr make_bool_impl(const String&) override;
        inline Expr make_real_impl(const String&) override;
        inline Expr make_bool_impl(bool) override;
        Expr make_real_impl(Rational) override;

        inline Expr neg(const Expr&) override;
        inline Expr conj(const Expr&, const Expr&) override;
        inline Expr disj(const Expr&, const Expr&) override;
        inline Expr implies(const Expr&, const Expr&) override;
        //+ inline Expr iff(const Expr&, const Expr&) override;
        inline Expr plus(const Expr&, const Expr&) override;
        inline Expr minus(const Expr&, const Expr&) override;
        inline Expr times(const Expr&, const Expr&) override;
        inline Expr eq(const Expr&, const Expr&) override;
        inline Expr leq(const Expr&, const Expr&) override;
        inline Expr geq(const Expr&, const Expr&) override;
        inline Expr lt(const Expr&, const Expr&) override;
        inline Expr gt(const Expr&, const Expr&) override;
        Expr min(const Expr&, const Expr&) override;
        Expr max(const Expr&, const Expr&) override;
        inline Expr make_disj(Expr_vector) override;

        void get_model_impl() override;
        inline Expr get_model_value_impl(const Expr&) const override;
        inline bool to_bool(const Expr&) const override;
        Float to_float(const Expr&) const override;
        Rational to_rational(const Expr&) const override;
        unsigned to_int(const Expr&) const override;

        inline void assert_expr_impl(Expr) override;

        inline void push_to_expr_vector(Expr_vector&, Expr) override;

        inline void push_impl() override;
        inline void pop_impl() override;

        Sat_result check_sat_impl() override;

        inline void add_solver_preferred_variable(Expr) override;
        inline void clear_solver_preferred_variables() override;

        inline String expr_to_smt2(const Expr&) const override;

        Unique_ptr<::Model> model_ptr{};
    private:
        inline auto& get_orig_number(const Expr&) const;
    };
}

#include "mapf_r/smt/solver/opensmt/tp.hpp"

#include "mapf_r/smt/solver/opensmt.inl"
