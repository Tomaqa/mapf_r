#pragma once

#include "mapf_r/smt/solver.hpp"

#include <z3++.h>

namespace mapf_r::smt::solver::z3 {
    struct Context;

    struct Conf : conf::Default {
        using Solver = ::z3::optimize;
        using Context = z3::Context;

        using Expr = ::z3::expr;
        using Expr_vector = ::z3::expr_vector;
        using Sat_result = ::z3::check_result;

        static constexpr Sat_result sat = ::z3::sat;
        static constexpr Sat_result unsat = ::z3::unsat;
        static constexpr Sat_result unknown = ::z3::unknown;

        static constexpr bool use_vertex_bvs = true;
    };

    class Model;
}

namespace mapf_r::smt::solver {
    class Z3;
}

// make these as visible as possible
// .. but it seems that it does not work with 'mapf_r' only
namespace mapf_r::smt {
    inline String to_string(const solver::z3::Conf::Sat_result&);
    // operator << is already implemented in Z3
}

namespace mapf_r::smt::solver::z3 {
    class Context : public Inherit<::z3::context> {
    public:
        using Floats_map = Map<Float, ::z3::expr>;

        using Inherit::Inherit;
        Context()                                                                         = default;
        ~Context()                                                                        = default;
        Context(const Context&)                                                            = delete;
        Context& operator =(const Context&)                                                = delete;
        Context(Context&&);
        Context& operator =(Context&&);

        const auto& cfloats_map() const noexcept                             { return _floats_map; }

        Optional<::z3::expr> try_add_real(::z3::expr);
    protected:
        void move_to(Context&)&&;
        void lmove_to(Context&)&&;

        auto& floats_map() noexcept                                          { return _floats_map; }
    private:
        mutable Floats_map _floats_map{};
    };

    class Model : public Inherit<::z3::model> {
    public:
        using Inherit::Inherit;

        inline Context& ctx() const;

        ::z3::expr eval(const ::z3::expr&, bool model_completion = false) const;
    };
}

namespace mapf_r::smt::solver {
    class Z3 : public Inherit<Solver<z3::Conf>, Z3> {
    public:
        using Inherit::Inherit;
        virtual ~Z3()                                                                     = default;
        Z3(Z3&&)                                                                          = default;
        Z3& operator =(Z3&&)                                                              = default;

        using Inherit::parse;
        void parse(String) override;

        void parse_file(const Path&) override;
    protected:
        inline Expr make_bool_impl(const String&) override;
        inline Expr make_real_impl(const String&) override;
        inline Expr make_bv_impl(const String&, size_t) override;
        inline Expr make_bool_impl(bool) override;
        inline Expr make_real_impl(Rational) override;
        inline Expr make_bv_impl(unsigned, size_t) override;

        inline Expr neg(const Expr&) override;
        inline Expr conj(const Expr&, const Expr&) override;
        inline Expr disj(const Expr&, const Expr&) override;
        inline Expr implies(const Expr&, const Expr&) override;
        inline Expr plus(const Expr&, const Expr&) override;
        inline Expr minus(const Expr&, const Expr&) override;
        inline Expr times(const Expr&, const Expr&) override;
        inline Expr eq(const Expr&, const Expr&) override;
        inline Expr leq(const Expr&, const Expr&) override;
        inline Expr min(const Expr&, const Expr&) override;
        inline Expr max(const Expr&, const Expr&) override;
        inline Expr make_disj(Expr_vector) override;

        inline void get_model_impl() override;
        inline Expr get_model_value_impl(const Expr&) const override;
        inline bool to_bool(const Expr&) const override;
        inline Float to_float(const Expr&) const override;
        Rational to_rational(const Expr&) const override;
        unsigned to_int(const Expr&) const override;

        inline Expr get_min_objective_time_impl() override;

        inline void assert_expr_impl(Expr) override;
        inline void assert_expr_vector(Expr_vector&) override;

        inline void clear_expr_vector(Expr_vector&) override;

        inline void push_impl() override;
        inline void pop_impl() override;

        inline Sat_result check_sat_impl() override;

        void assert_minimize_impl() override;

        void solve_init() override;

        Orig_solver::handle minimize_handle{0};

        z3::Model model{context()};
    };
}

#include "mapf_r/smt/solver/z3/tp.hpp"

#include "mapf_r/smt/solver/z3.inl"
