#pragma once

#include <tomaqa/util/run.hpp>
#include "mapf_r/smt/solver.hpp"

namespace mapf_r::smt::solver {
    template <Class S>
    class Base::Run : public virtual Inherit<util::Run> {
    public:
        using Solver = S;

        using Inherit::Inherit;
        virtual ~Run()                                                                    = default;

        void init() override;
        void do_stuff() override;
        void finish() override;

        String usage() const override;
    protected:
        virtual const Solver& csolver() const noexcept                           { return _solver; }
        virtual Solver& solver() noexcept                                        { return _solver; }

        const auto& cgraph() const noexcept                                  { return *_graph_ptr; }
        auto& graph() noexcept                                               { return *_graph_ptr; }
        const auto& clayout() const noexcept                                { return *_layout_ptr; }
        auto& layout() noexcept                                             { return *_layout_ptr; }

        String lusage() const;

        void linit();
        void lfinish();

        String getopt_str() const noexcept override;
        String lgetopt_str() const noexcept;
        bool process_opt(char) override;
        bool lprocess_opt(char);

        virtual void set_plan(const String& path_str);
        virtual void set_smt2(const String& path_str);

        virtual void set_ofstream(Path&, ofstream&, const String& path_str, const String& msg);

        void process_additional_args() override;

        virtual void print_result();
        virtual void print_profiling() const;

        void signal_handler(int signal) override;

        Path _plan_path{};
        ofstream _plan_ofs{};

        Path _smt2_path{};
        ofstream _smt2_ofs{};

        bool _printed_result{};
        bool _printed_profiling{};
    private:
        Solver _solver{};

        Unique_ptr<Graph> _graph_ptr{};
        Unique_ptr<agent::Layout> _layout_ptr{};
    };
}

#include "mapf_r/smt/solver/run.inl"
