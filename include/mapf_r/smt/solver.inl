#pragma once

namespace mapf_r::smt {
    template <solver::conf::Concept SolverConf, typename... Args>
    template <solver::Has_context T>
    Solver<SolverConf, Args...>::Solver(Context&& ctx, Args... args)
        : _context(move(ctx)), _solver(_context, FORWARD(args)...)
    { }

    template <solver::conf::Concept SolverConf, typename... Args>
    template <solver::Has_context T>
    Solver<SolverConf, Args...>::Solver(Args... args)
        : Solver(Context(), FORWARD(args)...)
    { }

    template <solver::conf::Concept SolverConf, typename... Args>
    bool Solver<SolverConf, Args...>::optimizing() const noexcept
    {
        if constexpr (Solver_conf::optimize) return _optimizing;
        else return false;
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::set_optimizing() noexcept
    {
        assert(Solver_conf::optimize);
        _optimizing = true;
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    bool Solver<SolverConf, Args...>::optimizing_makespan() const noexcept
    {
        if (auto flag = this->coptions().optimize_makespan; flag.valid()) {
            assert(this->coptions().optimize_soc != flag);
            return flag;
        }
        else return Solver_conf::optimize_makespan;
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    bool Solver<SolverConf, Args...>::optimizing_soc() const noexcept
    {
        if (auto flag = this->coptions().optimize_soc; flag.valid()) {
            assert(this->coptions().optimize_makespan != flag);
            return flag;
        }
        else return Solver_conf::optimize_soc;
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    float Solver<SolverConf, Args...>::suboptimal_coef() const noexcept
    {
        if (auto coef = this->coptions().suboptimal_coef) return coef;
        else return Solver_conf::suboptimal_coef;
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr Solver<SolverConf, Args...>::make_expr()
    {
        if constexpr (!has_context_v) return {};
        else return {context()};
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr_vector
    Solver<SolverConf, Args...>::make_expr_vector()
    {
        if constexpr (!has_context_v) return {};
        else return {context()};
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_int(const String& id)
    {
        return make_real(id);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_bv(const String& id, size_t bits)
    {
        return make_bv_directly(id, bits);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_bool(bool val)
    {
        return make_bool_impl(val);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_real(Rational val)
    {
        return make_real_impl(move(val));
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    template <Side sideV, Precision_c P>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_real(Float val)
    {
        return make_real(to_rational<sideV, P>(val));
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_real(integral auto val)
    {
        return make_real(Float(val));
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_int(unsigned val)
    {
        return make_real(val);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_bv(unsigned val, size_t bits)
    {
        return make_bv_directly(val, bits);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_int_directly(unsigned val)
    {
        return make_int_impl(val);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_bv_directly(unsigned val, size_t bits)
    {
        return make_bv_impl(val, bits);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_move_time_real_of(const Agent& ag, const graph::Vertex& from, const graph::Vertex& to)
    {
        const Rational rat = This::move_time_rational_of(ag, from, to);
        return make_real(rat);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::implies(const Expr& lhs, const Expr& rhs)
    {
        return disj(neg(lhs), rhs);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::iff(const Expr& lhs, const Expr& rhs)
    {
        return conj(implies(lhs, rhs), implies(rhs, lhs));
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::geq(const Expr& lhs, const Expr& rhs)
    {
        return leq(rhs, lhs);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::lt(const Expr& lhs, const Expr& rhs)
    {
        return neg(geq(lhs, rhs));
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::gt(const Expr& lhs, const Expr& rhs)
    {
        return lt(rhs, lhs);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    bool Solver<SolverConf, Args...>::get_model_bool(const Expr& e) const
    {
        return to_bool(get_model_value(e));
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    Float Solver<SolverConf, Args...>::get_model_float(const Expr& e) const
    {
        return to_float(get_model_value(e));
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    Rational Solver<SolverConf, Args...>::get_model_rational(const Expr& e) const
    {
        return to_rational(get_model_value(e));
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    unsigned Solver<SolverConf, Args...>::get_model_int(const Expr& e) const
    {
        return to_int(get_model_value(e));
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_vertex_bool_at(const Agent& ag, const graph::Vertex& v, Idx k)
    {
        assert(Solver_conf::use_vertex_bools);
        return make_bool(make_vertex_bool_name_at(ag, v, k));
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    size_t Solver<SolverConf, Args...>::vertex_expr_bit_width() const
    {
        assert(Solver_conf::use_vertex_bvs);
        return this->cgraph_properties().vertices_bit_width;
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_vertex_expr_at(const Agent& ag, Idx k)
    {
        assert(!Solver_conf::use_vertex_bools);
        return make_vertex_expr_at_impl(make_vertex_expr_name_at(ag, k));
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_vertex_expr_at_impl(const String& id)
    {
        if constexpr (Solver_conf::use_vertex_bvs) {
            return make_bv(id, vertex_expr_bit_width());
        }
        else {
            return make_int(id);
        }
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_vertex_id_expr(const graph::Vertex::Id& vid)
    {
        assert(!Solver_conf::use_vertex_bools);
        return make_vertex_id_expr_impl(vid);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_vertex_id_expr_impl(const graph::Vertex::Id& vid)
    {
        if constexpr (Solver_conf::use_vertex_bvs) {
            // better than using reals or integers, especially in large graphs
            return make_bv(vid, vertex_expr_bit_width());
        }
        else {
            return make_int(vid);
        }
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::vertex_expr_at(const agent::Id& aid, Idx k, const graph::Vertex::Id& vid)
    {
        auto& vexpr_s = vertex_exprs[aid][k];
        if constexpr (Solver_conf::use_vertex_bools) return vexpr_s[vid];
        else return eq(vexpr_s, make_vertex_id_expr(vid));
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::from_vertex_expr(const agent::Id& aid, Idx k)
    {
        if constexpr (Solver_conf::use_vertex_bools) return *find_active_vertex_expr(aid, k);
        else return vertex_exprs[aid][k];
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::from_vertex_expr(const agent::Id& aid, Idx k, const graph::Vertex::Id& vid)
    {
        return vertex_expr_at(aid, k, vid);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    template <bool waitsV>
    auto Solver<SolverConf, Args...>::to_vertex_expr(const agent::Id& aid, Idx k, Idx max_k)
    {
        if constexpr (waitsV) return true;
        else {
            if (k == max_k) return make_expr();
            if constexpr (Solver_conf::use_vertex_bools) return *find_active_vertex_expr(aid, k+1);
            else return vertex_exprs[aid][k+1];
        }
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    template <bool waitsV>
    auto Solver<SolverConf, Args...>::to_vertex_expr(const agent::Id& aid, Idx k, [[maybe_unused]] Idx max_k,
                                                     const graph::Vertex::Id& vid)
    {
        if constexpr (waitsV) return true;
        else {
            assert(k < max_k);
            return vertex_expr_at(aid, k+1, vid);
        }
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::init_wait_abs_time_real(const agent::Id& aid, Idx k)
    {
        return abs_time_reals[aid][k];
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::final_wait_abs_time_real(const agent::Id& aid, Idx k)
    {
        return plus(init_wait_abs_time_real(aid, k), wait_time_reals[aid][k]);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::init_move_abs_time_real(const agent::Id& aid, Idx k, Idx max_k)
    {
        if (k == max_k) return make_expr();
        return final_wait_abs_time_real(aid, k);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::final_move_abs_time_real(const agent::Id& aid, Idx k, Idx max_k)
    {
        if (k == max_k) return make_expr();
        return abs_time_reals[aid][k+1];
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    template <bool waitsV>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::init_abs_time_real(const agent::Id& aid, Idx k, Idx max_k)
    {
        if constexpr (waitsV) return init_wait_abs_time_real(aid, k);
        else return init_move_abs_time_real(aid, k, max_k);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    template <bool waitsV>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::final_abs_time_real(const agent::Id& aid, Idx k, Idx max_k)
    {
        if constexpr (waitsV) return final_wait_abs_time_real(aid, k);
        else return final_move_abs_time_real(aid, k, max_k);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    ostream& Solver<SolverConf, Args...>::print_expr(ostream& os, const Expr& e) const
    {
        os << expr_to_string(e);
        return os;
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    String Solver<SolverConf, Args...>::expr_to_string(const Expr& e) const
    {
        return expr_to_smt2(e);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    String Solver<SolverConf, Args...>::to_string(const Sat_result& res)
    {
        return to_c_str(res);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    ostream& Solver<SolverConf, Args...>::print(ostream& os, const Sat_result& res)
    {
        os << to_c_str(res);
        return os;
    }
}
