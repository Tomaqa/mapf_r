#pragma once

#include "mapf_r/smt/solver/base.hpp"

namespace mapf_r::smt::solver::conf {
    template <typename C> concept Concept = requires {
        typename C::Solver;
        typename C::Sat_result;
    };

    template <typename C> concept Has_context = Concept<C> && requires {
        requires !is_same_v<typename C::Context, Dummy>;
    };

    struct Default {
        using Context = Dummy;

        //+ try to improve encoding, hot-1 encoding is seemly bad
        static constexpr bool use_vertex_bools = false;
        static constexpr bool use_vertex_bvs = false;

        static constexpr bool optimize = true;

        static constexpr bool optimize_makespan = false;
        static constexpr bool optimize_soc = true;

        static constexpr bool manual_optimize = true;
        static constexpr float suboptimal_coef = 2;

        static constexpr bool always_optimize = false;

        static constexpr bool learn_only_first_conflict = false;

        static constexpr bool skip_not_shortest_steps = true;

        static constexpr bool goals_after_push = true;

        static constexpr bool avoid_waiting = true;
        // does not seem beneficial when optimizing, even when used only in min. stage
        static constexpr bool always_avoid_waiting = false;

        // assume by default that the solver does not support it (or is buggy with it)
        static constexpr bool solve_with_assumptions = false;
    };

    template <Concept ConfT>
    const char* to_c_str(const typename ConfT::Sat_result&);
    template <Concept ConfT>
    ostream& print(ostream&, const typename ConfT::Sat_result&);
}

namespace mapf_r::smt::solver {
    template <typename S> concept Has_context = conf::Has_context<typename S::Solver_conf>;
}

namespace mapf_r::smt {
    template <solver::conf::Concept SolverConf, typename... Args> class Solver;
}

namespace mapf_r::smt {
    template <solver::conf::Concept SolverConf, typename... Args>
    class Solver : public Inherit<solver::Base, Solver<SolverConf, Args...>> {
    public:
        using Inherit = tomaqa::Inherit<solver::Base, Solver<SolverConf, Args...>>;
        using typename Inherit::This;

        using Solver_conf = SolverConf;

        using Orig_solver = typename Solver_conf::Solver;

        using Context = typename Solver_conf::Context;

        using Sat_result = typename Solver_conf::Sat_result;

        using typename Inherit::Precision;

        template <Class T = This> requires (!solver::Has_context<T>)
        Solver(Args... args)                                         : _solver(FORWARD(args)...) { }
        virtual ~Solver()                                                                 = default;
        Solver(const Solver&)                                                              = delete;
        Solver& operator =(const Solver&)                                                  = delete;
        Solver(Solver&&)                                                                  = default;
        Solver& operator =(Solver&&)                                                      = default;
        template <Class T = This> requires (!solver::Has_context<T>)
        Solver(Orig_solver&& s)                                               : _solver(move(s)) { }

        inline bool optimizing() const noexcept;

        inline bool optimizing_makespan() const noexcept;
        inline bool optimizing_soc() const noexcept;

        inline float suboptimal_coef() const noexcept;

        virtual void print_final_objective_time(ostream&);

        using Inherit::to_string;
        static const char* to_c_str(const Sat_result&);
        static inline String to_string(const Sat_result&);
        static inline ostream& print(ostream&, const Sat_result&);
    protected:
        using Expr = typename Solver_conf::Expr;
        using Expr_vector = typename Solver_conf::Expr_vector;

        using Vertex_exprs =
            conditional_t<Solver_conf::use_vertex_bools, Vector<Vector<Vector<Expr>>>, Vector<Vector<Expr>>>;

        using Time_reals = Vector<Vector<Expr>>;

        //+ can be just a view
        //+ supports only true polarity
        using Preferred_variables_same_priority = Vector<Expr>;
        using Preferred_variables = Vector<Preferred_variables_same_priority>;
        static constexpr int init_wait_priority = 0;
        static constexpr int position_priority = 1;
        static constexpr int transition_priority = util::max(init_wait_priority, position_priority)+1;

        using Active_vertex_id_cache = Vector<Vector<Optional<Vertex::Id>>>;

        using Wait_preferred_variables =
            conditional_t<Solver_conf::always_avoid_waiting, Vector<Vector<Expr>>, Vector<Expr>>;
        using Position_preferred_variables = Vector<Vector<Expr>>;
        using Transition_preferred_variables = Vector<Vector<Vector<Expr>>>;

        template <solver::Has_context = This>
        Solver(Context&&, Args...);
        template <solver::Has_context = This>
        Solver(Args...);

        const auto& ccontext() const noexcept                                   { return _context; }
        auto& context() noexcept                                                { return _context; }

        const auto& csolver() const noexcept                                     { return _solver; }
        auto& solver() noexcept                                                  { return _solver; }

        inline void set_optimizing() noexcept;

        Expr& objective_time() noexcept                                  { return _objective_time; }

        Expr& max_k_assumption() noexcept                              { return _max_k_assumption; }

        Expr_vector& conflict_bools() noexcept                           { return _conflict_bools; }

        using Inherit::to_rational;

        inline Expr make_expr();
        inline Expr_vector make_expr_vector();
        Expr make_bool(const String&);
        Expr make_real(const String&);
        virtual inline Expr make_int(const String&);
        virtual inline Expr make_bv(const String&, size_t);
        Expr make_int_directly(const String&);
        Expr make_bv_directly(const String&, size_t);
        inline Expr make_bool(bool);
        inline Expr make_real(Rational);
        template <Side = Inherit::default_side, Precision_c = Precision>
        inline Expr make_real(Float);
        inline Expr make_real(integral auto);
        //+ use some Int and check bounds
        virtual inline Expr make_int(unsigned);
        //+ use some Int and check bounds
        virtual inline Expr make_bv(unsigned, size_t);
        inline Expr make_int_directly(unsigned);
        inline Expr make_bv_directly(unsigned, size_t);

        inline Expr make_move_time_real_of(const Agent&, const graph::Vertex& from, const graph::Vertex& to);

        virtual inline Expr implies(const Expr&, const Expr&);
        virtual inline Expr iff(const Expr&, const Expr&);
        virtual inline Expr geq(const Expr&, const Expr&);
        // beware, strict inequalities may cause troubles with optimization
        virtual inline Expr lt(const Expr&, const Expr&);
        virtual inline Expr gt(const Expr&, const Expr&);
        virtual Expr make_disj(Expr_vector);

        void get_model() override;
        virtual Expr get_model_value(const Expr&) const;
        inline bool get_model_bool(const Expr&) const;
        inline Float get_model_float(const Expr&) const;
        inline Rational get_model_rational(const Expr&) const;
        inline unsigned get_model_int(const Expr&) const;
        virtual unsigned to_int(const Expr&) const;

        virtual Expr get_objective_time();
        virtual Expr get_min_objective_time();
        virtual Expr get_min_objective_time_impl()                              { NOT_IMPLEMENTED; }

        virtual inline Expr make_vertex_bool_at(const Agent&, const graph::Vertex&, Idx k);
        virtual inline size_t vertex_expr_bit_width() const;
        virtual inline Expr make_vertex_expr_at(const Agent&, Idx k);
        virtual inline Expr make_vertex_expr_at_impl(const String&);
        virtual inline Expr make_vertex_id_expr(const graph::Vertex::Id&);
        virtual inline Expr make_vertex_id_expr_impl(const graph::Vertex::Id&);
        inline Expr vertex_expr_at(const agent::Id&, Idx k, const graph::Vertex::Id&);

        Vector<Expr>::const_iterator find_active_vertex_expr(const agent::Id& aid, Idx k) const;
        Vertex::Id active_vertex_id(const agent::Id&, Idx k) const;

        inline Expr from_vertex_expr(const agent::Id&, Idx k);
        inline Expr from_vertex_expr(const agent::Id&, Idx k, const graph::Vertex::Id&);
        template <bool waitsV>
        auto to_vertex_expr(const agent::Id&, Idx k, Idx max_k);
        template <bool waitsV>
        auto to_vertex_expr(const agent::Id&, Idx k, Idx max_k, const graph::Vertex::Id&);

        inline Expr init_wait_abs_time_real(const agent::Id&, Idx k);
        inline Expr final_wait_abs_time_real(const agent::Id&, Idx k);
        inline Expr init_move_abs_time_real(const agent::Id&, Idx k, Idx max_k);
        inline Expr final_move_abs_time_real(const agent::Id&, Idx k, Idx max_k);
        template <bool waitsV>
        Expr init_abs_time_real(const agent::Id&, Idx k, Idx max_k);
        template <bool waitsV>
        Expr final_abs_time_real(const agent::Id&, Idx k, Idx max_k);

        virtual void assert_expr(Expr);
        virtual void assert_expr_vector(Expr_vector&);

        virtual void assert_expr_pre(Expr&);

        virtual void push_to_expr_vector(Expr_vector&, Expr);
        virtual void clear_expr_vector(Expr_vector&);

        virtual Sat_result check_sat();

        virtual void check_sat_post(const Sat_result&);

        //+ supports only true polarity
        virtual void add_solver_preferred_variable(Expr)                                         { }
        virtual void clear_solver_preferred_variables()                                          { }

        virtual void before_push();
        virtual void after_push();
        virtual void before_pop();
        virtual void after_pop();

        void declare_consts(Idx k = 0);
        virtual void declare_consts_impl(Idx k);
        virtual void declare_wait_preferred_variables(const Agent&, Idx k);
        virtual void declare_position_preferred_variables(const Agent&, Idx k);
        virtual void declare_transition_preferred_variables(const Agent&, Idx k);

        void assert_init();
        virtual void assert_init_impl();

        void assert_positions(Idx k);
        virtual void assert_positions_impl(Idx k);
        virtual void assert_position_preferred_variables(const agent::Id&, Idx k);
        void assert_transitions(Idx k);
        virtual void assert_transitions_impl(Idx k);
        virtual void assert_transition_preferred_variables(const agent::Id&, Idx k,
            const Vertex::Id& from_vid, const Expr& from_vexpr
        );

        void assert_goal(Idx max_k);
        virtual void assert_goal_impl(Idx max_k);

        virtual void prefer_variables(Idx max_k);

        bool assert_any_conflict(Idx max_k);
        virtual void assert_conflict(Idx max_k, Expr conflict_clause);
        virtual void assert_conflicts_after_pop();
        bool check_all_sat(Idx max_k);

        void assert_minimize();
        virtual void assert_minimize_impl()                                     { NOT_IMPLEMENTED; }
        virtual void assert_objective_lower();

        void minimize(ostream&, Idx max_k);
        virtual void minimize_pre(ostream&, Idx max_k);
        virtual void minimize_impl(ostream&, Idx max_k);
        virtual void minimize_post(ostream&, Idx max_k);
        virtual void manual_minimize(ostream&, Idx max_k);
        virtual void manual_minimize_print_final_coef(ostream&);

        void solve_init() override;
        void solve_main(ostream& = std::cout) override;

        virtual void main_before_push(Idx max_k);
        virtual void main_push();
        virtual void main_after_push(Idx max_k);
        virtual void main_before_pop();
        virtual void main_pop();
        virtual void main_after_pop();
        virtual void max_k_assumption_before_push(Idx max_k);
        virtual void max_k_assumption_after_push(Idx max_k);
        virtual void max_k_assumption_before_pop();
        virtual void max_k_assumption_after_pop();

        virtual Float get_final_objective_time();
        virtual void print_final_objective_time(ostream&, Float obj_time) const;

        agent::plan::Global make_plan_impl(ostream&) override;
        agent::plan::Global make_plan_for(Idx max_k) const;

        virtual inline ostream& print_expr(ostream&, const Expr&) const;
        virtual inline String expr_to_string(const Expr&) const;
        virtual String expr_to_smt2(const Expr&) const                          { NOT_IMPLEMENTED; }

        void print_smt2_head_set_logic() const override;

        void print_smt2_assert(const Expr&) const;
        void print_smt2_check_sat_with_status(const Sat_result&) const;

        Vertex_exprs vertex_exprs{};

        Time_reals abs_time_reals{};
        Time_reals rel_time_reals{};
        Time_reals wait_time_reals{};
        Time_reals move_time_reals{};

        Preferred_variables preferred_variables{};

        Wait_preferred_variables wait_preferred_variables{};
        Position_preferred_variables position_preferred_variables{};
        Transition_preferred_variables transition_preferred_variables{};

        mutable Active_vertex_id_cache active_vertex_id_cache{};
    private:
        static constexpr bool has_context_v = solver::conf::Has_context<Solver_conf>;

        static_assert(!has_context_v || constructible_from<Orig_solver, Context&>);
        static_assert(has_context_v == !is_same_v<Context, Dummy>);

        static_assert(!Solver_conf::use_vertex_bools || !Solver_conf::use_vertex_bvs);

        virtual Expr make_bool_impl(const String&)                                              = 0;
        virtual Expr make_real_impl(const String&)                                              = 0;
        virtual Expr make_int_impl(const String&)                               { NOT_IMPLEMENTED; }
        virtual Expr make_bv_impl(const String&, size_t)                        { NOT_IMPLEMENTED; }
        virtual Expr make_bool_impl(bool)                                                       = 0;
        virtual Expr make_real_impl(Rational)                                                   = 0;
        virtual Expr make_int_impl(unsigned)                                    { NOT_IMPLEMENTED; }
        virtual Expr make_bv_impl(unsigned, size_t)                             { NOT_IMPLEMENTED; }

        virtual Expr neg(const Expr&)                                                           = 0;
        virtual Expr conj(const Expr&, const Expr&)                                             = 0;
        virtual Expr disj(const Expr&, const Expr&)                                             = 0;
        virtual Expr plus(const Expr&, const Expr&)                                             = 0;
        virtual Expr minus(const Expr&, const Expr&)                                            = 0;
        virtual Expr times(const Expr&, const Expr&)                                            = 0;
        virtual Expr eq(const Expr&, const Expr&)                                               = 0;
        virtual Expr leq(const Expr&, const Expr&)                                              = 0;
        virtual Expr min(const Expr&, const Expr&)                                              = 0;
        virtual Expr max(const Expr&, const Expr&)                                              = 0;

        virtual Expr get_model_value_impl(const Expr&) const                                    = 0;
        virtual bool to_bool(const Expr&) const                                                 = 0;
        virtual Float to_float(const Expr&) const                                               = 0;
        virtual Rational to_rational(const Expr&) const                                         = 0;

        virtual void assert_expr_impl(Expr)                                                     = 0;

        virtual Sat_result check_sat_impl()                                                     = 0;

        template <bool a1Waits, bool a2Waits, bool isFromModel = true>
        bool maybe_assert_conflict(Idx max_k, Idx k1, Idx k2,
                                   const Agent&, const Agent&, const Interval&, const Interval&,
                                   const graph::Vertex& from1, const graph::Vertex* to1_l,
                                   const graph::Vertex& from2, const graph::Vertex* to2_l);
        template <bool a1Waits, bool a2Waits>
        Expr make_conflict_clause(bool a1_waits_inf, bool a2_waits_inf,
                                  const Expr& from_vbool1, auto to_vbool1,
                                  const Expr& from_vbool2, auto to_vbool2,
                                  const Expr& a1_tl_e, const Expr& a1_tu_e,
                                  const Expr& a2_tl_e, const Expr& a2_tu_e,
                                  const Interval& a1_unsafe, const Interval& a2_unsafe,
                                  const Interval& a1_t, const Interval& a2_t);

        Context _context{};
        Orig_solver _solver;

        bool _optimizing{};

        Expr _objective_time{make_expr()};

        Expr _max_k_assumption{make_expr()};

        Expr_vector _conflict_bools{make_expr_vector()};

        using Conflicts_map_key = tuple<agent::Id, Vertex::Id, Vertex::Id, Idx, Interval::Range>;
        using Conflicts_map_value = Interval::Range;
        using Conflicts_map = Map<pair<Conflicts_map_key, Conflicts_map_key>,
                                  pair<Conflicts_map_value, Conflicts_map_value>,
                                  Apx_less<Precision>>;

        Conflicts_map _conflicts_map{};
    };
}

#include "mapf_r/smt/solver.inl"
