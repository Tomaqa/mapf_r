#pragma once

#include "mapf_r/agent.hpp"
#include "mapf_r/graph.hpp"

namespace mapf_r::smt {
    using math::Float;
    using Rational = Rational_for<float>;

    using graph::Vertex;

    constexpr const char* sat_str = "sat";
    constexpr const char* unsat_str = "unsat";
    constexpr const char* unknown_str = "unknown";

    constexpr const char* objective_time_var_name = "objtime";

    constexpr const char* abs_time_var_name = "atime";
    constexpr const char* rel_time_var_name = "rtime";
    constexpr const char* wait_time_var_name = "wtime";
    constexpr const char* move_time_var_name = "mtime";

    const char* to_sat_c_str(const Flag&);

    String agent_expr_name(const Agent&);

    String make_var_name(const Agent&, String name);
    inline String make_var_name_at(const Agent&, String name, Idx);
    String make_var_name_at(String name, Idx);

    inline String vertex_expr_name();
    String vertex_bool_name(const Vertex&);
    inline String make_vertex_expr_name(const Agent&);
    inline String make_vertex_expr_name_at(const Agent&, Idx);
    inline String make_vertex_bool_name(const Agent&, const Vertex&);
    inline String make_vertex_bool_name_at(const Agent&, const Vertex&, Idx);

    inline String make_abs_time_var_name(const Agent&);
    inline String make_rel_time_var_name(const Agent&);
    inline String make_wait_time_var_name(const Agent&);
    inline String make_move_time_var_name(const Agent&);
    inline String make_abs_time_var_name_at(const Agent&, Idx);
    inline String make_rel_time_var_name_at(const Agent&, Idx);
    inline String make_wait_time_var_name_at(const Agent&, Idx);
    inline String make_move_time_var_name_at(const Agent&, Idx);

    template <Side = Side::center, Precision_c = precision::Medium>
    inline Rational to_rational(Float);
}

#include "mapf_r/smt.inl"
