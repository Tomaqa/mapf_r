#pragma once

namespace mapf_r::graph {
    size_t Vertex::deg() const noexcept
    {
        return size(cneighbor_ids());
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace mapf_r {
    const auto& Graph::cvertex(const Vertex::Id& vid) const
    {
        return cvertices()[vid];
    }

    auto& Graph::vertex(const Vertex::Id& vid)
    {
        return vertices()[vid];
    }
}
