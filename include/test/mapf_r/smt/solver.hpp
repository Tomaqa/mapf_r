#include <tomaqa.hpp>
#include <tomaqa/test.hpp>

#include "mapf_r/smt/solver.hpp"

#include <fstream>

namespace mapf_r::test {
    using namespace tomaqa::test;
    using namespace mapf_r::smt::solver;

    ////////////////////////////////////////////////////////////////

    struct Solver_input {
        Path name;
        String graph_type;
        String graph_suff;
        String layout_type;
        String layout_suff;

        String filename(const String& type, const String& suff) const
                                                                { return name + suff + "." + type; }
        Path path(const Path& name_, const String& type, const String& suff) const
        {
            Path p("data");
            p /= name_;
            if (!contains({"g", "l", "p"}, type)) p /= type;
            const auto fn = filename(type, suff);
            if (exists(p/fn)) return p/fn;
            return p/name/fn;
        }
        Path graph_path() const                    { return path("graph", graph_type, graph_suff); }
        Path layout_path() const                { return path("layout", layout_type, layout_suff); }
        //+ create all needed directories
        Path plan_path() const   { return path(Path("plan")/"gen", "p", graph_suff + layout_suff); }

        String to_string() const
        {
            return name + " | "s + graph_suff + " " + layout_suff;
        }
    };

    static constexpr Idx makespan_idx = 0;
    static constexpr Idx soc_idx = 1;

    static constexpr Idx max_idx = soc_idx;

    using Solver_single_output = Optional<Float>;
    using Solver_output = array<Solver_single_output, max_idx+1>;

    template <Class S> struct Solver_case;

    template <Class S>
    struct Friend_solver : Inherit<S> {
        using Inherit = tomaqa::Inherit<S>;

        using Inherit::Inherit;

        friend Solver_case<S>;
    };

    template <Class S>
    struct Solver_case : Inherit<Case<Solver_input, Solver_output>> {
        using Inherit = tomaqa::Inherit<Case<Solver_input, Solver_output>>;

        using Solver = Friend_solver<S>;
        using Solver_ptr = Unique_ptr<Solver>;

        using Single_output = Solver_single_output;

        using Inherit::Inherit;

        static inline Solver_ptr solver_ptr{};

        const auto& csolver() const noexcept                                 { return *solver_ptr; }
        auto& solver() noexcept                                              { return *solver_ptr; }

        bool condition(const Output& expected_, const Output& result_) const override
        {
            for (Idx idx = 0; idx <= max_idx; ++idx) {
                auto& sexp = expected_[idx];
                auto& sres = result_[idx];
                if (!single_condition(sexp, sres)) return false;
            }

            return true;
        }

        virtual bool single_condition(const Single_output& expected_, const Single_output& result_) const
        {
            if constexpr (!Solver::Solver_conf::optimize) {
                if (should_throw()) return false;
                return result_.valid() == expected_.valid();
            }
            else if constexpr (Solver::Solver_conf::manual_optimize) {
                if (result_.valid() != expected_.valid()) return false;
                if (!expected_) return true;
                if (*expected_ < 0) return true;
                //! `apx_less` does not work with `Optional`
                if (apx_less<typename Solver::Precision>(*result_, *expected_)) return false;
                return apx_less_equal<typename Solver::Precision::Lower>(*result_/Solver::Solver_conf::suboptimal_coef, *expected_);
            }
            else return apx_equal<typename Solver::Precision::Lower>(result_, expected_);
        }

        Output result() override
        {
            Output out;
            for (Idx idx = 0; idx <= max_idx; ++idx) {
                out[idx] = single_result(idx);
            }

            return out;
        }

        virtual Single_output single_result(Idx idx)
        {
            auto& in = This::cinput();

            if (!solver_ptr) solver_ptr = make_unique<Solver>();
            auto& s = solver();
            s = {};
            Solver s2(move(s));
            s = move(s2);

            auto& opts = s.options();
            switch (idx) {
            case makespan_idx:
                opts.optimize_makespan = true;
                opts.optimize_soc = false;
                break;
            case soc_idx:
                opts.optimize_makespan = false;
                opts.optimize_soc = true;
                break;
            default: UNREACHABLE;
            }

            Graph g(ifstream(in.graph_path()));
            agent::Layout l(ifstream(in.layout_path()));
            s.set_graph(g);
            s.set_layout(l);

            agent::plan::Global plan;
            s.solve();
            const bool sat = s.is_sat();
            if (sat) {
                plan = s.make_plan();
                ofstream(in.plan_path()) << plan.compose();
            }

            cout << endl << in << endl << s.cprofile() << endl;

            if (!sat) return {};

            switch (idx) {
            case makespan_idx: return plan.makespan();
            case soc_idx: return plan.sum_times();
            default: UNREACHABLE;
            }
        }

        void finish() override
        {
            //+ only applies to `max_idx`
            using tomaqa::to_string;

            Inherit::finish();

            auto& s = solver();

            if (!s.is_sat()) return;

            if constexpr (!Solver::Solver_conf::manual_optimize) return;

            const auto min_time = s.get_final_objective_time();
            if (apx_equal<typename Solver::Precision>(min_time, 0)) return;

            const int k = s.cfinal_k();

            s.before_push();
            s.push();
            s.after_push();
            constexpr auto coef = 1./Solver::Solver_conf::suboptimal_coef;
            auto coef_e = s.make_real(coef);
            s.assert_expr(s.leq(s.objective_time(), s.times(s.get_min_objective_time(), coef_e)));
            expect(!s.check_all_sat(k),
                   "Violation of the guaranteed optimization coefficient "s + to_string(coef)
                   + " and solution " + to_string(min_time)
                   + " => optimum must be > " + to_string(min_time*coef)
                   + " but found: " + to_string(s.to_float(s.get_min_objective_time())));
            s.before_pop();
            s.pop();
            s.after_pop();
        }
    };
}

#include "test/mapf_r/smt/solver.inl"
