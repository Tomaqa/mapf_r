#include "test/mapf_r/smt/solver.hpp"
#include "mapf_r/smt/solver/mathsat.hpp"

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace mapf_r;
    using namespace mapf_r::test;
    using namespace std;
    // using tomaqa::ignore;

    ////////////////////////////////////////////////////////////////

    const String msat_msg = "MathSAT";
    Suite(msat_msg).test<Solver_case<Mathsat>>(solver_valid_data<Mathsat>());
    Suite(msat_msg, true).test<Solver_case<Mathsat>>(solver_invalid_data<Mathsat>());

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const tomaqa::Error& e) {
    std::cout << e << std::endl;
    throw;
}
