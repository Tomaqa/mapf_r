#include "test/mapf_r/smt/solver.hpp"
#include "mapf_r/smt/solver/opensmt.hpp"

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace mapf_r;
    using namespace mapf_r::test;
    using namespace std;
    using smt::solver::Opensmt;
    // using tomaqa::ignore;

    ////////////////////////////////////////////////////////////////

    const String opensmt_msg = "OpenSMT";
    Suite(opensmt_msg).test<Solver_case<Opensmt>>(solver_valid_data<Opensmt>());
    Suite(opensmt_msg, true).test<Solver_case<Opensmt>>(solver_invalid_data<Opensmt>());

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const tomaqa::Error& e) {
    std::cout << e << std::endl;
    throw;
}
