#include <tomaqa.hpp>
#include <tomaqa/test.hpp>

#include "mapf_r/agent.hpp"

namespace mapf_r::test {
    using namespace tomaqa::test;
    using namespace agent;
    using namespace agent::state;

    ////////////////////////////////////////////////////////////////

    struct State_input {
        Vec vec;
        Pos p;

        String to_string() const
        {
            using tomaqa::to_string;
            return "p="s + to_string(p) + " vec=" + to_string(vec);
        }
    };

    struct State_params {
        Float abs_v;
        Float delta{};

        String to_string() const
        {
            using tomaqa::to_string;
            String str = "V="s + to_string(abs_v);
            if (delta) str += " D=" + to_string(delta);
            return str;
        }
    };

    struct State_output {
        Float duration;
        Pos mid;

        String to_string() const
        {
            using tomaqa::to_string;
            return "T="s + to_string(duration)
                 + " F(T/2)=" + to_string(mid)
                 ;
        }
    };

    template <Class StateT>
    struct State_case : Inherit<Case<State_input, State_output, State_params>> {
        using Inherit = tomaqa::Inherit<Case<State_input, State_output, State_params>>;

        using State = StateT;

        using Inherit::Inherit;

        bool condition(const Output& expected_, const Output& result_) const override
        {
            return apx_equal(expected_.duration, result_.duration)
                && apx_equal(expected_.mid, result_.mid);
        }

        Output result() override
        {
            using tomaqa::to_string;

            auto& in = This::cinput();
            auto& p = in.p;
            auto& vec = in.vec;
            const auto q = p + vec;
            auto [abs_v, delta] = This::cparams();

            const auto pq = pair{p, q};
            State s(pq);
            assert(s.cpos() == p);
            assert(s.cinit_pos() == p);
            assert(s.cend_pos() == q);
            assert(apx_equal<precision::Huge>(s.cvec(), vec));
            [[maybe_unused]] State s2;
            if constexpr (!is_same_v<State, Parabolic>) {
                s.set(abs_v);
                s2.set_at(pq, abs_v);
            }
            else {
                assert(delta);
                s.set(abs_v, delta);
                s2.set_at(pq, abs_v, delta);
            }
            assert(s2.cpos() == p);
            assert(s2.cinit_pos() == p);
            assert(s2.cend_pos() == q);
            assert(apx_equal<precision::Huge>(s2.cvec(), vec));

            const Float duration = s.cduration();
            assert(s2.cduration() == duration);

            const auto p_ = s.pos_at(0);
            expect(p == p_, "F(0) != p: "s + to_string(p_) + " != " + to_string(p));
            assert(s2.pos_at(0) == p_);
            assert(s.dvec(0) == zero_vec);
            assert(s2.dvec(0) == zero_vec);
            assert(s.vec_at(0) == zero_vec);
            assert(s2.vec_at(0) == zero_vec);

            const auto q_ = s.pos_at(duration);
            expect(apx_equal<precision::Huge>(q, q_), "F(T) !~ p: T="s + to_string(duration) + ", " + to_string(q_) + " !~ " + to_string(q));
            assert(s2.pos_at(duration) == q_);
            assert(apx_equal<precision::Huge>(s.dvec(duration), vec));
            assert(apx_equal<precision::Huge>(s2.dvec(duration), vec));
            assert(apx_equal<precision::Huge>(s.vec_at(duration), vec));
            assert(apx_equal<precision::Huge>(s2.vec_at(duration), vec));

            const auto mid = s.pos_at(duration/2);
            assert(s2.pos_at(duration/2) == mid);
            assert(apx_equal<precision::Huge>(s.dvec(duration/2), mid - p));
            assert(apx_equal<precision::Huge>(s2.dvec(duration/2), mid - p));
            assert(apx_equal<precision::Huge>(s.vec_at(duration/2), mid - p));
            assert(apx_equal<precision::Huge>(s2.vec_at(duration/2), mid - p));

            return {.duration = duration, .mid = mid};
        }
    };

    ////////////////////////////////////////////////////////////////

    template <Class StateT>
    struct Trajectory_case : Inherit<Input_case<State_input, State_params>> {
        using Inherit = tomaqa::Inherit<Input_case<State_input, State_params>>;

        using State = StateT;

        using Inherit::Inherit;

        void do_stuff() override
        {
            using tomaqa::to_string;

            auto& in = This::cinput();
            auto& p = in.p;
            auto& vec = in.vec;
            const auto q = p + vec;
            auto [abs_v, delta] = This::cparams();

            const auto pq = pair{p, q};
            State s(pq);
            if constexpr (!is_same_v<State, Parabolic>) {
                s.set(abs_v);
            }
            else {
                assert(delta);
                s.set(abs_v, delta);
            }

            const State cs = s;
            assert(s.cpos() == cs.cpos());
            assert(s.cend_pos() == cs.cend_pos());
            assert(s.cvec() == cs.cvec());
            assert(s.cduration() == cs.cduration());

            const auto dur = cs.cduration();
            const auto d = cs.cdist();

            // y = ax^2 + bx
            const double alpha = angle(vec);
            const double a = -4*delta/(d*d);
            const double b = -a*d;
            const int n_steps = 100;
            const float step = d/n_steps;
            double dur_sum = 0;
            const auto step_t = cs.to_t(step);
            for (int i = 0; i <= n_steps; ++i) {
                const float x = i*step;
                const auto t = cs.to_t(x);
                if (i < n_steps) dur_sum += step_t;

                const Pos s_pos = s.cpos();
                const Pos cs_pos = cs.pos_at(t);
                expect(apx_equal<precision::Low>(s_pos, cs_pos),
                       "advance does not correspond to pos_at: at x="s + to_string(x) + ": "
                       + to_string(s_pos) + " != " + to_string(cs_pos));

                const float y = a*x*x + b*x;
                const Coord par_pos = p + rotate(Vec(x, y), alpha);
                expect(apx_equal<precision::Low>(s_pos, par_pos),
                       "advance does not correspond to parabolic trajectory: at x="s + to_string(x) + ": "
                       + to_string(s_pos) + " != " + to_string(par_pos));
                expect(apx_equal(cs_pos, par_pos),
                       "pos_at does not correspond to parabolic trajectory: at x="s + to_string(x) + ": "
                       + to_string(cs_pos) + " != " + to_string(par_pos));

                //! expect(apx_equal(dist(s.dvec(step_t))/step_t, abs_v));
                //! or at least in average case

                s.advance(step_t);
            }

            expect(apx_equal(dur_sum, dur),
                   "The actual elapsed time does not correspond to the computed duration: "s
                   + to_string(dur_sum) + " !~ " + to_string(dur));
        }
    };

    ////////////////////////////////////////////////////////////////
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace mapf_r;
    using namespace mapf_r::test;
    using namespace mapf_r::agent;
    using namespace mapf_r::agent::state;
    using namespace std;
    // using tomaqa::ignore;

    const Float _sqrt2_2 = sqrt2/2;
    const Float _sqrt2_4 = sqrt2/4;
    // const Float _2sqrt2 = 2*sqrt2;

    ////////////////////////////////////////////////////////////////

    const String state_msg = "agent state positions";
    Suite(state_msg).test<State_case<Parabolic>>({
        { { {1,0}, {0,0} },                              {2.323392, {0.5,1}},                                         {1, 1} },
        { { {1,0}, {1,0} },                              {2.323392, {1.5,1}},                                         {1, 1} },
        { { {1,0}, {0,1} },                              {2.323392, {0.5,2}},                                         {1, 1} },
        { { {1,0}, {1,1} },                              {2.323392, {1.5,2}},                                         {1, 1} },
        { { {1,0}, {_sqrt2_2,_sqrt2_2} },                {2.323392, {_sqrt2_2+0.5,1+_sqrt2_2}},                       {1, 1} },
        { { {1,0}, {sqrt2,sqrt2} },                      {2.323392, {sqrt2+0.5,1+sqrt2}},                             {1, 1} },
        { { {0,1}, {0,0} },                              {2.323392, {-1,0.5}},                                        {1, 1} },
        { { {0,1}, {1,0} },                              {2.323392, {0,0.5}},                                         {1, 1} },
        { { {0,1}, {0,1} },                              {2.323392, {-1,1.5}},                                        {1, 1} },
        { { {0,1}, {1,1} },                              {2.323392, {0,1.5}},                                         {1, 1} },
        { { {0,1}, {_sqrt2_2,_sqrt2_2} },                {2.323392, {_sqrt2_2-1,0.5+_sqrt2_2}},                       {1, 1} },
        { { {0,1}, {sqrt2,sqrt2} },                      {2.323392, {sqrt2-1,0.5+sqrt2}},                             {1, 1} },
        { { {sqrt2,0}, {0,0} },                          {2.562007, {_sqrt2_2,1}},                                    {1, 1} },
        { { {sqrt2,0}, {1,0} },                          {2.562007, {1+_sqrt2_2,1}},                                  {1, 1} },
        { { {sqrt2,0}, {0,1} },                          {2.562007, {_sqrt2_2,2}},                                    {1, 1} },
        { { {sqrt2,0}, {1,1} },                          {2.562007, {1+_sqrt2_2,2}},                                  {1, 1} },
        { { {sqrt2,0}, {_sqrt2_2,_sqrt2_2} },            {2.562007, {_sqrt2_2+_sqrt2_2,1+_sqrt2_2}},                  {1, 1} },
        { { {sqrt2,0}, {sqrt2,sqrt2} },                  {2.562007, {sqrt2+_sqrt2_2,1+sqrt2}},                        {1, 1} },
        { { {0,sqrt2}, {0,0} },                          {2.562007, {-1,_sqrt2_2}},                                   {1, 1} },
        { { {0,sqrt2}, {1,0} },                          {2.562007, {0,_sqrt2_2}},                                    {1, 1} },
        { { {0,sqrt2}, {0,1} },                          {2.562007, {-1,1+_sqrt2_2}},                                 {1, 1} },
        { { {0,sqrt2}, {1,1} },                          {2.562007, {0,1+_sqrt2_2}},                                  {1, 1} },
        { { {0,sqrt2}, {_sqrt2_2,_sqrt2_2} },            {2.562007, {_sqrt2_2-1,_sqrt2_2+_sqrt2_2}},                  {1, 1} },
        { { {0,sqrt2}, {sqrt2,sqrt2} },                  {2.562007, {sqrt2-1,_sqrt2_2+sqrt2}},                        {1, 1} },
        { { {2,0}, {0,0} },                              {2.957886, {1,1}},                                           {1, 1} },
        { { {2,0}, {1,0} },                              {2.957886, {2,1}},                                           {1, 1} },
        { { {2,0}, {0,1} },                              {2.957886, {1,2}},                                           {1, 1} },
        { { {2,0}, {1,1} },                              {2.957886, {2,2}},                                           {1, 1} },
        { { {2,0}, {_sqrt2_2,_sqrt2_2} },                {2.957886, {_sqrt2_2+1,1+_sqrt2_2}},                         {1, 1} },
        { { {2,0}, {sqrt2,sqrt2} },                      {2.957886, {sqrt2+1,1+sqrt2}},                               {1, 1} },
        { { {0,2}, {0,0} },                              {2.957886, {-1,1}},                                          {1, 1} },
        { { {0,2}, {1,0} },                              {2.957886, {0,1}},                                           {1, 1} },
        { { {0,2}, {0,1} },                              {2.957886, {-1,2}},                                          {1, 1} },
        { { {0,2}, {1,1} },                              {2.957886, {0,2}},                                           {1, 1} },
        { { {0,2}, {_sqrt2_2,_sqrt2_2} },                {2.957886, {_sqrt2_2-1,1+_sqrt2_2}},                         {1, 1} },
        { { {0,2}, {sqrt2,sqrt2} },                      {2.957886, {sqrt2-1,1+sqrt2}},                               {1, 1} },
        { { {_sqrt2_2,_sqrt2_2}, {0,0} },                {2.323392, {-_sqrt2_4,3*_sqrt2_4}},                          {1, 1} },
        { { {_sqrt2_2,_sqrt2_2}, {1,0} },                {2.323392, {1-_sqrt2_4,3*_sqrt2_4}},                         {1, 1} },
        { { {_sqrt2_2,_sqrt2_2}, {0,1} },                {2.323392, {-_sqrt2_4,1+3*_sqrt2_4}},                        {1, 1} },
        { { {_sqrt2_2,_sqrt2_2}, {1,1} },                {2.323392, {1-_sqrt2_4,1+3*_sqrt2_4}},                       {1, 1} },
        { { {_sqrt2_2,_sqrt2_2}, {_sqrt2_2,_sqrt2_2} },  {2.323392, {_sqrt2_2-_sqrt2_4,3*_sqrt2_4+_sqrt2_2}},         {1, 1} },
        { { {_sqrt2_2,_sqrt2_2}, {sqrt2,sqrt2} },        {2.323392, {sqrt2-_sqrt2_4,3*_sqrt2_4+sqrt2}},               {1, 1} },
        { { {1,1}, {0,0} },                              {2.562007, {(1-sqrt2)/2,(1+sqrt2)/2}},                       {1, 1} },
        { { {1,1}, {1,0} },                              {2.562007, {1+(1-sqrt2)/2,(1+sqrt2)/2}},                     {1, 1} },
        { { {1,1}, {0,1} },                              {2.562007, {(1-sqrt2)/2,1+(1+sqrt2)/2}},                     {1, 1} },
        { { {1,1}, {1,1} },                              {2.562007, {1+(1-sqrt2)/2,1+(1+sqrt2)/2}},                   {1, 1} },
        { { {1,1}, {_sqrt2_2,_sqrt2_2} },                {2.562007, {_sqrt2_2+(1-sqrt2)/2,(1+sqrt2)/2+_sqrt2_2}},     {1, 1} },
        { { {1,1}, {sqrt2,sqrt2} },                      {2.562007, {sqrt2+(1-sqrt2)/2,(1+sqrt2)/2+sqrt2}},           {1, 1} },
        { { {sqrt2,sqrt2}, {0,0} },                      {2.957886, {0,sqrt2}},                                       {1, 1} },
        { { {sqrt2,sqrt2}, {1,0} },                      {2.957886, {1,sqrt2}},                                       {1, 1} },
        { { {sqrt2,sqrt2}, {0,1} },                      {2.957886, {0,1+sqrt2}},                                     {1, 1} },
        { { {sqrt2,sqrt2}, {1,1} },                      {2.957886, {1,1+sqrt2}},                                     {1, 1} },
        { { {sqrt2,sqrt2}, {_sqrt2_2,_sqrt2_2} },        {2.957886, {_sqrt2_2+0,sqrt2+_sqrt2_2}},                     {1, 1} },
        { { {sqrt2,sqrt2}, {sqrt2,sqrt2} },              {2.957886, {sqrt2+0,sqrt2+sqrt2}},                           {1, 1} },
        { { {4,0}, {0,0} },                              {4.591174, {2,1}},                                           {1, 1} },
        { { {4,0}, {1,0} },                              {4.591174, {3,1}},                                           {1, 1} },
        { { {4,0}, {0,1} },                              {4.591174, {2,2}},                                           {1, 1} },
        { { {4,0}, {1,1} },                              {4.591174, {3,2}},                                           {1, 1} },
        { { {4,0}, {_sqrt2_2,_sqrt2_2} },                {4.591174, {_sqrt2_2+2,1+_sqrt2_2}},                         {1, 1} },
        { { {4,0}, {sqrt2,sqrt2} },                      {4.591174, {sqrt2+2,1+sqrt2}},                               {1, 1} },
        { { {0,4}, {0,0} },                              {4.591174, {-1,2}},                                          {1, 1} },
        { { {0,4}, {1,0} },                              {4.591174, {0,2}},                                           {1, 1} },
        { { {0,4}, {0,1} },                              {4.591174, {-1,3}},                                          {1, 1} },
        { { {0,4}, {1,1} },                              {4.591174, {0,3}},                                           {1, 1} },
        { { {0,4}, {_sqrt2_2,_sqrt2_2} },                {4.591174, {_sqrt2_2-1,2+_sqrt2_2}},                         {1, 1} },
        { { {0,4}, {sqrt2,sqrt2} },                      {4.591174, {sqrt2-1,2+sqrt2}},                               {1, 1} },
        { { {2,0}, {0,0} },                              {2.295587, {1,0.5}},                                         {1, 0.5} },
        { { {2,0}, {1,0} },                              {2.295587, {2,0.5}},                                         {1, 0.5} },
        { { {2,0}, {0,1} },                              {2.295587, {1,1.5}},                                         {1, 0.5} },
        { { {2,0}, {1,1} },                              {2.295587, {2,1.5}},                                         {1, 0.5} },
        { { {2,0}, {_sqrt2_2,_sqrt2_2} },                {2.295587, {_sqrt2_2+1,0.5+_sqrt2_2}},                       {1, 0.5} },
        { { {2,0}, {sqrt2,sqrt2} },                      {2.295587, {sqrt2+1,0.5+sqrt2}},                             {1, 0.5} },
        { { {0,2}, {0,0} },                              {2.295587, {-0.5,1}},                                        {1, 0.5} },
        { { {0,2}, {1,0} },                              {2.295587, {0.5,1}},                                         {1, 0.5} },
        { { {0,2}, {0,1} },                              {2.295587, {-0.5,2}},                                        {1, 0.5} },
        { { {0,2}, {1,1} },                              {2.295587, {0.5,2}},                                         {1, 0.5} },
        { { {0,2}, {_sqrt2_2,_sqrt2_2} },                {2.295587, {_sqrt2_2-0.5,1+_sqrt2_2}},                       {1, 0.5} },
        { { {0,2}, {sqrt2,sqrt2} },                      {2.295587, {sqrt2-0.5,1+sqrt2}},                             {1, 0.5} },
    });

    ////////////////////////////////////////////////////////////////

    const String traj_msg = "agent state trajectories";
    for (auto vec : initializer_list<Vec>{{1,0}, {0,1}, {sqrt2,0}, {0,sqrt2}, {2,0}, {0,2}, {_sqrt2_2,_sqrt2_2}, {1,1}, {sqrt2,sqrt2}}) {
        for (auto pos : initializer_list<Pos>{{0,0}, {1,0}, {0,1}, {1,1}, {_sqrt2_2,_sqrt2_2}, {sqrt2,sqrt2}}) {
            for (float abs_v : {1., 2., 0.5, 3.}) {
                for (float delta : {1., 2., 0.5, 3., 5., 10.}) {
                    Suite(traj_msg).test<Trajectory_case<Parabolic>>({
                        { { vec, pos },                                            {abs_v, delta} },
                    });
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const tomaqa::Error& e) {
    std::cout << e << std::endl;
    throw;
}
