#include <tomaqa.hpp>
#include <tomaqa/test.hpp>

#include "mapf_r/agent.hpp"
#include "mapf_r/agent/alg.hpp"

namespace mapf_r::test {
    using namespace tomaqa::test;
    using namespace agent;

    ////////////////////////////////////////////////////////////////

    using Collision_input = pair<Agent, Agent>;
    using Collision_output = Interval;

    struct Collision_case : Inherit<Case<Collision_input, Collision_output>> {
        using Inherit::Inherit;

        bool condition(const Output& expected_, const Output& result_) const override
        {
            return apx_equal(expected_, result_);
        }

        Output result() override
        {
            auto& [a, b] = cinput();
            return collision(a, b);
        }
    };

    ////////////////////////////////////////////////////////////////

    struct Conflict_input {
        Agent a, b;
        Interval t_a, t_b;

        String to_string() const
        {
            return a.to_string() + " t=" + t_a.to_string() + " x " + b.to_string() + " t=" + t_b.to_string();
        }
    };

    using Conflict_output = Interval;

    struct Conflict_case : Inherit<Case<Conflict_input, Conflict_output>> {
        using Inherit::Inherit;

        bool condition(const Output& expected_, const Output& result_) const override
        {
            return apx_equal(expected_, result_);
        }

        Output result() override
        {
            auto& [a, b, t_a, t_b] = cinput();
            return conflict(a, b, t_a, t_b);
        }

        static void check_input(const Input& in, bool valid_res)
        {
            using tomaqa::to_string;

            auto& [a, b, t_a, t_b] = in;

            const bool linear = !a.cstate().parabolic() && !b.cstate().parabolic();

            const bool in_confl = in_conflict(a, b, t_a, t_b);
            const bool rev_in_confl = in_conflict(b, a, t_b, t_a);
            expect(in_confl == rev_in_confl,
                   "in_conflict does not correspond to reversed in_conflict: "s
                   + to_string(in_confl) + " != " + to_string(rev_in_confl));

            Agent a_al = a;
            Agent b_al = b;
            bool al_in_confl;
            if (linear) {
                tie(a_al, b_al) = linear::aligned_agents(a, b, t_a, t_b);
                al_in_confl = linear::aligned_in_conflict(a_al, b_al, t_a, t_b);
                expect(in_confl == al_in_confl,
                       "linear::in_conflict does not correspond to linear::aligned_in_conflict: "s
                       + to_string(in_confl) + " != " + to_string(al_in_confl));
            }

            const Interval confl = conflict(a, b, t_a, t_b);
            const Interval rev_confl = conflict(b, a, t_b, t_a);
            expect(confl == rev_confl,
                   "conflict does not correspond to reversed conflict: "s
                   + to_string(confl) + " != " + to_string(rev_confl));

            Interval al_confl;
            Interval rev_al_confl;
            if (linear) {
                al_confl = linear::aligned_conflict(a_al, b_al, t_a, t_b);
                rev_al_confl = linear::aligned_conflict(b_al, a_al, t_b, t_a);
                expect(al_confl == rev_al_confl,
                       "linear::aligned_conflict does not correspond to reversed linear::aligned_conflict: "s
                       + to_string(al_confl) + " != " + to_string(rev_al_confl));

                expect(apx_equal<precision::High>(confl, al_confl),
                       "linear::conflict does not correspond to linear::aligned_conflict: "s
                       + to_string(confl) + " !~ " + to_string(al_confl));
            }

            const Optional<Float> t_max_confl = max_conflict(a, b, t_a, t_b);
            const Optional<Float> rev_t_max_confl = max_conflict(b, a, t_b, t_a);
            expect(t_max_confl == rev_t_max_confl,
                   "max_conflict does not correspond to reversed max_conflict: "s
                   + to_string(t_max_confl) + " != " + to_string(rev_t_max_confl));

            Optional<Float> al_t_max_confl;
            Optional<Float> rev_al_t_max_confl;
            if (linear) {
                al_t_max_confl = linear::aligned_max_conflict(a_al, b_al, t_a, t_b);
                rev_al_t_max_confl = linear::aligned_max_conflict(b_al, a_al, t_b, t_a);
                expect(al_t_max_confl == rev_al_t_max_confl,
                       "linear::aligned_max_conflict does not correspond to reversed linear::aligned_max_conflict: "s
                       + to_string(al_t_max_confl) + " != " + to_string(rev_al_t_max_confl));

                expect(apx_equal<precision::High>(t_max_confl, al_t_max_confl),
                       "linear::max_conflict does not correspond to linear::aligned_max_conflict: "s
                       + to_string(t_max_confl) + " !~ " + to_string(al_t_max_confl));
            }

            expect(valid_res == in_confl,
                   "Validity of the result does not correspond to in_conflict: "s
                   + to_string(valid_res) + " != " + to_string(in_confl));

            expect(in_confl == confl.valid(),
                   "in_conflict does not correspond to validity of conflict: "s
                   + to_string(in_confl) + " != " + confl.to_string());

            expect(in_confl == t_max_confl.valid(),
                   "in_conflict does not correspond to validity of max_conflict: "s
                   + to_string(in_confl) + " != " + t_max_confl.to_string());

            expect(!in_confl || confl.contains(t_max_confl.cvalue()),
                   "max_conflict in not an element of conflict: "s
                   + t_max_confl.to_string() + " !\\in " + confl.to_string());

            const auto check_max_f = [&t_max_confl, &a1=a, &a2=b, &t1=t_a, &t2=t_b]{
                if (!t_max_confl) return;
                const Float t_max = t_max_confl.cvalue();
                assert(a1.cstate().ct() == 0);
                assert(a2.cstate().ct() == 0);
                const Float tau1 = t_max - t1.clower();
                assert(tau1 >= 0);
                const auto p1 = a1.cstate().pos_at(tau1);
                const Float tau2 = t_max - t2.clower();
                assert(tau2 >= 0);
                const auto p2 = a2.cstate().pos_at(tau2);
                const Float d = dist({p1, p2});
                const Float r = a1.cradius() + a2.cradius();
                expect(d < r,
                        "Agents do not actually collide at the time of maximal collision "s
                        + to_string(t_max) + ":\n"
                        + a1.to_string() + " -> " + to_string(p1) + " after " + to_string(tau1) + "s\n"
                        + a2.to_string() + " -> " + to_string(p2) + " after " + to_string(tau2) + "s\n"
                        + "dist >= rad1+rad2: " + to_string(d) + " >= " + to_string(r));
            };
            check_max_f();

            if (!valid_res) return;

            if (!linear) return;

            Agent a_cp = a;
            Agent b_cp = b;
            auto& sa = a_cp.state();
            auto& sb = b_cp.state();
            if (sa.linear()) sa.set_parabolic();
            if (sb.linear()) sb.set_parabolic();
            constexpr Float eps = precision::Medium::abs_eps();
            constexpr Float eps2 = precision::High::abs_eps();
            constexpr Float eps3 = precision::Huge::abs_eps();
            constexpr Float eps4 = precision::Low::abs_eps();
            if (sa.parabolic()) a_cp.parabolic_state_set_delta(eps);
            if (sb.parabolic()) b_cp.parabolic_state_set_delta(eps);
            assert(parabolic::in_conflict(a, b, t_a, t_b));
            if (sa.parabolic()) a_cp.parabolic_state_set_delta(eps2);
            if (sb.parabolic()) b_cp.parabolic_state_set_delta(eps2);
            assert(parabolic::in_conflict(a, b, t_a, t_b));
            if (sa.parabolic()) a_cp.parabolic_state_set_delta(eps3);
            if (sb.parabolic()) b_cp.parabolic_state_set_delta(eps3);
            assert(parabolic::in_conflict(a, b, t_a, t_b));
            if (sa.parabolic()) a_cp.parabolic_state_set_delta(eps4);
            if (sb.parabolic()) b_cp.parabolic_state_set_delta(eps4);
            assert(parabolic::in_conflict(a, b, t_a, t_b));
        }

        void finish() override
        {
            Inherit::finish();

            using tomaqa::to_string;

            auto& in = cinput();
            auto& res = cresult();
            check_input(in, res.valid());
        }
    };

    ////////////////////////////////////////////////////////////////

    using Unsafe_interval_input = Conflict_input;

    using Unsafe_interval_output = pair<Interval, Interval>;

    struct Unsafe_interval_case : Inherit<Case<Unsafe_interval_input, Unsafe_interval_output>> {
        using Inherit::Inherit;

        bool condition(const Output& expected_, const Output& result_) const override
        {
            return apx_equal(expected_, result_);
        }

        Output result() override
        {
            auto& [a, b, t_a, t_b] = cinput();
            return unsafe_intervals(a, b, t_a, t_b);
        }

        void finish() override
        {
            Inherit::finish();

            using tomaqa::to_string;

            auto& in = cinput();
            auto& [a, b, t_a, t_b] = in;
            auto& res = cresult();

            const Output res_reverse = unsafe_intervals(b, a, t_b, t_a);
            expect(res.first == res_reverse.second && res.second == res_reverse.first,
                   "Reversing arguments does not yield symmetric result: "s + to_string(res) + " !~ " + to_string(res_reverse));

            expect(res.first.valid() == res.second.valid(),
                   "Either both unsafe intervals must be valid or neither of them, got: "s
                   + to_string(res));

            Conflict_case::check_input(in, res.first.valid());

            int counter = 0;
            const auto check_confl_f = [&counter](Agent a1, Agent a2, Interval t1, Interval t2, Interval u1, Interval /*u2*/){
                ++counter;

                assert(t1.valid() && t2.valid());

                auto [t1l, t1u] = t1.crange();
                auto [t2l, t2u] = t2.crange();

                auto [u1l, u1u] = u1.crange();

                const Interval t_int(t1l, t2u);
                expect(t_int.contains(u1),
                       "The computed unsafe interval " + to_string(u1)
                       + " is not a part of " + t_int.to_string()
                       + " of the agent no."s + to_string(counter));

                expect(!apx_equal(u1u, t2l) || u1u == t2l,
                       "Unsafe upper bound is not synchronized with the other agent's time lower bound: "s
                       + to_string(u1, 10) + " vs. " + to_string(t2, 10));

                const bool linear = !a1.cstate().parabolic() && !a2.cstate().parabolic();
                if (!linear) return;

                auto& s1 = a1.state();
                auto& s2 = a2.state();

                // Exclude cases where waiting cannot avoid the conflict
                if (u1u == t_int.cupper() || s1.idle() || t2u == inf) return;

                // "wait" to avoid the conflict, according to the threshold
                Float eps = precision::Medium::abs_eps();
                u1u += eps;
                s1.advance(-u1u);
                s2.advance(-t2l);
                t1 += u1u - t1l;
                const Interval confl_wait_int = linear::aligned_conflict(a1, a2, t1, t2);
                expect(confl_wait_int.empty(),
                       "Even the agent no."s + to_string(counter)
                       + " waits according to the computed unsafe interval " + to_string(u1u)
                       + ", it does not avoid the conflict: " + confl_wait_int.to_string());

                // "wait" shorter
                eps *= 2;
                s1.advance(eps);
                t1 -= eps;
                const Interval confl_wait_short_int = linear::aligned_conflict(a1, a2, t1, t2);
                expect(confl_wait_short_int.valid(),
                       "Even the agent no."s + to_string(counter)
                       + " waits little shorter than the computed unsafe interval " + to_string(u1)
                       + ", the conflict is still avoided: " + confl_wait_short_int.to_string());
            };

            if (!res.first) return;

            const auto& [ua, ub] = res;
            check_confl_f(a, b, t_a, t_b, ua, ub);
            check_confl_f(b, a, t_b, t_a, ub, ua);
        }
    };

    ////////////////////////////////////////////////////////////////
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace mapf_r;
    using namespace mapf_r::test;
    using namespace mapf_r::agent;
    using namespace std;
    using tomaqa::ignore;

    const Float _sqrt2_2 = sqrt2/2;
    const Float _2sqrt2 = 2*sqrt2;

    ////////////////////////////////////////////////////////////////

    using I = state::Idle;
    using L = state::Linear;
    using P = state::Parabolic;

    const String coll_msg = "collision of two agents";
    Suite(coll_msg).test<Collision_case>({
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{3.,0.}} } },                           {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{2.,0.}} } },                           {}, },    // "point collision" (inf)
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{1.9,0.}} } },                          inf_interval, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{2.1,0.}} } },                          {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,3.}} } },                           {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,2.}} } },                           {}, },    // "point collision" (inf)
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,1.9}} } },                          inf_interval, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,2.1}} } },                          {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Vec{-1.,0.}} } },              { 0., 2. }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Vec{1.,0.}} } },               {}, },  // "point collision" (inf)
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{1.9,0.}, Vec{1.,0.}} } },              inf_interval, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{-1.,0.}} } },              {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,1.99}, Vec{-1.,0.}} } },            { 1-sqrt(399)/200, 1+sqrt(399)/200 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{1.,0.}} } },               {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,sqrt3}, Vec{-1.,0.}} } },           { 0.5, 1.5 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{1.,sqrt3}, Vec{-1.,0.}} } },           { 0., 1. }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{1.,sqrt3}, Vec{1.,0.}} } },            {}, },  // almost "point collision" (inf)
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{1.,sqrt3-1e-2}, Vec{1.,0.}} } },       inf_interval, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Vec{0.,-1.}} } },              { 0., 2. }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Vec{0.,1.}} } },               {}, },  // "point collision" (inf)
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,1.9}, Vec{0.,1.}} } },              inf_interval, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{2.,2.}, Vec{0.,-1.}} } },              {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{1.99,2.}, Vec{0.,-1.}} } },            { 1-sqrt(399)/200, 1+sqrt(399)/200 }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{2.,2.}, Vec{0.,1.}} } },               {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt3,2.}, Vec{0.,-1.}} } },           { 0.5, 1.5 }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt3,1.}, Vec{0.,-1.}} } },           { 0., 1. }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt3,1.}, Vec{0.,1.}} } },            {}, },  // almost "point collision" (inf)
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt3-1e-2,1.}, Vec{0.,1.}} } },       inf_interval, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{sqrt2,sqrt2}, Vec{-1.,0.}} } },        { 0., sqrt2 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{sqrt2,sqrt2}, Vec{1.,0.}} } },         {}, },  // almost "point collision" (inf)
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{sqrt2-1e-2,sqrt2}, Vec{1.,0.}} } },    inf_interval, },
        { { { 1., 1., L{{0.,10.}, Vec{0.,-1.}} },       { 1., 1., L{{1.9,-10.}, Vec{0.,1.}} } },            { 9.68775, 10.31225 }, },
        //+ not actually verified these ...
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{sqrt2,sqrt2}, Vec{0.,-1.}} } },        { 0., _2sqrt2 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{0.,-1.}} } },              { 2-sqrt2, 2+sqrt2 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,-1.}} } },              { 3-sqrt2, 3+sqrt2 }, },
        { { { 1., 1., L{{-2.,0.}, Vec{1.,0.}} },        { 1., 1., L{{3.,3.}, Vec{0.,-1.}} } },              { 3., 5. }, },
        { { { 1., 1., L{{_2sqrt2,0.}, Vec{1.,0.}} },    { 1., 1., L{{3.,3.}, Vec{0.,-1.}} } },              {}, },  // "point collision" at 3-sqrt2
        { { { 1., 1., L{{_2sqrt2,0.}, Vec{1.,0.}} },    { 1., 1., L{{3.,2.99}, Vec{0.,-1.}} } },            { 1.461971, 1.699602 }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt2,sqrt2}, Vec{-1.,0.}} } },        { 0., _2sqrt2 }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{2.,2.}, Vec{-1.,0.}} } },              { 2-sqrt2, 2+sqrt2 }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{3.,3.}, Vec{-1.,0.}} } },              { 3-sqrt2, 3+sqrt2 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{sqrt2,sqrt2}, Vec{0.,1.}} } },         {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{sqrt2-1e-2,sqrt2}, Vec{0.,1.}} } },    { -0.123816, 0.113816 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{sqrt2,sqrt2-1e-2}, Vec{0.,1.}} } },    { -0.113816, 0.123816 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{0.,1.}} } },               {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,1.}} } },               {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt2,sqrt2}, Vec{1.,0.}} } },         {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt2-1e-2,sqrt2}, Vec{1.,0.}} } },    { -0.113816, 0.123816 }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt2,sqrt2-1e-2}, Vec{1.,0.}} } },    { -0.123816, 0.113816 }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{2.,2.}, Vec{1.,0.}} } },               {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{3.,3.}, Vec{1.,0.}} } },               {}, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,2.}, Vec{0.,1.}} } },           { -2., 0. }, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,2.}, Vec{1.,0.}} } },           { 0., _2sqrt2+2 }, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,2.}, Vec{-1.,0.}} } },          { 0., _2sqrt2-2 }, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,3.}, Vec{0.,1.}} } },           {}, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,3.}, Vec{1.,0.}} } },           { 1.481594, 5.761046 }, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,3.}, Vec{-1.,0.}} } },          {}, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,2.}, Vec{_sqrt2_2,-_sqrt2_2}} } }, { 0., _2sqrt2 }, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,3.}, Vec{_sqrt2_2,-_sqrt2_2}} } }, { _sqrt2_2, 2.5*sqrt2 }, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,2.}, Vec{-_sqrt2_2,-_sqrt2_2}} } }, { 0., sqrt2 }, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,3.}, Vec{-_sqrt2_2,-_sqrt2_2}} } }, {}, },
        { { { 1., 1., L{{-sqrt2,-sqrt2}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,0.}, Vec{0.,1.}} } },   { 0., 2. }, },
        { { { 1., 1., L{{-sqrt2,-sqrt2}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,0.1}, Vec{0.,1.}} } },  { 0.315503, 1.584497 }, },  //+ not verified
        { { { 1., 1., L{{2.,0.}, Coord{4.,0.}} },       { 1., 1., L{{4.,1.}, Coord{2.,1.}} } },             { 0.133975, 1.866025 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}} },       { 1., 1., L{{4.,1.}, Coord{0.,1.}} } },             { 0.113720, 2.366870 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, 1. },   { 1., 1., L{{4.,1.}, Coord{0.,1.}} } },             { 0.113720, 2.366870 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, -1. },  { 1., 1., L{{4.,1.}, Coord{0.,1.}} } },             { 0.353493, 1.320695 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, 1.1 },  { 1., 1., L{{4.,1.}, Coord{0.,1.}} } },             { 0.113945, 2.425940 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, -1.1 }, { 1., 1., L{{4.,1.}, Coord{0.,1.}} } },             { 0.426502, 1.038156 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, 1.2 },  { 1., 1., L{{4.,1.}, Coord{0.,1.}} } },             { 0.114248, 2.479906 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, -1.2 }, { 1., 1., L{{4.,1.}, Coord{0.,1.}} } },             {}, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, 1.25 }, { 1., 1., L{{4.,1.}, Coord{0.,1.}} } },             { 0.114422, 2.504597 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, -1.25 },{ 1., 1., L{{4.,1.}, Coord{0.,1.}} } },             {}, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, 1.5 },  { 1., 1., L{{4.,1.}, Coord{0.,1.}} } },             { 0.115415, 2.599941 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, -1.5 }, { 1., 1., L{{4.,1.}, Coord{0.,1.}} } },             {}, },
        { { { 1., 1., L{{2.,0.}, Coord{6.,0.}} },       { 1., 1., P{{4.,1.}, Coord{2.,1.}} } },             { 0.113720, 2.366870 }, },
        { { { 1., 1., L{{2.,0.}, Coord{6.,0.}} },       { 1., 1., P{{4.,1.}, Coord{2.,1.}}, 1. } },         { 0.113720, 2.366870 }, },
        { { { 1., 1., L{{2.,0.}, Coord{6.,0.}} },       { 1., 1., P{{4.,1.}, Coord{2.,1.}}, -1. } },        { 0.353493, 1.320695 }, },
        { { { 1., 1., L{{2.,0.}, Coord{6.,0.}} },       { 1., 1., P{{4.,1.}, Coord{2.,1.}}, 1.1 } },        { 0.113945, 2.425940 }, },
        { { { 1., 1., L{{2.,0.}, Coord{6.,0.}} },       { 1., 1., P{{4.,1.}, Coord{2.,1.}}, -1.1 } },       { 0.426502, 1.038156 }, },
        { { { 1., 1., L{{2.,0.}, Coord{6.,0.}} },       { 1., 1., P{{4.,1.}, Coord{2.,1.}}, 1.2 } },        { 0.114248, 2.479906 }, },
        { { { 1., 1., L{{2.,0.}, Coord{6.,0.}} },       { 1., 1., P{{4.,1.}, Coord{2.,1.}}, -1.2 } },       {}, },
        { { { 1., 1., L{{2.,0.}, Coord{6.,0.}} },       { 1., 1., P{{4.,1.}, Coord{2.,1.}}, 1.25 } },       { 0.114422, 2.504597 }, },
        { { { 1., 1., L{{2.,0.}, Coord{6.,0.}} },       { 1., 1., P{{4.,1.}, Coord{2.,1.}}, -1.25 } },      {}, },
        { { { 1., 1., L{{2.,0.}, Coord{6.,0.}} },       { 1., 1., P{{4.,1.}, Coord{2.,1.}}, 1.5 } },        { 0.115415, 2.599941 }, },
        { { { 1., 1., L{{2.,0.}, Coord{6.,0.}} },       { 1., 1., P{{4.,1.}, Coord{2.,1.}}, -1.5 } },       {}, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, 1. },   { 1., 1., P{{4.,1.}, Coord{2.,1.}}, 1. } },         { 0.102631, 2.855254 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, 1. },   { 1., 1., P{{4.,1.}, Coord{2.,1.}}, -1. } },        { 0.198141, 2.759745 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, -1. },  { 1., 1., P{{4.,1.}, Coord{2.,1.}}, 1. } },         { 0.198141, 2.759745 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, -1. },  { 1., 1., P{{4.,1.}, Coord{2.,1.}}, -1. } },        {}, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, 1.25 }, { 1., 1., P{{4.,1.}, Coord{2.,1.}}, 1.25 } },       { 0.104949, 3.246526 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, 1.25 }, { 1., 1., P{{4.,1.}, Coord{2.,1.}}, -1.25 } },      { 0.224506, 3.126969 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, -1.25 },{ 1., 1., P{{4.,1.}, Coord{2.,1.}}, 1.25 } },       { 0.224506, 3.126969 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, -1.25 },{ 1., 1., P{{4.,1.}, Coord{2.,1.}}, -1.25 } },      {}, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, 1.5 },  { 1., 1., P{{4.,1.}, Coord{2.,1.}}, 1.5 } },        { 0.107760, 3.660667 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, 1.5 },  { 1., 1., P{{4.,1.}, Coord{2.,1.}}, -1.5 } },       { 0.252437, 3.515990 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, -1.5 }, { 1., 1., P{{4.,1.}, Coord{2.,1.}}, 1.5 } },        { 0.252437, 3.515990 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, -1.5 }, { 1., 1., P{{4.,1.}, Coord{2.,1.}}, -1.5 } },       {}, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, 2. },   { 1., 1., P{{4.,1.}, Coord{2.,1.}}, 2. } },         { 0.113715, 4.533069 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, 2. },   { 1., 1., P{{4.,1.}, Coord{2.,1.}}, -2. } },        { 0.311275, 4.335508 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, -2. },  { 1., 1., P{{4.,1.}, Coord{2.,1.}}, 2. } },         { 0.311275, 4.335508 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}}, -2. },  { 1., 1., P{{4.,1.}, Coord{2.,1.}}, -2. } },        {}, },
        { { { 1., 1., L{{0.,2.}, Coord{0.,4.}} },       { 1., 1., L{{1.,4.}, Coord{1.,2.}} } },             { 0.133975, 1.866025 }, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}} },       { 1., 1., L{{1.,4.}, Coord{1.,0.}} } },             { 0.353493, 1.320695 }, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, 1. },   { 1., 1., L{{1.,4.}, Coord{1.,0.}} } },             { 0.353493, 1.320695 }, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, -1. },  { 1., 1., L{{1.,4.}, Coord{1.,0.}} } },             { 0.113720, 2.366870 }, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, 1.1 },  { 1., 1., L{{1.,4.}, Coord{1.,0.}} } },             { 0.426502, 1.038156 }, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, -1.1 }, { 1., 1., L{{1.,4.}, Coord{1.,0.}} } },             { 0.113945, 2.425940 }, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, 1.2 },  { 1., 1., L{{1.,4.}, Coord{1.,0.}} } },             {}, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, -1.2 }, { 1., 1., L{{1.,4.}, Coord{1.,0.}} } },             { 0.114248, 2.479906 }, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, 1.25 }, { 1., 1., L{{1.,4.}, Coord{1.,0.}} } },             {}, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, -1.25 },{ 1., 1., L{{1.,4.}, Coord{1.,0.}} } },             { 0.114422, 2.504597 }, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, 1.5 },  { 1., 1., L{{1.,4.}, Coord{1.,0.}} } },             {}, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, -1.5 }, { 1., 1., L{{1.,4.}, Coord{1.,0.}} } },             { 0.115415, 2.599941 }, },
        { { { 1., 1., L{{0.,2.}, Coord{0.,4.}} },       { 1., 1., P{{1.,4.}, Coord{1.,2.}} } },             { 0.353493, 1.320695 }, },
        { { { 1., 1., L{{0.,2.}, Coord{0.,4.}} },       { 1., 1., P{{1.,4.}, Coord{1.,2.}}, 1. } },         { 0.353493, 1.320695 }, },
        { { { 1., 1., L{{0.,2.}, Coord{0.,4.}} },       { 1., 1., P{{1.,4.}, Coord{1.,2.}}, -1. } },        { 0.113720, 2.366870 }, },
        { { { 1., 1., L{{0.,2.}, Coord{0.,4.}} },       { 1., 1., P{{1.,4.}, Coord{1.,2.}}, 1.1 } },        { 0.426502, 1.038156 }, },
        { { { 1., 1., L{{0.,2.}, Coord{0.,4.}} },       { 1., 1., P{{1.,4.}, Coord{1.,2.}}, -1.1 } },       { 0.113945, 2.425940 }, },
        { { { 1., 1., L{{0.,2.}, Coord{0.,4.}} },       { 1., 1., P{{1.,4.}, Coord{1.,2.}}, 1.2 } },        {}, },
        { { { 1., 1., L{{0.,2.}, Coord{0.,4.}} },       { 1., 1., P{{1.,4.}, Coord{1.,2.}}, -1.2 } },       { 0.114248, 2.479906 }, },
        { { { 1., 1., L{{0.,2.}, Coord{0.,4.}} },       { 1., 1., P{{1.,4.}, Coord{1.,2.}}, 1.25 } },       {}, },
        { { { 1., 1., L{{0.,2.}, Coord{0.,4.}} },       { 1., 1., P{{1.,4.}, Coord{1.,2.}}, -1.25 } },      { 0.114422, 2.504597 }, },
        { { { 1., 1., L{{0.,2.}, Coord{0.,4.}} },       { 1., 1., P{{1.,4.}, Coord{1.,2.}}, 1.5 } },        {}, },
        { { { 1., 1., L{{0.,2.}, Coord{0.,4.}} },       { 1., 1., P{{1.,4.}, Coord{1.,2.}}, -1.5 } },       { 0.115415, 2.599941 }, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, 1. },   { 1., 1., P{{1.,4.}, Coord{1.,2.}}, 1. } },         {}, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, 1. },   { 1., 1., P{{1.,4.}, Coord{1.,2.}}, -1. } },        { 0.198141, 2.759745 }, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, -1. },  { 1., 1., P{{1.,4.}, Coord{1.,2.}}, 1. } },         { 0.198141, 2.759745 }, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, -1. },  { 1., 1., P{{1.,4.}, Coord{1.,2.}}, -1. } },        { 0.102631, 2.855254 }, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, 1.25 }, { 1., 1., P{{1.,4.}, Coord{1.,2.}}, 1.25 } },       {}, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, 1.25 }, { 1., 1., P{{1.,4.}, Coord{1.,2.}}, -1.25 } },      { 0.224506, 3.126969 }, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, -1.25 },{ 1., 1., P{{1.,4.}, Coord{1.,2.}}, 1.25 } },       { 0.224506, 3.126969 }, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, -1.25 },{ 1., 1., P{{1.,4.}, Coord{1.,2.}}, -1.25 } },      { 0.104949, 3.246526 }, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, 1.5 },  { 1., 1., P{{1.,4.}, Coord{1.,2.}}, 1.5 } },        {}, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, 1.5 },  { 1., 1., P{{1.,4.}, Coord{1.,2.}}, -1.5 } },       { 0.252437, 3.515990 }, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, -1.5 }, { 1., 1., P{{1.,4.}, Coord{1.,2.}}, 1.5 } },        { 0.252437, 3.515990 }, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, -1.5 }, { 1., 1., P{{1.,4.}, Coord{1.,2.}}, -1.5 } },       { 0.107760, 3.660667 }, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, 2. },   { 1., 1., P{{1.,4.}, Coord{1.,2.}}, 2. } },         {}, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, 2. },   { 1., 1., P{{1.,4.}, Coord{1.,2.}}, -2. } },        { 0.311275, 4.335508 }, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, -2. },  { 1., 1., P{{1.,4.}, Coord{1.,2.}}, 2. } },         { 0.311275, 4.335508 }, },
        { { { 1., 1., P{{0.,2.}, Coord{0.,4.}}, -2. },  { 1., 1., P{{1.,4.}, Coord{1.,2.}}, -2. } },        { 0.113715, 4.533069 }, },
        { { { 1., 1., L{{2.,0.}, Coord{4.,2.}} },       { 1., 1., L{{4.,3.}, Coord{2.,1.}} } },             { 0.832353, 2.703181 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,2.}} },       { 1., 1., L{{4.,3.}, Coord{2.,1.}} } },             { 0.859525, 3.091255 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,2.}}, 1. },   { 1., 1., L{{4.,3.}, Coord{2.,1.}} } },             { 0.859525, 3.091255 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,2.}}, -1. },  { 1., 1., L{{4.,3.}, Coord{2.,1.}} } },             { 1.339985, 2.770272 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,2.}}, 1.25 }, { 1., 1., L{{4.,3.}, Coord{2.,1.}} } },             { 0.898218, 3.222114 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,2.}}, -1.25 },{ 1., 1., L{{4.,3.}, Coord{2.,1.}} } },             { 1.791441, 2.451231 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,2.}}, 1.5 },  { 1., 1., L{{4.,3.}, Coord{2.,1.}} } },             { 0.942811, 3.324519 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,2.}}, -1.5 }, { 1., 1., L{{4.,3.}, Coord{2.,1.}} } },             {}, },
        { { { 1., 1., L{{2.,0.}, Coord{4.,2.}} },       { 1., 1., P{{4.,3.}, Coord{2.,1.}} } },             { 0.859525, 3.091255 }, },
        { { { 1., 1., L{{2.,0.}, Coord{4.,2.}} },       { 1., 1., P{{4.,3.}, Coord{2.,1.}}, 1. } },         { 0.859525, 3.091255 }, },
        { { { 1., 1., L{{2.,0.}, Coord{4.,2.}} },       { 1., 1., P{{4.,3.}, Coord{2.,1.}}, -1. } },        { 1.339985, 2.770272 }, },
        { { { 1., 1., L{{2.,0.}, Coord{4.,2.}} },       { 1., 1., P{{4.,3.}, Coord{2.,1.}}, 1.25 } },       { 0.898218, 3.222114 }, },
        { { { 1., 1., L{{2.,0.}, Coord{4.,2.}} },       { 1., 1., P{{4.,3.}, Coord{2.,1.}}, -1.25 } },      { 1.791441, 2.451231 }, },
        { { { 1., 1., L{{2.,0.}, Coord{4.,2.}} },       { 1., 1., P{{4.,3.}, Coord{2.,1.}}, 1.5 } },        { 0.942811, 3.324519 }, },
        { { { 1., 1., L{{2.,0.}, Coord{4.,2.}} },       { 1., 1., P{{4.,3.}, Coord{2.,1.}}, -1.5 } },       {}, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,2.}}, 1. },   { 1., 1., P{{4.,3.}, Coord{2.,1.}}, 1. } },         { 1.162709, 3.483662 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,2.}}, 1. },   { 1., 1., P{{4.,3.}, Coord{2.,1.}}, -1. } },        { 1.058148, 3.436484 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,2.}}, -1. },  { 1., 1., P{{4.,3.}, Coord{2.,1.}}, 1. } },         { 1.058148, 3.436484 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,2.}}, -1. },  { 1., 1., P{{4.,3.}, Coord{2.,1.}}, -1. } },        {}, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,2.}}, 1.25 }, { 1., 1., P{{4.,3.}, Coord{2.,1.}}, 1.25 } },       { 1.826967, 3.824197 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,2.}}, 1.25 }, { 1., 1., P{{4.,3.}, Coord{2.,1.}}, -1.25 } },      { 1.159475, 3.765556 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,2.}}, -1.25 },{ 1., 1., P{{4.,3.}, Coord{2.,1.}}, 1.25 } },       { 1.159475, 3.765556 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,2.}}, -1.25 },{ 1., 1., P{{4.,3.}, Coord{2.,1.}}, -1.25 } },      {}, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,2.}}, 1.5 },  { 1., 1., P{{4.,3.}, Coord{2.,1.}}, 1.5 } },        { 2.841571, 4.193860 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,2.}}, 1.5 },  { 1., 1., P{{4.,3.}, Coord{2.,1.}}, -1.5 } },       { 1.269636, 4.123321 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,2.}}, -1.5 }, { 1., 1., P{{4.,3.}, Coord{2.,1.}}, 1.5 } },        { 1.269636, 4.123321 }, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,2.}}, -1.5 }, { 1., 1., P{{4.,3.}, Coord{2.,1.}}, -1.5 } },       {}, },
        { { { 1., 1., P{{0.,0.}, Coord{2.,0.}} },       { 1., 1., P{{2.,4.}, Coord{0.,4.}} } },             {}, },
        { { { 1., 1., P{{0.,1.}, Coord{2.,1.}} },       { 1., 1., P{{2.,4.}, Coord{0.,4.}} } },             { 0.638576, 2.319310 }, },
        { { { 1., 1., P{{0.,0.5}, Coord{2.,0.5}} },     { 1., 1., P{{2.,4.}, Coord{0.,4.}} } },             { 0.879627, 2.078259 }, },
        { { { 1., 1., P{{0.,0.1}, Coord{2.,0.1}} },     { 1., 1., P{{2.,4.}, Coord{0.,4.}} } },             { 1.209306, 1.748580 }, },
    });

    Suite(coll_msg, true).test<Collision_case>({
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{-1.,0.}} } },              { .99, 1.01 }, },
    });

    ////////////////////////////////////////////////////////////////

    const String confl_msg = "conflict of two agents";
    Suite(confl_msg).test<Conflict_case>({
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,0.}} },                             {0.,6.},    {6.,12.}    },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,0.}} },                             {0.,6.},    {6.1,12.1}  },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{1.99,0.}} },                           {0.,6.},    {6.,12.}    },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{1.99,0.}} },                           {0.,6.},    {6.1,12.1}  },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,1.99}} },                           {0.,6.},    {6.,12.}    },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,1.99}} },                           {0.,6.},    {6.1,12.1}  },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{2.,0.}} },                             {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{2.,0.}} },                             {0.,6.},    {3.,9.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{2.,0.}} },                             {0.,6.},    {6.,12.}    },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{2.,0.}} },                             {0.,6.},    {6.1,12.1}  },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,2.}} },                             {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,2.}} },                             {0.,6.},    {3.,9.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,2.}} },                             {0.,6.},    {6.,12.}    },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,2.}} },                             {0.,6.},    {6.1,12.1}  },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{3.,0.}} },                             {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{3.,0.}} },                             {0.,6.},    {3.,9.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{3.,0.}} },                             {0.,6.},    {6.,12.}    },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{3.,0.}} },                             {0.,6.},    {6.1,12.1}  },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{2.1,0.}} },                            {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{2.1,0.}} },                            {0.,6.},    {3.,9.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{2.1,0.}} },                            {0.,6.},    {6.,12.}    },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{2.1,0.}} },                            {0.,6.},    {6.1,12.1}  },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,3.}} },                             {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,3.}} },                             {0.,6.},    {3.,9.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,3.}} },                             {0.,6.},    {6.,12.}    },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,3.}} },                             {0.,6.},    {6.1,12.1}  },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,2.1}} },                            {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,2.1}} },                            {0.,6.},    {3.,9.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,2.1}} },                            {0.,6.},    {6.,12.}    },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,2.1}} },                            {0.,6.},    {6.1,12.1}  },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Vec{1.,0.}} },                 {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Vec{-1.,0.}} },                {0.,6.},    {0.,6.}     },  { 0., 2. }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Vec{0.,1.}} },                 {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Vec{0.,-1.}} },                {0.,6.},    {0.,6.}     },  { 0., 2. }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Vec{-1.,0.}} },                {0.,6.},    {1.,7.}     },  { 1., 2.5 }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Vec{0.,-1.}} },                {0.,6.},    {1.,7.}     },  { 1., 2.5 }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Vec{0.,-1.}} },                {1.,7.},    {0.,6.}     },  { 1., 2.5 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Vec{-1.,0.}} },                {1.,7.},    {0.,6.}     },  { 1., 2.5 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Vec{-1.,0.}} },                {1.,7.},    {1.,7.}     },  { 1., 3. }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Vec{0.,-1.}} },                {1.,7.},    {1.,7.}     },  { 1., 3. }, },
        { { { 1., 1., L{{0.,0.}, Coord{6.,0.}} },       { 1., 1., L{{2.,0.}, Vec{1.,0.}} },                 {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Coord{6.,0.}} },       { 1., 1., L{{2.,0.}, Vec{-1.,0.}} },                {0.,6.},    {0.,6.}     },  { 0., 2. }, },
        { { { 1., 1., L{{0.,0.}, Coord{0.,6.}} },       { 1., 1., L{{0.,2.}, Vec{0.,1.}} },                 {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Coord{0.,6.}} },       { 1., 1., L{{0.,2.}, Vec{0.,-1.}} },                {0.,6.},    {0.,6.}     },  { 0., 2. }, },
        { { { 1., 1., L{{0.,0.}, Coord{6.,0.}} },       { 1., 1., L{{2.,0.}, Vec{-1.,0.}} },                {0.,6.},    {1.,7.}     },  { 1., 2.5 }, },
        { { { 1., 1., L{{0.,0.}, Coord{0.,6.}} },       { 1., 1., L{{0.,2.}, Vec{0.,-1.}} },                {0.,6.},    {1.,7.}     },  { 1., 2.5 }, },
        { { { 1., 1., L{{0.,0.}, Coord{0.,6.}} },       { 1., 1., L{{0.,2.}, Vec{0.,-1.}} },                {1.,7.},    {0.,6.}     },  { 1., 2.5 }, },
        { { { 1., 1., L{{0.,0.}, Coord{6.,0.}} },       { 1., 1., L{{2.,0.}, Vec{-1.,0.}} },                {1.,7.},    {0.,6.}     },  { 1., 2.5 }, },
        { { { 1., 1., L{{0.,0.}, Coord{6.,0.}} },       { 1., 1., L{{2.,0.}, Vec{-1.,0.}} },                {1.,7.},    {1.,7.}     },  { 1., 3. }, },
        { { { 1., 1., L{{0.,0.}, Coord{0.,6.}} },       { 1., 1., L{{0.,2.}, Vec{0.,-1.}} },                {1.,7.},    {1.,7.}     },  { 1., 3. }, },
        { { { 1., 1., L{{0.,0.}, Coord{6.,0.}} },       { 1., 1., L{{2.,0.}, Coord{8.,0.}} },               {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Coord{6.,0.}} },       { 1., 1., L{{2.,0.}, Coord{-4.,0.}} },              {0.,6.},    {0.,6.}     },  { 0., 2. }, },
        { { { 1., 1., L{{0.,0.}, Coord{0.,6.}} },       { 1., 1., L{{0.,2.}, Coord{0.,8.}} },               {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Coord{0.,6.}} },       { 1., 1., L{{0.,2.}, Coord{0.,-4.}} },              {0.,6.},    {0.,6.}     },  { 0., 2. }, },
        { { { 1., 1., L{{0.,0.}, Coord{6.,0.}} },       { 1., 1., L{{2.,0.}, Coord{-4.,0.}} },              {0.,6.},    {1.,7.}     },  { 1., 2.5 }, },
        { { { 1., 1., L{{0.,0.}, Coord{0.,6.}} },       { 1., 1., L{{0.,2.}, Coord{0.,-4.}} },              {0.,6.},    {1.,7.}     },  { 1., 2.5 }, },
        { { { 1., 1., L{{0.,0.}, Coord{0.,6.}} },       { 1., 1., L{{0.,2.}, Coord{0.,-4.}} },              {1.,7.},    {0.,6.}     },  { 1., 2.5 }, },
        { { { 1., 1., L{{0.,0.}, Coord{6.,0.}} },       { 1., 1., L{{2.,0.}, Coord{-4.,0.}} },              {1.,7.},    {0.,6.}     },  { 1., 2.5 }, },
        { { { 1., 1., L{{0.,0.}, Coord{6.,0.}} },       { 1., 1., L{{2.,0.}, Coord{-4.,0.}} },              {1.,7.},    {1.,7.}     },  { 1., 3. }, },
        { { { 1., 1., L{{0.,0.}, Coord{0.,6.}} },       { 1., 1., L{{0.,2.}, Coord{0.,-4.}} },              {1.,7.},    {1.,7.}     },  { 1., 3. }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Coord{8.,0.}} },               {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Coord{-4.,0.}} },              {0.,6.},    {0.,6.}     },  { 0., 2. }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Coord{0.,8.}} },               {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Coord{0.,-4.}} },              {0.,6.},    {0.,6.}     },  { 0., 2. }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Coord{-4.,0.}} },              {0.,6.},    {1.,7.}     },  { 1., 2.5 }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Coord{0.,-4.}} },              {0.,6.},    {1.,7.}     },  { 1., 2.5 }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Coord{0.,-4.}} },              {1.,7.},    {0.,6.}     },  { 1., 2.5 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Coord{-4.,0.}} },              {1.,7.},    {0.,6.}     },  { 1., 2.5 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Coord{-4.,0.}} },              {1.,7.},    {1.,7.}     },  { 1., 3. }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Coord{0.,-4.}} },              {1.,7.},    {1.,7.}     },  { 1., 3. }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{1.99,0.}, Vec{1.,0.}} },               {0.,6.},    {0.,6.}     },  { 0., 6. }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,1.99}, Vec{0.,1.}} },               {0.,6.},    {0.,6.}     },  { 0., 6. }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{1.99,0.}, Vec{1.,0.}} },               {0.,6.},    {1.,7.}     },  { 1., 6. }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,1.99}, Vec{0.,1.}} },               {0.,6.},    {1.,7.}     },  { 1., 6. }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{1.99,0.}, Vec{1.,0.}} },               {1.,7.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,1.99}, Vec{0.,1.}} },               {1.,7.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{1.99,0.}, Vec{1.,0.}} },               {1.,7.},    {1.,7.}     },  { 1., 7. }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,1.99}, Vec{0.,1.}} },               {1.,7.},    {1.,7.}     },  { 1., 7. }, },
        { { { 1., 1., L{{0.,0.}, Vec{-1.,0.}} },        { 1., 1., L{{1.99,0.}, Vec{-1.,0.}} },              {0.,6.},    {0.,6.}     },  { 0., 6. }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,-1.}} },        { 1., 1., L{{0.,1.99}, Vec{0.,-1.}} },              {0.,6.},    {0.,6.}     },  { 0., 6. }, },
        { { { 1., 1., L{{0.,0.}, Vec{-1.,0.}} },        { 1., 1., L{{1.99,0.}, Vec{-1.,0.}} },              {0.,6.},    {1.,7.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,-1.}} },        { 1., 1., L{{0.,1.99}, Vec{0.,-1.}} },              {0.,6.},    {1.,7.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{-1.,0.}} },        { 1., 1., L{{1.99,0.}, Vec{-1.,0.}} },              {1.,7.},    {0.,6.}     },  { 1., 6. }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,-1.}} },        { 1., 1., L{{0.,1.99}, Vec{0.,-1.}} },              {1.,7.},    {0.,6.}     },  { 1., 6. }, },
        { { { 1., 1., L{{0.,0.}, Vec{-1.,0.}} },        { 1., 1., L{{1.99,0.}, Vec{-1.,0.}} },              {1.,7.},    {1.,7.}     },  { 1., 7. }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,-1.}} },        { 1., 1., L{{0.,1.99}, Vec{0.,-1.}} },              {1.,7.},    {1.,7.}     },  { 1., 7. }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{-1.,0.}} },                {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{-1.,0.}} },                {0.,6.},    {1.,7.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{-1.,0.}} },                {0.,6.},    {2.,8.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{-1.,0.}} },                {0.,6.},    {3.,9.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,1.99}, Vec{-1.,0.}} },              {0.,6.},    {0.,6.}     },  { 0.900125, 1.099875 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,1.99}, Vec{-1.,0.}} },              {0.,6.},    {1.,7.}     },  { 1.400125, 1.599875 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,1.99}, Vec{-1.,0.}} },              {0.,6.},    {2.,8.}     },  { 2., 2.099875 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,1.99}, Vec{-1.,0.}} },              {0.,6.},    {3.,9.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{1.,0.}} },                 {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{1.,sqrt3}, Vec{1.,0.}} },              {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{1.,sqrt3-1e-2}, Vec{1.,0.}} },         {0.,6.},    {0.,6.}     },  { 0., 6. }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{1.,sqrt3}, Vec{-1.,0.}} },             {0.,6.},    {0.,6.}     },  { 0., 1. }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,sqrt3}, Vec{-1.,0.}} },             {0.,6.},    {0.,6.}     },  { 0.5, 1.5 }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{2.,2.}, Vec{0.,-1.}} },                {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{1.99,2.}, Vec{0.,-1.}} },              {0.,6.},    {0.,6.}     },  { 0.900125, 1.099875 }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{2.,2.}, Vec{0.,1.}} },                 {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt3,1.}, Vec{0.,1.}} },              {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt3-1e-2,1.}, Vec{0.,1.}} },         {0.,6.},    {0.,6.}     },  { 0., 6. }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt3,1.}, Vec{0.,-1.}} },             {0.,6.},    {0.,6.}     },  { 0., 1. }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt3,2.}, Vec{0.,-1.}} },             {0.,6.},    {0.,6.}     },  { 0.5, 1.5 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{sqrt2,sqrt2}, Vec{1.,0.}} },           {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt2,sqrt2}, Vec{1.,0.}} },           {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{sqrt2-1e-2,sqrt2}, Vec{1.,0.}} },      {0.,6.},    {0.,6.}     },  { 0., 6. }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt2-1e-2,sqrt2}, Vec{1.,0.}} },      {0.,6.},    {0.,6.}     },  { 0., 0.123816 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{sqrt2,sqrt2}, Vec{0.,1.}} },           {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{sqrt2-1e-2,sqrt2}, Vec{0.,1.}} },      {0.,6.},    {0.,6.}     },  { 0., 0.113816 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{sqrt2,sqrt2}, Vec{-1.,0.}} },          {0.,6.},    {0.,6.}     },  { 0., sqrt2 }, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt2,sqrt2}, Vec{-1.,0.}} },          {0.,6.},    {0.,6.}     },  { 0., _2sqrt2 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{sqrt2,sqrt2}, Vec{0.,-1.}} },          {0.,6.},    {0.,6.}     },  { 0., _2sqrt2 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{0.,-1.}} },                {0.,6.},    {0.,6.}     },  { 2-sqrt2, 2+sqrt2 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{0.,-1.}} },                {0.,6.},    {1.,7.}     },  { 1.177124, 3.822876 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{0.,-1.}} },                {0.,6.},    {2.,8.}     },  { 2., 4. }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{0.,-1.}} },                {0.,6.},    {2.1,8.1}   },  { 2.102635, 3.997365 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{0.,-1.}} },                {0.,6.},    {2.5,8.5}   },  { 2.588562, 3.911438 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{0.,-1.}} },                {0.,6.},    {2.8,8.8}   },  { 3.2, 3.6 }, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{0.,-1.}} },                {0.,6.},    {2.9,8.9}   },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{0.,-1.}} },                {0.,6.},    {3.,9.}     },  {}, },
        //+ { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {0.,6.},    {0.,6.}     },  {{0.,_2sqrt2}, {0.,_2sqrt2}}, },
        //+ { { { 1., 1., L{{-2.,0.}, Vec{1.,0.}} },        { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {0.,6.},    {0.,6.}     },  {{0.,_2sqrt2-2}, {0.,4.732051}}, },
        //+ { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {-2.,4.},   {0.,6.}     },  {{-2.,_2sqrt2}, {0.,_2sqrt2-2}}, },
        //+ { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {0.,6.},    {1.,7.}     },  {{0.,_2sqrt2+1}, {1.,_2sqrt2}}, },
        //+ { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {0.,6.},    {2.,8.}     },  {{0.,_2sqrt2+2}, {2.,_2sqrt2}}, },
        //+ { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {0.,6.},    {2.1,8.1}   },  {{0.,_2sqrt2+2.1f}, {2.1,_2sqrt2}}, },
        //+ { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {0.,6.},    {2.5,8.5}   },  {{0.,_2sqrt2+2.5f}, {2.5,_2sqrt2}}, },
        //+ { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {0.,6.},    {2.8,8.8}   },  {{0.,_2sqrt2+2.8f}, {2.8,_2sqrt2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {0.,6.},    {2.9,8.9}   },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {0.,6.},    {3.,9.}     },  {}, },
        { { { 1., 1., L{{_2sqrt2,0.}, Vec{1.,0.}} },    { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {0.,6.},    {0.,6.}     },  {}, },
        //+ { { { 1., 1., L{{_2sqrt2,0.}, Vec{1.,0.}} },    { 1., 1., L{{3.,2.99}, Vec{0.,-1.}} },              {0.,6.},    {0.,6.}     },  {{0.,4.982627}, {0.,1e-2}}, },
        //+ { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{2.,2.}, Vec{-1.,0.}} },                {0.,6.},    {0.,6.}     },  {{0.,_2sqrt2}, {0.,_2sqrt2}}, },
        //+ { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{2.,2.}, Vec{-1.,0.}} },                {0.,6.},    {1.,7.}     },  {{0.,_2sqrt2+1}, {1.,_2sqrt2}}, },
        //+ { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{2.,2.}, Vec{-1.,0.}} },                {0.,6.},    {2.,8.}     },  {{0.,_2sqrt2+2}, {2.,_2sqrt2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{2.,2.}, Vec{-1.,0.}} },                {0.,6.},    {3.,9.}     },  {}, },
        //+ { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{3.,3.}, Vec{-1.,0.}} },                {0.,6.},    {0.,6.}     },  {{0.,_2sqrt2}, {0.,_2sqrt2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{0.,1.}} },                 {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,1.}} },                 {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{2.,2.}, Vec{1.,0.}} },                 {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{3.,3.}, Vec{1.,0.}} },                 {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,2.}, Vec{0.,1.}} },             {0.,6.},    {0.,6.}     },  {}, },
        //+ { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,2.}, Vec{1.,0.}} },             {0.,6.},    {0.,6.}     },  {{0.,1.336357}, {0.,2.993212}}, },
        //+ { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,2.}, Vec{-1.,0.}} },            {0.,6.},    {0.,6.}     },  {{0.,0.397825}, {0.,_2sqrt2}}, },
        //+ { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,2.}, Vec{_sqrt2_2,-_sqrt2_2}} }, {0.,6.},   {0.,6.}     },  {{0.,_2sqrt2}, {0.,_2sqrt2}}, },
        //+ { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,2.}, Vec{-_sqrt2_2,-_sqrt2_2}} }, {0.,6.},  {0.,6.}     },  {{0.,_2sqrt2}, {0.,_2sqrt2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,3.}, Vec{0.,1.}} },             {0.,6.},    {0.,6.}     },  {}, },
        //+ { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,3.}, Vec{1.,0.}} },             {0.,6.},    {0.,6.}     },  {{0.,0.922144}, {0.,3.407425}}, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,3.}, Vec{-1.,0.}} },            {0.,6.},    {0.,6.}     },  {}, },
        //+ { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,3.}, Vec{_sqrt2_2,-_sqrt2_2}} }, {0.,6.},   {0.,6.}     },  {{0.,_2sqrt2}, {0.,_2sqrt2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,3.}, Vec{-_sqrt2_2,-_sqrt2_2}} }, {0.,6.},  {0.,6.}     },  {}, },
        //+ { { { 1., 1., L{{-sqrt2,-sqrt2}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,0.1}, Vec{0.,1.}} },    {0.,6.},    {0.,6.}     },  {{0.,0.064784}, {0.,4.069460}}, },
        //+ { { { 1., 1., L{{-sqrt2,-sqrt2}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,-1e-3}, Vec{0.,1.}} },  {0.,6.},    {0.,6.}     },  {{0.,0.165784}, {0.,3.999293}}, },
        //+ { { { 1., 2., L{{0.,0.}, Vec{2.,0.}} },         { 1., 1., L{{3.,0.}, Vec{1.,0.}} },                 {0.,6.},    {0.,6.}     },  {{0.,2.5}, {0.,2.5}}, },
        //+ { { { 1., 2., L{{0.,0.}, Vec{2.,0.}} },         { 1., 1., L{{3.,0.}, Vec{1.,0.}} },                 {1.,5.},    {0.,6.}     },  {{1.,2.5}, {0.,3.5}}, },
        { { { 1., 2., L{{0.,0.}, Vec{2.,0.}} },         { 1., 1., L{{3.,0.}, Vec{1.,0.}} },                 {2.,5.},    {0.,6.}     },  {}, },
        //+ { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,3.},    {0.,3.}     },  {{1.,3.}, {0.,2.}}, },
        //+ { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,3.},    {1.,4.}     },  {{2.,3.}, {1.,2.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,3.},    {2.,5.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,3.},    {3.,6.}     },  {}, },
        //+ { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,100.},  {0.,3.}     },  {{1.,3.}, {0.,99.}}, },
        //+ { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {1.,100.},  {0.,3.}     },  {{1.,3.}, {0.,99.}}, },
        //+ { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {2.,100.},  {0.,3.}     },  {{2.,3.}, {0.,99.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {3.,100.},  {0.,3.}     },  {}, },
        //+ { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,100.},  {1.,4.}     },  {{2.,4.}, {1.,99.}}, },
        //+ { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,100.},  {2.,5.}     },  {{3.,5.}, {2.,99.}}, },
        //+ { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,100.},  {3.,6.}     },  {{4.,6.}, {3.,99.}}, },
        //+ { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,inf},   {0.,3.}     },  {{1.,3.}, {0.,2.}}, },
        //+ { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {1.,inf},   {0.,3.}     },  {{1.,3.}, {0.,2.}}, },
        //+ { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {2.,inf},   {0.,3.}     },  {{2.,3.}, {0.,2.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {3.,inf},   {0.,3.}     },  {}, },
        //+ { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,inf},   {1.,4.}     },  {{2.,4.}, {1.,3.}}, },
        //+ { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,inf},   {2.,5.}     },  {{3.,5.}, {2.,4.}}, },
        //+ { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,inf},   {3.,6.}     },  {{4.,6.}, {3.,5.}}, },
        //+ { { { .5, 1., I{{1.,0.}} },                     { .5, 1., L{{2.,0.}, Vec{-1.,0.}} },                {1.,1.5},   {1.,1.5}    },  {{1.,1.5}, {1.,1.5}}, },
        { { { .5, 1., I{{1.,0.}} },                     { .5, 1., L{{2.,0.}, Vec{-1.,0.}} },                {1.,1.5},   {2.,3.}     },  {}, },
        { { { .5, 1., I{{1.,0.}} },                     { .5, 1., L{{2.,0.}, Vec{-1.,0.}} },                {1.,1.5},   {1.5,2.5}   },  {}, },
        { { { .5, 1., L{{0.,0.}, Vec{1.,0.}} },         { .5, 1., L{{2.,0.}, Vec{-1.,0.}} },                {0.,1.},    {1.,2.}     },  {}, },
        //+ { { { .5, 1., L{{0.,0.}, Vec{1.,0.}} },         { .5, 1., L{{1.,0.}, Vec{1.,0.}} },                 {0.,1.},    {0.4,1.4}   },  {{0.,0.4}, {0.4,1.}}, },
        { { { 1., 1., P{{0.,0.}, Coord{2.,0.}} },       { 1., 1., P{{2.,4.}, Coord{0.,4.}} },               {0.,2.957886}, {0.,2.957886} }, {}, },
        { { { 1., 1., P{{0.,1.}, Coord{2.,1.}} },       { 1., 1., P{{2.,4.}, Coord{0.,4.}} },               {0.,2.957886}, {0.,2.957886} }, { 0.638576, 2.319310 }, },
        { { { 1., 1., P{{0.,0.5}, Coord{2.,0.5}} },     { 1., 1., P{{2.,4.}, Coord{0.,4.}} },               {0.,2.957886}, {0.,2.957886} }, { 0.879627, 2.078259 }, },
        { { { 1., 1., P{{0.,0.1}, Coord{2.,0.1}} },     { 1., 1., P{{2.,4.}, Coord{0.,4.}} },               {0.,2.957886}, {0.,2.957886} }, { 1.209306, 1.748580 }, },
        { { { 1., 1., P{{0.,0.}, Coord{2.,0.}} },       { 1., 1., P{{2.,4.}, Coord{0.,4.}} },               {0.,2.957886}, {1.,3.957886} }, {}, },
        { { { 1., 1., P{{0.,1.}, Coord{2.,1.}} },       { 1., 1., P{{2.,4.}, Coord{0.,4.}} },               {0.,2.957886}, {1.,3.957886} }, { 1.237853, 2.720032 }, },
        { { { 1., 1., P{{0.,0.5}, Coord{2.,0.5}} },     { 1., 1., P{{2.,4.}, Coord{0.,4.}} },               {0.,2.957886}, {1.,3.957886} }, { 1.535837, 2.422049 }, },
        { { { 1., 1., P{{0.,0.1}, Coord{2.,0.1}} },     { 1., 1., P{{2.,4.}, Coord{0.,4.}} },               {0.,2.957886}, {1.,3.957886} }, {}, },
    });

    ////////////////////////////////////////////////////////////////

    //| results if "point collision" was also considered as collision
    //* results if waiting of an agent in the place is also considered as conflict
    const String unsafe_interval_msg = "unsafe intervals of two agents";
    Suite(unsafe_interval_msg).test<Unsafe_interval_case>({
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,0.}} },                             {0.,6.},    {6.,12.}    },  {}, },  //| ignore
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,0.}} },                             {0.,6.},    {6.1,12.1}  },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{1.99,0.}} },                           {0.,6.},    {6.,12.}    },  {}, },  //| ignore
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{1.99,0.}} },                           {0.,6.},    {6.1,12.1}  },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,1.99}} },                           {0.,6.},    {6.,12.}    },  {}, },  //| ignore
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,1.99}} },                           {0.,6.},    {6.1,12.1}  },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{2.,0.}} },                             {0.,6.},    {0.,6.}     },  {}, },  //| ignore  //* {{6., 6.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{2.,0.}} },                             {0.,6.},    {3.,9.}     },  {}, },  //| ignore  //* {{6., 6.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{2.,0.}} },                             {0.,6.},    {6.,12.}    },  {}, },  //| ignore  //* {{6., 6.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{2.,0.}} },                             {0.,6.},    {6.1,12.1}  },  {}, },  //| ignore  //* {{6., 6.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,2.}} },                             {0.,6.},    {0.,6.}     },  {}, },  //| ignore  //* {{6., 6.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,2.}} },                             {0.,6.},    {3.,9.}     },  {}, },  //| ignore  //* {{6., 6.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,2.}} },                             {0.,6.},    {6.,12.}    },  {}, },  //| ignore  //* {{6., 6.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,2.}} },                             {0.,6.},    {6.1,12.1}  },  {}, },  //| ignore  //* {{6., 6.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{3.,0.}} },                             {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{3.,0.}} },                             {0.,6.},    {3.,9.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{3.,0.}} },                             {0.,6.},    {6.,12.}    },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{3.,0.}} },                             {0.,6.},    {6.1,12.1}  },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{2.1,0.}} },                            {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{2.1,0.}} },                            {0.,6.},    {3.,9.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{2.1,0.}} },                            {0.,6.},    {6.,12.}    },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{2.1,0.}} },                            {0.,6.},    {6.1,12.1}  },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,3.}} },                             {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,3.}} },                             {0.,6.},    {3.,9.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,3.}} },                             {0.,6.},    {6.,12.}    },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,3.}} },                             {0.,6.},    {6.1,12.1}  },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,2.1}} },                            {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,2.1}} },                            {0.,6.},    {3.,9.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,2.1}} },                            {0.,6.},    {6.,12.}    },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,2.1}} },                            {0.,6.},    {6.1,12.1}  },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Vec{1.,0.}} },                 {0.,6.},    {0.,6.}     },  {}, },  //| ignore  //* {{6., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Vec{-1.,0.}} },                {0.,6.},    {0.,6.}     },  {{0.,4.}, {0.,4.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Vec{0.,1.}} },                 {0.,6.},    {0.,6.}     },  {}, },  //| ignore  //* {{6., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Vec{0.,-1.}} },                {0.,6.},    {0.,6.}     },  {{0.,4.}, {0.,4.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Vec{-1.,0.}} },                {0.,6.},    {1.,7.}     },  {{0.,5.}, {1.,4.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Vec{0.,-1.}} },                {0.,6.},    {1.,7.}     },  {{0.,5.}, {1.,4.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Vec{0.,-1.}} },                {1.,7.},    {0.,6.}     },  {{1.,4.}, {0.,5.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Vec{-1.,0.}} },                {1.,7.},    {0.,6.}     },  {{1.,4.}, {0.,5.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Vec{-1.,0.}} },                {1.,7.},    {1.,7.}     },  {{1.,5.}, {1.,5.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Vec{0.,-1.}} },                {1.,7.},    {1.,7.}     },  {{1.,5.}, {1.,5.}}, },
        { { { 1., 1., L{{0.,0.}, Coord{6.,0.}} },       { 1., 1., L{{2.,0.}, Vec{1.,0.}} },                 {0.,6.},    {0.,6.}     },  {}, },  //| ignore  //* {{6., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Coord{6.,0.}} },       { 1., 1., L{{2.,0.}, Vec{-1.,0.}} },                {0.,6.},    {0.,6.}     },  {{0.,4.}, {0.,4.}}, },
        { { { 1., 1., L{{0.,0.}, Coord{0.,6.}} },       { 1., 1., L{{0.,2.}, Vec{0.,1.}} },                 {0.,6.},    {0.,6.}     },  {}, },  //| ignore  //* {{6., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Coord{0.,6.}} },       { 1., 1., L{{0.,2.}, Vec{0.,-1.}} },                {0.,6.},    {0.,6.}     },  {{0.,4.}, {0.,4.}}, },
        { { { 1., 1., L{{0.,0.}, Coord{6.,0.}} },       { 1., 1., L{{2.,0.}, Vec{-1.,0.}} },                {0.,6.},    {1.,7.}     },  {{0.,5.}, {1.,4.}}, },
        { { { 1., 1., L{{0.,0.}, Coord{0.,6.}} },       { 1., 1., L{{0.,2.}, Vec{0.,-1.}} },                {0.,6.},    {1.,7.}     },  {{0.,5.}, {1.,4.}}, },
        { { { 1., 1., L{{0.,0.}, Coord{0.,6.}} },       { 1., 1., L{{0.,2.}, Vec{0.,-1.}} },                {1.,7.},    {0.,6.}     },  {{1.,4.}, {0.,5.}}, },
        { { { 1., 1., L{{0.,0.}, Coord{6.,0.}} },       { 1., 1., L{{2.,0.}, Vec{-1.,0.}} },                {1.,7.},    {0.,6.}     },  {{1.,4.}, {0.,5.}}, },
        { { { 1., 1., L{{0.,0.}, Coord{6.,0.}} },       { 1., 1., L{{2.,0.}, Vec{-1.,0.}} },                {1.,7.},    {1.,7.}     },  {{1.,5.}, {1.,5.}}, },
        { { { 1., 1., L{{0.,0.}, Coord{0.,6.}} },       { 1., 1., L{{0.,2.}, Vec{0.,-1.}} },                {1.,7.},    {1.,7.}     },  {{1.,5.}, {1.,5.}}, },
        { { { 1., 1., L{{0.,0.}, Coord{6.,0.}} },       { 1., 1., L{{2.,0.}, Coord{8.,0.}} },               {0.,6.},    {0.,6.}     },  {}, },  //| ignore  //* {{6., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Coord{6.,0.}} },       { 1., 1., L{{2.,0.}, Coord{-4.,0.}} },              {0.,6.},    {0.,6.}     },  {{0.,4.}, {0.,4.}}, },
        { { { 1., 1., L{{0.,0.}, Coord{0.,6.}} },       { 1., 1., L{{0.,2.}, Coord{0.,8.}} },               {0.,6.},    {0.,6.}     },  {}, },  //| ignore  //* {{6., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Coord{0.,6.}} },       { 1., 1., L{{0.,2.}, Coord{0.,-4.}} },              {0.,6.},    {0.,6.}     },  {{0.,4.}, {0.,4.}}, },
        { { { 1., 1., L{{0.,0.}, Coord{6.,0.}} },       { 1., 1., L{{2.,0.}, Coord{-4.,0.}} },              {0.,6.},    {1.,7.}     },  {{0.,5.}, {1.,4.}}, },
        { { { 1., 1., L{{0.,0.}, Coord{0.,6.}} },       { 1., 1., L{{0.,2.}, Coord{0.,-4.}} },              {0.,6.},    {1.,7.}     },  {{0.,5.}, {1.,4.}}, },
        { { { 1., 1., L{{0.,0.}, Coord{0.,6.}} },       { 1., 1., L{{0.,2.}, Coord{0.,-4.}} },              {1.,7.},    {0.,6.}     },  {{1.,4.}, {0.,5.}}, },
        { { { 1., 1., L{{0.,0.}, Coord{6.,0.}} },       { 1., 1., L{{2.,0.}, Coord{-4.,0.}} },              {1.,7.},    {0.,6.}     },  {{1.,4.}, {0.,5.}}, },
        { { { 1., 1., L{{0.,0.}, Coord{6.,0.}} },       { 1., 1., L{{2.,0.}, Coord{-4.,0.}} },              {1.,7.},    {1.,7.}     },  {{1.,5.}, {1.,5.}}, },
        { { { 1., 1., L{{0.,0.}, Coord{0.,6.}} },       { 1., 1., L{{0.,2.}, Coord{0.,-4.}} },              {1.,7.},    {1.,7.}     },  {{1.,5.}, {1.,5.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Coord{8.,0.}} },               {0.,6.},    {0.,6.}     },  {}, },  //| ignore  //* {{6., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Coord{-4.,0.}} },              {0.,6.},    {0.,6.}     },  {{0.,4.}, {0.,4.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Coord{0.,8.}} },               {0.,6.},    {0.,6.}     },  {}, },  //| ignore  //* {{6., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Coord{0.,-4.}} },              {0.,6.},    {0.,6.}     },  {{0.,4.}, {0.,4.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Coord{-4.,0.}} },              {0.,6.},    {1.,7.}     },  {{0.,5.}, {1.,4.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Coord{0.,-4.}} },              {0.,6.},    {1.,7.}     },  {{0.,5.}, {1.,4.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Coord{0.,-4.}} },              {1.,7.},    {0.,6.}     },  {{1.,4.}, {0.,5.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Coord{-4.,0.}} },              {1.,7.},    {0.,6.}     },  {{1.,4.}, {0.,5.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Coord{-4.,0.}} },              {1.,7.},    {1.,7.}     },  {{1.,5.}, {1.,5.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,2.}, Coord{0.,-4.}} },              {1.,7.},    {1.,7.}     },  {{1.,5.}, {1.,5.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{1.99,0.}, Vec{1.,0.}} },               {0.,6.},    {0.,6.}     },  {{0.,0.01}, {0.,3.99}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,1.99}, Vec{0.,1.}} },               {0.,6.},    {0.,6.}     },  {{0.,0.01}, {0.,3.99}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{1.99,0.}, Vec{1.,0.}} },               {0.,6.},    {1.,7.}     },  {{0.,1.01}, {1.,3.99}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,1.99}, Vec{0.,1.}} },               {0.,6.},    {1.,7.}     },  {{0.,1.01}, {1.,3.99}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{1.99,0.}, Vec{1.,0.}} },               {1.,7.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,1.99}, Vec{0.,1.}} },               {1.,7.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{1.99,0.}, Vec{1.,0.}} },               {1.,7.},    {1.,7.}     },  {{1.,1.01}, {1.,4.99}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{0.,1.99}, Vec{0.,1.}} },               {1.,7.},    {1.,7.}     },  {{1.,1.01}, {1.,4.99}}, },
        { { { 1., 1., L{{0.,0.}, Vec{-1.,0.}} },        { 1., 1., L{{1.99,0.}, Vec{-1.,0.}} },              {0.,6.},    {0.,6.}     },  {{0.,3.99}, {0.,0.01}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,-1.}} },        { 1., 1., L{{0.,1.99}, Vec{0.,-1.}} },              {0.,6.},    {0.,6.}     },  {{0.,3.99}, {0.,0.01}}, },
        { { { 1., 1., L{{0.,0.}, Vec{-1.,0.}} },        { 1., 1., L{{1.99,0.}, Vec{-1.,0.}} },              {0.,6.},    {1.,7.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,-1.}} },        { 1., 1., L{{0.,1.99}, Vec{0.,-1.}} },              {0.,6.},    {1.,7.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{-1.,0.}} },        { 1., 1., L{{1.99,0.}, Vec{-1.,0.}} },              {1.,7.},    {0.,6.}     },  {{1.,3.99}, {0.,1.01}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,-1.}} },        { 1., 1., L{{0.,1.99}, Vec{0.,-1.}} },              {1.,7.},    {0.,6.}     },  {{1.,3.99}, {0.,1.01}}, },
        { { { 1., 1., L{{0.,0.}, Vec{-1.,0.}} },        { 1., 1., L{{1.99,0.}, Vec{-1.,0.}} },              {1.,7.},    {1.,7.}     },  {{1.,4.99}, {1.,1.01}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,-1.}} },        { 1., 1., L{{0.,1.99}, Vec{0.,-1.}} },              {1.,7.},    {1.,7.}     },  {{1.,4.99}, {1.,1.01}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{-1.,0.}} },                {0.,6.},    {0.,6.}     },  {}, },  //| {{2., 2.}}  //* {{6., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{-1.,0.}} },                {0.,6.},    {1.,7.}     },  {}, },  //| {{3., 2.}}  //* {{7., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{-1.,0.}} },                {0.,6.},    {2.,8.}     },  {}, },  //| {{4., 2.}}  //* {{8., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{-1.,0.}} },                {0.,6.},    {3.,9.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,1.99}, Vec{-1.,0.}} },              {0.,6.},    {0.,6.}     },  {{0.,2.19975}, {0.,2.19975}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,1.99}, Vec{-1.,0.}} },              {0.,6.},    {1.,7.}     },  {{0.,3.19975}, {1.,2.19975}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,1.99}, Vec{-1.,0.}} },              {0.,6.},    {2.,8.}     },  {{0.,4.19975}, {2.,2.19975}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,1.99}, Vec{-1.,0.}} },              {0.,6.},    {3.,9.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{1.,0.}} },                 {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{1.,sqrt3}, Vec{1.,0.}} },              {0.,6.},    {0.,6.}     },  {}, },  //| ignore  //* {{6., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{1.,sqrt3-1e-2}, Vec{1.,0.}} },         {0.,6.},    {0.,6.}     },  {{0., 0.017124}, {0., 2.017124}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{1.,sqrt3}, Vec{-1.,0.}} },             {0.,6.},    {0.,6.}     },  {{0.,2.}, {0.,2.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,sqrt3}, Vec{-1.,0.}} },             {0.,6.},    {0.,6.}     },  {{0.,3.}, {0.,3.}}, },  //* {{6., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{2.,2.}, Vec{0.,-1.}} },                {0.,6.},    {0.,6.}     },  {}, },  //| {{2., 2.}}  //* {{6., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{1.99,2.}, Vec{0.,-1.}} },              {0.,6.},    {0.,6.}     },  {{0.,2.19975}, {0.,2.19975}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{2.,2.}, Vec{0.,1.}} },                 {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt3,1.}, Vec{0.,1.}} },              {0.,6.},    {0.,6.}     },  {}, },  //| ignore  //* {{6., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt3-1e-2,1.}, Vec{0.,1.}} },         {0.,6.},    {0.,6.}     },  {{0., 0.017124}, {0., 2.017124}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt3,1.}, Vec{0.,-1.}} },             {0.,6.},    {0.,6.}     },  {{0.,2.}, {0.,2.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt3,2.}, Vec{0.,-1.}} },             {0.,6.},    {0.,6.}     },  {{0.,3.}, {0.,3.}}, },  //* {{6., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{sqrt2,sqrt2}, Vec{1.,0.}} },           {0.,6.},    {0.,6.}     },  {}, },  //| ignore  //* {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt2,sqrt2}, Vec{1.,0.}} },           {0.,6.},    {0.,6.}     },  {}, },  //| ignore
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{sqrt2-1e-2,sqrt2}, Vec{1.,0.}} },      {0.,6.},    {0.,6.}     },  {{0.,1e-2}, {0.,_2sqrt2-1e-2f}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt2-1e-2,sqrt2}, Vec{1.,0.}} },      {0.,6.},    {0.,6.}     },  {{0.,1e-2}, {0.,2.838357}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{sqrt2,sqrt2}, Vec{0.,1.}} },           {0.,6.},    {0.,6.}     },  {}, },  //| ignore
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{sqrt2-1e-2,sqrt2}, Vec{0.,1.}} },      {0.,6.},    {0.,6.}     },  {{0.,0.00993}, {0.,_2sqrt2-1e-2f}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{sqrt2,sqrt2}, Vec{-1.,0.}} },          {0.,6.},    {0.,6.}     },  {{0.,_2sqrt2}, {0., _2sqrt2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{sqrt2,sqrt2}, Vec{-1.,0.}} },          {0.,6.},    {0.,6.}     },  {{0.,_2sqrt2}, {0., _2sqrt2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{sqrt2,sqrt2}, Vec{0.,-1.}} },          {0.,6.},    {0.,6.}     },  {{0.,_2sqrt2}, {0., _2sqrt2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{0.,-1.}} },                {0.,6.},    {0.,6.}     },  {{0.,_2sqrt2}, {0., _2sqrt2}}, },    //* {{6., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{0.,-1.}} },                {0.,6.},    {1.,7.}     },  {{0.,_2sqrt2+1}, {1., _2sqrt2}}, },  //* {{7., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{0.,-1.}} },                {0.,6.},    {2.,8.}     },  {{0.,_2sqrt2+2}, {2., _2sqrt2}}, },  //* {{8., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{0.,-1.}} },                {0.,6.},    {2.1,8.1}   },  {{0.,_2sqrt2+2.1f}, {2.1, _2sqrt2}}, },    //* {{8.1, _2sqrt2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{0.,-1.}} },                {0.,6.},    {2.5,8.5}   },  {{0.,_2sqrt2+2.5f}, {2.5, _2sqrt2}}, },    //* {{8.5, _2sqrt2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{0.,-1.}} },                {0.,6.},    {2.8,8.8}   },  {{0.,_2sqrt2+2.8f}, {2.8, _2sqrt2}}, },    //* {{8.8, _2sqrt2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{0.,-1.}} },                {0.,6.},    {2.9,8.9}   },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{0.,-1.}} },                {0.,6.},    {3.,9.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {0.,6.},    {0.,6.}     },  {{0.,_2sqrt2}, {0.,_2sqrt2}}, },
        { { { 1., 1., L{{-2.,0.}, Vec{1.,0.}} },        { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {0.,6.},    {0.,6.}     },  {{0.,_2sqrt2-2}, {0.,4.732051}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {-2.,4.},   {0.,6.}     },  {{-2.,_2sqrt2}, {0.,_2sqrt2-2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {0.,6.},    {1.,7.}     },  {{0.,_2sqrt2+1}, {1.,_2sqrt2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {0.,6.},    {2.,8.}     },  {{0.,_2sqrt2+2}, {2.,_2sqrt2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {0.,6.},    {2.1,8.1}   },  {{0.,_2sqrt2+2.1f}, {2.1,_2sqrt2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {0.,6.},    {2.5,8.5}   },  {{0.,_2sqrt2+2.5f}, {2.5,_2sqrt2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {0.,6.},    {2.8,8.8}   },  {{0.,_2sqrt2+2.8f}, {2.8,_2sqrt2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {0.,6.},    {2.9,8.9}   },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {0.,6.},    {3.,9.}     },  {}, },
        { { { 1., 1., L{{_2sqrt2,0.}, Vec{1.,0.}} },    { 1., 1., L{{3.,3.}, Vec{0.,-1.}} },                {0.,6.},    {0.,6.}     },  {}, },   //| {{4.992627, 0.}}    //* {{6., 0.}} },
        { { { 1., 1., L{{_2sqrt2,0.}, Vec{1.,0.}} },    { 1., 1., L{{3.,2.99}, Vec{0.,-1.}} },              {0.,6.},    {0.,6.}     },  {{0.,4.982627}, {0.,1e-2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{2.,2.}, Vec{-1.,0.}} },                {0.,6.},    {0.,6.}     },  {{0.,_2sqrt2}, {0.,_2sqrt2}}, },    //* {{6., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{2.,2.}, Vec{-1.,0.}} },                {0.,6.},    {1.,7.}     },  {{0.,_2sqrt2+1}, {1.,_2sqrt2}}, },  //* {{7., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{2.,2.}, Vec{-1.,0.}} },                {0.,6.},    {2.,8.}     },  {{0.,_2sqrt2+2}, {2.,_2sqrt2}}, },  //* {{8., 6.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{2.,2.}, Vec{-1.,0.}} },                {0.,6.},    {3.,9.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{3.,3.}, Vec{-1.,0.}} },                {0.,6.},    {0.,6.}     },  {{0.,_2sqrt2}, {0.,_2sqrt2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,2.}, Vec{0.,1.}} },                 {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{3.,3.}, Vec{0.,1.}} },                 {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{2.,2.}, Vec{1.,0.}} },                 {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{0.,1.}} },         { 1., 1., L{{3.,3.}, Vec{1.,0.}} },                 {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,2.}, Vec{0.,1.}} },             {0.,6.},    {0.,6.}     },  {}, },   //| ignore //* {{0.164784, 4.164784}}, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,2.}, Vec{1.,0.}} },             {0.,6.},    {0.,6.}     },  {{0.,1.336357}, {0.,2.993212}}, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,2.}, Vec{-1.,0.}} },            {0.,6.},    {0.,6.}     },  {{0.,0.397825}, {0.,_2sqrt2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,2.}, Vec{_sqrt2_2,-_sqrt2_2}} }, {0.,6.},   {0.,6.}     },  {{0.,_2sqrt2}, {0.,_2sqrt2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,2.}, Vec{-_sqrt2_2,-_sqrt2_2}} }, {0.,6.},  {0.,6.}     },  {{0.,_2sqrt2}, {0.,_2sqrt2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,3.}, Vec{0.,1.}} },             {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,3.}, Vec{1.,0.}} },             {0.,6.},    {0.,6.}     },  {{0.,0.922144}, {0.,3.407425}}, },  //? 3.324
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,3.}, Vec{-1.,0.}} },            {0.,6.},    {0.,6.}     },  {}, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,3.}, Vec{_sqrt2_2,-_sqrt2_2}} }, {0.,6.},   {0.,6.}     },  {{0.,_2sqrt2}, {0.,_2sqrt2}}, },
        { { { 1., 1., L{{0.,0.}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,3.}, Vec{-_sqrt2_2,-_sqrt2_2}} }, {0.,6.},  {0.,6.}     },  {}, },
        { { { 1., 1., L{{-sqrt2,-sqrt2}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,0.1}, Vec{0.,1.}} },    {0.,6.},    {0.,6.}     },  {{0.,0.064784}, {0.,4.069460}}, },  //* {{0.064784, 6.}}, }, //? 2.928
        { { { 1., 1., L{{-sqrt2,-sqrt2}, Vec{_sqrt2_2,_sqrt2_2}} }, { 1., 1., L{{0.,-1e-3}, Vec{0.,1.}} },  {0.,6.},    {0.,6.}     },  {{0.,0.165784}, {0.,3.999293}}, },  //? {{0.,0.164784}, {0.,4.164784}}, },
        { { { 1., 2., L{{0.,0.}, Vec{2.,0.}} },         { 1., 1., L{{3.,0.}, Vec{1.,0.}} },                 {0.,6.},    {0.,6.}     },  {{0.,2.5}, {0.,2.5}}, }, //* {{2.5, 6.}}, },
        { { { 1., 2., L{{0.,0.}, Vec{2.,0.}} },         { 1., 1., L{{3.,0.}, Vec{1.,0.}} },                 {1.,5.},    {0.,6.}     },  {{1.,2.5}, {0.,3.5}}, }, //* {{2.5, 5.}}, },
        { { { 1., 2., L{{0.,0.}, Vec{2.,0.}} },         { 1., 1., L{{3.,0.}, Vec{1.,0.}} },                 {2.,5.},    {0.,6.}     },  {}, },  //| {{2., 4.5}}    //* {{2., 5.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,3.},    {0.,3.}     },  {{1.,3.}, {0.,2.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,3.},    {1.,4.}     },  {{2.,3.}, {1.,2.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,3.},    {2.,5.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,3.},    {3.,6.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,100.},  {0.,3.}     },  {{1.,3.}, {0.,99.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {1.,100.},  {0.,3.}     },  {{1.,3.}, {0.,99.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {2.,100.},  {0.,3.}     },  {{2.,3.}, {0.,99.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {3.,100.},  {0.,3.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,100.},  {1.,4.}     },  {{2.,4.}, {1.,99.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,100.},  {2.,5.}     },  {{3.,5.}, {2.,99.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,100.},  {3.,6.}     },  {{4.,6.}, {3.,99.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,inf},   {0.,3.}     },  {{1.,3.}, {0.,2.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {1.,inf},   {0.,3.}     },  {{1.,3.}, {0.,2.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {2.,inf},   {0.,3.}     },  {{2.,3.}, {0.,2.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {3.,inf},   {0.,3.}     },  {}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,inf},   {1.,4.}     },  {{2.,4.}, {1.,3.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,inf},   {2.,5.}     },  {{3.,5.}, {2.,4.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., L{{3.,0.}, Vec{-1.,0.}} },                {0.,inf},   {3.,6.}     },  {{4.,6.}, {3.,5.}}, },
        { { { .5, 1., I{{1.,0.}} },                     { .5, 1., L{{2.,0.}, Vec{-1.,0.}} },                {1.,1.5},   {1.,1.5}    },  {{1.,1.5}, {1.,1.5}}, },
        { { { .5, 1., I{{1.,0.}} },                     { .5, 1., L{{2.,0.}, Vec{-1.,0.}} },                {1.,1.5},   {2.,3.}     },  {}, },
        { { { .5, 1., I{{1.,0.}} },                     { .5, 1., L{{2.,0.}, Vec{-1.,0.}} },                {1.,1.5},   {1.5,2.5}   },  {}, },
        { { { .5, 1., L{{0.,0.}, Vec{1.,0.}} },         { .5, 1., L{{2.,0.}, Vec{-1.,0.}} },                {0.,1.},    {1.,2.}     },  {}, },
        { { { .5, 1., L{{0.,0.}, Vec{1.,0.}} },         { .5, 1., L{{1.,0.}, Vec{1.,0.}} },                 {0.,1.},    {0.4,1.4}   },  {{0.,0.4}, {0.4,1.}}, },
        { { { 1., 1., L{{2.,0.}, Coord{4.,0.}} },       { 1., 1., L{{4.,1.}, Coord{2.,1.}} },               {0.,2.},    {0.,2.}     },  {{0.,2.}, {0.,2.}}, },
        { { { 1., 1., P{{2.,0.}, Coord{4.,0.}} },       { 1., 1., L{{4.,1.}, Coord{0.,1.}} },               {0.,2.957886}, {0.,4.}  },  {{0.,3.732051}, {0.,2.957886}}, },
        { { { .5, 1., L{{2.,0.}, Coord{4.,0.}} },       { .5, 1., L{{4.,1.}, Coord{2.,1.}} },               {0.,2.},    {0.,2.}     },  {}, },
        { { { .5, 1., P{{2.,0.}, Coord{4.,0.}} },       { .5, 1., L{{4.,1.}, Coord{0.,1.}} },               {0.,2.295587}, {0.,4.}  },  {{0.,2.212611}, {0.,2.295587}}, },
        // grid_04x04_k4&k4_B
        { { { .5, 1., L{{0.,0.}, Coord{1.,-1.}} },      { .5, 1., L{{2.,0.}, Coord{0.,0.}} },               {0.,sqrt2}, {0.,2.}     },  {{0.,2.}, {0.,0.613126}}, },
        { { { .5, 1., L{{0.,0.}, Coord{2.,1.}} },       { .5, 1., L{{0.,1.}, Coord{-1.,1.}} },              {0.,sqrt(5)}, {0.,1.}   },  {{0.,0.116434}, {0.,0.894427}}, },
        // grid_04x04_k4&k5_D
        { { { .5, 1., L{{0.,0.}, Coord{2.,2.}} },       { .5, 1., L{{-1.,2.}, Coord{1.,1.}} },              {0.,_2sqrt2}, {0.,sqrt(5)} }, {{0.,1.821854}, {0.,0.410823}}, },
        { { { .5, 1., L{{0.,0.}, Coord{0.,-2.}} },      { .5, 1., L{{1.,-1.}, Coord{-1.,-2.}} },            {0.,2.}, {0.,sqrt(5)} }, {{0.,0.793604}, {0.,1.552786}}, },
        // the resulting upper bound of unsafe interval of agent 1 is *not* related to its time upper bound
        { { { .5, 1., L{{1.,0.}, Coord{2.,0.}} },       { .5, 1., L{{2.,0.}, Coord{2.,1.}} },               {0.,1.},    {2-sqrt2,3-sqrt2} },  {{0.,1.}, {2-sqrt2,1.}}, },
        { { { .5, 1., L{{1.,0.}, Coord{2.,0.}} },       { .5, .5, L{{2.,0.}, Coord{2.,1.}} },               {0.,1.},    {2-sqrt2,4-sqrt2} },  {{0.,1.821854f}, {2-sqrt2,1.}}, },
        // From Surynek MAPF_R paper
        { { { .5, 1., L{{3.,3.}, Coord{5.,1.}} },       { .5, 1., L{{3.,1.}, Coord{6.,5.}} },               {2.,2+_2sqrt2}, {2.,7.} },  {{2.,3.742636}, {2.,3.309859}}, },
        { { { .5, 1., L{{3.,3.}, Coord{5.,1.}} },       { .5, 1., L{{3.,1.}, Coord{6.,5.}} },               {6-_2sqrt2,6.}, {2.,7.} },  {{6-_2sqrt2,3.742636}, {2.,3.309859f+4-_2sqrt2}}, },
        { { { .5, 1., L{{3.,5.}, Coord{3.,3.}} },       { .5, 1., L{{3.,3.}, Coord{5.,1.}} },               {_2sqrt2,2+_2sqrt2}, {3.742636,3.742636f+_2sqrt2} },  {}, },
        // .. parabolic version
        { { { .5, 1., P{{3.,3.}, Coord{5.,1.}}, -.5},   { .5, 1., P{{3.,1.}, Coord{6.,5.}}, -.5},           {2.,5.049}, {2.,7.13} },    {{2.,2.716265}, {2.,4.017358}}, },
        { { { .5, 1., L{{5.,1.}, Coord{7.,1.}} },       { .5, 1., L{{3.,3.}, Coord{5.,1.}} },               {5.76527,7.76527}, {4.,4.+_2sqrt2} },  {{5.76527,6.828427}, {4.,4.019235}}, },
        // .. and one of the agents is reversed
        { { { .5, 1., P{{3.,3.}, Coord{3.,5.}}, -.5 },  { .5, 1., L{{1.,3.}, Coord{3.,3.}} },               {_2sqrt2,_2sqrt2+2.295587}, {1.,3.} },  {{_2sqrt2,3.}, {1.,1.897105}}, },
        { { { .5, 1., P{{3.,3.}, Coord{3.,5.}}, -.5 },  { .5, 1., P{{1.,3.}, Coord{3.,3.}}, -.5 },          {_2sqrt2,_2sqrt2+2.295587}, {1.,3.295587} },  {{_2sqrt2,3.295587}, {1.,1.532328}}, },
        { { { .5, 1., P{{5.,1.}, Coord{3.,3.}}, -.5},   { .5, 1., L{{3.,1.}, Coord{6.,5.}} },               {2.,5.049}, {2.,7.} },      {{2.,3.458187}, {2.,3.475351}}, },
        { { { .5, 1., L{{3.,3.}, Coord{1.,3.}} },       { .5, 1., L{{3.,1.}, Coord{3.,3.}} },               {5.049,7.049}, {4.,6.} },   {{5.049,6.}, {4.,4.463214}}, },
        { { { .5, 1., P{{5.,1.}, Coord{3.,3.}}, -.5},   { .5, 1., P{{3.,1.}, Coord{6.,5.}}, -.5 },          {2.,5.049}, {2.,7.13} },    {{2.,3.869213}, {2.,2.969244}}, },
        { { { .5, 1., P{{3.,5.}, Coord{3.,3.}} },       { .5, 1., L{{3.,3.}, Coord{1.,3.}} },               {3.,5.295587}, {5.049,7.049} },  {{3.,3.822091}, {5.049,5.295587}}, },
        { { { .5, 1., P{{3.,5.}, Coord{3.,3.}} },       { .5, 1., P{{3.,3.}, Coord{1.,3.}} },               {3.,5.295587}, {2+_2sqrt2,2+_2sqrt2+2.295587} },  {{3.,3.532328}, {2+_2sqrt2,5.295587}}, },
        // From Surynek boOX
        { { { .25, 1., L{{0.,0.}, Vec{1.,0.}} },        { .25, 1., I{{1.,.25}} },                           {0.,2.}, {.999,1.5} },      {{0.,0.933013}, {.999,1.433012}}, },  //* 2
        { { { .25, 1., L{{0.,0.}, Vec{1.,0.}} },        { .25, 1., I{{1.,.25}} },                           {1.,3.}, {1.999,2.001} },   {{1.,1.434012}, {1.999,2.001}}, }, //? 2.433012
        { { { .25, 1., L{{0.,1.}, Vec{1.,0.}} },        { .25, 1., L{{1.,0.}, Vec{0.,1.}} },                {0.,2.}, {0.,2.} },         {{0.,_sqrt2_2}, {0.,_sqrt2_2}}, },
        { { { .25, 1., L{{0.,1.}, Vec{1.,0.}} },        { .25, 1., L{{1.,0.}, Vec{0.,1.}} },                {_sqrt2_2,2+_sqrt2_2}, {0.,2.} },  {}, },   //| {{_sqrt2_2, sqrt2}}
        { { { .25, 1., L{{0.,1.}, Vec{1.,0.}} },        { .25, 1., L{{1.,0.}, Vec{0.,1.}} },                {0.,2.}, {_sqrt2_2,2+_sqrt2_2} },  {}, },   //| {{sqrt2, _sqrt2_2}}
        { { { .25, 1., L{{0.,1.}, Vec{1.,0.}} },        { .25, 1., L{{0.99,0.}, Vec{0.,1.}} },              {_sqrt2_2,2+_sqrt2_2}, {0.,2.} },  {{_sqrt2_2,_sqrt2_2+1e-2f}, {0.,sqrt2-1e-2f}}, },
        { { { .25, 1., L{{0.,1.}, Vec{1.,0.}} },        { .25, 1., L{{0.99,0.}, Vec{0.,1.}} },              {0.,2.}, {_sqrt2_2,2+_sqrt2_2} },  {}, },
        { { { .25, 1., L{{0.,1.}, Vec{1.,0.}} },        { .25, 1., L{{1.,1e-2}, Vec{0.,1.}} },              {_sqrt2_2,2+_sqrt2_2}, {0.,2.} },  {}, },
        { { { .25, 1., L{{0.,1.}, Vec{1.,0.}} },        { .25, 1., L{{1.,1e-2}, Vec{0.,1.}} },              {0.,2.}, {_sqrt2_2,2+_sqrt2_2} },  {{0.,sqrt2-1e-2f}, {_sqrt2_2,_sqrt2_2+1e-2f}}, },
        { { { .2, 1., L{{0.,0.}, Vec{1.,0.}} },         { .2, 1., L{{2.,0.}, Vec{-1.,0.}} },                {0.,1.}, {0.,1.} },         {{0.,0.4}, {0.,0.4}}, },
        { { { .2, 1., L{{0.,0.}, Vec{1.,0.}} },         { .2, 1., L{{1.,0.}, Vec{1.,0.}} },                 {0.,1.}, {0.,1.} },         {}, },
        { { { .2, 1., L{{1.,0.}, Vec{-1.,0.}} },        { .2, 1., L{{1.,0.}, Vec{1.,0.}} },                 {0.,1.}, {0.,1.} },         {{0.,0.4}, {0.,0.4}}, },
        { { { .4, 1., L{{0.,0.}, Vec{0.,1.}} },         { .4, 1., L{{0.,1.}, Vec{1.,0.}} },                 {0.,1.}, {0.,1.} },         {{0.,0.131371}, {0.,1.}}, },
        { { { .4, 1., L{{0.,0.}, Vec{0.,1.}} },         { .4, 1., L{{0.,1.}, Vec{1.,0.}} },                 {0.132,1.132}, {0.,1.} },   {}, },
        { { { .4, 1., L{{0.,0.}, Vec{0.,1.}} },         { .4, 1.1, L{{0.,1.}, Vec{1.1,0.}} },               {0.9,2.7}, {0.,1.} },       {}, },
        { { { .4, 1., L{{1.,0.}, Vec{1.,0.}} },         { .4, 1.112, L{{0.,0.}, Vec{1.112,0.}} },           {1.,2.}, {0.899,1.799} },   {{1.,1.799}, {0.899,0.9008}}, },
        { { { .4, 1., L{{1.,1.}, Vec{1.,0.}} },         { .4, 1., I{{1.,1.}} },                             {1.,2.}, {1.799,2.} },      {{1.,2.}, {1.799,1.8}}, },
        { { { .4, 1., L{{0.,1.}, Vec{1.,0.}} },         { .4, 1., L{{2.,1.}, Vec{-1.,0.}} },                {0.,1.}, {0.,1.} },         {{0.,0.8}, {0.,0.8}}, },
        { { { .1, 1., L{{0.,0.}, Vec{0.,1.}} },         { .1, 1., L{{1.,0.}, Vec{-1.,0.}} },                {1.,2.}, {0.2,1.2} },       {{1.,1.2}, {0.2,sqrt2/5}}, },    //? 2.
        { { { .1, 1., L{{0.,1.}, Vec{0.,-1.}} },        { .1, 1., L{{0.,0.}, Vec{1.,0.}} },                 {0.,1.}, {0.,1.} },         {}, },
    });

    Suite(unsafe_interval_msg, true).test<Unsafe_interval_case>({
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{3.,0.}} },                             {0.,6.},    {0.,6.}     },  {{0.,6.}, {0.,6.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,0.}, Vec{-1.,0.}} },                {0.,6.},    {0.,6.}     },  {{1.,4.}, {1.,4.}}, },
        { { { 1., 1., L{{0.,0.}, Vec{1.,0.}} },         { 1., 1., L{{2.,sqrt3}, Vec{-1.,0.}} },             {0.,6.},    {0.,6.}     },  {{0.,2.}, {0.,2.}}, },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,0.}} },                             {0.,6.},    {0.,6.}     },  ignore },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,0.}} },                             {0.,6.},    {3.,9.}     },  ignore },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,0.}} },                             {1.,7.},    {0.,6.}     },  ignore },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,0.}} },                             {1.,7.},    {3.,9.}     },  ignore },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,0.}} },                             {1.,7.},    {6.,12.}    },  ignore },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,0.}} },                             {1.,7.},    {6.1,12.1}  },  ignore },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{1.99,0.}} },                           {0.,6.},    {0.,6.}     },  ignore },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{1.99,0.}} },                           {0.,6.},    {3.,9.}     },  ignore },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{1.99,0.}} },                           {1.,7.},    {0.,6.}     },  ignore },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{1.99,0.}} },                           {1.,7.},    {3.,9.}     },  ignore },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{1.99,0.}} },                           {1.,7.},    {6.,12.}    },  ignore },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{1.99,0.}} },                           {1.,7.},    {6.1,12.1}  },  ignore },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,1.99}} },                           {0.,6.},    {0.,6.}     },  ignore },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,1.99}} },                           {0.,6.},    {3.,9.}     },  ignore },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,1.99}} },                           {1.,7.},    {0.,6.}     },  ignore },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,1.99}} },                           {1.,7.},    {3.,9.}     },  ignore },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,1.99}} },                           {1.,7.},    {6.,12.}    },  ignore },
        { { { 1., 1., I{{0.,0.}} },                     { 1., 1., I{{0.,1.99}} },                           {1.,7.},    {6.1,12.1}  },  ignore },
    });

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const tomaqa::Error& e) {
    std::cout << e << std::endl;
    throw;
}
