#include "mapf_r/smt.hpp"

namespace mapf_r::smt {
    const char* to_sat_c_str(const Flag& flag)
    {
        switch (int(flag)) {
        case Flag::true_value: return sat_str;
        case Flag::false_value: return unsat_str;
        default: return unknown_str;
        }
    }

    String agent_expr_name(const Agent& ag)
    {
        return "A"s + to_string(ag.cid());
    }

    String make_var_name(const Agent& ag, String name)
    {
        return agent_expr_name(ag) + "." + move(name);
    }

    String make_var_name_at(String name, Idx k)
    {
        return move(name) + "<" + to_string(k) + ">";
    }

    String vertex_bool_name(const Vertex& vertex)
    {
        return vertex_expr_name() + to_string(vertex.cid());
    }
}
