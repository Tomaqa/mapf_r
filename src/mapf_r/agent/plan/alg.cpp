#include "mapf_r/agent/plan/alg.hpp"

namespace mapf_r::agent::plan {}

namespace mapf_r::graph {
    Properties make_properties(const agent::plan::Global_states& states_plan)
    {
        Coord min_{inf, inf};
        Coord max_ = -min_;
        for (auto& [_, states] : states_plan) {
            const Float rad = states.radius;
            for (auto& s : states) {
                for (auto pos : {s.cinit_pos(), s.cend_pos(), s.pos_at(s.cduration()/2)}) {
                    min_.x = min(min_.x, pos.x-rad);
                    min_.y = min(min_.y, pos.y-rad);
                    max_.x = max(max_.x, pos.x+rad);
                    max_.y = max(max_.y, pos.y+rad);
                }
            }
        }

        return {.min = min_, .max = max_};
    }
}
