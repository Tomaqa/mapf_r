#include "mapf_r/agent.hpp"
#include "mapf_r/agent/state.hpp"

namespace mapf_r::agent::state {
    Base::Base(Pos pos_)
        : _pos(pos_)
    { }

    Base::Base(Pos pos_, Float duration_)
        : _pos(pos_), _duration(duration_)
    { }

    void Base::reset_pos(Pos pos_) noexcept
    {
        pos() = pos_;
    }

    void Base::reset_t(Float t_) noexcept
    {
        t() = t_;
    }

    void Base::reset(Pos pos_, Float t_) noexcept
    {
        reset_pos(pos_);
        reset_t(t_);
    }

    bool Base::equals(const This& rhs) const noexcept
    {
        return cpos() == rhs.cpos()
            && cduration() == rhs.cduration()
            && ct() == rhs.ct();
    }

    String Base::to_string() const
    {
        using tomaqa::to_string;
        return to_string(cpos());
    }

    ////////////////////////////////////////////////////////////////

    V_base::V_base(const Parent& rhs)
        : Inherit(rhs), _init_pos(cpos())
    { }

    V_base::V_base(Parent&& rhs)
        : Inherit(move(rhs)), _init_pos(cpos())
    { }

    V_base::V_base(Pos pos_)
        : Inherit(pos_), _init_pos(pos_)
    { }

    V_base::V_base(Pos_pair from_to)
        : V_base(from_to.first, compute_vec(move(from_to)))
    { }

    V_base::V_base(Pos from, Pos to)
        : V_base(from, compute_vec(from, to))
    { }

    V_base::V_base(Pos from, Vec vec_)
        : Inherit(from), _init_pos(from), _vec(vec_)
    {
        check_vec();
    }

    bool V_base::valid_vec(Vec vec_) noexcept
    {
        return vec_ != zero_vec;
    }

    void V_base::check_vec(Vec vec_)
    {
        expect(valid_vec(vec_), "Entered zero vector into a moving agent state.");
    }

    bool V_base::valid_vec() const noexcept
    {
        return valid_vec(cvec());
    }

    void V_base::check_vec() const
    {
        check_vec(cvec());
    }

    void V_base::set_init_pos(Pos pos_) noexcept
    {
        init_pos() = pos_;
    }

    void V_base::reset_pos(Pos pos_) noexcept
    {
        Inherit::reset_pos(pos_);
        set_init_pos(pos_);
    }

    void V_base::reset(Pos pos_, Float t_) noexcept
    {
        Inherit::reset(pos_, t_);
        set_init_pos(pos_);
    }

    Pos V_base::compute_end_pos(Pos from, Vec vec_)
    {
        assert(valid_vec(vec_));
        return from + vec_;
    }

    void V_base::set_vec(Vec vec_)
    {
        check_vec(vec_);
        vec() = vec_;
    }

    Float V_base::compute_duration(Vec vec_, Float abs_v)
    {
        return compute_duration(dist(vec_), abs_v);
    }

    Float V_base::compute_duration(Float len, Float abs_v)
    {
        assert(isfinite(len));
        assert(isnormal(abs_v));

        // A -> B ~ vec:
        //           B
        //          /|
        //      c  / | b = vec.y
        //        /__|
        //      A  a = vec.x
        // V ~ abs_v
        // v = [V * a/c, V * b/c] = V*[a, b]/c = [a, b]/T

        return len/abs_v;
    }

    Vec V_base::compute_linear_v() const
    {
        assert(valid_vec());
        return cvec()*scale(1);
    }

    void V_base::set_linear_v()
    {
        linear_v() = compute_linear_v();
    }

    Vec V_base::linear_dvec(Vec vec_, Float dt_) const
    {
        return scale(dt_)*vec_;
    }

    Vec V_base::linear_vec_at(Vec vec_, Float t_) const
    {
        return linear_dvec(vec_, dt(t_));
    }

    Vec V_base::linear_dvec(Vec v_, V_tag, Float dt_)
    {
        assert(valid_vec(v_));

        return dt_*v_;
    }

    Vec V_base::linear_vec_at(Vec v_, V_tag, Float t_) const
    {
        return linear_dvec(v_, v_tag, dt(t_));
    }

    bool V_base::equals(const This& rhs) const noexcept
    {
        return Inherit::equals(rhs)
            && cvec() == rhs.cvec()
            && clinear_v() == rhs.clinear_v();
    }

    ////////////////////////////////////////////////////////////////

    Idle::Idle(Pos pos_, Float duration_)
        : Inherit(pos_, duration_)
    {
        assert(cduration() == duration_ || isnan(duration_));
        check_duration();
    }

    bool Idle::valid_duration(Float duration_) noexcept
    {
        return duration_ >= 0;
    }

    void Idle::check_duration(Float duration_)
    {
        using tomaqa::to_string;
        assert(!isnan(duration_) || !valid_duration(duration_));
        expect(valid_duration(duration_), "Invalid wait time: "s + to_string(duration_));
    }

    void Idle::set_duration(Float duration_)
    {
        Inherit::set_duration(duration_);
        assert(cduration() == duration_ || isnan(duration_));
        check_duration();
    }

    String Idle::to_string_head() const
    {
        return Inherit::to_string_head() + "wait at ";
    }

    String Idle::to_string_tail() const
    {
        using tomaqa::to_string;
        String str = Inherit::to_string_tail();

        const Float t_ = cduration();
        assert(t_ >= 0);
        if (t_ > 0) str += " for " + to_string(t_);
        if (isfinite(t_)) str += "s";

        return str;
    }

    ////////////////////////////////////////////////////////////////

    template <Class B>
    void V_mixin<B>::post_set(Float /*abs_v*/)
    {
        Inherit::post_set();
        This::set_linear_v();
    }

    template <Class B>
    String V_mixin<B>::to_string_body() const
    {
        using tomaqa::to_string;
        return Inherit::to_string_body() + " -> "s + to_string(This::cend_pos())
             + " (vec: " + to_string(This::cvec()) + ")";
    }

    ////////////////////////////////////////////////////////////////

    Linear::Linear(Pos_pair from_to, Float abs_v)
        : Inherit(move(from_to))
    {
        set(abs_v);
    }

    Linear::Linear(Pos from, Pos to, Float abs_v)
        : Inherit(from, to)
    {
        set(abs_v);
    }

    Linear::Linear(Pos from, Vec vec_, Float abs_v)
        : Inherit(from, vec_)
    {
        set(abs_v);
    }

    template class V_mixin<Crtp_mixin<V_base, Linear>>;

    ////////////////////////////////////////////////////////////////

    Parabolic::Parabolic(Pos_pair from_to, Float abs_v, Float delta)
        : Inherit(move(from_to))
    {
        set(abs_v, delta);
    }

    Parabolic::Parabolic(Pos from, Pos to, Float abs_v, Float delta)
        : Inherit(from, to)
    {
        set(abs_v, delta);
    }

    Parabolic::Parabolic(Pos from, Vec vec_, Float abs_v, Float delta)
        : Inherit(from, vec_)
    {
        set(abs_v, delta);
    }

    Float Parabolic::scompute_duration_impl(Vec vec_, Float abs_v, Float delta)
    {
        assert(valid_vec(vec_));
        assert(isnormal(delta));

        // Compute length of parabolic arc: https://en.wikipedia.org/wiki/Parabolic#Arc_length
        // d = ||vec||
        // vertex = [d/2, delta]
        // y = a*(x - d/2)^2 + delta
        // a = -4*delta/d^2 (.. b = -a*d = 4*delta/d, c = 0)
        // perpendicular distance: p = d/2
        // focal length: f = 1/(4a) = d^2/(4*-4*delta) = -d^2/(16*delta)

        const Float d = compute_dist(vec_);
        const Float f = d*d/(16*abs(delta));
        const Float h = d/4;
        const Float q = hypot(f, h);
        const Float s = (h*q)/f + f*log((h+q)/f);
        const Float len = 2*s;

        return Base::compute_duration(len, abs_v);
    }

    Float Parabolic::normal_coef(Float delta) const noexcept
    {
        const Float d = cdist();
        assert(isnormal(d));
        return 4*delta/d;
    }

    Float Parabolic::normal_v_coef(Float delta) const noexcept
    {
        const Float dur = cduration();
        assert(isnormal(dur));
        return normal_coef(delta)/(dur*dur);
    }

    Vec Parabolic::compute_normal(Float delta) const
    {
        assert(valid_vec());

        // [d/2, 0] -> [d/2, delta] at time 1/2
        // vec = [d, 0]
        // normal N = vec^\bot = [0, d]
        // [d/2, 0] + (1/4)*C*N = [d/2, delta]
        // (1/4)*C*d = delta
        // C = 4*delta/d

        return normal_coef(delta)*math::normal(cvec());
    }

    Vec Parabolic::compute_normal_v(Float delta) const
    {
        assert(valid_vec());
        assert(valid_vec(clinear_v()));

        // [d/2, 0] -> [d/2, delta] at time T/2
        // vec = [d, 0]
        // normal N = vec^\bot = [0, d]
        // [d/2, 0] + (T/2)^2*C*N = [d/2, delta]
        // (T^2/4)*C*d = delta
        // C = 4*delta/(T^2*d)

        assert(apx_equal(normal_v_coef(delta)*math::normal(cvec()),
                         normal_coef(delta)*math::normal(clinear_v())/cduration()));
        return normal_v_coef(delta)*math::normal(cvec());
    }

    void Parabolic::set_normal(Float delta)
    {
        normal() = compute_normal(delta);
        normal_v() = compute_normal_v(delta);
    }

    void Parabolic::post_set(Float abs_v, Float delta)
    {
        Inherit::post_set(abs_v);
        set_normal(delta);
    }

    Vec Parabolic::dvec_impl(Float dt_) const
    {
        //? what about derivative of the parabola?
        //? x = dvec(dt_).x
        //? y = 1 - 2*scale(dt_)/d;

        return Inherit::dvec_impl(dt_) + dnormal(dt_);
    }

    Vec Parabolic::vec_at_impl(Float t_) const
    {
        return Inherit::vec_at_impl(t_) + normal_at(t_);
    }

    Vec Parabolic::dnormal(Vec normal_, Float dt_) const
    {
        //  ____________________
        //  |            |     |\.
        //  |            |n1   | \.
        //  |------------|-----|  \.
        //  |            |     |   N
        //  |            |n0   |  /
        //  |            |     | /
        //  |____________|_____|/
        //  0      t       dt
        //
        // N = n1 + n2 => n2 = N - n1

        const Vec n0 = normal_at(normal_, ct());
        const Vec n1 = normal_at(normal_, ct() + dt_);

        return n1 - n0;
    }

    Vec Parabolic::normal_at(Vec normal_, Float t_) const
    {
        assert(valid_vec(normal_));

        t_ = scale(t_);
        t_ *= (1 - t_);

        return linear_dvec(normal_, v_tag, t_);
    }

    Vec Parabolic::dnormal(Vec normal_v_, V_tag, Float dt_) const
    {
        const Vec n0 = normal_at(normal_v_, v_tag, ct());
        const Vec n1 = normal_at(normal_v_, v_tag, ct() + dt_);

        return n1 - n0;
    }

    Vec Parabolic::normal_at(Vec normal_v_, V_tag, Float t_) const
    {
        assert(valid_vec(normal_v_));

        t_ *= (cduration() - t_);

        return linear_dvec(normal_v_, v_tag, t_);
    }

    bool Parabolic::equals(const This& rhs) const noexcept
    {
        return Inherit::equals(rhs)
            && cnormal() == rhs.cnormal()
            && cnormal_v() == rhs.cnormal_v();
    }

    template class V_mixin<Crtp_mixin<V_base, Parabolic>>;
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace mapf_r::agent {
    State::State(Pos pos)
        : State(state::Idle(pos))
    { }

    State::State(Pos pos, Float duration)
        : State(state::Idle(pos, duration))
    { }

    State::State(Pos from, Pos to, Float abs_v)
        : State(state::Linear(from, to, abs_v))
    { }

    State::State(Pos from, Vec vec, Float abs_v)
        : State(state::Linear(from, vec, abs_v))
    { }

    State::State(Pos from, Pos to, Float abs_v, Float delta)
        : State(state::Parabolic(from, to, abs_v, delta))
    { }

    State::State(Pos from, Vec vec, Float abs_v, Float delta)
        : State(state::Parabolic(from, vec, abs_v, delta))
    { }

    State::State(String str, Float radius, Float abs_v)
    {
        istringstream iss(move(str));
        parse(iss, radius, abs_v);
        expect(iss.eof(), "Unexpected extra contents: "s + iss.str());
    }

    State::State(istream& is, Float radius, Float abs_v)
    {
        parse(is, radius, abs_v);
    }

    State::State(istream& is, Float radius, Float abs_v, Pos pos)
    {
        parse(is, radius, abs_v, pos);
    }

    void State::parse(istream& is, Float radius, Float abs_v)
    {
        using tomaqa::to_string;

        char c1, c2, c3;
        Float x, y;
        expect(is >> c1 >> x >> c2 >> y >> c3 && c1 == '[' && c2 == ',' && c3 == ']',
               "Expected a coordinate [x, y], got: "s + to_string(is));

        parse(is, radius, abs_v, {x, y});
    }

    void State::parse(istream& is, Float radius, Float abs_v, Pos pos)
    {
        using tomaqa::to_string;

        char c1, c2, c3;
        String str;
        Float x, y;

        is >> str;
        if (!is) {
            set_idle_at(pos);
            return;
        }

        if (str.starts_with('#')) {
            is.ignore(limits<streamsize>::max(), '\n');
            return parse(is, radius, abs_v, pos);
        }

        static const String wait_str = "wait";
        if (wait_str.starts_with(str)) {
            expect(is >> x, "Expected wait duration.");
            set_idle(pos, x);
            return;
        }

        expect(str == "->", "Expected '->', got: "s + str);

        expect(is >> c1 >> x >> c2 >> y >> c3 && c1 == '[' && c2 == ',' && c3 == ']',
               "Expected destination coordinate [x, y], got: "s + to_string(is));
        Pos to{x, y};

        Float delta = 0;
        assert(is);
        if (is >> c1) {
            is.unget();

            static const String delta_str = "delta";
            static const String radius_str = "radius";
            static const String nradius_str = "-"s + radius_str;
            if (c1 == delta_str[0]) {
                expect(is >> str && delta_str.starts_with(str),
                       "Expected "s + delta_str + ", got: "s + str + to_string(is));
                is >> str;
                const bool is_val = is_value<Float>(str);
                expect(is && (is_val || radius_str.starts_with(str) || nradius_str.starts_with(str)),
                       "Expected a value or [-]"s + radius_str + ", got: "s + str + to_string(is));
                if (is_val) get_value<Float>(delta, move(str));
                else {
                    assert(radius_str.starts_with(str) || nradius_str.starts_with(str));
                    assert(!str.empty());
                    if (str[0] == '-') delta = -radius;
                    else delta = radius;
                }
            }
        }

        if (delta == 0) set_linear(pos, to, abs_v);
        else set_parabolic(pos, to, abs_v, delta);
    }
}
