#include "mapf_r/agent/layout.hpp"

namespace mapf_r::agent {
    Layout::Layout(Start_ids_map sids_map, Goal_ids_map gids_map)
        : _start_ids_map(move(sids_map)), _goal_ids_map(move(gids_map))
    {
        init();
    }

    Layout::Layout(istream& is)
    {
        parse(is);
        init();
    }

    void Layout::parse(istream& is)
    {
        char c;
        expect(is >> c, "Input layout stream is not readable.");
        is.unget();
        switch (c) {
        case '{':
            return parse_l(is);
        case 'K':
            return parse_krur(is);
        default:
            THROW("Unrecognized layout format, first char was: "s + c);
        }
    }

    void Layout::parse_l(istream& is)
    {
        char c1, c2;
        Id aid;
        graph::Vertex::Id sid, gid;
        auto& sids_map = start_ids_map();
        auto& gids_map = goal_ids_map();
        auto& rad_map = radii_map();
        auto& absv_map = abs_vs_map();
        while (is >> c1) {
            switch (c1) {
            case '{':
                expect(is >> aid >> c1 >> sid >> c2 >> gid && c1 == ':' && c2 == ',',
                       "Expected {<agent_id>:<start_id>,<goal_id>}");
                sids_map.emplace(aid, move(sid));
                gids_map.emplace(aid, move(gid));
                //+ support also these
                rad_map.emplace(aid, default_radius);
                absv_map.emplace(aid, default_abs_v);
                expect(is >> c1 && c1 == '}', "Expected }");
                break;
            default:
                THROW("Expected a vertex or an edge, got: "s + c1);
            };
        }
    }

    void Layout::parse_krur(istream& is)
    {
        using tomaqa::to_string;
        using std::size;

        assert(is.peek() == 'K');

        String str;
        size_t n_agents;

        auto& rad_map = radii_map();
        auto& absv_map = abs_vs_map();
        {
            const String msg = "kruhobots";
            const char capital = toupper(msg.front());

            expect(getline(is, str, capital) && is >> std::noskipws && is >> str
                   && is >> std::skipws && str.starts_with(msg.substr(1)),
                   "Expected '"s + capital + msg.substr(1) + "'");

            expect((str.back() == ':' || getline(is, str, ':')) && is >> n_agents && getline(is, str),
                   "Expected number of "s + msg + " assignments.");
            Float val;
            for (Id aid = 0; aid < n_agents; ++aid) {
                for (int i = 1; i <= 2; ++i) {
                    auto& map = (i == 1) ? rad_map : absv_map;
                    const String val_msg = (i == 1) ? "radius" : "abs. v";

                    expect(getline(is, str, '=') && is >> val,
                           "Expected "s + val_msg + " assignment, got: "s + str + to_string(val));
                    map.emplace(aid, val);
                }
                getline(is, str);
            }
            assert(size(rad_map) == n_agents);
            assert(size(absv_map) == n_agents);
        }

        for (int i = 1; i <= 2; ++i) {
            auto& ids_map = (i == 1) ? start_ids_map() : goal_ids_map();
            const String msg = (i == 1) ? "start" : "goal";
            const char capital = toupper(msg.front());

            expect(getline(is, str, capital) && is >> std::noskipws && is >> str
                   && is >> std::skipws && str == msg.substr(1),
                   "Expected '"s + capital + msg.substr(1) + "'");

            expect(getline(is, str, ':') && is >> n_agents && getline(is, str),
                   "Expected number of "s + msg + " assignments.");
            expect(n_agents == size(rad_map),
                   "Number of assignments mismatch: "s
                   + to_string(size(rad_map)) + " != " + to_string(n_agents));
            graph::Vertex::Id vid;
            for (Id aid = 0; aid < n_agents; ++aid) {
                expect(getline(is, str, '>') && is >> vid,
                       "Expected "s + msg + " assignment, got: "s + str + to_string(vid));
                getline(is, str);
                ids_map.emplace(aid, move(vid));
            }
        }
    }

    void Layout::init()
    {
        static constexpr auto map_f = [](auto& map, auto& vids_map){
            for (auto& [aid, vid] : vids_map) {
                assert(!contains(map, vid));
                map.emplace(vid, aid);
                assert(contains(map, vid));
            }
        };

        map_f(start_to_agent_ids_map(), cstart_ids_map());
        map_f(goal_to_agent_ids_map(), cgoal_ids_map());
    }
}
