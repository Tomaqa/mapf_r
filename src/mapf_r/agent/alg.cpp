#include "mapf_r/agent/alg.hpp"

namespace mapf_r::agent {
    template <size_t degV>
    bool is_collision_tp(const Polynom<degV*2>& coefs)
    {
        return collision_tp<degV>(coefs).valid();
    }

    template bool is_collision_tp<1>(const Polynom<2>&);
    template bool is_collision_tp<2>(const Polynom<4>&);

    bool is_collision(const Agent& a1, const Agent& a2)
    {
        auto& s1 = a1.cstate();
        auto& s2 = a2.cstate();

        if (s1.parabolic() || s2.parabolic()) return parabolic::is_collision(a1, a2);
        return linear::is_collision(a1, a2);
    }

    Interval collision(const Agent& a1, const Agent& a2)
    {
        auto& s1 = a1.cstate();
        auto& s2 = a2.cstate();

        if (s1.parabolic() || s2.parabolic()) return parabolic::collision(a1, a2);
        return linear::collision(a1, a2);
    }

    Optional<Float> max_collision(const Agent& a1, const Agent& a2, const Interval& t)
    {
        auto& s1 = a1.cstate();
        auto& s2 = a2.cstate();

        if (s1.parabolic() || s2.parabolic()) return parabolic::max_collision(a1, a2, t);
        return linear::max_collision(a1, a2, t);
    }

    template <size_t degV>
    bool in_conflict_tp(const Polynom<degV*2>& coefs, const Interval& intersect_t)
    {
        // should be faster than `conflict.valid`
        //+ check if it is true
        return max_conflict_tp<degV>(coefs, intersect_t).valid();
    }

    template bool in_conflict_tp<1>(const Polynom<2>&, const Interval& intersect_t);
    template bool in_conflict_tp<2>(const Polynom<4>&, const Interval& intersect_t);

    bool in_conflict(const Interval& coll_int, const Interval& intersect_t)
    {
        return conflict(coll_int, intersect_t).valid();
    }

    template <size_t degV>
    Interval conflict_tp(const Polynom<degV*2>& coefs, const Interval& intersect_t)
    {
        const Interval coll_int = collision_tp<degV>(coefs);
        return conflict(coll_int, intersect_t);
    }

    template Interval conflict_tp<1>(const Polynom<2>&, const Interval& intersect_t);
    template Interval conflict_tp<2>(const Polynom<4>&, const Interval& intersect_t);

    Interval conflict(const Interval& coll_int, const Interval& intersect_t)
    {
        Interval res = coll_int&intersect_t;
        // Exclude "point conflicts", e.g. only point intersections of time intervals
        if (res.is_point()) return {};
        return res;
    }

    bool in_conflict(const Agent& a1, const Agent& a2, const Interval& t1, const Interval& t2)
    {
        auto& s1 = a1.cstate();
        auto& s2 = a2.cstate();

        if (s1.parabolic() || s2.parabolic()) return parabolic::in_conflict(a1, a2, t1, t2);
        return linear::in_conflict(a1, a2, t1, t2);
    }

    Interval conflict(const Agent& a1, const Agent& a2, const Interval& t1, const Interval& t2)
    {
        auto& s1 = a1.cstate();
        auto& s2 = a2.cstate();

        if (s1.parabolic() || s2.parabolic()) return parabolic::conflict(a1, a2, t1, t2);
        return linear::conflict(a1, a2, t1, t2);
    }

    Optional<Float>
    max_conflict(const Agent& a1, const Agent& a2, const Interval& t1, const Interval& t2)
    {
        auto& s1 = a1.cstate();
        auto& s2 = a2.cstate();

        if (s1.parabolic() || s2.parabolic()) return parabolic::max_conflict(a1, a2, t1, t2);
        return linear::max_conflict(a1, a2, t1, t2);
    }

    pair<Interval, Interval>
    unsafe_intervals(const Agent& a1, const Agent& a2, const Interval& t1, const Interval& t2)
    {
        auto& s1 = a1.cstate();
        auto& s2 = a2.cstate();

        if (s1.parabolic() || s2.parabolic()) return parabolic::unsafe_intervals(a1, a2, t1, t2);
        return linear::unsafe_intervals(a1, a2, t1, t2);
    }
}
