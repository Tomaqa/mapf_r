#include "mapf_r/agent/plan.hpp"

namespace mapf_r::agent::plan {
    Local::Local(istream& is)
    {
        parse(is);
    }

    Local::Local(graph::Vertex::Id start_id)
        : Local(Steps{
            {.from_id = start_id, .to_id = start_id, .abs_time = 0, .move_time = 0, .wait_time = inf}
          })
    { }

    Local::Local(graph::Vertex::Id start_id, graph::Vertex::Id goal_id)
        : Local(start_id)
    {
        add_step({.from_id = move(start_id), .to_id = move(goal_id), .abs_time = inf, .move_time = 0});
    }

    const Local::Step& Local::cfirst_step() const
    {
        expect(!empty(), "Cannot get first step: steps are empty.");
        return csteps().front();
    }

    const Local::Step& Local::clast_step() const
    {
        expect(!empty(), "Cannot get last step: steps are empty.");
        return csteps().back();
    }

    void Local::add_step(Step s)
    {
        assert(empty() || s.from_id == clast_step().to_id);

        steps().push_back(move(s));
    }

    Float Local::final_time() const noexcept
    {
        if (empty()) return 0;

        Float t = clast_step().final_time();

        if constexpr (_debug_) {
            Float t_ = cfirst_step().abs_time;
            for (auto& s : csteps()) {
                t_ += s.rel_time();
            }
            assert(apx_equal(t_, t));
        }

        return t;
    }

    void Local::parse(istream& is)
    {
        char c;
        while (is >> c) {
            is.unget();
            if (c == 'a') return;
            parse_step(is);
        };
    }

    void Local::parse_step(istream& is)
    {
        add_step(parsed_step(is));
    }

    Local::Step Local::parsed_step(istream& is)
    {
        using tomaqa::to_string;

        assert(is);
        const auto is_pos = is.tellg();
        try {
            Id from_id, to_id;
            Float atime;
            Float wtime = 0;
            String kw;
            char c1, c2;
            Float x;

            is >> from_id >> c1 >> c2 >> to_id;
            if (!is || c1 != '-' || c2 != '>') throw dummy;

            is >> c1 >> atime >> c2 >> kw >> x;
            if (!is || c1 != ':' || c2 != ',') throw dummy;

            static const String wait_str = "wait";
            static const String move_str = "move";
            if (wait_str.starts_with(kw)) {
                expect(x >= 0, "Expected non-negative waiting time, got: "s + to_string(x));
                wtime = x;
                is >> c1 >> kw >> x;
                if (!is || c1 != ',') throw dummy;
            }

            assert(is);
            if (!move_str.starts_with(kw)) throw dummy;
            // expect(x > 0, "Expected positive moving time, got: "s + to_string(x));
            expect(x >= 0, "Expected non-negative moving time, got: "s + to_string(x));
            const Float mtime = x;

            return {.from_id = from_id, .to_id = to_id,
                    .abs_time = atime, .move_time = mtime, .wait_time = wtime};
        }
        catch (Dummy) {
            is.clear();
            const auto end_pos = is.tellg();
            is.seekg(is_pos);
            THROW("Expected <from_id> -> <to_id> : <atime> [, wait <wtime>], move <mtime>, got:\n"s
                  + to_string(is, end_pos));
        }
    }

    ostream& Local::compose(ostream& os) const
    {
        for (auto& s : csteps()) compose_step(os, s);
        return os;
    }

    ostream& Local::compose_step(ostream& os, const Step& s)
    {
        os << s.from_id << "->" << s.to_id << ": ";

        constexpr auto prec = -precision::High::abs_eps_exp();
        const auto prev_prec = os.precision();
        os << std::setprecision(prec) << s.abs_time << ", ";
        assert(s.wait_time >= 0);
        if (s.wait_time > 0) os << "wait " << s.wait_time << ", ";
        assert(s.move_time >= 0);
        os << "move " << s.move_time << "\n";

        return os << std::setprecision(prev_prec);
    }

    String Local::to_string() const
    {
        if (empty()) return {};

        String str;
        for (auto& s : csteps()) str += s.to_string() + "\n";
        str.pop_back();
        return str;
    }

    ////////////////////////////////////////////////////////////////

    Global::Global(istream& is)
    {
        parse(is);
    }

    Global::Global(const Layout& l)
    {
        for (auto& [aid, sid] : l.cstart_ids_map()) {
            auto& gid = l.cgoal_id_of(aid);
            try_emplace(aid, sid, gid);
        }
    }

    void Global::parse(istream& is)
    {
        using tomaqa::to_string;

        Id aid;
        String str1;
        char c;
        while (is >> c) {
            is.unget();
            const auto is_pos = is.tellg();

            static const String ag_str = "agent";
            is >> str1 >> aid >> c;
            if (!is || !ag_str.starts_with(str1) || c != ':') {
                is.clear();
                const auto end_pos = is.tellg();
                is.seekg(is_pos);
                THROW("Expected "s + ag_str + " <agent_id> : , got:\n"s + to_string(is, end_pos));
            }

            emplace(aid, Local(is));
        }
    }

    ostream& Global::compose(ostream& os) const
    {
        for (auto& [aid, plan] : *this) {
            os << "agent " << aid << ":\n" << plan.compose() << "\n";
        }

        return os;
    }

    Float Global::max_time() const noexcept
    {
        if (empty()) return 0;

        return max(*this, [](auto& pair_){
            auto& plan = pair_.second;
            return plan.final_time();
        });
    }

    Float Global::sum_times() const noexcept
    {
        Float t = 0;
        for (auto& [_, plan] : *this) {
            t += plan.final_time();
        }
        return t;
    }

    const graph::Vertex::Id& Global::cstart_id_of(const Id& aid) const
    {
        return cat(aid).cfirst_step().from_id;
    }

    const graph::Vertex::Id& Global::cgoal_id_of(const Id& aid) const
    {
        return cat(aid).clast_step().to_id;
    }

    const Id* Global::find_agent_id_of_start(const graph::Vertex::Id& sid) const noexcept
    {
        for (auto& [aid, plan] : *this) {
            if (plan.cfirst_step().from_id == sid) return &aid;
        }
        return nullptr;
    }

    const Id* Global::find_agent_id_of_goal(const graph::Vertex::Id& gid) const noexcept
    {
        for (auto& [aid, plan] : *this) {
            if (plan.clast_step().to_id == gid) return &aid;
        }
        return nullptr;
    }

    String Global::to_string() const
    {
        using tomaqa::to_string;

        String str;
        for (auto& [aid, sp] : *this) {
            str += to_string(aid) + ":\n" + sp.to_string() + "\n\n";
        }
        str.pop_back();
        return str;
    }

    ////////////////////////////////////////////////////////////////

    Local_states::Local_states(Float radius_, Float abs_v_)
        : radius(radius_), abs_v(abs_v_)
    { }

    Local_states::Local_states(const Local& plan, const Graph& graph, Float radius_, Float abs_v_)
        : Local_states(radius_, abs_v_)
    {
        assert(!plan.empty());
        reserve(plan.size()*2);
        [[maybe_unused]] bool idle = false;
        for (auto& s : plan.csteps()) {
            auto& from_id = s.from_id;
            auto& to_id = s.to_id;
            auto& from = graph.cvertex(from_id);
            auto& to = graph.cvertex(to_id);
            const auto from_pos = from.cpos();
            const auto to_pos = to.cpos();

            const auto wtime = s.wait_time;
            if (wtime > 0) push_back(state::Idle(from_pos, wtime));

            const auto mtime = s.move_time;
            assert(!idle || mtime == 0);
            if (mtime == 0) {
                assert(!idle || !empty());
                if (empty()) push_back(state::Idle(from_pos));

                if constexpr (_debug_) {
                    idle = true;
                    continue;
                }
                else break;
            }

            state::Linear lin(from_pos, to_pos);
            lin.set(abs_v_);
            assert(apx_equal(mtime, lin.cduration()));
            push_back(move(lin));
        }
    }

    Local_states::Local_states(istream& is, Float radius_, Float abs_v_)
        : Local_states(radius_, abs_v_)
    {
        parse(is);
    }

    void Local_states::parse(istream& is)
    {
        using tomaqa::to_string;

        if (!is) return;

        State st(is, radius, abs_v);
        state::Pos last_pos = st.cend_pos();
        push_back(move(st));
        char c;
        while (is >> c) {
            is.unget();
            if (c == 'a') return;
            if (c == '#') {
                is.ignore(limits<streamsize>::max(), '\n');
                continue;
            }

            st = State(is, radius, abs_v, last_pos);
            last_pos = st.cend_pos();
            push_back(move(st));
        }
    }

    Float Local_states::total_time() const noexcept
    {
        Float t = 0;
        for (auto& st : *this) {
            assert(st.cduration() > 0);
            t += st.cduration();
        }
        return t;
    }

    String Local_states::to_string() const
    {
        String str;
        for (auto& st : *this) {
            str += st.to_string() + "\n";
        }
        return str;
    }

    ////////////////////////////////////////////////////////////////

    Global_states::Global_states(const Global& gplan, const Graph& graph, const Layout& layout)
    {
        for (auto& [aid, plan] : gplan) {
            Float radius = layout.cradius_of(aid);
            Float abs_v = layout.cabs_v_of(aid);
            Local_states lstates(plan, graph, radius, abs_v);
            [[maybe_unused]] const auto [it, inserted] = emplace(aid, move(lstates));
            assert(inserted);
            assert(!it->second.empty());
        }
    }

    Global_states::Global_states(istream& is)
    {
        parse(is);
    }

    void Global_states::parse(istream& is)
    {
        using tomaqa::to_string;

        Id aid;
        Float radius;
        Float abs_v;
        String str;
        char c;
        while (is >> c) {
            is.unget();
            if (c == '#') {
                is.ignore(limits<streamsize>::max(), '\n');
                continue;
            }

            static const String ag_str = "agent";
            static const String rad_str = "radius";
            static const String v_str = "v";
            expect(is >> str >> aid && ag_str.starts_with(str),
                   "Expected "s + ag_str + " <agent_id>, got:\n"s + to_string(is));
            while (is >> str && str.starts_with("#")) is.ignore(limits<streamsize>::max(), '\n');
            expect(is >> radius && rad_str.starts_with(str),
                   "Expected "s + rad_str + " <radius>, got:\n"s + to_string(is));
            while (is >> str && str.starts_with("#")) is.ignore(limits<streamsize>::max(), '\n');
            expect(is >> abs_v >> c && v_str.starts_with(str) && c == ':',
                   "Expected "s + v_str + " <abs_v>: , got:\n"s + to_string(is));

            //+ directly use an `Agent` ?
            emplace(aid, Local_states(is, radius, abs_v));
        }
    }

    Float Global_states::max_time() const noexcept
    {
        if (empty()) return 0;

        return max(*this, [](auto& pair_){
            auto& states = pair_.second;
            return states.total_time();
        });
    }

    Float Global_states::sum_times() const noexcept
    {
        Float t = 0;
        for (auto& [_, states] : *this) {
            const Float tt = states.total_time();
            assert(tt > 0);
            t += tt;
        }
        return t;
    }

    String Global_states::to_string() const
    {
        using tomaqa::to_string;

        String str;
        for (auto& [aid, sts] : *this) {
            str += to_string(aid) + ":\n" + sts.to_string() + "\n";
        }
        str.pop_back();
        return str;
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace mapf_r::agent::plan {
    String Local::Step::to_string() const
    {
        using tomaqa::to_string;
        return to_string(from_id) + "->" + to_string(to_id) + ": "
             + to_string(abs_time) + "+" + to_string(wait_time) + "+" + to_string(move_time)
             + " == "
             + to_string(abs_time) + "," + to_string(wait_abs_time()) + "," + to_string(final_time());
    }
}
