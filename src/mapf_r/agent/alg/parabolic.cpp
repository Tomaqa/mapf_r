#include "mapf_r/agent/alg.hpp"
#include "mapf_r/agent/alg/parabolic.hpp"

#include "mapf_r/agent/alg/detail.hpp"

#include <tomaqa/math/alg.hpp>

namespace mapf_r::agent::parabolic {
    Interval collision(const Polynom<4>& coefs)
    {
        const auto& p = coefs;
        const auto [c4, c3, c2, c1, c0] = p;
        assert(c4 >= 0);
        if (c4 == 0 && c3 == 0) return linear::collision({c2, c1, c0});
        assert(c4 != 0);

        auto [t0, t1, t2, t3] = quartic_roots(c4, c3, c2, c1, c0);

        if (isnan(t0)) return {};
        assert(!isnan(t1));
        const Float tmax = isnan(t2) ? t1 : isnan(t3) ? t2 : t3;
        Interval res(t0, tmax);
        if (res.apx_empty<precision::High>()) return {};
        return res;
    }

    Optional<Float> max_collision(const Polynom<4>& coefs, const Interval& t)
    {
        const auto& p = coefs;
        const auto [c4, c3, c2, c1, c0] = p;
        if (c4 == 0 && c3 == 0) return linear::max_collision({c2, c1, c0}, t);

        if (t.empty() || t.is_point()) return {};

        const auto dp = deriv(p);
        const auto [dc3, dc2, dc1, dc0] = dp;

        auto [dt0, dt1, dt2] = cubic_roots(dc3, dc2, dc1, dc0);

        const auto [tl, tu] = t.crange();
        const Float t_min = min({tl, tu, dt0, dt1, dt2}, [&t, &p](Float l, Float r){
            if (isnan(l)) return false;
            if (isnan(r)) return true;
            if (!t.contains(l)) return false;
            if (!t.contains(r)) return true;
            return p(l) < p(r);
        });
        assert(t.contains(t_min));

        const Float pt_min = p(t_min);

        if constexpr (_debug_) {
            if (contains({tl, tu}, t_min)) {
                assert(t_min != tl || apx_greater_equal(dp(t_min), 0.) || t_min == tu);
                assert(t_min != tu || apx_less_equal(dp(t_min), 0.) || t_min == tl);
            }
        }

        if (apx_greater_equal(pt_min, 0.)) return {};
        return t_min;
    }

    pair<Interval, Interval>
    unsafe_intervals(const Agent& a1, const Agent& a2, const Interval& t1, const Interval& t2)
    {
        assert(a1.cstate().parabolic() || a2.cstate().parabolic());

        Interval confl_int = parabolic::conflict(a1, a2, t1, t2);
        //! inconsistent with `in_conflict` where there is no apx.
        if (confl_int.apx_empty()) return {};

        return detail::sim_unsafe_intervals_tp<2>(a1, a2, t1, t2, move(confl_int));
    }
}
