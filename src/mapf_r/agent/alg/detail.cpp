#include "mapf_r/agent/alg/detail.hpp"

namespace mapf_r::agent::detail {
    namespace {
        template <size_t degV, bool reverseV = false>
        pair<Interval, Interval>
        sim_wait_unsafe_intervals_tp(const Agent& a1, const Agent& a2,
                                     const Interval& t1, const Interval& t2, Interval confl_int)
        {
            assert(a1.cstate().idle());
            assert(!a2.cstate().idle());
            assert(degV != 1 || a2.cstate().linear());
            assert(degV != 2 || a2.cstate().parabolic());

            Interval& a1_unsafe = confl_int;

            // If `t1u == inf`, return the max. possible unsafe interval and use it also for `a2_unsafe`
            // Not implicitly using sooner times than t1l (do it explicitly if you want)
            auto& t1_ref = (t1.cupper() == inf) ? a1_unsafe : t1;

            Interval a2_unsafe = sim_unsafe_interval_tp<degV>(a2, a1, t2, t1_ref);

            if constexpr (!reverseV)
                return {move(a1_unsafe), move(a2_unsafe)};
            else return {move(a2_unsafe), move(a1_unsafe)};
        }
    }

    template <size_t degV>
    pair<Interval, Interval>
    sim_unsafe_intervals_tp(const Agent& a1, const Agent& a2,
                            const Interval& t1, const Interval& t2, Interval confl_int)
    {
        assert(in_conflict_tp<degV>(a1, a2, t1, t2));
        assert(t1&t2);

        const bool a1_idle = a1.cstate().idle();
        const bool a2_idle = a2.cstate().idle();

        if constexpr (_debug_ && degV == 1) {
            const auto [a1_al, a2_al] = linear::aligned_agents(a1, a2, t1, t2);
            assert(linear::is_collision(a1_al, a2_al));
            assert(linear::aligned_in_conflict(a1_al, a2_al, t1, t2));

            assert(a1_idle == a1_al.cstate().idle());
            assert(a2_idle == a2_al.cstate().idle());
            assert(!a1_idle || !a2_idle);
        }

        //? more efficient, but still should yield the same results?
        if (a1_idle) return sim_wait_unsafe_intervals_tp<degV>(a1, a2, t1, t2, move(confl_int));
        if (a2_idle) return sim_wait_unsafe_intervals_tp<degV, true>(a2, a1, t2, t1, move(confl_int));

        auto p = pair{sim_unsafe_interval_tp<degV>(a1, a2, t1, t2),
                      sim_unsafe_interval_tp<degV>(a2, a1, t2, t1)};

        if constexpr (_debug_) {
            auto& [ui1, ui2] = p;
            assert(ui1.empty() == ui2.empty());
            assert(ui1.empty() || ui1.is_point() || !ui1.apx_empty());
            assert(ui2.empty() || ui2.is_point() || !ui2.apx_empty());
        }

        return p;
    }

    template pair<Interval, Interval>
        sim_unsafe_intervals_tp<1>(const Agent&, const Agent&,
                                   const Interval& t1, const Interval& t2, Interval confl_int);
    template pair<Interval, Interval>
        sim_unsafe_intervals_tp<2>(const Agent&, const Agent&,
                                   const Interval& t1, const Interval& t2, Interval confl_int);

    template <size_t degV>
    Interval sim_unsafe_interval_tp(const Agent& a1, const Agent& a2,
                                    const Interval& t1, const Interval& t2)
    {
        assert(t1.valid() && t2.valid());

        const Float t1l = t1.clower();
        const auto [t2l, t2u] = t2.crange();
        assert(t1l <= t2u);
        assert(t2u != inf);

        // Not asserting `!a1_idle` - should work, only not efficiently

        Interval t1_cp = t1;
        auto confl_pred = [&a1_=a1, &a2_=a2, &t1_=t1_cp, &t2_=t2, was_confl{true}](Float& tau) mutable {
            t1_ += tau;
            //? linear: use aligned if it turns out to be more efficient/accurate
            //? linear: or reuse collision coefs if it is faster
            const bool is_confl = in_conflict_tp<degV>(a1_, a2_, t1_, t2_);
            const bool reverse = is_confl != was_confl;
            if (!reverse) tau /= 2;
            else {
                was_confl = !was_confl;
                tau /= -2;
            }

            assert(was_confl == is_confl);
            return is_confl;
        };

        // Maximal tau s.t. there surely be no conflict -> empty intersection
        // max(t1l+tau, t2l) > min(t1u+tau, t2u) -> tau=?
        // t1l+tau > t2u -> tau = t2u - t1l

        Float tau = t2u - t1l;
        if (confl_pred(tau)) {
            return {t1l, t2u};
        }

        bool was_confl = true;
        constexpr Float eps = precision::Huge::abs_eps();
        while (abs(tau) > eps) {
            was_confl = confl_pred(tau);
            assert(t1_cp.clower() >= t1l);
            assert(t1_cp.clower() <= t2u);
        }

        if (was_confl) {
            assert(tau > 0);
            assert(tau < eps);
            tau *= 2;
            confl_pred(tau);
        }

        const Float t1_cpl = t1_cp.clower();
        // `precision::Medium` fails sometimes, which should be actually treated as "no conflict",
        // but then a) one UI can be empty and the second not,
        // b) forcing the result to "no conflict" does not seem to make any effect in release mode
        assert(apx_greater<precision::High>(t1_cpl, t1l));
        if (apx_equal(t1_cpl, t1l)) return t1l;
        assert(apx_less_equal(t1_cpl, t2u));
        // It can happen that `apx_equal(t1_cpl, t1.cupper())`, but they are not related
        Float t = apx_equal(t1_cpl, t2u) ? t2u
                : apx_equal(t1_cpl, t2l) ? t2l
                : t1_cpl;
        assert(apx_greater(t, t1l));
        assert(t <= t2u);

        if constexpr (_debug_ && degV == 1) {
            const auto [_, a2_al] = linear::aligned_agents(a1, a2, t1, t2);

            t1_cp = Interval(0, t1.size()) + t;
            Agent a1_al = a1;
            auto& s1_al = a1_al.state();
            s1_al.advance(-t);
            assert(t1_cp.clower() >= t1l);
            assert(t1_cp.clower() <= t2u);
            assert(linear::aligned_conflict(a1_al, a2_al, t1_cp, t2).apx_empty());
            assert(linear::conflict(a1, a2, t1_cp, t2).apx_empty());

            /*+ sometimes it fails but I think that the assertions are just too strict
            constexpr Float eps_ = precision::Medium::abs_eps();
            t1_cp -= eps_;
            s1_al.advance(eps_);
            assert(linear::aligned_in_conflict(a1_al, a2_al, t1_cp, t2));
            assert(linear::in_conflict(a1, a2, t1_cp, t2));

            t1_cp += 2*eps_;
            s1_al.advance(-2*eps_);
            assert(!linear::aligned_in_conflict(a1_al, a2_al, t1_cp, t2));
            assert(!linear::in_conflict(a1, a2, t1_cp, t2));

            Agent a1_cp = a1;
            auto& s1_cp = a1_cp.state();
            s1_cp.advance(-t-eps_);
            assert(!linear::aligned_in_conflict(a1_cp, a2_al, t1_cp, t2));
            */
        }

        return {t1l, t};
    }

    template Interval sim_unsafe_interval_tp<1>(const Agent&, const Agent&,
                                                const Interval& t1, const Interval& t2);
    template Interval sim_unsafe_interval_tp<2>(const Agent&, const Agent&,
                                                const Interval& t1, const Interval& t2);
}
