#include "mapf_r/agent/alg.hpp"
#include "mapf_r/agent/alg/linear.hpp"

#include "mapf_r/agent/alg/detail.hpp"
// #include "mapf_r/agent/alg/linear/detail.hpp"

#include <tomaqa/math/alg.hpp>

namespace mapf_r::agent::linear {
    Interval collision(const Polynom<2>& coefs)
    {
        const auto [a, b, c] = coefs;
        assert(a >= 0);

        // This equation has a real solution if its discriminant is positive,
        // therefore a collision is possible
        // If discriminant is zero, it is *not* considered as collision
        Interval res = quadratic_ineq(coefs);
        // Exclude cases of "point collisions"
        if (res.is_point()) return {};
        if (res.infinite()) {
            if (apx_equal<precision::Huge>(c, 0)) return {};
        }
        return res;
    }

    Optional<Float> max_collision(const Polynom<2>& coefs, const Interval& t)
    {
        if (t.empty() || t.is_point()) return {};

        const auto& p = coefs;
        const auto dp = deriv(p);
        const auto [da, db] = dp;

        const auto [tl, tu] = t.crange();
        const auto da_cmp = da <=> 0;
        Float t_min;
        Float pt_min;
        if (is_eq(da_cmp)) {
            const auto db_cmp = db <=> 0;
            if (is_eq(db_cmp)) {
                if (apx_greater_equal<precision::Huge>(p.constant(), 0)) return {};
                return tl;
            }
            t_min = is_gt(db_cmp) ? tl : tu;
            pt_min = p(t_min);
        }
        else {
            const Float root = -db/da;
            if (is_gt(da_cmp) && t.contains(root)) {
                t_min = root;
                pt_min = p(t_min);
            }
            else {
                const Float ptl = p(tl);
                const Float ptu = p(tu);
                if (ptl < ptu) {
                    t_min = tl;
                    pt_min = ptl;
                }
                else {
                    t_min = tu;
                    pt_min = ptu;
                }
            }
        }
        assert(isfinite(t_min));
        assert(isfinite(pt_min));
        assert(t.contains(t_min));

        if (pt_min >= 0) return {};
        return t_min;
    }

    void align_agents(Agent& a1, Agent& a2, const Interval& t1, const Interval& t2)
    {
        const Float t1l = t1.clower();
        const Float t2l = t2.clower();
        assert(t1.valid() && t2.valid());
        assert(!t1.infinite() && !t2.infinite());
        assert(t1l <= t2.cupper() || (t1&t2).empty());

        a1.state().advance(-t1l);
        a2.state().advance(-t2l);
    }

    pair<Agent, Agent>
    aligned_agents(const Agent& a1, const Agent& a2, const Interval& t1, const Interval& t2)
    {
        Agent a1_cp = a1;
        Agent a2_cp = a2;
        align_agents(a1_cp, a2_cp, t1, t2);

        return {move(a1_cp), move(a2_cp)};
    }

    bool aligned_in_conflict(const Agent& a1, const Agent& a2, const Interval& intersect_t)
    {
        return linear::max_collision(a1, a2, intersect_t).valid();
    }

    Interval aligned_conflict(const Agent& a1, const Agent& a2, const Interval& intersect_t)
    {
        const Interval coll_int = linear::collision(a1, a2);
        return agent::conflict(coll_int, intersect_t);
    }

    namespace {
        String agent_to_string(const Agent& a, const Interval& t)
        {
            return a.to_string() + " at " + t.to_string();
        }
    }

    pair<Interval, Interval>
    unsafe_intervals(const Agent& a1, const Agent& a2, const Interval& t1, const Interval& t2)
    {
        Interval confl_int = linear::conflict(a1, a2, t1, t2);
        //! inconsistent with `in_conflict` where there is no apx.
        if (confl_int.apx_empty()) return {};

        if (a1.cstate().idle() && a2.cstate().idle()) {
            [[maybe_unused]] const auto [a1_al, a2_al] = aligned_agents(a1, a2, t1, t2);
            assert(linear::collision(a1_al, a2_al).infinite());
            THROW("Unavoidable conflict of idle agents\n"s
                  + agent_to_string(a1, t1) + "\n" + agent_to_string(a2, t2)
                  + "\nis happenning: "s + confl_int.to_string());
        }

        return detail::sim_unsafe_intervals_tp<1>(a1, a2, t1, t2, move(confl_int));
    }
}
