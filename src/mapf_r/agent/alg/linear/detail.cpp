#include "mapf_r/agent/alg/linear/detail.hpp"

#include <tomaqa/math/alg.hpp>

namespace mapf_r::agent::linear::detail {
    //+ pouzit primo jeho rozhrani ?
    Interval unsafe_interval(const Agent& a1, const Agent& a2, const Interval& t1, const Interval& t2)
    {
        assert(linear::is_collision(a1, a2));
        assert(t1.valid() && t2.valid());

        auto& s1 = a1.cstate();
        auto& s2 = a2.cstate();

        const Float abs_v1 = a1.cabs_v();
        const Float abs_v2 = a2.cabs_v();
        const auto v1 = s1.clinear_v();
        const auto v2 = s2.clinear_v();
        const auto v = v1 - v2;
        const Float rad = a1.cradius() + a2.cradius();

        const Float t1l = t1.clower();

        const auto rpos1 = s1.pos_at(-t1l);
        const auto rpos2 = s2.pos_at(-t2.clower());
        const Vec rvec(rpos1 - rpos2);

        Float alpha = v*v;
        Float beta_0 = 2*(rvec*v1 - rvec*v2);
        Float beta_1 = 2*(v1*v);
        Float gamma_0 = rvec*rvec - rad*rad;
        Float gamma_1 = 2*(rvec*v1);
        Float gamma_2 = abs_v1*abs_v1;

        Float a = beta_1*beta_1 - 4*alpha*gamma_2;
        Float b = 2*beta_0*beta_1 - 4*alpha*gamma_1;
        Float c = beta_0*beta_0 - 4*alpha*gamma_0;

        if (!apx_equal(a, 0))
        {
            Interval avoid_int = quadratic_ineq(a, b, c);
            if (!avoid_int) return {};

            avoid_int.neg() += t1l;
            //? why?
            avoid_int &= pos_inf_interval;
            avoid_int &= t2;
            if (avoid_int.apx_empty()) return {};

            assert(avoid_int.clower() == t1l);
            return avoid_int;
        }

        const Float beta = 2*(rvec*v);

        if (Interval avoid_int = quadratic_ineq(alpha, beta, gamma_0)) {
            //? why?
            avoid_int &= pos_inf_interval;
            avoid_int &= t1;
            if (avoid_int.apx_empty()) return {};
        }
        else return {};

        alpha = abs_v1*abs_v1 + abs_v2*abs_v2 - 2*(v1*v2);
        beta_1 = 2*(abs_v1*abs_v1 - v1*v2);

        const Float t2u = t2.cupper();

        a = alpha*gamma_2;
        b = alpha*beta_1*t2u + alpha*gamma_1;
        c = alpha*beta_0*t2u + alpha*gamma_0 + alpha*alpha*t2u*t2u;

        Interval avoid_int = quadratic_ineq(a, b, c);
        if (!avoid_int) return {};

        //? why?
        avoid_int.neg() &= pos_inf_interval;
        avoid_int += t1l;
        //? why?
        avoid_int &= pos_inf_interval;
        avoid_int &= t1;
        if (avoid_int.apx_empty()) return {};

        assert(avoid_int.clower() == t1l);
        return avoid_int;
    }
}
