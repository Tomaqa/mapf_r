#include "mapf_r/graph/alg.hpp"

#include "tomaqa/util/alg.hpp"

#include "mapf_r/graph/alg.tpp"

namespace mapf_r::graph {
    size_t compute_vertices_bit_width(const Graph& g)
    {
        return bit_width(size(g.cvertices()));
    }

    Properties make_properties(const Graph& g)
    {
        Coord min_{inf,inf};
        Coord max_ = -min_;

        auto& vs = g.cvertices();
        size_t min_deg = size(vs)-1;
        size_t max_deg = 0;

        for (auto& v : vs) {
            auto pos = v.cpos();
            min_.x = min(min_.x, pos.x);
            min_.y = min(min_.y, pos.y);
            max_.x = max(max_.x, pos.x);
            max_.y = max(max_.y, pos.y);

            const size_t deg = v.deg();
            min_deg = min(min_deg, deg);
            max_deg = max(max_deg, deg);
        }

        size_t vertices_bit_width = compute_vertices_bit_width(g);

        return {.min = min_, .max = max_, .min_deg = min_deg, .max_deg = max_deg,
                .vertices_bit_width = vertices_bit_width};
    }

    template Path make_shortest_path_of(const Min_dist_tp<conf::Min_dist_default>&);

    template Vector<Vertex::Id>
    sorted_successors_of(const Min_dist_tp<conf::Min_dist_default>&, const Vertex::Id&);
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace mapf_r::graph {
    Float Properties::width() const noexcept
    {
        return max.x - min.x;
    }

    Float Properties::height() const noexcept
    {
        return max.y - min.y;
    }

    ////////////////////////////////////////////////////////////////

    template class Min_dist_from_tp<conf::Min_dist_default>;

    ////////////////////////////////////////////////////////////////

    template class Min_dist_tp<conf::Min_dist_default>;

    ////////////////////////////////////////////////////////////////

    Reachable::Reachable(const Graph& g, const Vertex::Id& start_id_)
        : _graph(g), _start_id(start_id_)
    {
        auto& vec = _vertex_ids_vector;
        auto& sid = start_id();
        vec.reserve(g.cvertices().size()/2 + 1);
        vec.push_back({sid});
        _vertex_ids.insert(sid);
    }

    void Reachable::compute_next_jump()
    {
        auto& vec = _vertex_ids_vector;
        auto& vids = _vertex_ids;
        assert(!vec.empty());
        assert(!vids.empty());
        assert(n_jumps() >= 0);
        auto& g = graph();
        assert(vids.size() <= size(g.cvertices()));
        if (vids.size() == size(g.cvertices())) return;

        auto& last_vids = vec.back();
        Vertex_ids curr_vids;
        curr_vids.reserve(last_vids.size()*2);
        for (auto& vid : last_vids) {
            auto& v = g.cvertex(vid);
            for (auto& nid : v.cneighbor_ids()) {
                //? checking `last_vids` is sufficient? but it would be third lookup
                const auto [_, inserted] = vids.insert(nid);
                if (!inserted) continue;

                assert(!last_vids.contains(nid));
                curr_vids.insert(nid);
            }
        }

        vec.push_back(move(curr_vids));
    }
}
