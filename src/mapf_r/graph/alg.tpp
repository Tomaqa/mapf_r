#include "mapf_r/graph/alg.hpp"

#include "tomaqa/util/alg.hpp"

namespace mapf_r::graph {
    template <Class ConfT>
    Path make_shortest_path_of(const Min_dist_tp<ConfT>& min_dist)
    {
        Path path;
        path.reserve(min_dist.n_jumps());
        for (auto vid = min_dist.start_id(); vid != Vertex::invalid_id; vid = min_dist.successor_of(vid)) {
            path.push_back(vid);
        }
        return path;
    }

    template <Class ConfT>
    Vector<Vertex::Id> sorted_successors_of(const Min_dist_tp<ConfT>& min_dist, const Vertex::Id& vid)
    {
        using Conf = typename Min_dist_tp<ConfT>::Conf;

        typename Conf::Conv_dist conv_dist{};
        auto& g = min_dist.graph();

        assert(vid != Vertex::invalid_id);
        if (vid == min_dist.goal_id()) return {};
        assert(min_dist.from(vid) > 0);

        auto& v = g.cvertex(vid);
        auto& neighbor_ids = v.cneighbor_ids();
        assert(!empty(neighbor_ids));

        auto vec = to<Vector<Vertex::Id>>(neighbor_ids);
        const auto pos = v.cpos();
        sort(vec, [&min_dist, &conv_dist, &g, &pos, vid](auto& vid1, auto& vid2){
            if (vid1 == min_dist.successor_of(vid)) return true;
            if (vid2 == min_dist.successor_of(vid)) return false;
            auto& v1 = g.cvertex(vid1);
            auto& v2 = g.cvertex(vid2);
            const auto d1 = invoke(conv_dist, dist({pos, v1.cpos()}));
            const auto d2 = invoke(conv_dist, dist({pos, v2.cpos()}));
            assert(d1 > 0);
            assert(d2 > 0);
            assert(min_dist.from(vid1) >= 0);
            assert(min_dist.from(vid2) >= 0);
            return d1 + min_dist.from(vid1) < d2 + min_dist.from(vid2);
        });
        assert(size(neighbor_ids) == size(vec));
        assert(all_of(vec, [&neighbor_ids](auto& vid_){ return neighbor_ids.contains(vid_); }));
        assert(vec.front() == min_dist.successor_of(vid));
        assert(vec.size() == 1
               || invoke(conv_dist, dist({pos, g.cvertex(vec.front()).cpos()})) + min_dist.from(vec.front()) <=
                  invoke(conv_dist, dist({pos, g.cvertex(vec[1]).cpos()})) + min_dist.from(vec[1]));

        return vec;
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace mapf_r::graph {
    template <Class ConfT>
    Min_dist_from_tp<ConfT>::Min_dist_from_tp(const Graph& g, const Vertex::Id& start_id_)
        : _graph(g), _start_id(start_id_)
    {
        typename Conf::Conv_dist conv_dist{};

        Queue q;

        const size_t n_vertices = size(g.cvertices());

        dist_to_map.reserve(n_vertices);
        n_jumps_to_map.reserve(n_vertices);
        predecessor_map.reserve(n_vertices);

        for (auto& v : g.cvertices()) {
            auto& vid = v.cid();

            Float d = (vid == start_id_) ? 0. : inf;

            assert(vid == size(dist_to_map));
            assert(vid == size(n_jumps_to_map));
            assert(vid == size(predecessor_map));
            dist_to_map.push_back(d);
            n_jumps_to_map.push_back(0);
            predecessor_map.push_back(Vertex::invalid_id);
            q.push({.vid = vid, .dist = d});
        }
        assert(size(dist_to_map) == n_vertices);
        assert(size(n_jumps_to_map) == n_vertices);
        assert(size(predecessor_map) == n_vertices);

        while (!q.empty()) {
            auto [vid, d] = q.top();
            q.pop();
            assert(q.empty() || d <= q.top().dist);
            assert(d >= dist_to_map[vid]);
            if (d > dist_to_map[vid]) continue;
            auto& v = g.cvertex(vid);
            for (auto& nid : v.cneighbor_ids()) {
                auto& n = g.cvertex(nid);
                const Float vn_dist = dist({v.cpos(), n.cpos()});
                Float alt_dist = d + invoke(conv_dist, vn_dist);
                if (alt_dist >= dist_to_map[nid]) continue;

                dist_to_map[nid] = alt_dist;
                n_jumps_to_map[nid] = n_jumps_to_map[vid] + 1;
                predecessor_map[nid] = vid;
                q.push({.vid = nid, .dist = alt_dist});
            }
        }
    }

    ////////////////////////////////////////////////////////////////

    template <Class ConfT>
    Min_dist_tp<ConfT>::Min_dist_tp(const Graph& g, const Vertex::Id& start_id_, const Vertex::Id& goal_id_)
        : Inherit(g, start_id_), _goal_id(goal_id_)
    {
        //? inefficient
        auto min_dist_from_goal = Inherit(g, goal_id_);
        assert(to(goal_id_) == min_dist_from_goal.to(start_id_));
        assert(n_jumps_to(goal_id_) == min_dist_from_goal.n_jumps_to(start_id_));
        assert(all_of(g.cvertices(), [&](auto& v){
            auto& vid = v.cid();
            if (vid == Vertex::invalid_id) return false;
            const auto pid = this->predecessor_of(vid);
            const auto sid = min_dist_from_goal.predecessor_of(vid);
            if ((vid == start_id_) != (pid == Vertex::invalid_id)) return false;
            if ((vid == goal_id_) != (sid == Vertex::invalid_id)) return false;

            return true;
        }));
        dist_from_map = move(min_dist_from_goal.dist_to_map);
        n_jumps_from_map = move(min_dist_from_goal.n_jumps_to_map);
        successor_map = move(min_dist_from_goal.predecessor_map);
    }
}
