#include "mapf_r/smt/solver/base.hpp"

#include "mapf_r/graph/alg.hpp"

#include <fstream>
#include <chrono>

#include "mapf_r/graph/alg.tpp"

namespace mapf_r::smt::solver {
    String Profile::to_string() const
    {
        using tomaqa::to_string;

        return Inherit::to_string() + "\n"
             + declare_consts.to_string() + "\n"
             + assert_positions.to_string() + "\n"
             + assert_transitions.to_string() + "\n"
             + check_sat.to_string() + "\n"
             + get_model.to_string() + "\n"
             + assert_any_conflict.to_string() + "\n"
             + get_agent_values.to_string() + "\n"
             + unsafe_intervals.to_string() + "\n"
             + conflict_clause.to_string() + "\n"
             + minimize.to_string() + "\n"
             + "# encountered conflicts: " + to_string(n_conflicts) + "\n"
             ;
    }

    ////////////////////////////////////////////////////////////////

    void Base::set_graph(Graph& g)
    {
        _graph_l = &g;

        graph_properties() = graph::make_properties(cgraph());
    }

    void Base::set_layout(agent::Layout& l)
    {
        _layout_l = &l;

        const int n_agents = l.size();
        auto& ags = agents();
        auto& g = cgraph();
        ags.clear();
        ags.reserve(n_agents);
        for (int i = 0; i < n_agents; ++i) {
            agent::Id aid = i;
            auto& sid = l.cstart_id_of(aid);
            auto& start = g.cvertex(sid);
            assert(aid == int(ags.size()));
            ags.emplace_back(aid, l.cradius_of(aid), l.cabs_v_of(aid), start.cpos());
        }
    }

    void Base::produce_smt2_to(ostream& os)
    {
        _smt2_os_l = &os;
        options().produce_smt2 = true;
    }

    Float Base::guaranteed_suboptimal_coef() const noexcept
    {
        const Float obj = cfinal_objective_time();
        const Float lo = cfinal_objective_lower();
        if (obj == lo) return 1;

        assert(lo > 0);
        assert(obj > 0);

        return obj/lo;
    }

    void Base::parse(String str)
    {
        istringstream iss(move(str));
        parse(iss);
    }

    void Base::parse(istream& is)
    {
        using tomaqa::to_string;
        parse(to_string(is));
    }

    void Base::parse_file(const Path& path)
    {
        ifstream ifs(path);
        parse(ifs);
    }

    Float Base::move_time_float_of(const Agent& ag, const graph::Vertex& from, const graph::Vertex& to)
    {
        const Float d = math::dist({from.cpos(), to.cpos()});
        return move_time_float_of_dist(ag, d);
    }

    Float Base::move_time_float_of_dist(const Agent& ag, Float d)
    {
        const Float abs_v = ag.cabs_v();
        return d/abs_v;
    }

    void Base::solve(ostream& os)
    {
        auto& prof = profile();
        const auto prof_sg = prof.make_scope_guard();

        solve_init();
        prof.lap();
        solve_main(os);
        prof.lap();
        solve_finish();
    }

    void Base::solve_init()
    {
        auto& g = cgraph();
        auto& l = clayout();
        auto& ags = cagents();
        const auto n_ags = size(ags);

        auto& min_dist_map = graph_min_dists_map();
        auto& reach_map = graph_reachables_map();
        auto& path_map = graph_shortest_paths_map();
        min_dist_map.reserve(n_ags);
        reach_map.reserve(n_ags);
        path_map.reserve(n_ags);
        for (auto& ag : ags) {
            auto& aid = ag.cid();
            auto& sid = l.cstart_id_of(aid);
            auto& gid = l.cgoal_id_of(aid);
            assert(aid == size(min_dist_map));
            min_dist_map.push_back(Graph_min_dist(g, sid, gid));
            auto& min_dist = min_dist_map.back();

            assert(aid == size(reach_map));
            reach_map.push_back(graph::Reachable(g, sid));

            assert(aid == size(path_map));
            path_map.push_back(graph::make_shortest_path_of(min_dist));
        }

        if (producing_smt2()) print_smt2_head();
    }

    void Base::solve_finish()
    {
        if (producing_smt2()) print_smt2_tail();
    }

    void Base::get_model()
    {
        CVERB2LN("get_model()");
        // if (producing_smt2()) print_smt2("get-model");
        const auto prof_sg = profile().get_model.make_scope_guard();

        get_model_impl();
    }

    void Base::push()
    {
        CVERB2LN("push()");
        if (producing_smt2()) print_smt2_push();

        push_impl();
    }

    void Base::pop()
    {
        CVERB2LN("pop()");
        if (producing_smt2()) print_smt2_pop();

        pop_impl();
    }

    void Base::make_bool_pre(const String& id)
    {
        if (producing_smt2()) print_smt2_make_bool(id);
    }

    void Base::make_real_pre(const String& id)
    {
        if (producing_smt2()) print_smt2_make_real(id);
    }

    void Base::make_int_pre(const String& id)
    {
        if (producing_smt2()) print_smt2_make_int(id);
    }

    void Base::make_bv_pre(const String& id, size_t bits)
    {
        if (producing_smt2()) print_smt2_make_bv(id, bits);
    }

    void Base::check_sat_pre()
    {
        // Not used at this moment because we may be interested in the return status before this
        // if (producing_smt2()) print_smt2_check_sat();
    }

    agent::plan::Global Base::make_plan(ostream& os)
    {
        return make_plan_impl(os);
    }

    void Base::print_smt2_declare_const(const String& id, const String& type_str) const
    {
        print_smt2("declare-const "s + id + " " + type_str);
    }

    void Base::print_smt2_head() const
    {
        namespace chrono = std::chrono;
        using tomaqa::to_string;

        assert(producing_smt2());

        const auto now{chrono::system_clock::now()};
        const chrono::year_month_day date{chrono::floor<chrono::days>(now)};
        ostringstream source_oss;
        //+ some fields should probably be printed only if it is intended for SMT-COMP
        source_oss << "set-info :source |\n"
                   << "Generated by: Tomas Kolarik\n"
                   << "Generated on: " << date << "\n"
                   << "Generator: gitlab.com/Tomaqa/mapf_r\n"
                   << "Application: Multi-Agent Path-Finding with Continuous Time\n"
                   << "Target solver: Z3, CVC4, MathSAT\n"
                   << "Publications: Tomas Kolarik, Stefan Ratschan and Pavel Surynek:"
                   << " \"Multi-Agent Path-Finding with Continuous Time Using SAT Modulo Linear Real Arithmetic\""
                   << " in ICAART, SCITEPRESS, 2024.\n"
                   << "The benchmarks mimic MAPF problems with continuous time where the objective time must be sub-optimal, bounded by a user-specified coefficient."
                   << " In the original solver, a simulator checks whether there are collisions between particular agents, according to the current model."
                   << " This check is missing in the case of the final 'check-sat'."
                   << " The benchmarks also lack commands for preferring certain variables during the search which is of huge importance when searching for short paths in a graph."
                   << " However, the final plan must still avoid all collisions encountered by the original solver and the objective time must obey the sub-optimal coefficient."
                   << " Producing models is crucial for the application since the intended solver communicates values of particular variables with the simulator."
                   << " This communication is ommited though for simplicity."
                   << " The final 'get-value' allows to compare with the original solver that the objective time indeed obeys the coefficient."
                   << " Filenames without the extensions correspond to filenames of resulting plans of the original solver.\n"
                   << "|";

        print_smt2("set-info :smt-lib-version 2.6");
        print_smt2_head_set_logic();
        print_smt2(move(source_oss).str());
        print_smt2(R"(set-info :license "https://creativecommons.org/licenses/by/4.0/")");
        print_smt2(R"(set-info :category "industrial")");
        print_smt2("set-option :produce-models true");
    }

    void Base::print_smt2_tail() const
    {
        assert(producing_smt2());

        if (is_sat()) {
            print_smt2("get-value ("s + objective_time_var_name + ")");
        }
        print_smt2("exit");
    }

    void Base::print_smt2_push() const
    {
        print_smt2("push 1");
    }

    void Base::print_smt2_pop() const
    {
        print_smt2("pop 1");
    }

    void Base::print_smt2_make_bool(const String& id) const
    {
        print_smt2_declare_const(id, "Bool");
    }

    void Base::print_smt2_make_real(const String& id) const
    {
        print_smt2_declare_const(id, "Real");
    }

    void Base::print_smt2_make_int(const String& id) const
    {
        print_smt2_declare_const(id, "Int");
    }

    void Base::print_smt2_make_bv(const String& id, size_t bits) const
    {
        using tomaqa::to_string;

        print_smt2_declare_const(id, "(_ BitVec "s + to_string(bits) + ")");
    }

    void Base::print_smt2_check_sat() const
    {
        return print_smt2("check-sat");
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace mapf_r::graph {
    template Path make_shortest_path_of(const Min_dist_tp<smt::solver::Base::Graph_min_dist_conf>&);

    template Vector<Vertex::Id>
    sorted_successors_of(const Min_dist_tp<smt::solver::Base::Graph_min_dist_conf>&, const Vertex::Id&);
}
