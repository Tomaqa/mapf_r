#include "mapf_r/smt/solver/mathsat.hpp"

#include <regex>

namespace mapf_r::smt::solver {
    Mathsat::~Mathsat() noexcept
    {
        if (!_destroy) return;

        if (!MSAT_ERROR_MODEL(model)) ::msat_destroy_model(model);

        auto& env = solver();
        if constexpr (!Solver_conf::manual_optimize) {
            if constexpr (Solver_conf::always_optimize) ::msat_destroy_objective(env, objective);
            else if (optimizing()) ::msat_destroy_objective(env, objective);
        }
        ::msat_destroy_env(env);
    }

    Mathsat::Mathsat(Mathsat&& rhs) noexcept
        : Inherit(move(rhs))
        , _destroy(rhs._destroy)
    {
        move(rhs).lmove_to(*this);
    }

    Mathsat& Mathsat::operator =(Mathsat&& rhs) noexcept
    {
        Inherit::operator =(move(rhs));
        move(rhs).move_to(*this);
        return *this;
    }

    void Mathsat::move_to(Mathsat& rhs)&&
    {
        if (this == &rhs) return;

        rhs._destroy = _destroy;
        move(*this).lmove_to(rhs);
    }

    void Mathsat::lmove_to(Mathsat& rhs)&&
    {
        rhs.objective = move(objective);
        rhs.model = move(model);
        rhs._bool_type = move(_bool_type);
        rhs._real_type = move(_real_type);
        rhs._int_type = move(_int_type);
        rhs._inf = move(_inf);
        rhs._eps = move(_eps);

        _destroy = false;
    }

    void Mathsat::parse(String str)
    {
        Expr phi = ::msat_from_smtlib2(solver(), str.c_str());
        assert_expr(move(phi));
    }

    void Mathsat::parse_file(const Path& path)
    {
        FILE* f = fopen(path.c_str(), "r");
        Expr phi = ::msat_from_smtlib2_file(solver(), f);
        fclose(f);
        assert_expr(move(phi));
    }

    Mathsat::Expr Mathsat::make_var(const String& id, ::msat_type type)
    {
        auto& env = solver();
        ::msat_decl decl = ::msat_declare_function(env, id.c_str(), type);
        assert(!MSAT_ERROR_DECL(decl));
        return ::msat_make_constant(env, decl);
    }

    Mathsat::Expr Mathsat::make_real_impl(Rational rat)
    {
        ::mpq_t mpq_val;
        ::mpq_init(mpq_val);
        ::mpq_set_si(mpq_val, rat.numerator(), rat.denominator());
        return make_real_impl(move(mpq_val));
    }

    Mathsat::Expr Mathsat::make_real_impl(::mpq_t mpq_val)
    {
        auto& env = solver();
        Expr e = ::msat_make_mpq_number(env, mpq_val);
        assert(!MSAT_ERROR_TERM(e));
        ::mpq_clear(mpq_val);
        return e;
    }

    Mathsat::Expr Mathsat::min(const Expr& lhs, const Expr& rhs)
    {
        auto& env = solver();
        return ::msat_make_term_ite(env, leq(lhs, rhs), lhs, rhs);
    }

    Mathsat::Expr Mathsat::max(const Expr& lhs, const Expr& rhs)
    {
        auto& env = solver();
        return ::msat_make_term_ite(env, geq(lhs, rhs), lhs, rhs);
    }

    void Mathsat::get_model_impl()
    {
        auto& env = solver();
        // `::msat_load_objective_model` seems to be useless
        if (!MSAT_ERROR_MODEL(model)) ::msat_destroy_model(model);
        model = ::msat_get_model(env);
    }

    Float Mathsat::to_float(const Expr& e) const
    {
        ::mpq_t mpq_val;
        set_mpq(mpq_val, e);
        Float val = ::mpq_get_d(mpq_val);
        ::mpq_clear(mpq_val);
        return val;
    }

    Rational Mathsat::to_rational(const Expr& e) const
    {
        ::mpq_t mpq_val;
        set_mpq(mpq_val, e);
        Rational val{::mpz_get_si(mpq_numref(mpq_val)), ::mpz_get_si(mpq_denref(mpq_val))};
        ::mpq_clear(mpq_val);
        return val;
    }

    void Mathsat::set_mpq(::mpq_t mpq_val, const Expr& e) const
    {
        auto& env = csolver();
        ::mpq_init(mpq_val);
        [[maybe_unused]] const int res = ::msat_term_to_number(env, e, mpq_val);
        assert(res == 0);
    }

    Mathsat::Expr Mathsat::get_min_objective_time_impl()
    {
        assert(Solver_conf::optimize);
        assert(!Solver_conf::manual_optimize);
        auto& env = solver();
        assert(::msat_objective_value_is_unbounded(env, objective, MSAT_OPTIMUM) == 0);
        // `::msat_objective_value_is_strict` can hold due to `_eps`
        // `MSAT_OPTIMUM` seems to be equivalent with `MSAT_FINAL_LOWER`
        return ::msat_objective_value_term(env, objective, MSAT_OPTIMUM, _inf, _eps);
    }

    Mathsat::Sat_result Mathsat::check_sat_impl()
    {
        auto& env = solver();
        if constexpr (Solver_conf::solve_with_assumptions) {
            return ::msat_solve_with_assumptions(env, &max_k_assumption(), 1);
        }

        // there seems to be a bug in the case of solving with assumptions and optimization
        Sat_result res = ::msat_solve(env);
        assert(res != Solver_conf::sat || !optimizing() || Solver_conf::manual_optimize
               || util::contains({MSAT_OPT_SAT_OPTIMAL, MSAT_OPT_SAT_APPROX},
                                 ::msat_objective_result(env, objective)));
        return res;
    }

    void Mathsat::assert_minimize_impl()
    {
        assert(!Solver_conf::manual_optimize);

        auto& env = solver();

        objective = ::msat_make_minimize(env, objective_time());
        assert(!MSAT_ERROR_OBJECTIVE(objective));

        [[maybe_unused]] const int res = ::msat_assert_objective(env, objective);
        assert(res == 0);
    }

    void Mathsat::solve_init()
    {
        using tomaqa::to_string;

        auto& env = solver();
        auto cfg = ::msat_create_default_config("QF_LRA");
        ::msat_set_option(cfg, "model_generation", "true");
        ::msat_set_option(cfg, "preprocessor.full_cnf_conversion", "false");
        ::msat_set_option(cfg, "preprocessor.partial_nnf_conversion", "false");
        // I suppose that disabling simplifications is beneficial unless there are too many conflicts
        ::msat_set_option(cfg, "preprocessor.simplification", "0");
        // as a side effect this significantly reduces 'active_vertex_id' profile time
        ::msat_set_option(cfg, "preprocessor.toplevel_propagation", "false");
        // prefer 'false'
        ::msat_set_option(cfg, "dpll.branching_initial_phase", "0");
        ::msat_set_option(cfg, "dpll.ghost_filtering", "true");
        ::msat_set_option(cfg, "dpll.minimize_model", "true");
        ::msat_set_option(cfg, "dpll.preprocessor.mode", "0");
        // dynamic Luby strategy ("frequent restarts")
        // non-dynamic ('1') seems to be the same ..
        // geometric ('0', default) seems to be quite similar ..
        ::msat_set_option(cfg, "dpll.restart_strategy", "2");
        // <= '2' is bad
        ::msat_set_option(cfg, "dpll.restart_initial", "4");
        ::msat_set_option(cfg, "theory.la.enabled", "true");
        ::msat_set_option(cfg, "theory.na.enabled", "false");
        ::msat_set_option(cfg, "theory.arr.enabled", "false");
        ::msat_set_option(cfg, "theory.fp.enabled", "false");
        ::msat_set_option(cfg, "theory.euf.enabled", "false");
        // 'lazy_expl_threshold' has effect but I doubt there is a value that works well in general
        if constexpr (Solver_conf::use_vertex_bvs) {
            ::msat_set_option(cfg, "theory.bv.enabled", "true");
            // seems reasonable since only equalities are used; much faster in large graphs
            ::msat_set_option(cfg, "theory.bv.eager", "true");
            // '0' is bad, '2' used to seem more stable, but currently '1' is just better ..
            ::msat_set_option(cfg, "theory.bv.bit_blast_mode", "1");
        }
        else {
            ::msat_set_option(cfg, "theory.bv.enabled", "false");
        }
        if constexpr (Solver_conf::optimize && !Solver_conf::manual_optimize) {
            constexpr auto eps_ = Precision::Lower::abs_eps()*1.5;

            ::msat_set_option(cfg, "optimization", "true");
            ::msat_set_option(cfg, "opt.strategy", "bin");
            ::msat_set_option(cfg, "opt.bin.pivot_position", "0.5");
            //? the resulting objective can be way more different than this
            ::msat_set_option(cfg, "opt.abort_interval", to_string(eps_).c_str());
            // 'opt.soft_timeout' does not seem to have any effect
        }
        else {
            ::msat_set_option(cfg, "optimization", "false");
        }
        env = ::msat_create_opt_env(cfg);
        ::msat_destroy_config(cfg);

        _bool_type = ::msat_get_bool_type(env);
        assert(!MSAT_ERROR_TYPE(_bool_type));
        _real_type = ::msat_get_rational_type(env);
        assert(!MSAT_ERROR_TYPE(_real_type));
        _int_type = ::msat_get_integer_type(env);
        assert(!MSAT_ERROR_TYPE(_int_type));

        Inherit::solve_init();

        _inf = make_real("oo");
        if constexpr (Solver_conf::optimize && !Solver_conf::manual_optimize) {
            //? this may fail `is_apx_point` assertion
            _eps = make_real(Precision::abs_eps()*5);
        }
        else {
            _eps = make_real("eps");
        }

        _minus_one = make_real(-1);

        assert(MSAT_ERROR_MODEL(model));
    }

    String Mathsat::expr_to_string(const Expr& e) const
    {
        char* c_str = ::msat_term_repr(e);
        String str(c_str);
        ::msat_free(c_str);
        return str;
    }

    String Mathsat::expr_to_smt2(const Expr& e) const
    {
        auto& env = csolver();
        char* c_str = ::msat_to_smtlib2_term(env, e);

        static const std::regex re_to_real(R"(\(to_real ([0-9]+|\([^)]+\))\))");
        String str = std::regex_replace(c_str, re_to_real, "$1");

        static const std::regex re_def(R"(\.def_([0-9]+))");
        str = std::regex_replace(str.c_str(), re_def, "def_$1");

        ::msat_free(c_str);
        return str;
    }
}
