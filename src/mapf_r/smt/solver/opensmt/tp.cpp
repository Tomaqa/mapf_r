#include "mapf_r/smt/solver/opensmt.hpp"

#include "mapf_r/smt/solver.tpp"

namespace mapf_r::smt {
    template class
        Solver<solver::opensmt::Conf, ::opensmt_logic, const char*, Unique_ptr<::SMTConfig>>;
}
