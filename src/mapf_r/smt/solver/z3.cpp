#include "mapf_r/smt/solver/z3.hpp"

#include <tomaqa/util/rob.hpp>

namespace tomaqa::util::rob {
    // `set_context` is private - rob it
    struct Z3_set_context_tag : Member_function_tag_base<::z3::context, void, ::Z3_context> {
        friend Type access(Z3_set_context_tag);
    };
    constexpr Z3_set_context_tag z3_set_context_tag;

    template struct Access<Z3_set_context_tag, &::z3::context::set_context>;

    // `detach` is private - rob it
    struct Z3_detach_context_tag : Member_function_tag_base<::z3::context> {
        friend Type access(Z3_detach_context_tag);
    };
    constexpr Z3_detach_context_tag z3_detach_context_tag;

    template struct Access<Z3_detach_context_tag, &::z3::context::detach>;
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace mapf_r::smt::solver::z3 {
    using ::z3::expr;

    Context::Context(Context&& rhs)
    {
        move(rhs).lmove_to(*this);
    }

    Context& Context::operator =(Context&& rhs)
    {
        move(rhs).move_to(*this);
        return *this;
    }

    void Context::move_to(Context& rhs)&&
    {
        if (this == &rhs) return;

        move(*this).lmove_to(rhs);
    }

    void Context::lmove_to(Context& rhs)&&
    {
        (rhs.*access(rob::z3_set_context_tag))(*this);
        (this->*access(rob::z3_detach_context_tag))();
    }

    Optional<expr> Context::try_add_real(expr e)
    {
        return {};

        auto& map = floats_map();

        assert(e.is_real());
        const Float val = e.as_double();
        const auto [it, inserted] = map.emplace(val, e);
        if (!inserted) return {};

        Optional<expr> opt_e;
        if (it != map.begin()) {
            auto [prev_val, prev_e] = *prev(it);
            assert(prev_val < val);
            if (apx_equal(val, prev_val)) {
                opt_e.emplace(prev_e);
            }
        }
        if (it != --map.end()) {
            auto [next_val, next_e] = *next(it);
            assert(next_val > val);
            if (apx_equal(val, next_val)) {
                assert(!opt_e);
                opt_e.emplace(next_e);
            }
        }

        if (opt_e) map.erase(it);

        return opt_e;
    }

    ////////////////////////////////////////////////////////////////

    expr Model::eval(const expr& n, bool model_completion) const
    {
        auto e = Inherit::eval(n, model_completion);
        assert(!e.is_real() || !ctx().try_add_real(e));
        return e;
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace mapf_r::smt::solver {
    void Z3::parse(String str)
    {
        solver().from_string(move(str).c_str());
    }

    void Z3::parse_file(const Path& path)
    {
        // It seems that it only parses declarations and assertions, nothing else
        solver().from_file(path.c_str());
    }

    Rational Z3::to_rational(const Expr& e) const
    {
        return {e.numerator().as_int64(), e.denominator().as_int64()};
    }

    unsigned Z3::to_int(const Expr& e) const
    {
        auto val = e.as_uint64();
        assert(val <= limits<unsigned>::max());
        return val;
    }

    void Z3::assert_minimize_impl()
    {
        minimize_handle = solver().minimize(objective_time());
    }

    void Z3::solve_init()
    {
        auto& ctx = context();
        auto& s = solver();

        ::z3::set_param("auto_config", true);
        ::z3::set_param("model", true);
        ::z3::set_param("model_validate", false);
        ::z3::set_param("verbose", 0);

        ::z3::params params(ctx);
        // 'maxsat_engine' and 'optsmt_engine' seem to have no effect
        // 'maxres.*' params seem to have no significant effect
        //? params.set("opt.incremental", true);
        s.set(params);

        Inherit::solve_init();
    }
}
