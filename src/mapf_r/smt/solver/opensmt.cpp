#include "mapf_r/smt/solver/opensmt.hpp"

#include <regex>

namespace mapf_r::smt::solver {
    Opensmt::Opensmt()
        : Opensmt(::opensmt_logic::qf_lra, "mapf_r", new_config())
    { }

    Unique_ptr<::SMTConfig> Opensmt::new_config()
    {
        auto cfg_ptr = make_unique<::SMTConfig>();
        auto& cfg = *cfg_ptr;

        const char* msg;
        [[maybe_unused]] bool ok = true;

        //? ok &= cfg.setOption(::SMTConfig::o_sat_pure_lookahead, ::SMTOption(1), msg);
        //? ok &= cfg.setOption(::SMTConfig::o_sat_picky, ::SMTOption(1), msg);
        assert(ok);

        //? cfg.sat_preprocess_booleans = 0;
        //? cfg.sat_preprocess_theory = 1;

        return cfg_ptr;
    }

    void Opensmt::parse(String str)
    {
        NOT_IMPLEMENTED_YET;
    }

    Opensmt::Expr Opensmt::make_real_impl(Rational rat)
    {
        static_assert(is_same_v<::opensmt::Number, ::FastRational>);
        static_assert(sizeof(Rational::int_type) <= sizeof(::word));
        static_assert(sizeof(::uword) == sizeof(::word));

        return logic().mkRealConst({rat.numerator(), rat.denominator()});
    }

    Opensmt::Expr Opensmt::min(const Expr& lhs, const Expr& rhs)
    {
        return logic().mkIte(leq(lhs, rhs), lhs, rhs);
    }

    Opensmt::Expr Opensmt::max(const Expr& lhs, const Expr& rhs)
    {
        return logic().mkIte(geq(lhs, rhs), lhs, rhs);
    }

    void Opensmt::get_model_impl()
    {
        model_ptr = solver().getModel();
    }

    Float Opensmt::to_float(const Expr& e) const
    {
        static_assert(sizeof(Float) >= sizeof(double));

        auto& orat = get_orig_number(e);
        return orat.get_d();
    }

    Rational Opensmt::to_rational(const Expr& e) const
    {
        auto& orat = get_orig_number(e);
        // type checking is already done in `make_real_impl`
        auto opt_pair = orat.tryGetNumDen();
        assert(opt_pair);
        return {move(opt_pair->first), move(opt_pair->second)};
    }

    unsigned Opensmt::to_int(const Expr& e) const
    {
        auto& orat = get_orig_number(e);
        auto opt_pair = orat.tryGetNumDen();
        assert(opt_pair);
        assert(opt_pair->second == 1);
        auto num = move(opt_pair->first);
        static_assert(sizeof(num) <= sizeof(unsigned));
        assert(num >= 0);
        return num;
    }

    Opensmt::Sat_result Opensmt::check_sat_impl()
    {
        return solver().check();
    }

    String Opensmt::expr_to_smt2(const Expr& e) const
    {
        String str = clogic().pp(e);

        static const std::regex re_minus(R"(-([0-9]+[0-9/]*))");
        str = std::regex_replace(str.c_str(), re_minus, "(- $1)");

        static const std::regex re_div(R"(([0-9]+)/([0-9]+))");
        str = std::regex_replace(str.c_str(), re_div, "(/ $1 $2)");

        return str;
    }
}
