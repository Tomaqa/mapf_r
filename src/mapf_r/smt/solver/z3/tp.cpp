#include "mapf_r/smt/solver/z3.hpp"

#include "mapf_r/smt/solver.tpp"

namespace mapf_r::smt {
    template class Solver<solver::z3::Conf>;
}
