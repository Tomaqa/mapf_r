#include "mapf_r/smt/solver/mathsat.hpp"

#include "mapf_r/smt/solver.tpp"

namespace mapf_r::smt {
    template class Solver<solver::mathsat::Conf>;
}
