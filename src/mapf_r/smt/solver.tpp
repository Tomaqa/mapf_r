#pragma once

#include "mapf_r/smt/solver.hpp"

#include "mapf_r/agent/alg.hpp"

#include "tomaqa/util/random.hpp"
#include "tomaqa/util/scope_guard.hpp"

namespace mapf_r::smt {
    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_bool(const String& id)
    {
        this->make_bool_pre(id);
        return make_bool_impl(id);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_real(const String& id)
    {
        this->make_real_pre(id);
        return make_real_impl(id);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_int_directly(const String& id)
    {
        this->make_int_pre(id);
        return make_int_impl(id);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_bv_directly(const String& id, size_t bits)
    {
        this->make_bv_pre(id, bits);
        return make_bv_impl(id, bits);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_disj(Expr_vector vec)
    {
        const int size_ = size(vec);
        assert(size_ >= 1);
        Expr e = move(vec[0]);
        for (Idx i = 1; i < size_; ++i) {
            e = disj(e, vec[i]);
        }

        return e;
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::get_model()
    {
        Inherit::get_model();

        for (auto& ag_active_vertex_id_cache : active_vertex_id_cache) {
            for (auto& opt_vid : ag_active_vertex_id_cache) {
                opt_vid.invalidate();
            }
        }
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::get_model_value(const Expr& e) const
    {
        // no profiling here - it is too frequent operation

        return get_model_value_impl(e);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    unsigned Solver<SolverConf, Args...>::to_int(const Expr& e) const
    {
        // fallback implementation that uses conversion to rational, potentionally inefficient
        auto rat = to_rational(e);
        assert(rat.denominator() == 1);
        auto val = move(rat.numerator());
        assert(val >= 0);
        static_assert(sizeof(val) <= sizeof(unsigned));
        return val;
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::get_objective_time()
    {
        if constexpr (!Solver_conf::manual_optimize) {
            if constexpr (Solver_conf::always_optimize) return get_min_objective_time();
            else if (optimizing()) return get_min_objective_time();
        }

        return get_model_value(objective_time());
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::get_min_objective_time()
    {
        if constexpr (Solver_conf::optimize && !Solver_conf::manual_optimize) {
            assert(optimizing());
            return get_min_objective_time_impl();
        }
        else return get_objective_time();
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    Vector<typename Solver<SolverConf, Args...>::Expr>::const_iterator
    Solver<SolverConf, Args...>::find_active_vertex_expr(const agent::Id& aid, Idx k) const
    {
        if constexpr (Solver_conf::use_vertex_bools) {
            auto& vexprs = vertex_exprs[aid][k];
            const auto find_f = [this](auto& e){
                return get_model_bool(e);
            };
            auto it = find_if(vexprs, find_f);
            assert(it != cend(vexprs));
            assert(find_if(it+1, cend(vexprs), find_f) == cend(vexprs));

            return it;
        }
        else {
            auto& vexprs = vertex_exprs[aid];
            auto it = std::next(cbegin(vexprs), k);
            assert(it != cend(vexprs));
            return it;
        }
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    Vertex::Id Solver<SolverConf, Args...>::active_vertex_id(const agent::Id& aid, Idx k) const
    {
        // it has almost zero effect in the case of `use_vertex_bools`
        auto& opt_vid = active_vertex_id_cache[aid][k];
        if (opt_vid) return *opt_vid;

        const auto it = find_active_vertex_expr(aid, k);

        Vertex::Id vid;
        if constexpr (Solver_conf::use_vertex_bools) {
            auto& vexprs = vertex_exprs[aid][k];
            vid = distance(begin(vexprs), it);
        }
        else {
            const Expr& vexpr = *it;
            auto val = get_model_int(vexpr);
            assert(val <= limits<Vertex::Id>::max());
            vid = move(val);
            assert(vid >= 0);
        }

        opt_vid.emplace(vid);
        return vid;
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::assert_expr(Expr e)
    {
        assert_expr_pre(e);
        return assert_expr_impl(move(e));
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::assert_expr_vector(Expr_vector& vec)
    {
        // it may happen that `Expr_vector` does not return lref
        for (auto&& c : vec) {
            assert_expr(move(c));
        }
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::assert_expr_pre(Expr& e)
    {
        if (this->producing_smt2()) print_smt2_assert(e);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::push_to_expr_vector(Expr_vector& vec, Expr e)
    {
        if constexpr (requires { vec.push_back(move(e)); })
            vec.push_back(move(e));
        else NOT_IMPLEMENTED;
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::clear_expr_vector(Expr_vector& vec)
    {
        if constexpr (requires { vec.clear(); })
            vec.clear();
        else NOT_IMPLEMENTED;
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    typename Solver<SolverConf, Args...>::Sat_result
    Solver<SolverConf, Args...>::check_sat()
    {
        this->check_sat_pre();
        auto res = check_sat_impl();
        check_sat_post(res);

        return res;
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::check_sat_post(const Sat_result& res)
    {
        if (this->producing_smt2()) print_smt2_check_sat_with_status(res);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::before_push()
    { }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::after_push()
    { }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::before_pop()
    { }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::after_pop()
    {
        if constexpr (!Solver_conf::solve_with_assumptions) {
            assert_conflicts_after_pop();
        }
        else if constexpr (Solver_conf::manual_optimize) {
            if (optimizing()) assert_conflicts_after_pop();
        }
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::declare_consts(Idx k)
    {
        CVERBLN("declare_consts(" << k << ")");
        const auto prof_sg = this->profile().declare_consts.make_scope_guard();
        declare_consts_impl(k);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::declare_consts_impl(Idx k)
    {
        auto& g = this->cgraph();

        const bool first_call = k == 0;

        const Expr zero_e = make_real(0);

        if (first_call) {
            auto& gprop = this->cgraph_properties();
            const auto max_deg = gprop.max_deg;
            assert(max_deg > 0);

            const size_t n_priorities = transition_priority + max_deg;
            preferred_variables.resize(n_priorities);
        }

        for (auto& ag : this->cagents()) {
            auto& aid = ag.cid();

            if (first_call) {
                vertex_exprs.emplace_back();
                abs_time_reals.emplace_back();
                rel_time_reals.emplace_back();
                wait_time_reals.emplace_back();
                move_time_reals.emplace_back();
                if constexpr (Solver_conf::always_avoid_waiting) wait_preferred_variables.emplace_back();
                position_preferred_variables.emplace_back();
                transition_preferred_variables.emplace_back();
                active_vertex_id_cache.emplace_back();
            }

            auto& ag_vexprs = vertex_exprs[aid];
            auto& ag_atimes = abs_time_reals[aid];
            auto& ag_rtimes = rel_time_reals[aid];
            auto& ag_wtimes = wait_time_reals[aid];
            auto& ag_mtimes = move_time_reals[aid];
            auto& ag_active_vertex_id_cache = active_vertex_id_cache[aid];
            if constexpr (Solver_conf::use_vertex_bools) ag_vexprs.emplace_back();
            else ag_vexprs.emplace_back(make_expr());
            ag_atimes.emplace_back(make_expr());
            ag_rtimes.emplace_back(make_expr());
            ag_wtimes.emplace_back(make_expr());
            ag_mtimes.emplace_back(make_expr());
            ag_active_vertex_id_cache.emplace_back();

            if constexpr (Solver_conf::use_vertex_bools) for (auto& v : g.cvertices()) {
                Expr vexpr = make_vertex_bool_at(ag, v, k);

                ag_vexprs.back().emplace_back(vexpr);
            }
            else {
                ag_vexprs.back() = make_vertex_expr_at(ag, k);
            }

            Expr atime = make_real(make_abs_time_var_name_at(ag, k));

            if (first_call) {
                assert_expr(eq(atime, zero_e));
            }
            else {
                auto& prev_atime = ag_atimes[k-1];
                auto& prev_rtime = ag_rtimes[k-1];
                assert_expr(eq(atime, plus(prev_atime, prev_rtime)));
            }

            ag_atimes.back() = atime;

            Expr rtime = make_real(make_rel_time_var_name_at(ag, k));
            Expr wtime = make_real(make_wait_time_var_name_at(ag, k));
            Expr mtime = make_real(make_move_time_var_name_at(ag, k));

            assert_expr(eq(rtime, plus(wtime, mtime)));
            assert_expr(geq(wtime, zero_e));
            assert_expr(geq(mtime, zero_e));

            ag_rtimes.back() = rtime;
            ag_wtimes.back() = wtime;
            ag_mtimes.back() = mtime;

            declare_wait_preferred_variables(ag, k);
            declare_position_preferred_variables(ag, k);
            declare_transition_preferred_variables(ag, k);
        }
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::declare_wait_preferred_variables(const Agent& ag, Idx k)
    {
        static_assert(!(!Solver_conf::avoid_waiting && Solver_conf::always_avoid_waiting));

        using tomaqa::to_string;

        auto& aid = ag.cid();

        if constexpr (!Solver_conf::always_avoid_waiting) {
            const bool first_call = k == 0;
            if (!first_call) return;

            wait_preferred_variables.emplace_back(make_bool("wpref"s + to_string(aid)));
        }

        const Expr zero_e = make_real(0);

        auto& wtime = wait_time_reals[aid][k];
        auto& ag_wpref = invoke([&]() -> auto& {
            if constexpr (!Solver_conf::always_avoid_waiting) return wait_preferred_variables.back();
            else {
                auto& ag_wprefs = wait_preferred_variables[aid];
                ag_wprefs.emplace_back(make_bool(make_var_name_at(ag, "wpref", k)));
                return ag_wprefs.back();
            }
        });

        // without optimization:
        // `avoid_waiting`: a bit faster plans (~94%), slightly slower to compute (~118%)
        // `always_avoid_waiting`: faster plans (~~83% ^^), very much slower to compute ..

        // regardless optimization:
        // it seems that keeping the pref. vars is still beneficial, even when `!avoid_waiting`
        assert_expr(implies(ag_wpref, eq(wtime, zero_e)));
        if constexpr (Solver_conf::avoid_waiting) {
            preferred_variables[init_wait_priority].push_back(ag_wpref);
        }
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::declare_position_preferred_variables(const Agent& ag, Idx k)
    {
        auto& aid = ag.cid();

        auto& pos_prefs = position_preferred_variables[aid];
        pos_prefs.emplace_back(make_bool(make_var_name_at(ag, "ppref", k)));
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::declare_transition_preferred_variables(const Agent& ag, Idx k)
    {
        auto& aid = ag.cid();

        auto& gprop = this->cgraph_properties();
        const auto max_deg = gprop.max_deg;

        auto& all_trans_prefs = transition_preferred_variables[aid];
        all_trans_prefs.emplace_back();
        auto& trans_prefs = all_trans_prefs.back();
        for (Idx i = 1; i <= max_deg; ++i) {
            trans_prefs.emplace_back(make_bool(make_var_name_at(make_var_name_at(ag, "spref", k), i)));
        }
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::assert_init()
    {
        CVERBLN("assert_init()");
        assert_init_impl();
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::assert_init_impl()
    {
        auto& sids_map = this->clayout().cstart_ids_map();
        assert(size(sids_map) == size(this->cagents()));

        Expr_vector vec = make_expr_vector();
        if constexpr (requires { vec.reserve(1); }) {
            vec.reserve(size(sids_map));
        }

        const Expr zero_e = make_real(0);

        for (auto& [aid, sid] : sids_map) {
            assert_expr(vertex_expr_at(aid, 0, sid));

            assert(size(wait_time_reals[aid]) == 1);
            auto& wtime = wait_time_reals[aid].back();
            push_to_expr_vector(vec, eq(wtime, zero_e));
        }

        assert(size(vec) == size(this->cagents()));
        assert_expr(make_disj(move(vec)));
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::assert_positions(Idx k)
    {
        CVERBLN("assert_positions(" << k << ")");
        const auto prof_sg = this->profile().assert_positions.make_scope_guard();
        assert_positions_impl(k);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::assert_positions_impl(Idx k)
    {
        [[maybe_unused]] auto& g = this->cgraph();

        for (auto& ag : this->cagents()) {
            auto& aid = ag.cid();

            if constexpr (Solver_conf::use_vertex_bools) {
                auto& ag_vexprs = vertex_exprs[aid];
                auto& vertices = g.cvertices();
                const Idx last_vid = size(vertices)-1;
                for (auto& v1 : vertices) {
                    auto& vid1 = v1.cid();
                    for (Vertex::Id vid2 = vid1+1; vid2 <= last_vid; ++vid2) {
                        assert_expr(neg(conj(ag_vexprs[k][vid1], ag_vexprs[k][vid2])));
                    }
                }
            }

            assert_position_preferred_variables(aid, k);
        }
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::assert_position_preferred_variables(const agent::Id& aid, Idx k)
    {
        auto& path = this->graph_shortest_path_of(aid);
        if (k >= path.size()) return;
        auto& vid = path[k];
        const Expr vexpr = vertex_expr_at(aid, k, vid);
        auto& pos_pref = position_preferred_variables[aid][k];
        assert_expr(implies(pos_pref, vexpr));
        preferred_variables[position_priority].push_back(pos_pref);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::assert_transitions(Idx k)
    {
        CVERBLN("assert_transitions(" << k << ")");
        const auto prof_sg = this->profile().assert_transitions.make_scope_guard();
        assert_transitions_impl(k);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::assert_transitions_impl(Idx k)
    {
        auto& g = this->cgraph();
        auto& l = this->clayout();

        assert(k > 0);

        const Expr zero_e = make_real(0);

        for (auto& ag : this->cagents()) {
            auto& aid = ag.cid();

            auto& reach = this->graph_reachable_of(aid);
            if (k > 1) reach.compute_next_jump();
            auto& reach_vids = reach.vertex_ids();

            auto& ag_wtimes = wait_time_reals[aid];
            auto& ag_mtimes = move_time_reals[aid];
            auto& ag_goal_id = l.cgoal_id_of(aid);
            for (auto& from_vid : reach_vids) {
                auto& from_v = g.cvertex(from_vid);
                const Expr ag_from_vexpr = vertex_expr_at(aid, k-1, from_vid);
                auto or_vec = make_expr_vector();
                for (auto to_vid : from_v.cneighbor_ids()) {
                    assert(to_vid != Vertex::invalid_id);
                    assert(to_vid != from_vid);
                    auto& to_v = g.cvertex(to_vid);
                    Expr t_e = make_move_time_real_of(ag, from_v, to_v);
                    const Expr ag_to_vexpr = vertex_expr_at(aid, k, to_vid);
                    assert_expr(implies(conj(ag_from_vexpr, ag_to_vexpr),
                                        eq(ag_mtimes[k-1], t_e)));

                    push_to_expr_vector(or_vec, ag_to_vexpr);
                }

                if (from_vid == ag_goal_id) {
                    const Expr ag_goal_vexpr = vertex_expr_at(aid, k, ag_goal_id);
                    assert_expr(implies(conj(ag_from_vexpr, ag_goal_vexpr),
                                        conj(eq(ag_wtimes[k-1], zero_e), eq(ag_mtimes[k-1], zero_e))));
                    if (k > 1) {
                        // If staying at goal, it must stay til' the end
                        assert_expr(implies(conj(vertex_expr_at(aid, k-2, ag_goal_id), ag_from_vexpr),
                                            ag_goal_vexpr));
                    }

                    push_to_expr_vector(or_vec, ag_goal_vexpr);
                }

                assert_expr(implies(ag_from_vexpr, make_disj(move(or_vec))));

                assert_transition_preferred_variables(aid, k, from_vid, ag_from_vexpr);
            }
        }
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::assert_transition_preferred_variables(const agent::Id& aid, Idx k,
        const Vertex::Id& from_vid, const Expr& from_vexpr
    ){
        auto& min_dist = this->graph_min_dist_of(aid);
        auto sorted_succ_ids = graph::sorted_successors_of(min_dist, from_vid);
        assert((min_dist.successor_of(from_vid) == Vertex::invalid_id) == empty(sorted_succ_ids));
        if (empty(sorted_succ_ids)) return;
        assert(from_vid != min_dist.goal_id());

        auto& gprop = this->cgraph_properties();
        const auto max_deg = gprop.max_deg;
        constexpr float max_deg_rate = 0.5;
        const Idx max_d = ceil(max_deg*max_deg_rate);
        assert(max_d >= 1);
        const auto max_i = max_d-1;
        auto& trans_prefs = transition_preferred_variables[aid][k-1];
        for (Idx i = 0; auto& succ_vid : sorted_succ_ids) {
            assert(succ_vid != from_vid);
            assert(contains(this->cgraph().cvertex(from_vid).cneighbor_ids(), succ_vid));
            const Expr succ_vexpr = vertex_expr_at(aid, k, succ_vid);
            auto& trans_pref = trans_prefs[i];
            assert_expr(implies(conj(from_vexpr, trans_pref), succ_vexpr));
            preferred_variables[transition_priority+i].push_back(trans_pref);

            assert(i <= max_i);
            if (i++ == max_i) break;
        }
        // do not care about the unused preferred variables
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::assert_goal(Idx max_k)
    {
        CVERBLN("assert_goal(" << max_k << ")");
        assert_goal_impl(max_k);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::assert_goal_impl(Idx max_k)
    {
        auto& atimes = abs_time_reals;

        [[maybe_unused]] Expr& assump = max_k_assumption();
        Expr objective = make_real(0);
        for (auto& [aid, gid] : this->clayout().cgoal_ids_map()) {
            Expr goal_vexpr = vertex_expr_at(aid, max_k, gid);
            if constexpr (!Solver_conf::goals_after_push || Solver_conf::solve_with_assumptions) {
                goal_vexpr = implies(assump, goal_vexpr);
            }
            assert_expr(goal_vexpr);

            static_assert(!Solver_conf::optimize_makespan || !Solver_conf::optimize_soc);

            auto& ag_obj_time = atimes[aid][max_k];
            if (optimizing_makespan()) objective = max(objective, ag_obj_time);
            else {
                assert(optimizing_soc());
                objective = plus(objective, ag_obj_time);
            }
        }

        objective = eq(objective_time(), objective);
        if constexpr (!Solver_conf::goals_after_push || Solver_conf::solve_with_assumptions) {
            objective = implies(assump, objective);
        }
        assert_expr(objective);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::prefer_variables(Idx /*max_k*/)
    {
        clear_solver_preferred_variables();

        for (Idx i = 0; auto& prior_prefs : preferred_variables) {
            //? it is not clear whether it is really useful
            //? if (i >= transition_priority) shuffle(prior_prefs);
            for (auto& e : prior_prefs) {
                add_solver_preferred_variable(e);
            }
            ++i;
        }
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    bool Solver<SolverConf, Args...>::assert_any_conflict(Idx max_k)
    {
        CVERB2LN("assert_any_conflict(" << max_k << ")");

        auto& g = this->cgraph();
        auto& ags = this->agents();
        const int n_ags = size(ags);

        auto& prof = this->profile();
        auto& prof_any = prof.assert_any_conflict;
        auto& prof_agent = prof.get_agent_values;

        const auto prof_any_sg = prof_any.make_scope_guard();

        static const auto t_interval_f = [](auto waits, bool last_k, Float t0, Float tmax, Agent& ag, const auto& from, const auto to_l) -> Interval {
            if constexpr (!waits) if (last_k) return {};

            Interval t{t0, tmax, Interval::not_point_tag};

            if constexpr (!waits) {
                assert(!t.is_apx_point<Precision>());

                if (t) {
                    ag.set_linear_state(from.cpos(), to_l->cpos());
                }
            }
            else {
                // The length of waiting can actually be a small number in general
                assert(!t.is_apx_point<typename Precision::Higher>());

                ag.set_idle_state(from.cpos());
            }

            return t;
        };

        const auto alt_f = [this, &g, max_k](auto fixed_waits, auto a1_is_alt, Idx alt_k, Float alt_t0,
                                             const Agent& alt_ag_idle, const auto& alt_from, auto& alt_orig_to_id,
                                             Idx fixed_k, const Agent& fixed_ag, const Interval& fixed_t,
                                             const auto& fixed_from, const auto fixed_to_l
        ){
            assert(alt_ag_idle.cstate().idle());

            for (auto& alt_to_id : alt_from.cneighbor_ids()) {
                if (alt_to_id == alt_orig_to_id) continue;
                auto& alt_to = g.cvertex(alt_to_id);
                const auto alt_to_l = &alt_to;

                const Float alt_tmax = alt_t0 + math::to_float(This::move_time_rational_of(alt_ag_idle, alt_from, alt_to));

                Agent alt_ag = alt_ag_idle;
                const Interval alt_t = t_interval_f(false_type(), false, alt_t0, alt_tmax, alt_ag, alt_from, &alt_to);
                if (!alt_t) continue;

                if constexpr (a1_is_alt) {
                    maybe_assert_conflict<false, fixed_waits, false>(max_k, alt_k, fixed_k,
                        alt_ag, fixed_ag, alt_t, fixed_t,
                        alt_from, alt_to_l, fixed_from, fixed_to_l
                    );
                }
                else {
                    maybe_assert_conflict<fixed_waits, false, false>(max_k, fixed_k, alt_k,
                        fixed_ag, alt_ag, fixed_t, alt_t,
                        fixed_from, fixed_to_l, alt_from, alt_to_l
                    );
                }
            }
        };

        bool any;
        if constexpr (!Solver_conf::learn_only_first_conflict) any = false;

        // 'k1A1k2A2' order of loops performed best (and 'k1k2A1A2' performed similarly)
        // `!learn_only_first_conflict`: still best but it is not that significant any more
        for (Idx k1 = 0; k1 <= max_k; ++k1) {
            const bool last_k1 = k1 == max_k;
            for (auto& ag1 : ags) {
                // in the case of MathSAT, early calls of `get_model_value` are expensive
                prof_agent.start();

                auto& aid1 = ag1.cid();
                Agent ag1_idle = ag1;

                const auto from_id1 = active_vertex_id(aid1, k1);
                auto& from1 = g.cvertex(from_id1);

                const Vertex* to1_l = nullptr;
                Vertex::Id to_id1 = Vertex::invalid_id;
                const Expr a1_t0_e = init_move_abs_time_real(aid1, k1, max_k);
                const Expr a1_tmax_e = final_move_abs_time_real(aid1, k1, max_k);
                Expr a1_t0_val_e = make_expr();
                Expr a1_tmax_val_e = make_expr();
                Float a1_t0{};
                Float a1_tmax;
                if (!last_k1) {
                    to_id1 = active_vertex_id(aid1, k1+1);
                    to1_l = &g.cvertex(to_id1);

                    a1_t0_val_e = get_model_value(a1_t0_e);
                    a1_tmax_val_e = get_model_value(a1_tmax_e);
                    a1_t0 = to_float(a1_t0_val_e);
                    a1_tmax = to_float(a1_tmax_val_e);
                }
                const Interval a1_t = t_interval_f(false_type(), last_k1, a1_t0, a1_tmax, ag1, from1, to1_l);

                Expr a1_idle_t0_e = init_wait_abs_time_real(aid1, k1);
                Expr a1_idle_tmax_e = final_wait_abs_time_real(aid1, k1);
                Expr a1_idle_t0_val_e = get_model_value(a1_idle_t0_e);
                Expr a1_idle_tmax_val_e = get_model_value(a1_idle_tmax_e);
                const Float a1_idle_t0 = to_float(a1_idle_t0_val_e);
                const Float a1_idle_tmax = to_float(a1_idle_tmax_val_e);
                const Interval a1_idle_t = t_interval_f(true_type(), last_k1, a1_idle_t0, a1_idle_tmax, ag1_idle, from1, to1_l);

                prof_agent.finish();

                for (Idx k2 = 0; k2 <= max_k; ++k2) {
                    const bool last_k2 = k2 == max_k;
                    for (agent::Id aid2 = aid1+1; aid2 < n_ags; ++aid2) {
                        // too frequent and the time is not as significant as of the 1st ag.
                        // using a local profile object does not help to avoid the overhead
                        // prof_agent.start();

                        auto& ag2 = ags[aid2];
                        Agent ag2_idle = ag2;

                        const auto from_id2 = active_vertex_id(aid2, k2);
                        auto& from2 = g.cvertex(from_id2);

                        const Vertex* to2_l = nullptr;
                        Vertex::Id to_id2 = Vertex::invalid_id;
                        const Expr a2_t0_e = init_move_abs_time_real(aid2, k2, max_k);
                        const Expr a2_tmax_e = final_move_abs_time_real(aid2, k2, max_k);
                        Expr a2_t0_val_e = make_expr();
                        Expr a2_tmax_val_e = make_expr();
                        Float a2_t0{};
                        Float a2_tmax;
                        if (!last_k2) {
                            to_id2 = active_vertex_id(aid2, k2+1);
                            to2_l = &g.cvertex(to_id2);

                            a2_t0_val_e = get_model_value(a2_t0_e);
                            a2_tmax_val_e = get_model_value(a2_tmax_e);
                            a2_t0 = to_float(a2_t0_val_e);
                            a2_tmax = to_float(a2_tmax_val_e);
                        }
                        const Interval a2_t = t_interval_f(false_type(), last_k2, a2_t0, a2_tmax, ag2, from2, to2_l);

                        Expr a2_idle_t0_e = init_wait_abs_time_real(aid2, k2);
                        Expr a2_idle_tmax_e = final_wait_abs_time_real(aid2, k2);
                        Expr a2_idle_t0_val_e = get_model_value(a2_idle_t0_e);
                        Expr a2_idle_tmax_val_e = get_model_value(a2_idle_tmax_e);
                        const Float a2_idle_t0 = to_float(a2_idle_t0_val_e);
                        const Float a2_idle_tmax = to_float(a2_idle_tmax_val_e);
                        const Interval a2_idle_t = t_interval_f(true_type(), last_k2, a2_idle_t0, a2_idle_tmax, ag2_idle, from2, to2_l);

                        // prof_agent.finish();

                        if constexpr (Solver_conf::learn_only_first_conflict) any = false;
                        const bool move_conflict = maybe_assert_conflict<false, false>(max_k, k1, k2,
                            ag1, ag2, a1_t, a2_t,
                            from1, to1_l, from2, to2_l
                        );
                        const bool wait2_conflict = maybe_assert_conflict<false, true>(max_k, k1, k2,
                            ag1, ag2_idle, a1_t, a2_idle_t,
                            from1, to1_l, from2, to2_l
                        );
                        const bool wait1_conflict = maybe_assert_conflict<true, false>(max_k, k1, k2,
                            ag1_idle, ag2, a1_idle_t, a2_t,
                            from1, to1_l, from2, to2_l
                        );

                        any |= move_conflict || wait1_conflict || wait2_conflict;
                        if (!any) continue;
                        prof.n_conflicts += move_conflict + wait1_conflict + wait2_conflict;
                        if constexpr (Solver_conf::learn_only_first_conflict) return true;
                        if (last_k1 || last_k2) continue;

                        // These can be more expensive than the gain
                        if (move_conflict) {
                            alt_f(false_type(), true_type(), k1, a1_t0, ag1_idle, from1, to_id1,
                                  k2, ag2, a2_t, from2, to2_l);
                            alt_f(false_type(), false_type(), k2, a2_t0, ag2_idle, from2, to_id2,
                                  k1, ag1, a1_t, from1, to1_l);
                        }
                        // .. but these seem to be generally beneficial
                        if (wait2_conflict) {
                            alt_f(true_type(), true_type(), k1, a1_t0, ag1_idle, from1, to_id1,
                                  k2, ag2_idle, a2_idle_t, from2, to2_l);
                        }
                        if (wait1_conflict) {
                            alt_f(true_type(), false_type(), k2, a2_t0, ag2_idle, from2, to_id2,
                                  k1, ag1_idle, a1_idle_t, from1, to1_l);
                        }
                    }
                }
            }
        }

        if constexpr (Solver_conf::learn_only_first_conflict) return false;
        else return any;
    }

    namespace {
        template <bool a1Waits, bool a2Waits>
        pair<const Interval&, const Interval&>
        update_t_refs(const Agent& ag1, const Agent& ag2, const Interval& a1_t, const Interval& a2_t,
                      Interval& a1_unsafe, Interval& a2_unsafe)
        {
            assert(a1_t);
            assert(a2_t);
            assert(a1_unsafe);
            assert(a2_unsafe);

            // Retrieve maximal possible unsafe intervals with a waiting agent
            constexpr auto t_ref_f = [](auto waits, const Interval& t) -> const Interval& {
                if constexpr (!waits) return t;
                else return pos_inf_interval;
            };
            auto& a1_t_ref = t_ref_f(bool_constant<a1Waits>(), a1_t);
            auto& a2_t_ref = t_ref_f(bool_constant<a2Waits>(), a2_t);

            if constexpr (a1Waits || a2Waits) {
                tie(a1_unsafe, a2_unsafe) = agent::linear::unsafe_intervals(ag1, ag2, a1_t_ref, a2_t_ref);
            }

            constexpr auto upd_t_ref_f = [](auto waits, const Interval& t_ref, const Interval& unsafe) -> const Interval& {
                if constexpr (!waits) return t_ref;
                else return unsafe;
            };
            return {upd_t_ref_f(bool_constant<a1Waits>(), a1_t_ref, a1_unsafe),
                    upd_t_ref_f(bool_constant<a2Waits>(), a2_t_ref, a2_unsafe)};
        }
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    template <bool a1Waits, bool a2Waits, bool isFromModel>
    bool Solver<SolverConf, Args...>::maybe_assert_conflict(Idx max_k, Idx k1, Idx k2,
        const Agent& ag1, const Agent& ag2, const Interval& a1_t, const Interval& a2_t,
        const graph::Vertex& from1, const graph::Vertex* to1_l,
        const graph::Vertex& from2, const graph::Vertex* to2_l
    ){
        using tomaqa::to_string;

        constexpr bool a1_waits = a1Waits;
        constexpr bool a2_waits = a2Waits;
        static_assert(!(a1_waits && a2_waits));

        static constexpr auto last_k_f = [](Idx k, Idx max_k_) -> bool {
            return k == max_k_;
        };

        if constexpr (!a1_waits) {
            if (last_k_f(k1, max_k)) return false;
        }
        if constexpr (!a2_waits) {
            if (last_k_f(k2, max_k)) return false;
        }

        // These are actually the idle cases with zero waiting or staying at goal
        if (a1_t.empty()) return false;
        if (a2_t.empty()) return false;

        auto& prof = this->profile();
        auto& prof_ui = prof.unsafe_intervals;

        prof_ui.start();
        auto&& [a1_unsafe, a2_unsafe] = agent::linear::unsafe_intervals(ag1, ag2, a1_t, a2_t);
        prof_ui.finish();
        assert(a1_unsafe.empty() == a2_unsafe.empty());
        if (!a1_unsafe) return false;

        // it is not too important to use these `a_t_ref` instead of `a_t`
        // because the updated values are actually not directly used in conflict clauses
        prof_ui.start();
        auto&& [a1_t_ref, a2_t_ref] = update_t_refs<a1_waits, a2_waits>(ag1, ag2, a1_t, a2_t, a1_unsafe, a2_unsafe);
        assert(a1_unsafe);
        assert(a2_unsafe);
        prof_ui.finish();

        assert(a1_waits || a1_t_ref == a1_t);
        assert(a2_waits || a2_t_ref == a2_t);
        assert(!a1_waits || a1_t_ref == a1_unsafe);
        assert(!a2_waits || a2_t_ref == a2_unsafe);

        constexpr auto to_f = [](auto waits, auto& to_l) -> auto& {
            if constexpr (!waits) return *to_l;
            else return to_l;
        };
        auto& to1 = to_f(bool_constant<a1_waits>(), to1_l);
        auto& to2 = to_f(bool_constant<a2_waits>(), to2_l);
        auto& from_id1 = from1.cid();
        auto& from_id2 = from2.cid();
        const auto to_id1 = invoke([&to1]{
            if constexpr (a1_waits) return Vertex::invalid_id;
            else return to1.cid();
        });
        const auto to_id2 = invoke([&to2]{
            if constexpr (a2_waits) return Vertex::invalid_id;
            else return to2.cid();
        });

        if constexpr (_debug_) {
            const auto check_bounds_f = [this](bool a2_waits_, const agent::Id& aid2, Idx k2_, auto a1_uu_, auto& a2_t_){
                using P [[maybe_unused]] = typename Precision::Higher::Higher;

                const auto [a2_tl_, a2_tu_] = a2_t_.crange();

                assert(a1_uu_ <= a2_tu_);
                //? should work also for `!isFromModel`?
                assert(!isFromModel || a2_waits_ ||
                    apx_less_equal<P>(a1_uu_ - a2_tl_, get_model_float(move_time_reals[aid2][k2_]))
                );
                assert(!isFromModel || a2_waits_ ||
                    (apx_equal<P>(a1_uu_, a2_tu_)) ==
                    (apx_equal<P>(a1_uu_ - a2_tl_, get_model_float(move_time_reals[aid2][k2_])))
                );
            };
            check_bounds_f(a2_waits, ag2.cid(), k2, a1_unsafe.cupper(), a2_t_ref);
            check_bounds_f(a1_waits, ag1.cid(), k1, a2_unsafe.cupper(), a1_t_ref);

            const auto map_val = pair{a1_unsafe.crange(), a2_unsafe.crange()};
            const auto p1 = pair{a1_t_ref, a2_t_ref};
            const auto p2 = pair{a1_t, a2_t};
            for (auto& [a1t, a2t] : {p1, p2}) {
                auto map_key = pair{tuple{ag1.cid(), from_id1, to_id1, k1, a1t.crange()},
                                    tuple{ag2.cid(), from_id2, to_id2, k2, a2t.crange()}};
                auto [it, inserted] = _conflicts_map.emplace(map_key, map_val);
                if (isFromModel && !inserted) {
                    assert(it->second == map_val);
                    UNREACHABLE;
                }

                if (apx_equal<Precision>(p1, p2)) break;
            }
        }

        static constexpr auto waits_inf_f = [](auto waits, Idx k, Idx max_k_) -> bool {
            return waits && last_k_f(k, max_k_);
        };

        auto& prof_ccl = prof.conflict_clause;

        [[maybe_unused]] const Idx k1_max = a1_waits ? max_k : max_k-1;
        [[maybe_unused]] const Idx k2_max = a2_waits ? max_k : max_k-1;
        assert(k1 <= k1_max);
        assert(k2 <= k2_max);
        // It seems that learning for also other k's is generally contraproductive
        // (it has huge impact on 'corridor_001_k3_H', but it is not too realistic scenario)
        // Maybe some more sophisticated method to select also other k's may be useful ..
        constexpr auto k_f = [](Idx k, [[maybe_unused]] Idx k_max) -> pair<Idx, Idx> {
            return {k, k};
        };
        const auto [k1_lo, k1_hi] = k_f(k1, k1_max);
        const auto [k2_lo, k2_hi] = k_f(k2, k2_max);
        [[maybe_unused]] bool learned_orig_conflict = false;
        for (Idx k1_ = k1_lo; k1_ <= k1_hi; ++k1_) {
            const bool a1_waits_inf = waits_inf_f(bool_constant<a1_waits>(), k1_, max_k);
            for (Idx k2_ = k2_lo; k2_ <= k2_hi; ++k2_) {
                const auto prof_ccl_sg = prof_ccl.make_scope_guard();

                const bool a2_waits_inf = waits_inf_f(bool_constant<a2_waits>(), k2_, max_k);

                // It is actually faster if not learning for other agents too
                auto& aid1 = ag1.cid();
                const Expr from_vexpr1 = from_vertex_expr(aid1, k1_, from_id1);
                const auto to_vexpr1 = to_vertex_expr<a1_waits>(aid1, k1_, max_k, to_id1);
                const Expr a1_tl_e = init_abs_time_real<a1_waits>(aid1, k1_, max_k);
                const Expr a1_tu_e = final_abs_time_real<a1_waits>(aid1, k1_, max_k);

                auto& aid2 = ag2.cid();
                const Expr from_vexpr2 = from_vertex_expr(aid2, k2_, from_id2);
                const auto to_vexpr2 = to_vertex_expr<a2_waits>(aid2, k2_, max_k, to_id2);
                const Expr a2_tl_e = init_abs_time_real<a2_waits>(aid2, k2_, max_k);
                const Expr a2_tu_e = final_abs_time_real<a2_waits>(aid2, k2_, max_k);

                Expr conflict_clause = make_conflict_clause<a1_waits, a2_waits>(
                    a1_waits_inf, a2_waits_inf,
                    from_vexpr1, to_vexpr1, from_vexpr2, to_vexpr2,
                    a1_tl_e, a1_tu_e, a2_tl_e, a2_tu_e,
                    a1_unsafe, a2_unsafe, a1_t_ref, a2_t_ref
                );

                if constexpr (_debug_) {
                    if (k1_ == k1 && k2_ == k2 && aid1 == ag1.cid() && aid2 == ag2.cid()) {
                        learned_orig_conflict = true;
                        assert(!isFromModel || !get_model_bool(conflict_clause));
                    }
                }

                assert_conflict(max_k, conflict_clause);
            }
        }
        assert(learned_orig_conflict);

        return true;
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    template <bool a1Waits, bool a2Waits>
    typename Solver<SolverConf, Args...>::Expr
    Solver<SolverConf, Args...>::make_conflict_clause(
        bool a1_waits_inf, bool a2_waits_inf,
        const Expr& from_vexpr1, auto to_vexpr1, const Expr& from_vexpr2, auto to_vexpr2,
        const Expr& a1_tl_e, const Expr& a1_tu_e, const Expr& a2_tl_e, const Expr& a2_tu_e,
        const Interval& a1_unsafe, const Interval& a2_unsafe, const Interval& a1_t, const Interval& a2_t
    ){
        constexpr bool a1_waits = a1Waits;
        constexpr bool a2_waits = a2Waits;
        static_assert(!(a1_waits && a2_waits));

        constexpr auto lt_e_f = [](This* this_l,
            auto a2_waits_, auto a1_uu_, auto a2_tl_,
            const Expr& a1_tl_e_, const Expr& a2_tl_e_,
            [[maybe_unused]] auto a2_tu_, [[maybe_unused]] const Expr& a2_tu_e_
        ){
            if constexpr (a2_waits_) return false;
            else {
                assert(!a2_waits_);
                Expr lhs_e = this_l->minus(a1_tl_e_, a2_tl_e_);
                // using lower precision here seems to be contraproductive
                // reusing `a2_tu_e_` when `a1_uu_ ~ a2_tu_` seems to be contraproductive
                assert(a1_uu_ <= a2_tu_);
                Expr rhs_e = this_l->template make_real<Side::right>(a1_uu_ - a2_tl_);

                // `a1_waits_inf_` nor `max_k_assumption` does not help in resolving this conflict
                // because `a1_uu_` has valid value even when waiting inf.
                return this_l->geq(lhs_e, rhs_e);
            }
        };
        constexpr auto gt_e_f = [](This* this_l,
            auto a1_waits_, [[maybe_unused]] auto a2_waits_, bool a1_waits_inf_,
            auto a1_ul_, auto a2_tl_,
            const Expr& a1_tu_e_, const Expr& a2_tl_e_,
            [[maybe_unused]] auto a1_tl_, [[maybe_unused]] const Expr& a1_tl_e_
        ){
            if constexpr (!a1_waits_) return false;
            else {
                assert(a1_waits_);
                assert(!a2_waits_);

                Expr lhs_e = this_l->minus(a1_tu_e_, a2_tl_e_);
                assert(a1_ul_ == a1_tl_);
                Expr rhs_e = this_l->template make_real<Side::left>(a1_ul_ - a2_tl_);

                Expr e = this_l->leq(lhs_e, rhs_e);
                // `a1_waits_inf_` cannot be resolved in the final `k`,
                // but it is not beneficial to deal with it explicitly
                return !a1_waits_inf_ ? e : this_l->conj(this_l->neg(this_l->max_k_assumption()), e);
            }
        };

        const auto [a1_ul, a1_uu] = a1_unsafe.crange();
        const auto [a2_ul, a2_uu] = a2_unsafe.crange();

        const auto [a1_tl, a1_tu] = a1_t.crange();
        const auto [a2_tl, a2_tu] = a2_t.crange();

        auto vec = make_expr_vector();
        if constexpr (requires { vec.reserve(8); }) vec.reserve(8);
        const auto maybe_push_f = [this, &vec](auto cond, auto do_neg, [[maybe_unused]] auto e){
            if constexpr (cond) {
                if constexpr (!do_neg) push_to_expr_vector(vec, e);
                else push_to_expr_vector(vec, neg(e));
            }
            else assert(e == do_neg);
        };

        push_to_expr_vector(vec, neg(from_vexpr1));
        maybe_push_f(bool_constant<!a1_waits>(), true_type(), to_vexpr1);
        push_to_expr_vector(vec, neg(from_vexpr2));
        maybe_push_f(bool_constant<!a2_waits>(), true_type(), to_vexpr2);
        auto lt_e1 = lt_e_f(this, bool_constant<a2_waits>(), a1_uu, a2_tl, a1_tl_e, a2_tl_e, a2_tu, a2_tu_e);
        maybe_push_f(bool_constant<!a2_waits>(), false_type(), move(lt_e1));
        auto lt_e2 = lt_e_f(this, bool_constant<a1_waits>(), a2_uu, a1_tl, a2_tl_e, a1_tl_e, a1_tu, a1_tu_e);
        maybe_push_f(bool_constant<!a1_waits>(), false_type(), move(lt_e2));
        auto gt_e1 = gt_e_f(this, bool_constant<a1_waits>(), bool_constant<a2_waits>(), a1_waits_inf, a1_ul, a2_tl, a1_tu_e, a2_tl_e, a1_tl, a1_tl_e);
        maybe_push_f(bool_constant<a1_waits>(), false_type(), move(gt_e1));
        auto gt_e2 = gt_e_f(this, bool_constant<a2_waits>(), bool_constant<a1_waits>(), a2_waits_inf, a2_ul, a1_tl, a2_tu_e, a1_tl_e, a2_tl, a2_tl_e);
        maybe_push_f(bool_constant<a2_waits>(), false_type(), move(gt_e2));
        return make_disj(move(vec));
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::assert_conflict(Idx /*max_k*/, Expr conflict_clause)
    {
        if constexpr (!Solver_conf::solve_with_assumptions) {
            push_to_expr_vector(conflict_bools(), conflict_clause);
        }
        else if constexpr (Solver_conf::manual_optimize) {
            if (optimizing()) push_to_expr_vector(conflict_bools(), conflict_clause);
        }

        assert_expr(move(conflict_clause));
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::assert_conflicts_after_pop()
    {
        assert_expr_vector(conflict_bools());
        clear_expr_vector(conflict_bools());
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    bool Solver<SolverConf, Args...>::check_all_sat(Idx max_k)
    {
        CVERBLN("check_all_sat(" << max_k << ")");

        auto& prof = this->profile();
        auto& prof_sat = prof.check_sat;
        auto& prof_model = prof.get_model;

        Float last_obj = 0;

        for (;;) {
            prof_sat.start();
            CVERB2LN("check_sat(" << max_k << ")");
            const auto res = check_sat();
            assert(res != Solver_conf::unknown);
            prof_sat.finish();
            if (res == Solver_conf::unsat) {
                CVERB2LN("unsat");
                return false;
            }

            assert(res == Solver_conf::sat);
            get_model();

            prof_model.start();
            Expr objective = objective_time();
            Expr objective_val = get_objective_time();
            const auto obj = to_float(objective_val);
            assert(obj == get_model_float(objective));
            assert(!optimizing() || obj == to_float(get_min_objective_time()));
            prof_model.template finish<false>();

            if (!assert_any_conflict(max_k)) {
                assert(obj == get_model_float(objective));
                assert(!this->cfinal_objective_time() || this->cfinal_objective_time() >= obj);
                this->final_objective_time() = obj;
                break;
            }

            assert(obj == get_model_float(objective));
            assert(obj == to_float(objective_val));
            assert(!optimizing() || obj == to_float(get_min_objective_time()));

            if constexpr (Solver_conf::manual_optimize) continue;

            if constexpr (!Solver_conf::always_optimize) {
                if (!optimizing()) continue;
            }

            assert(obj >= last_obj);
            if (obj == last_obj) continue;
            assert(apx_greater<typename Precision::Higher>(obj, last_obj));
            last_obj = obj;

            // say explicitly that while dealing with conflicts, the time cannot improve
            //? if optimization is only apx. and not optimal, this is wrong?
            assert_expr(geq(objective, objective_val));
        }

        CVERB2LN("sat");
        return true;
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::assert_minimize()
    {
        assert(Solver_conf::optimize);
        assert(!optimizing());
        if constexpr (!Solver_conf::manual_optimize) assert_minimize_impl();
        assert_objective_lower();
        set_optimizing();
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::assert_objective_lower()
    {
        assert(Solver_conf::optimize);

        const Float lo = this->cmin_objective_lower();
        assert(apx_greater_equal<Precision>(this->cfinal_objective_time(), lo));

        Expr geq_lo_e = geq(objective_time(), make_real(lo));

        if (apx_equal<Precision>(this->cfinal_objective_time(), lo)) {
            if (this->producing_smt2()) print_smt2_assert(move(geq_lo_e));
            return;
        }

        assert_expr(move(geq_lo_e));
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::minimize(ostream& os, Idx max_k)
    {
        assert(Solver_conf::optimize);
        assert(this->is_sat());

        const auto prof_sg = this->profile().minimize.make_scope_guard();

        minimize_pre(os, max_k);
        minimize_impl(os, max_k);
        minimize_post(os, max_k);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::minimize_pre(ostream&, Idx /*max_k*/)
    {
        assert(!optimizing());
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::minimize_impl(ostream& os, Idx max_k)
    {
        using std::endl;

        if constexpr (Solver_conf::always_optimize) return;

        assert_minimize();
        assert(optimizing());

        const Float lo = this->cmin_objective_lower();
        os << "minimize ...\nlower bound: " << lo << endl;

        if (apx_equal<Precision>(this->cfinal_objective_time(), lo)) {
            os << "objective time is already at the lower bound" << endl;
            if constexpr (Solver_conf::manual_optimize) {
                this->final_objective_lower() = lo;
            }
            return;
        }

        if constexpr (Solver_conf::manual_optimize) return manual_minimize(os, max_k);

        [[maybe_unused]] const bool sat = check_all_sat(max_k);
        assert(sat);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::minimize_post(ostream& os, [[maybe_unused]] Idx max_k)
    {
        assert(optimizing());

        assert(!assert_any_conflict(max_k));

        if constexpr (!Solver_conf::manual_optimize) return;

        manual_minimize_print_final_coef(os);

        if (!this->producing_smt2()) return;

        const Float lo = this->cmin_objective_lower();
        const Float obj = this->cfinal_objective_time();
        if (apx_less<Precision>(lo, obj)) return;
        assert(apx_equal<Precision>(lo, obj));

        // in this case no upper bound was asserted
        // but requiring precisely the lower bound may be too hard, set it to sth. sensibly close
        const Float coef = 1 + (suboptimal_coef() - 1)/2;
        Float val = lo*coef;
        assert(apx_greater<Precision>(val, obj) || val == 0);

        print_smt2_assert(leq(objective_time(), make_real(val)));
        print_smt2_check_sat_with_status(Solver_conf::sat);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::manual_minimize(ostream& os, Idx max_k)
    try {
        using std::endl;

        assert(Solver_conf::manual_optimize);

        const Float target_coef = suboptimal_coef();
        assert(target_coef > 1);

        assert(apx_greater<Precision>(this->cfinal_objective_time(), this->cmin_objective_lower()));

        Expr objective = objective_time();

        if constexpr (Solver_conf::solve_with_assumptions) assert_expr(max_k_assumption());

        const auto check_hi_f = [target_coef](Float lo, Float hi){
            return apx_greater<Precision>(hi, lo*target_coef);
        };

        const auto new_hi_f = [this, &os, &objective]{
            Expr hi_e = get_objective_time();
            Float hi = this->cfinal_objective_time();
            assert(hi == to_float(hi_e));
            os << "new minimum: " << hi << endl;
            assert(hi >= 0);
            if (apx_equal<Precision>(hi, 0)) throw ignore;

            return pair{move(hi), move(hi_e)};
        };

        const auto new_val_f = [target_coef](Float lo, Float hi){
            const Float mid = (lo + hi)/2;
            // It does not seem to be beneficial to also handle `lo*target_coef*target_coef`
            const Float lo_c = lo*target_coef;
            assert(hi > lo_c);
            assert(lo_c > lo);
            return math::max(lo_c, mid);
        };

        auto [hi, hi_e] = new_hi_f();
        assert_expr(leq(objective, hi_e));

        Float lo = this->cmin_objective_lower();
        bool need_to_recover = true;
        while (check_hi_f(lo, hi)) {
            Float val = new_val_f(lo, hi);
            assert(apx_less<Precision>(lo, val));
            assert(apx_less<Precision>(val, hi));
            assert(apx_greater<Precision>(val, 0));

            CVERBLN("searching for a plan that is " << hi/val << "x better than " << hi
                    << " (i.e. <= " << val << ")");
            before_push();
            this->push();
            after_push();
            assert(val >= lo*target_coef);
            assert_expr(leq(objective, make_real(val)));
            const bool sat = check_all_sat(max_k);

            need_to_recover = !sat;
            if (!sat) {
                lo = val;
                os << "new lower bound: " << lo << endl;
                assert(this->cfinal_objective_time() > 0);
                before_pop();
                this->pop();
                after_pop();
                //? would not this improve efficiency? assert_expr(geq(objective, make_real(val)));
                continue;
            }

            tie(hi, hi_e) = new_hi_f();
            assert(hi <= val);
            assert(!need_to_recover);
            // if true, checking twice in a row, but other ways are ugly
            if (check_hi_f(lo, hi)) {
                // assert only if another check-sat will follow
                assert_expr(leq(objective, hi_e));
                continue;
            }

            if (!this->producing_smt2()) break;

            // guarantee that the trace requires to satisfy the coefficient
            // assert only if it is higher than `val` which was already asserted
            Float hi_print_smt2 = lo*target_coef;
            assert(hi_print_smt2 >= hi);
            assert(apx_less_equal<Precision>(hi_print_smt2, val));
            if (apx_equal<Precision>(hi_print_smt2, val)) break;

            print_smt2_assert(leq(objective, make_real(hi_print_smt2)));
            print_smt2_check_sat_with_status(Solver_conf::sat);
            break;
        }

        assert(lo > 0);
        assert(hi == this->cfinal_objective_time());
        assert(apx_less_equal<Precision>(hi/lo, target_coef));
        this->final_objective_lower() = lo;

        if (!need_to_recover) return;

        //+ this is only necessary to recover the previous model, can it be just copied?
        CVERBLN("recovering to the last satisfiable assignment");
        [[maybe_unused]] const bool sat = check_all_sat(max_k);
        assert(sat);
        assert(hi >= this->cfinal_objective_time());
    }
    catch (Ignore) { }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::manual_minimize_print_final_coef(ostream& os)
    {
        const auto coef = this->guaranteed_suboptimal_coef();
        assert(apx_greater_equal<typename Precision::Higher>(coef, 1));
        assert(apx_less_equal<Precision>(coef, suboptimal_coef()));

        os << "guaranteed suboptimal coefficient: " << coef << std::endl;
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::solve_init()
    {
        Inherit::solve_init();

        objective_time() = make_real(objective_time_var_name);

        if constexpr (Solver_conf::optimize) {
            Float min_obj = 0;
            for (auto& ag : this->cagents()) {
                auto& aid = ag.cid();
                auto ag_min_dist = this->cgraph_min_dist_of(aid)();
                if (optimizing_makespan()) min_obj = util::max(min_obj, ag_min_dist);
                else {
                    assert(optimizing_soc());
                    min_obj += ag_min_dist;
                }
            }
            this->min_objective_lower() = min_obj;
        }

        if constexpr (Solver_conf::optimize && Solver_conf::always_optimize) {
            assert_minimize();
        }
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::solve_main(ostream& os)
    {
        using std::endl;

        os << "cost function: " << (optimizing_makespan() ? "makespan" : "soc") << endl;

        if constexpr (Solver_conf::manual_optimize) {
            os << "target coefficient: " << suboptimal_coef() << endl;
        }

        //+ consider also k corresponding to the longest distance of all agents
        const Idx min_max_k = util::max(this->cagents(), [this](auto& ag){
            auto& aid = ag.cid();
            return this->cgraph_min_dist_of(aid).n_jumps();
        });
        //+ explain
        const Idx max_max_k = this->cgraph().cn_edges()*2;
        assert(max_max_k >= min_max_k);

        declare_consts();
        assert_init();

        for (Idx max_k = 0;; ++max_k) {
            const bool first = max_k == 0;

            if (!first) declare_consts(max_k);
            assert_positions(max_k);
            if (!first) assert_transitions(max_k);

            if constexpr (Solver_conf::skip_not_shortest_steps) {
                if (max_k < min_max_k) continue;
            }

            main_before_push(max_k);
            main_push();
            main_after_push(max_k);

            os << "max_k=" << max_k << " ... ";
            os.flush();
            const bool sat = check_all_sat(max_k);
            assert(!this->is_sat() || sat);
            os << (sat ? Solver_conf::sat : Solver_conf::unsat) << endl;
            if (sat) {
                this->set_sat();

                if constexpr (!Solver_conf::skip_not_shortest_steps) {
                    if (max_k < min_max_k) {
                        main_before_pop();
                        main_pop();
                        main_after_pop();
                        continue;
                    }
                }

                assert(max_k >= min_max_k);
                this->final_k() = max_k;

                if constexpr (Solver_conf::optimize) minimize(os, max_k);
                assert(Solver_conf::optimize == optimizing());

                break;
            }

            assert(max_k <= max_max_k);
            if (max_k == max_max_k) {
                this->set_unsat();

                assert((Solver_conf::optimize && Solver_conf::always_optimize) == optimizing());

                break;
            }

            main_before_pop();
            main_pop();
            main_after_pop();
        }

        this->set_finished();
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::main_before_push(Idx max_k)
    {
        before_push();
        max_k_assumption_before_push(max_k);

        if constexpr (!Solver_conf::goals_after_push) assert_goal(max_k);

        auto& wtimes = wait_time_reals;
        auto& mtimes = move_time_reals;
        Expr& assump = max_k_assumption();
        const Expr zero_e = make_real(0);
        Expr& objective = objective_time();
        for (auto& [aid, gid] : this->clayout().cgoal_ids_map()) {
            // something like "waits until the whole plan is certainly finished"
            assert_expr(implies(assump, eq(wtimes[aid][max_k], objective)));
            assert_expr(implies(assump, eq(mtimes[aid][max_k], zero_e)));
        }

        prefer_variables(max_k);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::main_push()
    {
        if constexpr (!Solver_conf::solve_with_assumptions) this->push();
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::main_after_push(Idx max_k)
    {
        after_push();
        max_k_assumption_after_push(max_k);

        if constexpr (Solver_conf::goals_after_push) assert_goal(max_k);
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::main_before_pop()
    {
        max_k_assumption_before_pop();
        before_pop();
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::main_pop()
    {
        if constexpr (!Solver_conf::solve_with_assumptions) this->pop();
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::main_after_pop()
    {
        max_k_assumption_after_pop();
        after_pop();
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::max_k_assumption_before_push(Idx max_k)
    {
        using tomaqa::to_string;

        max_k_assumption() = make_bool("kass"s + to_string(max_k));
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::max_k_assumption_after_push(Idx /*max_k*/)
    {
        if constexpr (!Solver_conf::solve_with_assumptions) assert_expr(max_k_assumption());
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::max_k_assumption_before_pop()
    { }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::max_k_assumption_after_pop()
    {
        // makes it worse on average when `!always_optimize`
        if constexpr (Solver_conf::always_optimize) assert_expr(neg(max_k_assumption()));
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    Float Solver<SolverConf, Args...>::get_final_objective_time()
    {
        if constexpr (!Solver_conf::optimize) return get_model_float(objective_time());

        const Float final_obj_time = this->cfinal_objective_time();

        if constexpr (_debug_) {
            if (this->finished()) {
                assert(final_obj_time == get_model_float(objective_time()));
            }
        }

        return final_obj_time;
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::print_final_objective_time(ostream& os)
    {
        print_final_objective_time(os, get_final_objective_time());
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::print_final_objective_time(ostream& os, Float obj_time) const
    {
        os << std::endl;
        if (optimizing()) {
            if (!this->finished()) os << "partially ";
            os << "minimized ";
        }
        os << "objective time: " << obj_time << std::endl;
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    agent::plan::Global Solver<SolverConf, Args...>::make_plan_impl(ostream& os)
    {
        using std::endl;
        using tomaqa::to_string;

        assert(this->is_sat());
        //+ it should be somehow possible even if not finished yet
        assert(this->finished());
        // this may arrive at a different minimum which would mess up the following debug assertions
        // assert(check_sat() == Solver_conf::sat);
        assert(Solver_conf::optimize == optimizing());

        const Float min_time = get_final_objective_time();
        print_final_objective_time(os, min_time);
        assert(apx_greater_equal<Precision>(min_time, this->cmin_objective_lower()));
        assert(apx_greater_equal<Precision>(min_time, this->cfinal_objective_lower()));

        const Idx k = this->cfinal_k();
        auto plan = make_plan_for(k);

        os << "\nResulting plan:\n" << plan << endl;

        const auto plan_time_f = [this](auto&& p){
            if (optimizing_makespan()) return p.makespan();
            else {
                assert(optimizing_soc());
                return p.sum_times();
            }
        };
        const auto plan_time = plan_time_f(plan);
        expect(apx_equal<typename Precision::Higher>(min_time, plan_time),
               "Minimized value is not consistent with the resulting model: "s
               + to_string(min_time) + " !~ " + to_string(plan_time));

        if constexpr (_debug_) {
            assert(!assert_any_conflict(k));

            get_model();
            const Float obj_time = get_model_float(objective_time());
            assert(obj_time == to_float(get_objective_time()));
            assert(obj_time == to_float(get_min_objective_time()));
            assert(obj_time == get_model_float(objective_time()));
            assert(obj_time == this->cfinal_objective_time());

            assert(obj_time == min_time || (obj_time < min_time && obj_time >= this->cmin_objective_lower()));

            assert(apx_equal<typename Precision::Higher>(obj_time, plan_time_f(make_plan_for(k))));

            assert(!assert_any_conflict(k));
        }

        os << "makespan: " << plan.makespan() << endl;
        os << "sum of costs: " << plan.sum_times() << endl;

        return plan;
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    agent::plan::Global Solver<SolverConf, Args...>::make_plan_for(Idx max_k) const
    {
        auto& l = this->clayout();
        agent::plan::Global gplan;
        for (auto& ag : this->cagents()) {
            auto& aid = ag.cid();
            auto& ag_atimes = abs_time_reals[aid];
            auto& ag_wtimes = wait_time_reals[aid];
            auto& ag_mtimes = move_time_reals[aid];

            agent::plan::Local lplan;
            Vertex::Id from_id = l.cstart_id_of(aid);
            for (Idx k = 0; k < max_k; ++k) {
                auto to_id = active_vertex_id(aid, k+1);
                lplan.add_step({
                    .from_id = from_id,
                    .to_id = to_id,
                    .abs_time = get_model_float(ag_atimes[k]),
                    .move_time = get_model_float(ag_mtimes[k]),
                    .wait_time = get_model_float(ag_wtimes[k]),
                });

                from_id = to_id;
            }
            gplan.emplace(aid, move(lplan));
        }

        return gplan;
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    const char* Solver<SolverConf, Args...>::to_c_str(const Sat_result& res)
    {
        // not using switch, it does not have to be literal
        if (res == Solver_conf::sat) return sat_str;
        if (res == Solver_conf::unsat) return unsat_str;
        if (res == Solver_conf::unknown) return unknown_str;
        UNREACHABLE;
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::print_smt2_head_set_logic() const
    {
        //+ customize
        this->print_smt2("set-logic QF_BVLRA");
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::print_smt2_assert(const Expr& e) const
    {
        this->print_smt2("assert "s + expr_to_smt2(e));
    }

    template <solver::conf::Concept SolverConf, typename... Args>
    void Solver<SolverConf, Args...>::print_smt2_check_sat_with_status(const Sat_result& res) const
    {
        // result of the trace will still likely not correspond to a feasible plan,
        // because the simulator does not check the particular model
        this->print_smt2("set-info :status "s + to_c_str(res));
        this->print_smt2_check_sat();
    }
}
