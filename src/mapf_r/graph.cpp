#include "mapf_r/graph.hpp"

namespace mapf_r::graph {
    bool Vertex::is_neighbor(const Id& id_) const noexcept
    {
        return cneighbor_ids().contains(id_);
    }

    void Vertex::add_neighbor(const Id& id_)
    {
        expect(id_ != cid(), "Edges to itself are not permitted: "s + to_string());

        // Ignore duplicate edges ..
        neighbor_ids().insert(id_);
    }

    bool Vertex::equals(const Vertex& rhs) const noexcept
    {
        assert(cid() == rhs.cid() || cpos() != rhs.cpos());
        return cid() == rhs.cid();
    }

    String Vertex::to_string() const
    {
        using tomaqa::to_string;
        return to_string(cid()) + ": " + to_string(cpos());
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace mapf_r {
    Graph::Graph(istream& is)
    {
        parse(is);
    }

    void Graph::parse(istream& is)
    {
        char c;
        expect(is >> c, "Input graph stream is not readable.");
        is.unget();
        switch (c) {
        case '[':
        case '{':
            return parse_g(is);
        case 'V':
            return parse_mpf(is);
        case 'L':
            return parse_mapr(is);
        default:
            THROW("Unrecognized graph format, first char was: "s + c);
        }
    }

    void Graph::parse_g(istream& is)
    {
        parse_basic_impl<'[', ':'>(is);
    }

    void Graph::parse_mpf(istream& is)
    {
        parse_basic_impl<'(', ','>(is, [](istream&, char c){
            switch (c) {
            case 'V':
            case 'E':
            case '=':
                return true;
            default:
                return false;
            }
        });
    }

    void Graph::parse_mapr(istream& is)
    {
        parse_basic_impl<0, ':'>(is, [](istream& is_, char c){
            if (isalpha(c)) {
                String str;
                return bool(getline(is_, str));
            }

            return false;
        });
    }

    template <char vertex_lbracket, char vertex_id_delim, char edge_lbracket, char edge_delim,
              predicate<istream&, char> OtherCharsPred>
    void Graph::parse_basic_impl(istream& is, OtherCharsPred other_chars_pred)
    {
        using tomaqa::to_string;

        constexpr auto rbracket_f = [](char lbracket) consteval -> char {
            switch (lbracket) {
            case '(': return ')';
            case '[': return ']';
            case '{': return '}';
            case 0: return 0;
            default: UNREACHABLE;
            }
        };
        constexpr char vertex_rbracket = rbracket_f(vertex_lbracket);
        constexpr char edge_rbracket = rbracket_f(edge_lbracket);

        constexpr auto bracket_to_string_f = [](char bracket) -> String {
            if (bracket == 0) return "";
            return to_string(bracket);
        };

        static_assert(vertex_id_delim != 0);
        static_assert(edge_lbracket != 0);
        static_assert(edge_delim != 0);

        char c1, c2;
        Float x, y;
        Vertex::Id vid1, vid2;
        while (is >> c1) {
            if (vertex_lbracket == 0 && isdigit(c1)) {
                is.unget();
                c1 = vertex_lbracket;
            }

            switch (c1) {
            case vertex_lbracket:
                expect(is >> vid1 >> c1 >> x >> c2 >> y && c1 == vertex_id_delim && c2 == ',',
                       "Expected "s + bracket_to_string_f(vertex_lbracket)
                       + "<vertex_id>" + vertex_id_delim + "<x>,<y>" + bracket_to_string_f(vertex_rbracket));
                add_vertex({move(vid1), {x, y}});
                if constexpr (vertex_rbracket != 0) {
                    expect(is >> c1 && c1 == vertex_rbracket, "Expected "s + vertex_rbracket);
                }
                break;
            case edge_lbracket:
                expect(is >> vid1 >> c1 >> vid2 && c1 == edge_delim,
                       "Expected "s + edge_lbracket + "<vertex_id1>" + edge_delim + "<vertex_id2>" + edge_rbracket);
                add_edge(vid1, vid2);
                expect(is >> c1 && c1 == edge_rbracket, "Expected "s + edge_rbracket);
                break;
            default:
                if (other_chars_pred(is, c1)) break;
                THROW("Expected a vertex or an edge, got: "s + c1);
            };
        }
    }

    void Graph::add_vertex(Vertex v)
    {
        auto& vs = vertices();
        auto& vid = v.cid();
        if (vid == size(vs)) return vs.push_back(move(v));

        //++ resize, but then gaps must be handled, or it mus be assured that there will be none
        UNREACHABLE;
    }

    void Graph::add_edge(const Vertex::Id& vid1, const Vertex::Id& vid2)
    {
        vertex(vid1).add_neighbor(vid2);
        vertex(vid2).add_neighbor(vid1);
        ++n_edges();
    }

    String Graph::to_string() const
    {
        using tomaqa::to_string;
        return to_string(cvertices());
    }
}
