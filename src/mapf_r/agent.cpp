#include "mapf_r/agent.hpp"

namespace mapf_r::agent {
    Base::Base(Id id_, Float rad, Float abs_v_)
        : _id(move(id_)), _radius(rad), _abs_v(abs_v_)
    {
        check();
    }

    Base::Base(Float rad, Float abs_v_)
        : Base(make_id(), rad, abs_v_)
    { }

    Id Base::make_id()
    {
        static constinit Id counter = 0;
        return ++counter;
    }

    bool Base::valid() const noexcept
    {
        return valid_radius()
            && valid_abs_v();
    }

    void Base::check() const
    {
        check_radius();
        check_abs_v();
    }

    bool Base::valid_radius() const noexcept
    {
        return cradius() > 0;
    }

    void Base::check_radius() const
    {
        using tomaqa::to_string;
        expect(valid_radius(), "Invalid radius: "s + to_string(cradius()));
    }

    bool Base::valid_abs_v() const noexcept
    {
        return cabs_v() > 0;
    }

    void Base::check_abs_v() const
    {
        using tomaqa::to_string;
        expect(valid_abs_v(), "Invalid absolute velocity: "s + to_string(cabs_v()));
    }

    bool Base::equals(const This& rhs) const noexcept
    {
        return cradius() == rhs.cradius()
            && cabs_v() == rhs.cabs_v();
    }

    String Base::to_string() const
    {
        using tomaqa::to_string;
        String str = "agent "s + to_string(cid()) + ":";
        str += " (" + to_string(cradius()) + ")";
        str += " <" + to_string(cabs_v()) + ">";

        return str;
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace mapf_r {
    using namespace agent;

    Agent::Agent(Id id_, Float rad, Float abs_v_, State state_)
        : Inherit(move(id_), rad, abs_v_), _state(move(state_))
    {
        init();
    }

    Agent::Agent(Float rad, Float abs_v_, State state_)
        : Inherit(rad, abs_v_), _state(move(state_))
    {
        init();
    }

    Agent::Agent(Id id_, Float rad, Float abs_v_, State::Parabolic state_, Float delta_)
        : Agent(move(id_), rad, abs_v_, move(state_))
    {
        parabolic_state_set(delta_);
    }

    Agent::Agent(Float rad, Float abs_v_, State::Parabolic state_, Float delta_)
        : Agent(rad, abs_v_, move(state_))
    {
        parabolic_state_set(delta_);
    }

    void Agent::init()
    {
        auto& s = state();
        if (s.idle()) return;
        if (s.linear()) return linear_state_set();
        parabolic_state_set();
    }

    void Agent::idle_state_set()
    {
        assert(cstate().idle());
    }

    void Agent::linear_state_set()
    {
        assert(cstate().linear());
        state().get_linear().set(cabs_v());
    }

    void Agent::parabolic_state_set(Float delta_)
    {
        assert(cstate().parabolic());
        state().get_parabolic().set(cabs_v(), delta_);
    }

    bool Agent::equals(const This& rhs) const noexcept
    {
        return Inherit::equals(rhs)
            && cstate() == rhs.cstate();
    }

    String Agent::to_string() const
    {
        return Inherit::to_string() + " " + cstate().to_string();
    }
}
