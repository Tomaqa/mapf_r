#include "mapf_r/smt/solver/run.hpp"
#include "mapf_r/smt/solver/mathsat.hpp"

using namespace mapf_r::smt::solver;

int main(int argc, const char* argv[])
{
    return Mathsat::Run<Mathsat>(argc, argv).run();
}
