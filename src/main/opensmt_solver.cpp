#include "mapf_r/smt/solver/run.hpp"
#include "mapf_r/smt/solver/opensmt.hpp"

int main(int argc, const char* argv[])
{
    using mapf_r::smt::solver::Opensmt;

    return Opensmt::Run<Opensmt>(argc, argv).run();
}
