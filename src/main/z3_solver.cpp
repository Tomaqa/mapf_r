#include "mapf_r/smt/solver/run.hpp"
#include "mapf_r/smt/solver/z3.hpp"

using namespace mapf_r::smt::solver;

int main(int argc, const char* argv[])
{
    return Z3::Run<Z3>(argc, argv).run();
}
