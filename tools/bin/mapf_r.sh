#!/bin/bash

DIR=`dirname "$0"`
ROOT=`realpath "$DIR"`
while [[ ! `basename "$ROOT"` =~ ^mapf_r ]]; do
  ROOT=`dirname "$ROOT"`
done
ROOT=`realpath --relative-to="$PWD" "$ROOT"`

source "$ROOT/tools/lib"

CMD="$DIR/mathsat_solver"

exec "$CMD" "$@"
