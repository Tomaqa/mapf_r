#!/bin/bash

cd "$1"
for f in $(ls); do
  k=$(sed 's/^.*\_k\([0-9]*\).kruR$/\1/' <<<"$f")
  dir=k$k
  mkdir -p $dir &>/dev/null
  mv "$f" "$dir/$f"
done
