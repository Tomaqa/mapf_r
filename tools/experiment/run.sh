#!/bin/bash

ROOT=$(realpath "`dirname "$0"`")
while [[ ! `basename "$ROOT"` =~ ^mapf_r ]]; do
  ROOT=`dirname "$ROOT"`
done
ROOT=$(realpath --relative-to=. "$ROOT")

source tools/lib

DIR="$TOOLS_DIR/experiment"

DEF_SOLVER=mathsat
OTHER_SOLVER=opensmt

DEF_MODE=release

DEF_TIMEOUT=1m

function usage {
  local cmd="$0"
  local ret=1
  [[ -n $1 ]] && ret=$1

  printf "Usage: %s [<option>*] <name> [<timeout>]\n" "$cmd" >&2
  printf "Options:\n" >&2
  printf "\t-h           prints this help message and exits\n" >&2
  printf "\t-s <solver>  run with given solver (default: %s)\n" $DEF_SOLVER >&2
  printf "\t-d           run in debug mode\n" >&2
  printf "\t-t           produce SMT-LIB2 traces (degrades performance!)\n" >&2
  printf "\t-e           skip errors and print them at the end\n" >&2
  printf "\t-n           no-confirm mode\n" >&2
  printf "\t-k           \"keep\" - do not overwrite top-level results\n" >&2
  printf "\t-o <args>    additional argument or string of args passed to the solver\n" >&2
  printf "Arguments:\n" >&2
  printf "\t<name>       the name of file with experiments or of a collection\n" >&2
  printf "\t<timeout>    a number with time unit suffix [s]|m|h|d (default: %s)\n" $DEF_TIMEOUT >&2
  printf "\nExamples:\n" >&2
  printf "\t%s -h\n" "$cmd" >&2
  printf "\t%s sample\n" "$cmd" >&2
  printf "\t%s -n grid\n" "$cmd" >&2
  printf "\t%s -en base 150\n" "$cmd" >&2
  printf "\t%s -k -o -Fmakespan base 2m\n" "$cmd" >&2
  printf "\t%s -ko '-Fmakespan -B1.25' base 0.25h\n" "$cmd" >&2
  printf "\t%s -s %s base\n" "$cmd" $OTHER_SOLVER >&2

  exit $ret
}

SOLVER=$DEF_SOLVER

MODE=$DEF_MODE

SKIPPED_ERROR_GFILES=()
SKIPPED_ERROR_LFILES=()
SKIPPED_ERROR_PFILES=()

ARGS=()

while getopts ":hs:dtenko:" opt; do
  case $opt in
    h)  usage 0;;
    s)  SOLVER="$OPTARG";;
    d)  MODE=debug;;
    t)  SMT2_TRACES=1;;
    e)  SKIP_ERRORS=1;;
    n)  NO_CONFIRM=1;;
    k)  KEEP_RESULTS=1;;
    o)  ARGS=($OPTARG);;
    \?) printf "Invalid option: -%s\n" "$OPTARG" >&2; usage 1;;
    :)  printf "Option -%s requires an argument\n" "$OPTARG" >&2; usage 1;;
    *)  printf "Unexpected argument: %s\n" "$opt" >&2; usage 1;;
  esac
done

shift $(( $OPTIND - 1 ))

ORIG_CMD=("$BIN_DIR/${MODE}/${SOLVER}_solver")

function check_executable {
  local cmd="$1"
  [[ -x $cmd ]] && return 0

  printf "The binary '%s' is not executable.\n" "$cmd" >&2
  usage
}

check_executable "${ORIG_CMD[0]}"

EXPERIMENTS_NAME="$1"
_COLLECTION_FILE="$DIR/collection/$EXPERIMENTS_NAME"
_IN_FILE="$DIR/in/$EXPERIMENTS_NAME"
if [[ -n $EXPERIMENTS_NAME && -r $_COLLECTION_FILE ]]; then
  EXPERIMENT_NAMES=(`<"$_COLLECTION_FILE"`)
  IS_COLLECTION=1
elif [[ -n $EXPERIMENTS_NAME && -r $_IN_FILE ]]; then
  EXPERIMENT_NAMES=($EXPERIMENTS_NAME)
  IS_COLLECTION=0
else
  printf "The provided file with experiments '%s' is not readable.\n" "$EXPERIMENTS_NAME" >&2
  usage
fi
shift

[[ -n $1 ]] && {
  TIMEOUT=$1
  shift
}
[[ -z $TIMEOUT ]] && TIMEOUT=$DEF_TIMEOUT

[[ -n $1 ]] && {
  printf "Additional arguments: %s\n" "$@"
  usage
}

[[ -n $SMT2_TRACES ]] && command -v dolmen &>/dev/null && USE_DOLMEN=1

N_PROC=$(n_processes)
N_CORES=$(n_cores)
(( $N_PROC > 1 )) && {
  printf "I will use %d parallel processes (out of %d cores).\n" $N_PROC $N_CORES
}

MEM_SIZE_KB=$(mem_size_kb)
MEM_SIZE_GB=$(fpa "$MEM_SIZE_KB/10^6" 2)
printf "Each process can use up to %.1f GB memory on average (out of %.1f GB).\n" $(fpa "$MEM_SIZE_GB/$N_PROC") $MEM_SIZE_GB

if [[ -t 1 && -z $NO_CONFIRM ]]; then
  printf "Confirm..."
  read
else
  printf "\n"
fi

cmd_basename=${EXPERIMENTS_NAME}_cmd
CMD=("$LOCAL_DIR/$cmd_basename" "${ORIG_CMD[@]:1}")
unset IDX
if [[ -e "${CMD[0]}" ]]; then
  printf "Temporary command %s already exists, creating another one.\n\n" "${CMD[0]}"
  KEEP_RESULTS=1
else
  mkdir -p "$LOCAL_DIR"
fi
[[ -n $KEEP_RESULTS ]] && {
  for (( idx=1;; ++idx )); do
    dir="$LOCAL_DIR/$idx"
    [[ -e $dir/$cmd_basename ]] && continue
    IDX=$idx
    LOCAL_DIR="$dir"
    break
  done
  mkdir -p "$LOCAL_DIR"
  CMD[0]="$LOCAL_DIR/$cmd_basename"
}
cp "${ORIG_CMD[0]}" "${CMD[0]}"
chmod +x "${CMD[0]}"
check_executable "${CMD[0]}"

function get_files {
  local f="$1"
  [[ -r $f ]] || {
    printf "File '%s' is not readable!\n" "$f" >&2
    return 2
  }
  [[ -d $f ]] || {
    printf "%s" "$f"
    return 0
  }

  for f2 in $(ls -v "$f"); do
    get_files "$f/$f2" || return $?
    printf " "
  done

  return 0
}

printf "Preparing data...\n"

declare -a {OUT,TSV}_FILES
for ename in ${EXPERIMENT_NAMES[@]}; do
  declare -a ${ename^^}_{GRAPH,LAYOUT,PLAN,SMT2}_FILES
  declare -n gfiles=${ename^^}_GRAPH_FILES
  declare -n lfiles=${ename^^}_LAYOUT_FILES

  file="$DIR/in/$ename"
  [[ -r $file ]] || {
    printf "The file with inputs '%s' for experiments '%s' is not readable.\n" "$file" "$EXPERIMENTS_NAME" >&2
    usage
  }

  while read line; do
    declare -a pair
    split_string "$line" pair '|'
    gfile="$DATA_DIR/graph/$(trim "${pair[0]}")"
    lfile="$DATA_DIR/layout/$(trim "${pair[1]}")"
    l_gfiles=($(get_files "$gfile")) || exit $?
    l_lfiles=($(get_files "$lfile")) || exit $?
    for gfile in "${l_gfiles[@]}"; do
      for lfile in "${l_lfiles[@]}"; do
        gfiles+=("$gfile")
        lfiles+=("$lfile")
      done
    done
  done <"$file"

  for kw in out tsv; do
    file="$LOCAL_DIR/${ename}.${kw}"
    >"$file" || {
      printf "The output file '%s' is not writeable file.\n" "$file" >&2
      exit 2
    }
    declare -n files=${kw^^}_FILES
    files+=("$file")
  done

  (( $IS_COLLECTION )) && {
    TSV_FILE="$LOCAL_DIR/${EXPERIMENTS_NAME}.tsv"
    >"$TSV_FILE" || {
      printf "The overall results file '%s' is not writeable file.\n" "$TSV_FILE" >&2
      exit 2
    }
  }
done

function print_info {
  printf "%s: graph='%s' layout='%s' plan='%s' out='%s'.\n\n" "$@"
}

function n_jobs {
  jobs -p | wc -l
}

function n_running_jobs {
  jobs -r -p | wc -l
}

function killer {
  local sleep_time=$(fpa "10/$N_PROC")
  local free_perc
  while ((1)); do
    sleep $sleep_time
    local free_kb=$(mem_free_kb)
    fpa free_perc "($free_kb/$MEM_SIZE_KB)*100" 0
    (( $free_perc >= 10 )) && continue

    local pid=$(ps -e --no-headers -o pid --sort=-%mem | head -n 1 | xargs)

    printf "\nOut of memory, killing %d ...\n" $pid
    kill -INT $pid
  done
}

function serializer {
  for idx in ${!EXPERIMENT_NAMES[@]}; do
    local -n ename=EXPERIMENT_NAMES[$idx]
    local -n gfiles=${ename^^}_GRAPH_FILES
    local -n lfiles=${ename^^}_LAYOUT_FILES
    local -n g_ofile=OUT_FILES[$idx]
    local -n rfile=TSV_FILES[$idx]

    for f_idx in ${!gfiles[@]}; do
      local -n gfile=gfiles[$f_idx]
      local -n lfile=lfiles[$f_idx]
      local ofile="${g_ofile}_${f_idx}"
      local pnfile="${ofile/.out/.pname}"
      printf "\n" >>"$g_ofile"
      printf "input graph: %s\n" "$gfile" >>"$g_ofile"
      printf "input layout: %s\n" "$lfile" >>"$g_ofile"
      while ((1)); do
        [[ -e $pnfile ]] && break
        sleep 0.5
      done
      local pfile=`<"$pnfile"`
      printf "output plan: %s\n" "$pfile" >>"$g_ofile"
      cat "$ofile" >>"$g_ofile"
    done
    printf "\nOutputs of '%s' serialized into '%s'.\n" $ename "$g_ofile"

    awk -f "$TOOLS_DIR/results.awk" "$g_ofile" >"$rfile"
    printf "Results of '%s' stored in '%s'.\n\n" $ename "$rfile"

    (( $IS_COLLECTION )) && {
      cat "$rfile" >>"$TSV_FILE"
    }
  done

  (( $IS_COLLECTION )) && {
    printf "All results serialized into '%s'.\n" "$TSV_FILE"
  }
}

N_AUX_PROC=0

killer &
KILLER_PID=$!
(( ++N_AUX_PROC ))

serializer &
SERIALIZER_PID=$!
(( ++N_AUX_PROC ))

(( N_PROC += $N_AUX_PROC ))

function cleanup {
  [[ $1 == 0 ]] && {
    printf "Waiting on the serializer ...\n"
    wait $SERIALIZER_PID
  }

  pkill -P $$
  printf "Waiting on child processes ...\n"
  wait

  rm -f "${CMD[0]}"

  for idx in ${!EXPERIMENT_NAMES[@]}; do
    local ename=${EXPERIMENT_NAMES[$idx]}
    local -n gfiles=${ename^^}_GRAPH_FILES

    for f_idx in ${!gfiles[@]}; do
      local ofile="${OUT_FILES[$idx]}_${f_idx}"
      local pnfile="${ofile/.out/.pname}"
      rm -f "$ofile" "$pnfile"
    done
  done

  exit $1
}

TIMEOUT_RET=124
KILL_RET=130

trap cleanup INT TERM QUIT

N_EXECUTED=0
:&
wait -n -p pid &>/dev/null || {
  wait -n || {
    printf "wait command failed!\n" >&2
    cleanup 1
  }

  PIDS=()
  USE_PIDS=1
}

function wait_f {
  local n_running=$(($(n_running_jobs) - $N_AUX_PROC))
  local n_finished=$(( $N_EXECUTED - $n_running ))
  printf "%d running jobs, " $n_running
  if (( $n_finished > 0 )); then
    printf "%d already finished" $n_finished
  else
    printf "waiting ..."
  fi

  local ret
  local pid
  if [[ -z $USE_PIDS ]]; then
    wait -n -p pid
    ret=$?
  else
    wait -n
    ret=$?

    local jobs_pids=($(jobs -r -p))
    for i in ${!PIDS[@]}; do
      local p=${PIDS[$i]}
      [[ " ${jobs_pids[*]} " =~ " $p " ]] && continue
      pid=$p
      unset "PIDS[$i]"
      break
    done
  fi
  printf "\n"

  [[ -z $pid ]] && {
    printf "Failed to load PID of a child process.\n" >&2
    cleanup $ret
  }
  (( --N_EXECUTED ))

  local -n e_idx=E_IDX_OF_PID[$pid]
  local -n f_idx=F_IDX_OF_PID[$pid]
  local -n ename=EXPERIMENT_NAMES[$e_idx]
  local -n gfiles=${ename^^}_GRAPH_FILES
  local -n lfiles=${ename^^}_LAYOUT_FILES
  local -n pfiles=${ename^^}_PLAN_FILES
  local -n tfiles=${ename^^}_SMT2_FILES
  local -n gfile=gfiles[$f_idx]
  local -n lfile=lfiles[$f_idx]
  local -n pfile=pfiles[$f_idx]
  local -n tfile=tfiles[$f_idx]
  local ofile="${OUT_FILES[$e_idx]}_${f_idx}"
  local pnfile="${ofile/.out/.pname}"

  printf "$pfile" >"$pnfile"

  local msg
  case $ret in
    0) msg=Finished;;
    $TIMEOUT_RET) msg=Timeout;;
    $KILL_RET) msg=Killed;;
    *) msg=Failed;;
  esac
  print_info "$msg" "$gfile" "$lfile" "$pfile" "$ofile"

  case $ret in
    0);;
    $TIMEOUT_RET) printf "\ntimeout!\n" >>"$ofile";;
    $KILL_RET) printf "\nout of memory!\n" >>"$ofile";;
    *)
      if [[ -z $SKIP_ERRORS ]]; then
        cleanup $ret
      else
        SKIPPED_ERROR_GFILES+=("$gfile")
        SKIPPED_ERROR_LFILES+=("$lfile")
        SKIPPED_ERROR_PFILES+=("$pfile")
      fi;;
  esac

  [[ -z $SMT2_TRACES ]] && return
  [[ -r $tfile ]] || {
    printf "SMT-LIB2 trace file '%s' not found.\n" "$tfile" >&2
    cleanup 200
  }
  if (( $ret == 0 )); then
    local exit_str='(exit)'
    [[ $(tail -n 1 <"$tfile") == $exit_str ]] || {
      printf "The produced SMT-LIB2 trace file '%s' does not end with '%s'.\n" "$tfile" "$exit_str" >&2
      cleanup 201
    }

    [[ -z $USE_DOLMEN ]] && return
    dolmen -i smt2 --check-headers=true --header-lang-version=2.6 "$tfile" || {
      cleanup 202
    }
  else
    rm "$tfile"
  fi
}

declare -A {E_IDX,F_IDX}_OF_PID
for idx in ${!EXPERIMENT_NAMES[@]}; do
  ename=${EXPERIMENT_NAMES[$idx]}
  declare -n gfiles=${ename^^}_GRAPH_FILES
  declare -n lfiles=${ename^^}_LAYOUT_FILES
  declare -n pfiles=${ename^^}_PLAN_FILES
  declare -n tfiles=${ename^^}_SMT2_FILES

  for f_idx in ${!gfiles[@]}; do
    gfile="${gfiles[$f_idx]}"
    lfile="${lfiles[$f_idx]}"
    ofile="${OUT_FILES[$idx]}_${f_idx}"

    pdir="$DATA_DIR/plan/gen/$ename"
    mkdir -p "$pdir" &>/dev/null
    gbase=$(basename "${gfile%.*}")
    lbase=$(basename "${lfile%.*}")
    pbase=$(printf "%s\n%s\n" "$gbase" "$lbase" | sed -rne 'N;s/^(.*)(|(_.*))\n\1(|([_-].*))$/\1\3\5/p')
    [[ -z $pbase ]] && {
      printf "Failed to find a common base between graph base '%s' and layout base '%s'.\n" "$gbase" "$lbase" >&2
      cleanup 3
    }
    pfile="$pdir/${pbase}.p"
    pfiles+=("$pfile")

    if [[ -z $SMT2_TRACES ]]; then
      targs=()
    else
      tdir="${pdir/plan/smt2}"
      mkdir -p "$tdir" &>/dev/null
      tbase="${pbase/plan/smt2}"
      tfile="$tdir/${tbase}.smt2"
      tfiles+=("$tfile")
      targs=(-t "$tfile")
    fi

    print_info Running "$gfile" "$lfile" "$pfile" "$ofile"
    timeout -s INT $TIMEOUT "${CMD[@]}" -g "$gfile" -l "$lfile" -p "$pfile" "${targs[@]}" -o "$ofile" "${ARGS[@]}" &
    E_IDX_OF_PID[$!]=$idx
    F_IDX_OF_PID[$!]=$f_idx
    [[ -n $USE_PIDS ]] && PIDS+=($!)
    (( ++N_EXECUTED ))

    while (( $(n_jobs) >= $N_PROC )); do
      wait_f
    done
  done
done

while (( $(n_jobs) > $N_AUX_PROC )); do
  wait_f
done

[[ -n $SKIP_ERRORS ]] && (( ${#SKIPPED_ERROR_PFILES[@]} > 0 )) && {
  printf "\nThere has been %d failures:\n" ${#SKIPPED_ERROR_PFILES[@]} >&2
  for idx in ${!SKIPPED_ERROR_PFILES[@]}; do
    gfile="${SKIPPED_ERROR_GFILES[$idx]}"
    lfile="${SKIPPED_ERROR_LFILES[$idx]}"
    pfile="${SKIPPED_ERROR_PFILES[$idx]}"
    printf "\t%s %s %s\n" "$gfile" "$lfile" "$pfile" >&2
  done
}

printf "Success.\n"
cleanup 0
