BEGIN {
    FS = ": "
    print_x1 = 1
    print_x2 = 1
    print_nl = 0
    just_started = 0
    coef = ""
}

/output plan:/ {
    if (print_nl) {
        printf("\n")
        print_nl = 0
    }

    sub(/^.*data\/plan\/gen\//, "", $2)
    sub(/^[^\/]*\//, "", $2)
    sub(/\.p$/, "", $2)
    printf("%s\t", $2)

    just_started = 1
}

/objective time:/ {
    if (just_started) {
        printf("%s\t", $2)
        print_x1 = 0
        just_started = 0
    }
}

/guarant[^:]*coef[^:]*:/ {
    coef = $2
}

/makespan:/ {
    printf("%s\t", $2)
    print_x2 = 0
}

/sum of costs:/ {
    printf("%s\t%s\t", $2, coef)
    coef = ""
}

/duration:/ {
    if (print_x1) {
        printf("\t")
        print_x1 = 0
    }
    if (print_x2) {
        printf("\t\t\t")
        print_x2 = 0
    }
    sub(/ s.*$/, "", $2)
    printf("%s\t", $2)
}

/count:/ {
    sub(/ x.*$/, "", $2)
    printf("%s\t", $2)
}

/conflicts:/ {
    printf("%s", $2)
    print_x1 = 1
    print_x2 = 1
    print_nl = 1
}

function kill(str)
{
    if (!print_nl) {
        print_nl = 1
    }
    else {
        printf("\t")
    }
    printf("%s", str)
}

/timeout/ {
    kill("timeout")
}

/(memory|killed)/ {
    kill("killed")
}

END {
    if (print_nl) printf("\n")
}
