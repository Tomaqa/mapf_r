#!/bin/bash

TOOLS_TEST_DIR=`dirname "$0"`/test

DEBUG=0
RDIR=release
[[ $1 =~ ^[dD] ]] && {
  DEBUG=1
  RDIR=debug
  shift
}

N=1
[[ -n $1 ]] && N=$1

set -e
set -v

TEST=test/$RDIR/mathsat_test
unset FLAGS
(( $DEBUG )) && FLAGS="DEBUG=1"

make $FLAGS $TEST
cp $TEST local/test$N
chmod +x local/test$N
local/test$N >local/o$N
tail <local/o$N
#$TOOLS_TEST_DIR/results.sh <local/o$N | xclip -sel clip
awk -f tools/results.awk <local/o$N | xclip -sel clip
