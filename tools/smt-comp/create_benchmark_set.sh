#!/bin/bash

ROOT=$(realpath "`dirname "$0"`")
while [[ ! `basename "$ROOT"` =~ ^mapf_r ]]; do
  ROOT=`dirname "$ROOT"`
done
ROOT=$(realpath --relative-to=. "$ROOT")

source tools/lib

function usage {
  local cmd="$0"

  printf "USAGE: %s <logic> <cost_f> <coef> <dir>+\n" "$cmd"

  [[ -n $1 ]] && exit $1
}

[[ $1 =~ ^QF_[A-Z]+$ ]] || {
  printf "Expected a logic, got: %s\n" "$1" >&2
  usage 1
}
LOGIC=$1
shift

[[ $1 =~ ^(makespan|soc)$ ]] || {
  printf "Expected a cost function, got: %s\n" "$1" >&2
  usage 1
}
COST_F=$1
shift

( [[ $1 =~ ^[0-9]+(|\.[0-9]*[1-9]+)$ ]] && (( $(bc -l <<<"$1 > 1") == 1 )) ) || {
  printf "Expected a sub-optimal coefficient >1, got: %s\n" "$1" >&2
  usage 1
}
COEF=$1
COEF_STR="coef_$COEF"
shift

[[ -z $1 ]] && {
  printf "Expected at least one directory.\n" >&2
  usage 1
}

TMP=`mktemp` || exit $?

YEAR=$(date '+%Y')
DATE=$(date '+%Y%m%d')

TARGET_DIR_BASE="$DATA_DIR/smt2/gen/smt-comp/$YEAR"
TARGET_DIR_SUFFIX=incremental/$LOGIC/${DATE}-mapf_r/$COST_F/$COEF_STR
TARGET_DIR="$TARGET_DIR_BASE/$TARGET_DIR_SUFFIX"

CHECK_SCRIPT=$(realpath "$TOOLS_DIR/smt-comp/quick-check.sh")

function cleanup {
  local ret=0
  [[ -n $1 ]] && ret=$1

  rm -f $TMP
}

mkdir -p "$TARGET_DIR" || cleanup $?

for d in "$@"; do
  [[ -d $d ]] || {
    printf "Expected a directory, got: %s\n" "$d" >&2
    usage
    cleanup 1
  }
  d_base=$(basename "$d")
  target_d="$TARGET_DIR/$d_base"
  printf "Copying %s -> %s ...\n" "$d" "$target_d"
  cp -r -t "$TARGET_DIR" "$d" || cleanup $?
done

printf "\nChecking files ...\n"

## $CHECK_SCRIPT does not support checking from arbitrary position ..
pushd "$TARGET_DIR_BASE" >/dev/null
DIRS=($(find $TARGET_DIR_SUFFIX/.. -mindepth 1 -maxdepth 1 -type d ! -name $COEF_STR))
find $TARGET_DIR_SUFFIX -type f -name '*.smt2' | while read path; do
  $CHECK_SCRIPT "$path" >$TMP || {
    cat $TMP
    cleanup 2
  }

  base=$(basename "$path")
  (( ${#DIRS[@]} > 0 )) && find "${DIRS[@]}" -type f -name "$base" | while read path2; do
    cmp -s "$path" "$path2" && {
      printf "Removing duplicate benchmark '%s'\n" "$path"
      rm "$path" || cleanup 201
      break
    }
  done
done
popd >/dev/null

cleanup
printf "\nSuccess.\n"
