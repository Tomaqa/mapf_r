#!/bin/bash

function usage {
  local cmd="$0"

  printf "USAGE: %s <solver> <dir>\n" "$cmd"

  [[ -n $1 ]] && exit $1
}

[[ $1 =~ ^(z3|cvc[45]|mathsat)$ ]] || {
  printf "Expected an SMT solver, got: %s\n" "$1" >&2
  usage 1
}
SOLVER=$1
shift

[[ -d $1 ]] || {
  printf "Expected a directory, got: %s\n" "$1" >&2
  usage 1
}
DIR="$1"
shift

ARGS=()
[[ $SOLVER =~ ^cvc ]] && ARGS+=(--incremental)

find "$DIR" -type f -name '*.smt2' -not -name '*_unsat*' | xargs -I+ sh -c "printf '%s\t' +; $SOLVER ${ARGS[@]} + 2>/dev/null | sed -n 's|(/ \([0-9.]*\) \([0-9.]*\))|\1/\2|;s|.*objtime \([^) ]*\).*$|scale=3;\1|p' | bc -l"
