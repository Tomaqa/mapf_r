;; (set-logic QF_RDL)
(set-logic QF_LRA)

(set-option :produce-models true)
(set-option :opt.priority pareto)

(declare-const totaltime Real)

(declare-const a1.A<0> Bool)
(declare-const a1.B<1> Bool)
(declare-const a1.F<2> Bool)
(declare-const a1.I<3> Bool)

(declare-const a2.E<0> Bool)
(declare-const a2.F<1> Bool)
(declare-const a2.I<2> Bool)
(declare-const a2.J<3> Bool)

(declare-const a3.G<0> Bool)
(declare-const a3.H<1> Bool)
(declare-const a3.C<2> Bool)
(declare-const a3.D<3> Bool)

(declare-const a1.atime<0> Real)
(declare-const a1.atime<1> Real)
(declare-const a1.atime<2> Real)
(declare-const a1.atime<3> Real)

(declare-const a2.atime<0> Real)
(declare-const a2.atime<1> Real)
(declare-const a2.atime<2> Real)
(declare-const a2.atime<3> Real)

(declare-const a3.atime<0> Real)
(declare-const a3.atime<1> Real)
(declare-const a3.atime<2> Real)
(declare-const a3.atime<3> Real)

(declare-const a1.rtime<0> Real)
(declare-const a1.rtime<1> Real)
(declare-const a1.rtime<2> Real)
(declare-const a1.wtime<0> Real)
(declare-const a1.wtime<1> Real)
(declare-const a1.wtime<2> Real)
(declare-const a1.mtime<0> Real)
(declare-const a1.mtime<1> Real)
(declare-const a1.mtime<2> Real)

(declare-const a2.rtime<0> Real)
(declare-const a2.rtime<1> Real)
(declare-const a2.rtime<2> Real)
(declare-const a2.wtime<0> Real)
(declare-const a2.wtime<1> Real)
(declare-const a2.wtime<2> Real)
(declare-const a2.mtime<0> Real)
(declare-const a2.mtime<1> Real)
(declare-const a2.mtime<2> Real)

(declare-const a3.rtime<0> Real)
(declare-const a3.rtime<1> Real)
(declare-const a3.rtime<2> Real)
(declare-const a3.wtime<0> Real)
(declare-const a3.wtime<1> Real)
(declare-const a3.wtime<2> Real)
(declare-const a3.mtime<0> Real)
(declare-const a3.mtime<1> Real)
(declare-const a3.mtime<2> Real)

(define-fun max ((a Real) (b Real)) Real (ite (> a b) a b))

;; true forall k

(minimize totaltime)

(assert (and
    a1.A<0>
    (= a1.atime<0> 0)
    (= a1.atime<1> (+ a1.atime<0> a1.rtime<0>))
    (= a1.atime<2> (+ a1.atime<1> a1.rtime<1>))
    (= a1.atime<3> (+ a1.atime<2> a1.rtime<2>))
    (= a1.rtime<0> (+ a1.wtime<0> a1.mtime<0>))
    (= a1.rtime<1> (+ a1.wtime<1> a1.mtime<1>))
    (= a1.rtime<2> (+ a1.wtime<2> a1.mtime<2>))
    (>= a1.wtime<0> 0)
    (>= a1.wtime<1> 0)
    (>= a1.wtime<2> 0)
    (> a1.mtime<0> 0)
    (> a1.mtime<1> 0)
    (> a1.mtime<2> 0)
    a2.E<0>
    (= a2.atime<0> 0)
    (= a2.atime<1> (+ a2.atime<0> a2.rtime<0>))
    (= a2.atime<2> (+ a2.atime<1> a2.rtime<1>))
    (= a2.atime<3> (+ a2.atime<2> a2.rtime<2>))
    (= a2.rtime<0> (+ a2.wtime<0> a2.mtime<0>))
    (= a2.rtime<1> (+ a2.wtime<1> a2.mtime<1>))
    (= a2.rtime<2> (+ a2.wtime<2> a2.mtime<2>))
    (>= a2.wtime<0> 0)
    (>= a2.wtime<1> 0)
    (>= a2.wtime<2> 0)
    (> a2.mtime<0> 0)
    (> a2.mtime<1> 0)
    (> a2.mtime<2> 0)
    a3.G<0>
    (= a3.atime<0> 0)
    (= a3.atime<1> (+ a3.atime<0> a3.rtime<0>))
    (= a3.atime<2> (+ a3.atime<1> a3.rtime<1>))
    (= a3.atime<3> (+ a3.atime<2> a3.rtime<2>))
    (= a3.rtime<0> (+ a3.wtime<0> a3.mtime<0>))
    (= a3.rtime<1> (+ a3.wtime<1> a3.mtime<1>))
    (= a3.rtime<2> (+ a3.wtime<2> a3.mtime<2>))
    (>= a3.wtime<0> 0)
    (>= a3.wtime<1> 0)
    (>= a3.wtime<2> 0)
    (> a3.mtime<0> 0)
    (> a3.mtime<1> 0)
    (> a3.mtime<2> 0)
    (=> (and a1.A<0> a1.B<1>) (= a1.mtime<0> 2))
    (=> (and a1.B<1> a1.F<2>) (= a1.mtime<1> 2))
    (=> (and a1.F<2> a1.I<3>) (= a1.mtime<2> 2.828))
    (=> (and a2.E<0> a2.F<1>) (= a2.mtime<0> 2))
    (=> (and a2.F<1> a2.I<2>) (= a2.mtime<1> 2.828))
    (=> (and a2.I<2> a2.J<3>) (= a2.mtime<2> 2))
    (=> (and a3.G<0> a3.H<1>) (= a3.mtime<0> 2))
    (=> (and a3.H<1> a3.C<2>) (= a3.mtime<1> 5))
    (=> (and a3.C<2> a3.D<3>) (= a3.mtime<2> 1))
    (=> a1.A<0> (or a1.B<1>))
    (=> a1.B<1> (or a1.F<2>))
    (=> a2.F<1> (or a2.I<2>))
    (=> a2.I<2> (or a2.J<3>))
    (=> a3.H<1> (or a3.C<2>))
    (=> a3.C<2> (or a3.D<3>))
))

;; only k == 3
(push)

(assert (and
    (= totaltime (max a1.atime<3> (max a2.atime<3> a3.atime<3>)))
    ;; (= totaltime (+ a1.atime<3> a2.atime<3> a3.atime<3>))
    (=> a1.F<2> (or a1.I<3>))
    (=> a2.E<0> (or a2.F<1>))
    (=> a3.G<0> (or a3.H<1>))
    a1.I<3>
    a2.J<3>
    a3.D<3>

    (= a1.wtime<0> 0)
))

(push)
(assert (and
    (= a2.wtime<0> 0)
    (= a3.wtime<0> 0)
))

(check-sat)

(pop)

;; ... is there conflict? ...

;; conflict (20)
;; (assert (not (and
;;     a2.F<1> a2.I<2> (= a2.atime<1> 2) (= a2.atime<2> 4.828)
;;     a3.H<1> a3.C<2> (= a3.atime<1> 2) (= a3.atime<2> 7)
;; )))
;; (assert (not (and
;;     a2.F<1> a2.I<2> (= a2.atime<1> 2) (< a2.wtime<1> 1.31)
;;     a3.H<1> a3.C<2> (= a3.atime<1> 2) (< a3.wtime<1> 1.743)
;; )))

;; unsafe intervals (15)
;; a2: [2, 3.31)
;; a3: [2, 3.743)
;; relatively:
;; a2: [0, 1.31)
;; a3: [0, 1.743)
;; in general:
;; a2: [a2l, a2u)
;; a3: [a3l, a3u)
;; relatively:
;; a2: [0, a2u-a2l)
;; a3: [a3l-a2l, a3u-a2l)
(assert (not (and
    ;; forall i,j F<i> I<i+1> H<j> C<j+1>
    a2.F<1> a2.I<2>
    a3.H<1> a3.C<2>
    ;; l|u ~ unsafe intervals
    ;; absolute times:
    (let ( (cxl 2.0) (cxu 3.743) (cyl 2.0) (cyu 3.31) (x (+ a2.atime<1> a2.wtime<1>)) (y (+ a3.atime<1> a3.wtime<1>)))
        (and (< (- x y) (- cxu cyl))
             (< (- y x) (- cyu cxl))
)))))

(check-sat)

(assert (and
    (= a2.wtime<0> 5)
    (= a3.wtime<0> 5)
))
(check-sat)

;; if unsat: ... --> k == 4

;; (pop)

;; (declare-const a1.E<3> Bool)
;; (declare-const a1.I<4> Bool)

;; (declare-const a1.atime<4> Real)

;; (= a1.atime<4> 8000)

;; (=> a1.I<3> (or a1.I<4>))

(get-model)
(get-objectives)
