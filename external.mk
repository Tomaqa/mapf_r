ifdef DEBUG
  CMAKE_BUILD_TYPE_OPT := -DCMAKE_BUILD_TYPE=Debug
else
  CMAKE_BUILD_TYPE_OPT :=
endif

$(LIB_DIR)/libopensmt.a:
	@cd $(EXTERNAL_DIR)/opensmt; mkdir -p build; cd build; cmake $(CMAKE_BUILD_TYPE_OPT) -DCMAKE_INSTALL_PREFIX=../../.. ..
	@$(MAKE) -C $(EXTERNAL_DIR)/opensmt/build install
	@mv -f $(LIB_DIR_BASE)/$(@F) $@
	@rm -fr $(LIB_DIR_BASE)/cmake
