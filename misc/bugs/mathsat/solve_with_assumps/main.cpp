#include <cassert>
#include <iostream>
#include <string>
#include <optimathsat.h>

using std::cout;
using std::cerr;
using std::endl;
using std::ostream;
using std::string;

::msat_term make_var(::msat_env& env, const string& id, ::msat_type type)
{
    ::msat_decl decl = ::msat_declare_function(env, id.c_str(), type);
    assert(!MSAT_ERROR_DECL(decl));
    return ::msat_make_constant(env, decl);
}

::msat_term make_real(::msat_env& env, int n, int d = 1)
{
    ::mpq_t mpq_val;
    ::mpq_init(mpq_val);
    ::mpq_set_si(mpq_val, n, d);
    auto e = ::msat_make_mpq_number(env, mpq_val);
    ::mpq_clear(mpq_val);
    return e;
}

void assert_expr(::msat_env& env, ::msat_term e)
{
    [[maybe_unused]] const int res = ::msat_assert_formula(env, e);
    assert(res == 0);
}

::msat_result check_sat(::msat_env& env)
{
    #ifdef OK
    return ::msat_solve(env);
    #endif
    #ifdef FAIL
    return ::msat_solve_with_assumptions(env, nullptr, 0);
    #endif

    cerr << "Compile either with -DOK or -DFAIL." << endl;
    exit(1);
}

ostream& operator <<(ostream& os, const ::msat_term& e)
{
    char* c_str = ::msat_term_repr(e);
    os << c_str;
    ::msat_free(c_str);
    return os;
}

int main()
{
    auto cfg = ::msat_create_config();
    ::msat_set_option(cfg, "model_generation", "true");
    auto env = ::msat_create_opt_env(cfg);
    ::msat_destroy_config(cfg);

    const auto real_type = ::msat_get_rational_type(env);
    assert(!MSAT_ERROR_TYPE(real_type));
    const auto bool_type = ::msat_get_bool_type(env);
    assert(!MSAT_ERROR_TYPE(bool_type));

    auto total_time = make_var(env, "totaltime", real_type);
    const auto zero_e = make_real(env, 0);
    const auto ten_e = make_real(env, 10);
    assert_expr(env, ::msat_make_leq(env, zero_e, total_time));
    assert_expr(env, ::msat_make_leq(env, total_time, ten_e));

    [[maybe_unused]] auto sat = check_sat(env);
    assert(sat == MSAT_SAT);

    auto model = ::msat_get_model(env);
    auto total = ::msat_model_eval(model, total_time);
    cout << "total: " << total << endl;

    auto objective = ::msat_make_minimize(env, total_time);
    assert(!MSAT_ERROR_OBJECTIVE(objective));
    [[maybe_unused]] const int res = ::msat_assert_objective(env, objective);
    assert(res == 0);

    sat = check_sat(env);
    assert(sat == MSAT_SAT);

    /// ... fails here
    assert(::msat_objective_value_is_unbounded(env, objective, MSAT_OPTIMUM) == 0);
    assert(::msat_objective_value_is_strict(env, objective, MSAT_OPTIMUM) == 0);
    const auto inf = make_var(env, "oo", real_type);
    const auto eps = make_var(env, "eps", real_type);
    auto min_total = ::msat_objective_value_term(env, objective, MSAT_OPTIMUM, inf, eps);
    cout << "min_total: " << min_total << endl;

    ::msat_destroy_model(model);
    ::msat_destroy_objective(env, objective);
    ::msat_destroy_env(env);

    return 0;
}
