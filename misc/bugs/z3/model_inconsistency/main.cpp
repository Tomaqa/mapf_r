#include <iostream>
#include <z3++.h>

using ::z3::expr;
using ::z3::expr_vector;

using namespace std::string_literals;

#define expect(condition, msg) { if (condition); else throw ::z3::exception(std::string(msg).c_str()); }

int main()
{
    ::z3::context ctx;
    ::z3::optimize s(ctx);

    ::z3::set_param("auto_config", true);
    ::z3::set_param("model", true);
    ::z3::set_param("model_validate", false);
    ::z3::set_param("verbose", 1);

    ::z3::params params(ctx);
    //!! with this it will work OK
    // params.set("opt.incremental", true);
    s.set(params);

    expr totaltime = ctx.real_const("totaltime");
    auto minimize_handle = s.minimize(totaltime);

    const auto check_f = [&](auto expected_res){
        const auto res = s.check();
        expect(res == expected_res, "res != expected_res");
        if (expected_res != ::z3::sat) return;

        const auto min_totaltime = s.lower(minimize_handle).as_double();

        auto model = s.get_model();
        expr total_e = model.eval(totaltime);
        const double total = total_e.as_double();

        expect(min_totaltime == total,
               "lower != model: "s + std::to_string(min_totaltime) + " != " + std::to_string(total));

        s.add(totaltime >= total_e);
    };

    expr_vector conflicts(ctx);

    const auto add_conflict_f = [&](expr clause){
        expr conflict = !clause;
        s.add(conflict);
        conflicts.push_back(conflict);
    };

    expr a0_atime_0 = ctx.real_const("A0.atime<0>");
    expr a0_mtime_0 = ctx.real_const("A0.mtime<0>");
    expr a0_wtime_0 = ctx.real_const("A0.wtime<0>");
    expr a0_rtime_0 = ctx.real_const("A0.rtime<0>");
    expr a1_atime_0 = ctx.real_const("A1.atime<0>");
    expr a1_mtime_0 = ctx.real_const("A1.mtime<0>");
    expr a1_wtime_0 = ctx.real_const("A1.wtime<0>");
    expr a1_rtime_0 = ctx.real_const("A1.rtime<0>");
    expr a2_atime_0 = ctx.real_const("A2.atime<0>");
    expr a2_mtime_0 = ctx.real_const("A2.mtime<0>");
    expr a2_wtime_0 = ctx.real_const("A2.wtime<0>");
    expr a2_rtime_0 = ctx.real_const("A2.rtime<0>");
    expr a3_atime_0 = ctx.real_const("A3.atime<0>");
    expr a3_mtime_0 = ctx.real_const("A3.mtime<0>");
    expr a3_wtime_0 = ctx.real_const("A3.wtime<0>");
    expr a3_rtime_0 = ctx.real_const("A3.rtime<0>");
    expr a3_v2_0 = ctx.bool_const("A3.V2<0>");
    expr a2_v3_0 = ctx.bool_const("A2.V3<0>");
    expr a1_v1_0 = ctx.bool_const("A1.V1<0>");
    expr a0_v0_0 = ctx.bool_const("A0.V0<0>");
    expr a0_v1_0 = ctx.bool_const("A0.V1<0>");
    expr a0_v2_0 = ctx.bool_const("A0.V2<0>");
    expr a0_v3_0 = ctx.bool_const("A0.V3<0>");
    expr a1_v0_0 = ctx.bool_const("A1.V0<0>");
    expr a1_v2_0 = ctx.bool_const("A1.V2<0>");
    expr a1_v3_0 = ctx.bool_const("A1.V3<0>");
    expr a2_v1_0 = ctx.bool_const("A2.V1<0>");
    expr a2_v0_0 = ctx.bool_const("A2.V0<0>");
    expr a2_v2_0 = ctx.bool_const("A2.V2<0>");
    expr a3_v1_0 = ctx.bool_const("A3.V1<0>");
    expr a3_v0_0 = ctx.bool_const("A3.V0<0>");
    expr a3_v3_0 = ctx.bool_const("A3.V3<0>");
    expr ass0 = ctx.bool_const("ass0");

    s.add(a0_atime_0 == 0);
    s.add(a0_rtime_0 == a0_wtime_0 + a0_mtime_0);
    s.add(a0_wtime_0 >= 0);
    s.add(a0_mtime_0 >= 0);
    s.add(a1_atime_0 == 0);
    s.add(a1_rtime_0 == a1_wtime_0 + a1_mtime_0);
    s.add(a1_wtime_0 >= 0);
    s.add(a1_mtime_0 >= 0);
    s.add(a2_atime_0 == 0);
    s.add(a2_rtime_0 == a2_wtime_0 + a2_mtime_0);
    s.add(a2_wtime_0 >= 0);
    s.add(a2_mtime_0 >= 0);
    s.add(a3_atime_0 == 0);
    s.add(a3_rtime_0 == a3_wtime_0 + a3_mtime_0);
    s.add(a3_wtime_0 >= 0);
    s.add(a3_mtime_0 >= 0);
    s.add(a3_v2_0);
    s.add(a2_v3_0);
    s.add(a1_v1_0);
    s.add(a0_v0_0);
    s.add(!(a0_v0_0 && a0_v1_0));
    s.add(!(a0_v0_0 && a0_v2_0));
    s.add(!(a0_v0_0 && a0_v3_0));
    s.add(!(a0_v1_0 && a0_v2_0));
    s.add(!(a0_v1_0 && a0_v3_0));
    s.add(!(a0_v2_0 && a0_v3_0));
    s.add(!(a1_v0_0 && a1_v1_0));
    s.add(!(a1_v0_0 && a1_v2_0));
    s.add(!(a1_v0_0 && a1_v3_0));
    s.add(!(a1_v1_0 && a1_v2_0));
    s.add(!(a1_v1_0 && a1_v3_0));
    s.add(!(a1_v2_0 && a1_v3_0));
    s.add(!(a2_v0_0 && a2_v1_0));
    s.add(!(a2_v0_0 && a2_v2_0));
    s.add(!(a2_v0_0 && a2_v3_0));
    s.add(!(a2_v1_0 && a2_v2_0));
    s.add(!(a2_v1_0 && a2_v3_0));
    s.add(!(a2_v2_0 && a2_v3_0));
    s.add(!(a3_v0_0 && a3_v1_0));
    s.add(!(a3_v0_0 && a3_v2_0));
    s.add(!(a3_v0_0 && a3_v3_0));
    s.add(!(a3_v1_0 && a3_v2_0));
    s.add(!(a3_v1_0 && a3_v3_0));
    s.add(!(a3_v2_0 && a3_v3_0));
    s.add(implies(ass0, a3_v3_0));
    s.add(implies(ass0, a3_wtime_0 == totaltime));
    s.add(implies(ass0, a3_mtime_0 == 0));
    s.add(implies(ass0, a2_v2_0));
    s.add(implies(ass0, a2_wtime_0 == totaltime));
    s.add(implies(ass0, a2_mtime_0 == 0));
    s.add(implies(ass0, a1_v0_0));
    s.add(implies(ass0, a1_wtime_0 == totaltime));
    s.add(implies(ass0, a1_mtime_0 == 0));
    s.add(implies(ass0, a0_v1_0));
    s.add(implies(ass0, a0_wtime_0 == totaltime));
    s.add(implies(ass0, a0_mtime_0 == 0));

    {
        expr total_t_e = ctx.real_val(0);
        for (const auto& e : {a3_atime_0, a2_atime_0, a1_atime_0, a0_atime_0}) {
            total_t_e = ::z3::max(total_t_e, e);
        }
        s.add(implies(ass0, totaltime == total_t_e));
    }

    s.push();
    s.add(ass0);
    check_f(::z3::unsat);
    s.pop();

    expr a0_atime_1 = ctx.real_const("A0.atime<1>");
    expr a0_mtime_1 = ctx.real_const("A0.mtime<1>");
    expr a0_wtime_1 = ctx.real_const("A0.wtime<1>");
    expr a0_rtime_1 = ctx.real_const("A0.rtime<1>");
    expr a1_atime_1 = ctx.real_const("A1.atime<1>");
    expr a1_mtime_1 = ctx.real_const("A1.mtime<1>");
    expr a1_wtime_1 = ctx.real_const("A1.wtime<1>");
    expr a1_rtime_1 = ctx.real_const("A1.rtime<1>");
    expr a2_atime_1 = ctx.real_const("A2.atime<1>");
    expr a2_mtime_1 = ctx.real_const("A2.mtime<1>");
    expr a2_wtime_1 = ctx.real_const("A2.wtime<1>");
    expr a2_rtime_1 = ctx.real_const("A2.rtime<1>");
    expr a3_atime_1 = ctx.real_const("A3.atime<1>");
    expr a3_mtime_1 = ctx.real_const("A3.mtime<1>");
    expr a3_wtime_1 = ctx.real_const("A3.wtime<1>");
    expr a3_rtime_1 = ctx.real_const("A3.rtime<1>");
    expr a0_v1_1 = ctx.bool_const("A0.V1<1>");
    expr a0_v0_1 = ctx.bool_const("A0.V0<1>");
    expr a0_v2_1 = ctx.bool_const("A0.V2<1>");
    expr a0_v3_1 = ctx.bool_const("A0.V3<1>");
    expr a1_v1_1 = ctx.bool_const("A1.V1<1>");
    expr a1_v0_1 = ctx.bool_const("A1.V0<1>");
    expr a1_v2_1 = ctx.bool_const("A1.V2<1>");
    expr a1_v3_1 = ctx.bool_const("A1.V3<1>");
    expr a2_v1_1 = ctx.bool_const("A2.V1<1>");
    expr a2_v0_1 = ctx.bool_const("A2.V0<1>");
    expr a2_v2_1 = ctx.bool_const("A2.V2<1>");
    expr a2_v3_1 = ctx.bool_const("A2.V3<1>");
    expr a3_v1_1 = ctx.bool_const("A3.V1<1>");
    expr a3_v0_1 = ctx.bool_const("A3.V0<1>");
    expr a3_v2_1 = ctx.bool_const("A3.V2<1>");
    expr a3_v3_1 = ctx.bool_const("A3.V3<1>");
    expr ass1 = ctx.bool_const("ass1");

    s.add(a0_atime_1 == a0_atime_0 + a0_rtime_0);
    s.add(a0_rtime_1 == a0_wtime_1 + a0_mtime_1);
    s.add(a0_wtime_1 >= 0);
    s.add(a0_mtime_1 >= 0);
    s.add(a1_atime_1 == a1_atime_0 + a1_rtime_0);
    s.add(a1_rtime_1 == a1_wtime_1 + a1_mtime_1);
    s.add(a1_wtime_1 >= 0);
    s.add(a1_mtime_1 >= 0);
    s.add(a2_atime_1 == a2_atime_0 + a2_rtime_0);
    s.add(a2_rtime_1 == a2_wtime_1 + a2_mtime_1);
    s.add(a2_wtime_1 >= 0);
    s.add(a2_mtime_1 >= 0);
    s.add(a3_atime_1 == a3_atime_0 + a3_rtime_0);
    s.add(a3_rtime_1 == a3_wtime_1 + a3_mtime_1);
    s.add(a3_wtime_1 >= 0);
    s.add(a3_mtime_1 >= 0);
    s.add(!(a0_v0_1 && a0_v1_1));
    s.add(!(a0_v0_1 && a0_v2_1));
    s.add(!(a0_v0_1 && a0_v3_1));
    s.add(!(a0_v1_1 && a0_v2_1));
    s.add(!(a0_v1_1 && a0_v3_1));
    s.add(!(a0_v2_1 && a0_v3_1));
    s.add(!(a1_v0_1 && a1_v1_1));
    s.add(!(a1_v0_1 && a1_v2_1));
    s.add(!(a1_v0_1 && a1_v3_1));
    s.add(!(a1_v1_1 && a1_v2_1));
    s.add(!(a1_v1_1 && a1_v3_1));
    s.add(!(a1_v2_1 && a1_v3_1));
    s.add(!(a2_v0_1 && a2_v1_1));
    s.add(!(a2_v0_1 && a2_v2_1));
    s.add(!(a2_v0_1 && a2_v3_1));
    s.add(!(a2_v1_1 && a2_v2_1));
    s.add(!(a2_v1_1 && a2_v3_1));
    s.add(!(a2_v2_1 && a2_v3_1));
    s.add(!(a3_v0_1 && a3_v1_1));
    s.add(!(a3_v0_1 && a3_v2_1));
    s.add(!(a3_v0_1 && a3_v3_1));
    s.add(!(a3_v1_1 && a3_v2_1));
    s.add(!(a3_v1_1 && a3_v3_1));
    s.add(!(a3_v2_1 && a3_v3_1));

    constexpr long sqrt_n = 5931641;
    constexpr long sqrt_d = 4194304;
    expr sqrt_e = ctx.real_val(sqrt_n, sqrt_d);

    s.add(implies(a0_v0_0 && a0_v1_1, a0_mtime_0 == 1));
    s.add(implies(a0_v0_0 && a0_v3_1, a0_mtime_0 == sqrt_e));
    s.add(implies(a0_v0_0 && a0_v2_1, a0_mtime_0 == 1));
    s.add(implies(a0_v0_0, a0_v1_1 || a0_v3_1 || a0_v2_1));
    s.add(implies(a0_v1_0 && a0_v3_1, a0_mtime_0 == 1));
    s.add(implies(a0_v1_0 && a0_v2_1, a0_mtime_0 == sqrt_e));
    s.add(implies(a0_v1_0 && a0_v0_1, a0_mtime_0 == 1));
    s.add(implies(a0_v1_0 && a0_v1_1, a0_wtime_0 == 0 && a0_mtime_0 == 0));
    s.add(implies(a0_v1_0, a0_v3_1 || a0_v2_1 || a0_v0_1 || a0_v1_1));
    s.add(implies(a0_v2_0 && a0_v3_1, a0_mtime_0 == 1));
    s.add(implies(a0_v2_0 && a0_v1_1, a0_mtime_0 == sqrt_e));
    s.add(implies(a0_v2_0 && a0_v0_1, a0_mtime_0 == 1));
    s.add(implies(a0_v2_0, a0_v3_1 || a0_v1_1 || a0_v0_1));
    s.add(implies(a0_v3_0 && a0_v2_1, a0_mtime_0 == 1));
    s.add(implies(a0_v3_0 && a0_v1_1, a0_mtime_0 == 1));
    s.add(implies(a0_v3_0 && a0_v0_1, a0_mtime_0 == sqrt_e));
    s.add(implies(a0_v3_0, a0_v2_1 || a0_v1_1 || a0_v0_1));
    s.add(implies(a1_v0_0 && a1_v1_1, a1_mtime_0 == 1));
    s.add(implies(a1_v0_0 && a1_v3_1, a1_mtime_0 == sqrt_e));
    s.add(implies(a1_v0_0 && a1_v2_1, a1_mtime_0 == 1));
    s.add(implies(a1_v0_0 && a1_v0_1, a1_wtime_0 == 0 && a1_mtime_0 == 0));
    s.add(implies(a1_v0_0, a1_v1_1 || a1_v3_1 || a1_v2_1 || a1_v0_1));
    s.add(implies(a1_v1_0 && a1_v3_1, a1_mtime_0 == 1));
    s.add(implies(a1_v1_0 && a1_v2_1, a1_mtime_0 == sqrt_e));
    s.add(implies(a1_v1_0 && a1_v0_1, a1_mtime_0 == 1));
    s.add(implies(a1_v1_0, a1_v3_1 || a1_v2_1 || a1_v0_1));
    s.add(implies(a1_v2_0 && a1_v3_1, a1_mtime_0 == 1));
    s.add(implies(a1_v2_0 && a1_v1_1, a1_mtime_0 == sqrt_e));
    s.add(implies(a1_v2_0 && a1_v0_1, a1_mtime_0 == 1));
    s.add(implies(a1_v2_0, a1_v3_1 || a1_v1_1 || a1_v0_1));
    s.add(implies(a1_v3_0 && a1_v2_1, a1_mtime_0 == 1));
    s.add(implies(a1_v3_0 && a1_v1_1, a1_mtime_0 == 1));
    s.add(implies(a1_v3_0 && a1_v0_1, a1_mtime_0 == sqrt_e));
    s.add(implies(a1_v3_0, a1_v2_1 || a1_v1_1 || a1_v0_1));
    s.add(implies(a2_v0_0 && a2_v1_1, a2_mtime_0 == 1));
    s.add(implies(a2_v0_0 && a2_v3_1, a2_mtime_0 == sqrt_e));
    s.add(implies(a2_v0_0 && a2_v2_1, a2_mtime_0 == 1));
    s.add(implies(a2_v0_0, a2_v1_1 || a2_v3_1 || a2_v2_1));
    s.add(implies(a2_v1_0 && a2_v3_1, a2_mtime_0 == 1));
    s.add(implies(a2_v1_0 && a2_v2_1, a2_mtime_0 == sqrt_e));
    s.add(implies(a2_v1_0 && a2_v0_1, a2_mtime_0 == 1));
    s.add(implies(a2_v1_0, a2_v3_1 || a2_v2_1 || a2_v0_1));
    s.add(implies(a2_v2_0 && a2_v3_1, a2_mtime_0 == 1));
    s.add(implies(a2_v2_0 && a2_v1_1, a2_mtime_0 == sqrt_e));
    s.add(implies(a2_v2_0 && a2_v0_1, a2_mtime_0 == 1));
    s.add(implies(a2_v2_0 && a2_v2_1, a2_wtime_0 == 0 && a2_mtime_0 == 0));
    s.add(implies(a2_v2_0, a2_v3_1 || a2_v1_1 || a2_v0_1 || a2_v2_1));
    s.add(implies(a2_v3_0 && a2_v2_1, a2_mtime_0 == 1));
    s.add(implies(a2_v3_0 && a2_v1_1, a2_mtime_0 == 1));
    s.add(implies(a2_v3_0 && a2_v0_1, a2_mtime_0 == sqrt_e));
    s.add(implies(a2_v3_0, a2_v2_1 || a2_v1_1 || a2_v0_1));
    s.add(implies(a3_v0_0 && a3_v1_1, a3_mtime_0 == 1));
    s.add(implies(a3_v0_0 && a3_v3_1, a3_mtime_0 == sqrt_e));
    s.add(implies(a3_v0_0 && a3_v2_1, a3_mtime_0 == 1));
    s.add(implies(a3_v0_0, a3_v1_1 || a3_v3_1 || a3_v2_1));
    s.add(implies(a3_v1_0 && a3_v3_1, a3_mtime_0 == 1));
    s.add(implies(a3_v1_0 && a3_v2_1, a3_mtime_0 == sqrt_e));
    s.add(implies(a3_v1_0 && a3_v0_1, a3_mtime_0 == 1));
    s.add(implies(a3_v1_0, a3_v3_1 || a3_v2_1 || a3_v0_1));
    s.add(implies(a3_v2_0 && a3_v3_1, a3_mtime_0 == 1));
    s.add(implies(a3_v2_0 && a3_v1_1, a3_mtime_0 == sqrt_e));
    s.add(implies(a3_v2_0 && a3_v0_1, a3_mtime_0 == 1));
    s.add(implies(a3_v2_0, a3_v3_1 || a3_v1_1 || a3_v0_1));
    s.add(implies(a3_v3_0 && a3_v2_1, a3_mtime_0 == 1));
    s.add(implies(a3_v3_0 && a3_v1_1, a3_mtime_0 == 1));
    s.add(implies(a3_v3_0 && a3_v0_1, a3_mtime_0 == sqrt_e));
    s.add(implies(a3_v3_0 && a3_v3_1, a3_wtime_0 == 0 && a3_mtime_0 == 0));
    s.add(implies(a3_v3_0, a3_v2_1 || a3_v1_1 || a3_v0_1 || a3_v3_1));
    s.add(implies(ass1, a3_v3_1));
    s.add(implies(ass1, a3_wtime_1 == totaltime));
    s.add(implies(ass1, a3_mtime_1 == 0));
    s.add(implies(ass1, a2_v2_1));
    s.add(implies(ass1, a2_wtime_1 == totaltime));
    s.add(implies(ass1, a2_mtime_1 == 0));
    s.add(implies(ass1, a1_v0_1));
    s.add(implies(ass1, a1_wtime_1 == totaltime));
    s.add(implies(ass1, a1_mtime_1 == 0));
    s.add(implies(ass1, a0_v1_1));
    s.add(implies(ass1, a0_wtime_1 == totaltime));
    s.add(implies(ass1, a0_mtime_1 == 0));

    {
        expr total_t_e = ctx.real_val(0);
        for (const auto& e : {a3_atime_1, a2_atime_1, a1_atime_1, a0_atime_1}) {
            total_t_e = ::z3::max(total_t_e, e);
        }
        s.add(implies(ass1, totaltime == total_t_e));
    }

    s.push();
    s.add(ass1);
    check_f(::z3::sat);

    expr c1_1_1 = a0_v0_0 && a0_v1_1 && a1_v1_0 && a1_v0_1
             && (a0_atime_0 + a0_wtime_0) - (a1_atime_0 + a1_wtime_0) < 1
             && (a1_atime_0 + a1_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
             && true && true;
    add_conflict_f(c1_1_1);
    expr c1_1_2 = a0_v0_0 && a0_v1_1 && a2_v1_0 && a2_v0_1
             && (a0_atime_0 + a0_wtime_0) - (a2_atime_0 + a2_wtime_0) < 1
             && (a2_atime_0 + a2_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
             && true && true;
    add_conflict_f(c1_1_2);
    expr c1_1_3 = a0_v0_0 && a0_v1_1 && a3_v1_0 && a3_v0_1
             && (a0_atime_0 + a0_wtime_0) - (a3_atime_0 + a3_wtime_0) < 1
             && (a3_atime_0 + a3_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
             && true && true;
    add_conflict_f(c1_1_3);
    check_f(::z3::sat);

    expr c1_2_1 = a0_v0_0 && true && a1_v1_0 && a1_v0_1
             && a0_atime_0 - (a1_atime_0 + a1_wtime_0) < 1
             && true
             && (a0_atime_0 + a0_wtime_0) - (a1_atime_0 + a1_wtime_0) > 0
             && true;
    add_conflict_f(c1_2_1);
    expr c1_2_2 = a0_v0_0 && true && a2_v1_0 && a2_v0_1
             && a0_atime_0 - (a2_atime_0 + a2_wtime_0) < 1
             && true
             && (a0_atime_0 + a0_wtime_0) - (a2_atime_0 + a2_wtime_0) > 0
             && true;
    add_conflict_f(c1_2_2);
    expr c1_2_3 = a0_v0_0 && true && a3_v1_0 && a3_v0_1
             && a0_atime_0 - (a3_atime_0 + a3_wtime_0) < 1
             && true
             && (a0_atime_0 + a0_wtime_0) - (a3_atime_0 + a3_wtime_0) > 0
             && true;
    add_conflict_f(c1_2_3);
    check_f(::z3::sat);

    expr c1_3_1 = a0_v0_0 && a0_v1_1 && a1_v1_0 && true
             && true
             && a1_atime_0 - (a0_atime_0 + a0_wtime_0) < 1
             && true
             && (a1_atime_0 + a1_wtime_0) - (a0_atime_0 + a0_wtime_0) > 0;
    add_conflict_f(c1_3_1);
    expr c1_3_2 = a0_v0_0 && a0_v1_1 && a2_v1_0 && true
             && true
             && a2_atime_0 - (a0_atime_0 + a0_wtime_0) < 1
             && true
             && (a2_atime_0 + a2_wtime_0) - (a0_atime_0 + a0_wtime_0) > 0;
    add_conflict_f(c1_3_2);
    expr c1_3_3 = a0_v0_0 && a0_v1_1 && a3_v1_0 && true
             && true
             && a3_atime_0 - (a0_atime_0 + a0_wtime_0) < 1
             && true
             && (a3_atime_0 + a3_wtime_0) - (a0_atime_0 + a0_wtime_0) > 0;
    add_conflict_f(c1_3_3);
    check_f(::z3::unsat);

    s.pop();
    s.add(conflicts);
    conflicts.resize(0);

    expr a0_atime_2 = ctx.real_const("A0.atime<2>");
    expr a0_mtime_2 = ctx.real_const("A0.mtime<2>");
    expr a0_wtime_2 = ctx.real_const("A0.wtime<2>");
    expr a0_rtime_2 = ctx.real_const("A0.rtime<2>");
    expr a1_atime_2 = ctx.real_const("A1.atime<2>");
    expr a1_mtime_2 = ctx.real_const("A1.mtime<2>");
    expr a1_wtime_2 = ctx.real_const("A1.wtime<2>");
    expr a1_rtime_2 = ctx.real_const("A1.rtime<2>");
    expr a2_atime_2 = ctx.real_const("A2.atime<2>");
    expr a2_mtime_2 = ctx.real_const("A2.mtime<2>");
    expr a2_wtime_2 = ctx.real_const("A2.wtime<2>");
    expr a2_rtime_2 = ctx.real_const("A2.rtime<2>");
    expr a3_atime_2 = ctx.real_const("A3.atime<2>");
    expr a3_mtime_2 = ctx.real_const("A3.mtime<2>");
    expr a3_wtime_2 = ctx.real_const("A3.wtime<2>");
    expr a3_rtime_2 = ctx.real_const("A3.rtime<2>");
    expr a0_v1_2 = ctx.bool_const("A0.V1<2>");
    expr a0_v0_2 = ctx.bool_const("A0.V0<2>");
    expr a0_v2_2 = ctx.bool_const("A0.V2<2>");
    expr a0_v3_2 = ctx.bool_const("A0.V3<2>");
    expr a1_v1_2 = ctx.bool_const("A1.V1<2>");
    expr a1_v0_2 = ctx.bool_const("A1.V0<2>");
    expr a1_v2_2 = ctx.bool_const("A1.V2<2>");
    expr a1_v3_2 = ctx.bool_const("A1.V3<2>");
    expr a2_v1_2 = ctx.bool_const("A2.V1<2>");
    expr a2_v0_2 = ctx.bool_const("A2.V0<2>");
    expr a2_v2_2 = ctx.bool_const("A2.V2<2>");
    expr a2_v3_2 = ctx.bool_const("A2.V3<2>");
    expr a3_v1_2 = ctx.bool_const("A3.V1<2>");
    expr a3_v0_2 = ctx.bool_const("A3.V0<2>");
    expr a3_v2_2 = ctx.bool_const("A3.V2<2>");
    expr a3_v3_2 = ctx.bool_const("A3.V3<2>");
    expr ass2 = ctx.bool_const("ass2");

    s.add(a0_atime_2 == a0_atime_1 + a0_rtime_1);
    s.add(a0_rtime_2 == a0_wtime_2 + a0_mtime_2);
    s.add(a0_wtime_2 >= 0);
    s.add(a0_mtime_2 >= 0);
    s.add(a1_atime_2 == a1_atime_1 + a1_rtime_1);
    s.add(a1_rtime_2 == a1_wtime_2 + a1_mtime_2);
    s.add(a1_wtime_2 >= 0);
    s.add(a1_mtime_2 >= 0);
    s.add(a2_atime_2 == a2_atime_1 + a2_rtime_1);
    s.add(a2_rtime_2 == a2_wtime_2 + a2_mtime_2);
    s.add(a2_wtime_2 >= 0);
    s.add(a2_mtime_2 >= 0);
    s.add(a3_atime_2 == a3_atime_1 + a3_rtime_1);
    s.add(a3_rtime_2 == a3_wtime_2 + a3_mtime_2);
    s.add(a3_wtime_2 >= 0);
    s.add(a3_mtime_2 >= 0);
    s.add(!(a0_v0_2 && a0_v1_2));
    s.add(!(a0_v0_2 && a0_v2_2));
    s.add(!(a0_v0_2 && a0_v3_2));
    s.add(!(a0_v1_2 && a0_v2_2));
    s.add(!(a0_v1_2 && a0_v3_2));
    s.add(!(a0_v2_2 && a0_v3_2));
    s.add(!(a1_v0_2 && a1_v1_2));
    s.add(!(a1_v0_2 && a1_v2_2));
    s.add(!(a1_v0_2 && a1_v3_2));
    s.add(!(a1_v1_2 && a1_v2_2));
    s.add(!(a1_v1_2 && a1_v3_2));
    s.add(!(a1_v2_2 && a1_v3_2));
    s.add(!(a2_v0_2 && a2_v1_2));
    s.add(!(a2_v0_2 && a2_v2_2));
    s.add(!(a2_v0_2 && a2_v3_2));
    s.add(!(a2_v1_2 && a2_v2_2));
    s.add(!(a2_v1_2 && a2_v3_2));
    s.add(!(a2_v2_2 && a2_v3_2));
    s.add(!(a3_v0_2 && a3_v1_2));
    s.add(!(a3_v0_2 && a3_v2_2));
    s.add(!(a3_v0_2 && a3_v3_2));
    s.add(!(a3_v1_2 && a3_v2_2));
    s.add(!(a3_v1_2 && a3_v3_2));
    s.add(!(a3_v2_2 && a3_v3_2));

    s.add(implies(a0_v0_1 && a0_v1_2, a0_mtime_1 == 1));
    s.add(implies(a0_v0_1 && a0_v3_2, a0_mtime_1 == sqrt_e));
    s.add(implies(a0_v0_1 && a0_v2_2, a0_mtime_1 == 1));
    s.add(implies(a0_v0_1, a0_v1_2 || a0_v3_2 || a0_v2_2));
    s.add(implies(a0_v1_1 && a0_v3_2, a0_mtime_1 == 1));
    s.add(implies(a0_v1_1 && a0_v2_2, a0_mtime_1 == sqrt_e));
    s.add(implies(a0_v1_1 && a0_v0_2, a0_mtime_1 == 1));
    s.add(implies(a0_v1_1 && a0_v1_2, a0_wtime_1 == 0 && a0_mtime_1 == 0));
    s.add(implies(a0_v1_0 && a0_v1_1, a0_v1_2));
    s.add(implies(a0_v1_1, a0_v3_2 || a0_v2_2 || a0_v0_2 || a0_v1_2));
    s.add(implies(a0_v2_1 && a0_v3_2, a0_mtime_1 == 1));
    s.add(implies(a0_v2_1 && a0_v1_2, a0_mtime_1 == sqrt_e));
    s.add(implies(a0_v2_1 && a0_v0_2, a0_mtime_1 == 1));
    s.add(implies(a0_v2_1, a0_v3_2 || a0_v1_2 || a0_v0_2));
    s.add(implies(a0_v3_1 && a0_v2_2, a0_mtime_1 == 1));
    s.add(implies(a0_v3_1 && a0_v1_2, a0_mtime_1 == 1));
    s.add(implies(a0_v3_1 && a0_v0_2, a0_mtime_1 == sqrt_e));
    s.add(implies(a0_v3_1, a0_v2_2 || a0_v1_2 || a0_v0_2));
    s.add(implies(a1_v0_1 && a1_v1_2, a1_mtime_1 == 1));
    s.add(implies(a1_v0_1 && a1_v3_2, a1_mtime_1 == sqrt_e));
    s.add(implies(a1_v0_1 && a1_v2_2, a1_mtime_1 == 1));
    s.add(implies(a1_v0_1 && a1_v0_2, a1_wtime_1 == 0 && a1_mtime_1 == 0));
    s.add(implies(a1_v0_0 && a1_v0_1, a1_v0_2));
    s.add(implies(a1_v0_1, a1_v1_2 || a1_v3_2 || a1_v2_2 || a1_v0_2));
    s.add(implies(a1_v1_1 && a1_v3_2, a1_mtime_1 == 1));
    s.add(implies(a1_v1_1 && a1_v2_2, a1_mtime_1 == sqrt_e));
    s.add(implies(a1_v1_1 && a1_v0_2, a1_mtime_1 == 1));
    s.add(implies(a1_v1_1, a1_v3_2 || a1_v2_2 || a1_v0_2));
    s.add(implies(a1_v2_1 && a1_v3_2, a1_mtime_1 == 1));
    s.add(implies(a1_v2_1 && a1_v1_2, a1_mtime_1 == sqrt_e));
    s.add(implies(a1_v2_1 && a1_v0_2, a1_mtime_1 == 1));
    s.add(implies(a1_v2_1, a1_v3_2 || a1_v1_2 || a1_v0_2));
    s.add(implies(a1_v3_1 && a1_v2_2, a1_mtime_1 == 1));
    s.add(implies(a1_v3_1 && a1_v1_2, a1_mtime_1 == 1));
    s.add(implies(a1_v3_1 && a1_v0_2, a1_mtime_1 == sqrt_e));
    s.add(implies(a1_v3_1, a1_v2_2 || a1_v1_2 || a1_v0_2));
    s.add(implies(a2_v0_1 && a2_v1_2, a2_mtime_1 == 1));
    s.add(implies(a2_v0_1 && a2_v3_2, a2_mtime_1 == sqrt_e));
    s.add(implies(a2_v0_1 && a2_v2_2, a2_mtime_1 == 1));
    s.add(implies(a2_v0_1, a2_v1_2 || a2_v3_2 || a2_v2_2));
    s.add(implies(a2_v1_1 && a2_v3_2, a2_mtime_1 == 1));
    s.add(implies(a2_v1_1 && a2_v2_2, a2_mtime_1 == sqrt_e));
    s.add(implies(a2_v1_1 && a2_v0_2, a2_mtime_1 == 1));
    s.add(implies(a2_v1_1, a2_v3_2 || a2_v2_2 || a2_v0_2));
    s.add(implies(a2_v2_1 && a2_v3_2, a2_mtime_1 == 1));
    s.add(implies(a2_v2_1 && a2_v1_2, a2_mtime_1 == sqrt_e));
    s.add(implies(a2_v2_1 && a2_v0_2, a2_mtime_1 == 1));
    s.add(implies(a2_v2_1 && a2_v2_2, a2_wtime_1 == 0 && a2_mtime_1 == 0));
    s.add(implies(a2_v2_0 && a2_v2_1, a2_v2_2));
    s.add(implies(a2_v2_1, a2_v3_2 || a2_v1_2 || a2_v0_2 || a2_v2_2));
    s.add(implies(a2_v3_1 && a2_v2_2, a2_mtime_1 == 1));
    s.add(implies(a2_v3_1 && a2_v1_2, a2_mtime_1 == 1));
    s.add(implies(a2_v3_1 && a2_v0_2, a2_mtime_1 == sqrt_e));
    s.add(implies(a2_v3_1, a2_v2_2 || a2_v1_2 || a2_v0_2));
    s.add(implies(a3_v0_1 && a3_v1_2, a3_mtime_1 == 1));
    s.add(implies(a3_v0_1 && a3_v3_2, a3_mtime_1 == sqrt_e));
    s.add(implies(a3_v0_1 && a3_v2_2, a3_mtime_1 == 1));
    s.add(implies(a3_v0_1, a3_v1_2 || a3_v3_2 || a3_v2_2));
    s.add(implies(a3_v1_1 && a3_v3_2, a3_mtime_1 == 1));
    s.add(implies(a3_v1_1 && a3_v2_2, a3_mtime_1 == sqrt_e));
    s.add(implies(a3_v1_1 && a3_v0_2, a3_mtime_1 == 1));
    s.add(implies(a3_v1_1, a3_v3_2 || a3_v2_2 || a3_v0_2));
    s.add(implies(a3_v2_1 && a3_v3_2, a3_mtime_1 == 1));
    s.add(implies(a3_v2_1 && a3_v1_2, a3_mtime_1 == sqrt_e));
    s.add(implies(a3_v2_1 && a3_v0_2, a3_mtime_1 == 1));
    s.add(implies(a3_v2_1, a3_v3_2 || a3_v1_2 || a3_v0_2));
    s.add(implies(a3_v3_1 && a3_v2_2, a3_mtime_1 == 1));
    s.add(implies(a3_v3_1 && a3_v1_2, a3_mtime_1 == 1));
    s.add(implies(a3_v3_1 && a3_v0_2, a3_mtime_1 == sqrt_e));
    s.add(implies(a3_v3_1 && a3_v3_2, a3_wtime_1 == 0 && a3_mtime_1 == 0));
    s.add(implies(a3_v3_0 && a3_v3_1, a3_v3_2));
    s.add(implies(a3_v3_1, a3_v2_2 || a3_v1_2 || a3_v0_2 || a3_v3_2));
    s.add(implies(ass2, a3_v3_2));
    s.add(implies(ass2, a3_wtime_2 == totaltime));
    s.add(implies(ass2, a3_mtime_2 == 0));
    s.add(implies(ass2, a2_v2_2));
    s.add(implies(ass2, a2_wtime_2 == totaltime));
    s.add(implies(ass2, a2_mtime_2 == 0));
    s.add(implies(ass2, a1_v0_2));
    s.add(implies(ass2, a1_wtime_2 == totaltime));
    s.add(implies(ass2, a1_mtime_2 == 0));
    s.add(implies(ass2, a0_v1_2));
    s.add(implies(ass2, a0_wtime_2 == totaltime));
    s.add(implies(ass2, a0_mtime_2 == 0));

    {
        expr total_t_e = ctx.real_val(0);
        //!! with this it will work OK
        // for (const auto& e : {a0_atime_2, a1_atime_2, a2_atime_2, a3_atime_2}) {
        for (const auto& e : {a3_atime_2, a2_atime_2, a1_atime_2, a0_atime_2}) {
            total_t_e = ::z3::max(total_t_e, e);
        }
        s.add(implies(ass2, totaltime == total_t_e));
    }

    s.push();
    s.add(ass2);

    check_f(::z3::sat);

    // val2_e can be slightly incremented:
    // OK with 0 and 43, but fails with 1, 2, ..., 42
    //- constexpr long inc_val2 = 0;
    constexpr long inc_val2 = 1;
    //- constexpr long inc_val2 = 42;
    //- constexpr long inc_val2 = 43;
    expr val1_e = ctx.real_val(sqrt_n + sqrt_d, sqrt_d);
    expr val2_e = ctx.real_val(sqrt_n - sqrt_d + inc_val2, sqrt_d);
    expr val3_e = ctx.real_val(2*sqrt_d - sqrt_n, sqrt_d);

    //!! with any of these it will work OK
    // expr val1_e = sqrt_e + 1;
    // expr val2_e = sqrt_e - 1;
    // expr val3_e = 2 - sqrt_e;

    expr c2_1_1 = a0_v0_0 && a0_v2_1 && a1_v3_0 && a1_v2_1
               && (a0_atime_0 + a0_wtime_0) - (a1_atime_0 + a1_wtime_0) < 1
               && (a1_atime_0 + a1_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_1_1);
    expr c2_1_2 = a0_v0_0 && a0_v2_1 && a2_v3_0 && a2_v2_1
               && (a0_atime_0 + a0_wtime_0) - (a2_atime_0 + a2_wtime_0) < 1
               && (a2_atime_0 + a2_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_1_2);
    expr c2_1_3 = a0_v0_0 && a0_v2_1 && a3_v3_0 && a3_v2_1
               && (a0_atime_0 + a0_wtime_0) - (a3_atime_0 + a3_wtime_0) < 1
               && (a3_atime_0 + a3_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_1_3);
    check_f(::z3::sat);

    expr c2_2_1 = a0_v0_0 && a0_v1_1 && a1_v3_1 && a1_v0_2
               && (a0_atime_0 + a0_wtime_0) - (a1_atime_1 + a1_wtime_1) < sqrt_e
               && (a1_atime_1 + a1_wtime_1) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_2_1);
    expr c2_2_2 = a0_v0_0 && a0_v1_1 && a2_v3_1 && a2_v0_2
               && (a0_atime_0 + a0_wtime_0) - (a2_atime_1 + a2_wtime_1) < sqrt_e
               && (a2_atime_1 + a2_wtime_1) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_2_2);
    expr c2_2_3 = a0_v0_0 && a0_v1_1 && a3_v3_1 && a3_v0_2
               && (a0_atime_0 + a0_wtime_0) - (a3_atime_1 + a3_wtime_1) < sqrt_e
               && (a3_atime_1 + a3_wtime_1) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_2_3);
    check_f(::z3::sat);

    expr c2_3_1 = a0_v0_0 && a0_v3_1 && a1_v1_0 && a1_v0_1
               && (a0_atime_0 + a0_wtime_0) - (a1_atime_0 + a1_wtime_0) < 1
               && (a1_atime_0 + a1_wtime_0) - (a0_atime_0 + a0_wtime_0) < sqrt_e
               && true && true;
    add_conflict_f(c2_3_1);
    expr c2_3_2 = a0_v0_0 && a0_v3_1 && a2_v1_0 && a2_v0_1
               && (a0_atime_0 + a0_wtime_0) - (a2_atime_0 + a2_wtime_0) < 1
               && (a2_atime_0 + a2_wtime_0) - (a0_atime_0 + a0_wtime_0) < sqrt_e
               && true && true;
    add_conflict_f(c2_3_2);
    expr c2_3_3 = a0_v0_0 && a0_v3_1 && a3_v1_0 && a3_v0_1
               && (a0_atime_0 + a0_wtime_0) - (a3_atime_0 + a3_wtime_0) < 1
               && (a3_atime_0 + a3_wtime_0) - (a0_atime_0 + a0_wtime_0) < sqrt_e
               && true && true;
    add_conflict_f(c2_3_3);
    check_f(::z3::sat);

    expr c2_4_1 = a0_v0_0 && a0_v2_1 && a1_v2_0 && a1_v3_1
               && (a0_atime_0 + a0_wtime_0) - (a1_atime_0 + a1_wtime_0) < val2_e
               && (a1_atime_0 + a1_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_4_1);
    expr c2_4_2 = a0_v0_0 && a0_v2_1 && a2_v2_0 && a2_v3_1
               && (a0_atime_0 + a0_wtime_0) - (a2_atime_0 + a2_wtime_0) < val2_e
               && (a2_atime_0 + a2_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_4_2);
    expr c2_4_3 = a0_v0_0 && a0_v2_1 && a3_v2_0 && a3_v3_1
               && (a0_atime_0 + a0_wtime_0) - (a3_atime_0 + a3_wtime_0) < val2_e
               && (a3_atime_0 + a3_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_4_3);
    check_f(::z3::sat);

    /*
    expr c2_5_1 = a0_v0_0 && a0_v2_1 && a1_v1_0 && a1_v0_1
               && (a0_atime_0 + a0_wtime_0) - (a1_atime_0 + a1_wtime_0) < 1
               && (a1_atime_0 + a1_wtime_0) - (a0_atime_0 + a0_wtime_0) < val2_e
               && true && true;
    add_conflict_f(c2_5_1);
    expr c2_5_2 = a0_v0_0 && a0_v2_1 && a2_v1_0 && a2_v0_1
               && (a0_atime_0 + a0_wtime_0) - (a2_atime_0 + a2_wtime_0) < 1
               && (a2_atime_0 + a2_wtime_0) - (a0_atime_0 + a0_wtime_0) < val2_e
               && true && true;
    add_conflict_f(c2_5_2);
    expr c2_5_3 = a0_v0_0 && a0_v2_1 && a3_v1_0 && a3_v0_1
               && (a0_atime_0 + a0_wtime_0) - (a3_atime_0 + a3_wtime_0) < 1
               && (a3_atime_0 + a3_wtime_0) - (a0_atime_0 + a0_wtime_0) < val2_e
               && true && true;
    add_conflict_f(c2_5_3);
    check_f(::z3::sat);
    */

    expr c2_6_1 = a0_v0_0 && a0_v3_1 && a1_v1_0 && a1_v3_1
               && (a0_atime_0 + a0_wtime_0) - (a1_atime_0 + a1_wtime_0) < val3_e
               && (a1_atime_0 + a1_wtime_0) - (a0_atime_0 + a0_wtime_0) < sqrt_e
               && true && true;
    add_conflict_f(c2_6_1);
    expr c2_6_2 = a0_v0_0 && a0_v3_1 && a2_v1_0 && a2_v3_1
               && (a0_atime_0 + a0_wtime_0) - (a2_atime_0 + a2_wtime_0) < val3_e
               && (a2_atime_0 + a2_wtime_0) - (a0_atime_0 + a0_wtime_0) < sqrt_e
               && true && true;
    add_conflict_f(c2_6_2);
    expr c2_6_3 = a0_v0_0 && a0_v3_1 && a3_v1_0 && a3_v3_1
               && (a0_atime_0 + a0_wtime_0) - (a3_atime_0 + a3_wtime_0) < val3_e
               && (a3_atime_0 + a3_wtime_0) - (a0_atime_0 + a0_wtime_0) < sqrt_e
               && true && true;
    add_conflict_f(c2_6_3);
    check_f(::z3::sat);

    /*
    expr c2_7_1 = a0_v0_0 && a0_v3_1 && a1_v1_0 && a1_v2_1
               && (a0_atime_0 + a0_wtime_0) - (a1_atime_0 + a1_wtime_0) < sqrt_e
               && (a1_atime_0 + a1_wtime_0) - (a0_atime_0 + a0_wtime_0) < sqrt_e
               && true && true;
    add_conflict_f(c2_7_1);
    expr c2_7_2 = a0_v0_0 && a0_v3_1 && a2_v1_0 && a2_v2_1
               && (a0_atime_0 + a0_wtime_0) - (a2_atime_0 + a2_wtime_0) < sqrt_e
               && (a2_atime_0 + a2_wtime_0) - (a0_atime_0 + a0_wtime_0) < sqrt_e
               && true && true;
    add_conflict_f(c2_7_2);
    expr c2_7_3 = a0_v0_0 && a0_v3_1 && a3_v1_0 && a3_v2_1
               && (a0_atime_0 + a0_wtime_0) - (a3_atime_0 + a3_wtime_0) < sqrt_e
               && (a3_atime_0 + a3_wtime_0) - (a0_atime_0 + a0_wtime_0) < sqrt_e
               && true && true;
    add_conflict_f(c2_7_3);
    check_f(::z3::sat);
    */

    expr c2_8_1 = a0_v0_0 && a0_v2_1 && a1_v3_0 && a1_v0_1
               && (a0_atime_0 + a0_wtime_0) - (a1_atime_0 + a1_wtime_0) < sqrt_e
               && (a1_atime_0 + a1_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_8_1);
    expr c2_8_2 = a0_v0_0 && a0_v2_1 && a2_v3_0 && a2_v0_1
               && (a0_atime_0 + a0_wtime_0) - (a2_atime_0 + a2_wtime_0) < sqrt_e
               && (a2_atime_0 + a2_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_8_2);
    expr c2_8_3 = a0_v0_0 && a0_v2_1 && a3_v3_0 && a3_v0_1
               && (a0_atime_0 + a0_wtime_0) - (a3_atime_0 + a3_wtime_0) < sqrt_e
               && (a3_atime_0 + a3_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_8_3);
    check_f(::z3::sat);

    expr c2_9_1 = a0_v0_0 && true && a1_v1_0 && a1_v2_1
               && (a0_atime_0) - (a1_atime_0 + a1_wtime_0) < sqrt_e
               && true
               && (a0_atime_0 + a0_wtime_0) - (a1_atime_0 + a1_wtime_0) > 0
               && true;
    add_conflict_f(c2_9_1);
    expr c2_9_2 = a0_v0_0 && true && a2_v1_0 && a2_v2_1
               && (a0_atime_0) - (a2_atime_0 + a2_wtime_0) < sqrt_e
               && true
               && (a0_atime_0 + a0_wtime_0) - (a2_atime_0 + a2_wtime_0) > 0
               && true;
    add_conflict_f(c2_9_2);
    expr c2_9_3 = a0_v0_0 && true && a3_v1_0 && a3_v2_1
               && (a0_atime_0) - (a3_atime_0 + a3_wtime_0) < sqrt_e
               && true
               && (a0_atime_0 + a0_wtime_0) - (a3_atime_0 + a3_wtime_0) > 0
               && true;
    add_conflict_f(c2_9_3);
    check_f(::z3::sat);

    //!! commenting out any of the following marked tails "fixes" the bug
    expr c2_10_1 = a0_v0_0 && a0_v2_1 && a1_v1_0 && a1_v2_1
               && (a0_atime_0 + a0_wtime_0) - (a1_atime_0 + a1_wtime_0) < sqrt_e
               && (a1_atime_0 + a1_wtime_0) - (a0_atime_0 + a0_wtime_0) < val3_e
               //! this ..
               && true && true
               ;
    add_conflict_f(c2_10_1);
    expr c2_10_2 = a0_v0_0 && a0_v2_1 && a2_v1_0 && a2_v2_1
               && (a0_atime_0 + a0_wtime_0) - (a2_atime_0 + a2_wtime_0) < sqrt_e
               && (a2_atime_0 + a2_wtime_0) - (a0_atime_0 + a0_wtime_0) < val3_e
               //! .. or this ..
               && true && true
               ;
    add_conflict_f(c2_10_2);
    expr c2_10_3 = a0_v0_0 && a0_v2_1 && a3_v1_0 && a3_v2_1
               && (a0_atime_0 + a0_wtime_0) - (a3_atime_0 + a3_wtime_0) < sqrt_e
               && (a3_atime_0 + a3_wtime_0) - (a0_atime_0 + a0_wtime_0) < val3_e
               //! .. or this ..
               && true && true
               ;
    add_conflict_f(c2_10_3);
    check_f(::z3::sat);

    expr c2_11_1 = a0_v0_0 && a0_v2_1 && a1_v2_0 && true
               && true
               && (a1_atime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true
               && (a1_atime_0 + a1_wtime_0) - (a0_atime_0 + a0_wtime_0) > 0;
    add_conflict_f(c2_11_1);
    expr c2_11_2 = a0_v0_0 && a0_v2_1 && a2_v2_0 && true
               && true
               && (a2_atime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true
               && (a2_atime_0 + a2_wtime_0) - (a0_atime_0 + a0_wtime_0) > 0;
    add_conflict_f(c2_11_2);
    expr c2_11_3 = a0_v0_0 && a0_v2_1 && a3_v2_0 && true
               && true
               && (a3_atime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true
               && (a3_atime_0 + a3_wtime_0) - (a0_atime_0 + a0_wtime_0) > 0;
    add_conflict_f(c2_11_3);
    check_f(::z3::sat);

    expr c2_12_1 = a0_v0_0 && a0_v3_1 && a1_v1_0 && true
               && true
               && (a1_atime_0) - (a0_atime_0 + a0_wtime_0) < sqrt_e
               && true
               && (a1_atime_0 + a1_wtime_0) - (a0_atime_0 + a0_wtime_0) > 0;
    add_conflict_f(c2_12_1);
    expr c2_12_2 = a0_v0_0 && a0_v3_1 && a2_v1_0 && true
               && true
               && (a2_atime_0) - (a0_atime_0 + a0_wtime_0) < sqrt_e
               && true
               && (a2_atime_0 + a2_wtime_0) - (a0_atime_0 + a0_wtime_0) > 0;
    add_conflict_f(c2_12_2);
    expr c2_12_3 = a0_v0_0 && a0_v3_1 && a3_v1_0 && true
               && true
               && (a3_atime_0) - (a0_atime_0 + a0_wtime_0) < sqrt_e
               && true
               && (a3_atime_0 + a3_wtime_0) - (a0_atime_0 + a0_wtime_0) > 0;
    add_conflict_f(c2_12_3);
    check_f(::z3::sat);

    expr c2_13_1 = a0_v0_0 && a0_v1_1 && a1_v1_0 && a1_v2_1
               && (a0_atime_0 + a0_wtime_0) - (a1_atime_0 + a1_wtime_0) < sqrt_e
               && (a1_atime_0 + a1_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_13_1);
    expr c2_13_2 = a0_v0_0 && a0_v1_1 && a2_v1_0 && a2_v2_1
               && (a0_atime_0 + a0_wtime_0) - (a2_atime_0 + a2_wtime_0) < sqrt_e
               && (a2_atime_0 + a2_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_13_2);
    expr c2_13_3 = a0_v0_0 && a0_v1_1 && a3_v1_0 && a3_v2_1
               && (a0_atime_0 + a0_wtime_0) - (a3_atime_0 + a3_wtime_0) < sqrt_e
               && (a3_atime_0 + a3_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_13_3);
    check_f(::z3::sat);

    /*
    expr c2_14_1 = a0_v0_0 && a0_v1_1 && a1_v1_0 && a1_v3_1
               && (a0_atime_0 + a0_wtime_0) - (a1_atime_0 + a1_wtime_0) < val2_e
               && (a1_atime_0 + a1_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_14_1);
    expr c2_14_2 = a0_v0_0 && a0_v1_1 && a2_v1_0 && a2_v3_1
               && (a0_atime_0 + a0_wtime_0) - (a2_atime_0 + a2_wtime_0) < val2_e
               && (a2_atime_0 + a2_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_14_2);
    expr c2_14_3 = a0_v0_0 && a0_v1_1 && a3_v1_0 && a3_v3_1
               && (a0_atime_0 + a0_wtime_0) - (a3_atime_0 + a3_wtime_0) < val2_e
               && (a3_atime_0 + a3_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_14_3);
    check_f(::z3::sat);

    expr c2_15_1 = a0_v0_0 && a0_v2_1 && a1_v2_0 && a1_v1_1
               && (a0_atime_0 + a0_wtime_0) - (a1_atime_0 + a1_wtime_0) < sqrt_e
               && (a1_atime_0 + a1_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_15_1);
    expr c2_15_2 = a0_v0_0 && a0_v2_1 && a2_v2_0 && a2_v1_1
               && (a0_atime_0 + a0_wtime_0) - (a2_atime_0 + a2_wtime_0) < sqrt_e
               && (a2_atime_0 + a2_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_15_2);
    expr c2_15_3 = a0_v0_0 && a0_v2_1 && a3_v2_0 && a3_v1_1
               && (a0_atime_0 + a0_wtime_0) - (a3_atime_0 + a3_wtime_0) < sqrt_e
               && (a3_atime_0 + a3_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_15_3);
    check_f(::z3::sat);

    expr c2_16_1 = a0_v0_0 && a0_v2_1 && a1_v2_0 && a1_v0_1
               && (a0_atime_0 + a0_wtime_0) - (a1_atime_0 + a1_wtime_0) < 1
               && (a1_atime_0 + a1_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_16_1);
    expr c2_16_2 = a0_v0_0 && a0_v2_1 && a2_v2_0 && a2_v0_1
               && (a0_atime_0 + a0_wtime_0) - (a2_atime_0 + a2_wtime_0) < 1
               && (a2_atime_0 + a2_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_16_2);
    expr c2_16_3 = a0_v0_0 && a0_v2_1 && a3_v2_0 && a3_v0_1
               && (a0_atime_0 + a0_wtime_0) - (a3_atime_0 + a3_wtime_0) < 1
               && (a3_atime_0 + a3_wtime_0) - (a0_atime_0 + a0_wtime_0) < 1
               && true && true;
    add_conflict_f(c2_16_3);
    check_f(::z3::sat);
    */

    std::cout << "OK" << std::endl;

    return 0;
}
