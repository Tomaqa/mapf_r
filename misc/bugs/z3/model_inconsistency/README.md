## Inconsistency bug between `handle` value and `model` value

### Compile

```
g++ main.cpp -lz3
```

### Version

We reproduced the bug with Z3 of version >= 4.12.0

The bug is *not* reproduced with Z3 of version 4.11.*

### Description

Consistency is checked within lambda function `check_f`:
```c++
::z3::context ctx;
::z3::optimize s(ctx);

...

expr totaltime = ctx.real_const("totaltime");
auto minimize_handle = s.minimize(totaltime);

const auto check_f = [&](...){
    ...

    const auto min_totaltime = s.lower(minimize_handle).as_double();

    auto model = s.get_model();
    expr total_e = model.eval(totaltime);
    const double total = total_e.as_double();

    expect(min_totaltime == total,
           "lower != model: "s + std::to_string(min_totaltime) + " != " + std::to_string(total));

    s.add(totaltime >= total_e);
};
```
The assertion `min_totaltime == total` is violated at the end of the example program.

The fact that we keep adding constraint `totaltime >= total_e`
seems to have significant impact on appearance of the bug.

<br/>

There is a rational approximation of `sqrt2` value, represented by
```c++
constexpr long sqrt_n = 5931641;
constexpr long sqrt_d = 4194304;
expr sqrt_e = ctx.real_val(sqrt_n, sqrt_d);
```

... and, also, there are three derived values:
```c++
expr val1_e = ctx.real_val(sqrt_n + sqrt_d, sqrt_d);
expr val2_e = ctx.real_val(sqrt_n - sqrt_d + inc_val2, sqrt_d);
expr val3_e = ctx.real_val(2*sqrt_d - sqrt_n, sqrt_d);
```
where `val2_e` may be a bit "inaccurate", involving an increment `inc_val2`.

The values actually "correspond" to
```c++
expr val1_e = sqrt_e + 1;
expr val2_e = sqrt_e - 1;
expr val3_e = 2 - sqrt_e;
```
... but with such defined variables (or even when using only a single one of them), the bug does not appear.

The part of the code is marked with a `//!!` comment.
There are also other parts like this, which, when (un)commented, causes the bug to vanish.

Values `val2_e` and `val3_e` are being used within constraints,
while `val1_e` is actually not.
However, using `expr val1_e = sqrt_e + 1;` still causes the bug to disappear,
so it seems to have some effect.

<br/>

The bug appears when `inc_val2` is one of {1, 2, ..., 42}.

I did not manage to further reduce the sample program `main.cpp`...

### Outputs

The output files `ov<v>_i<i>` are outputs of the program
compiled with verbosity `<v>`
and where increment `inc_val2` is set to `<i>`.

Corresponding files `ov<v>_i1` and `ov<v>_i42` are equivalent -
in both cases, the bug appears in the same way.

Cases `ov<v>_i0` and `ov<v>_i43` are with no violation, but the outputs differ.
